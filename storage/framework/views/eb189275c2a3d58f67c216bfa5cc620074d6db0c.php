<?php $__env->startSection('content'); ?>

<div class="page-wrapper container" id="sap">
<div class="main_grid">

<?php echo e(Form::open(array('url' => '/downloadsap', 'method' => 'post','enctype'=>'multipart/form-data'))); ?>


 <br>


<ul class="nav nav-tabs  border-0 uploadtab">
	
   
	<li class='nav-item'>
    <a class='nav-link active'   href='<?php echo e(url("/sapDispatch")); ?>'>SAP Despatch</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='<?php echo e(url("/retails")); ?>'>Retail</a>
   </li>  
    <li class='nav-item'>
    <a class='nav-link'   href='<?php echo e(url("/allSiam")); ?>'>All India SIAM</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='<?php echo e(url("/pdPlan")); ?>'>PD Plan</a>
 </li>
   <li class='nav-item'>
    <a class='nav-link ' href='<?php echo e(url("/consolidateUpload")); ?>'>Consolidated Townwise for HO</a>
  </li>
 </ul>

<div class="container p-0">
<div class="row ">
<div class="col-lg-12 col-sm-12">


    <!-- /.Main Heading -->
<div class="row page-header  pt-0 "> 
<div class="col-lg-12 col-md-12 col-sm-12 text-right">
    <button class="btn greybox border border-dark btn-sm" type="button" @click="window.location='<?php echo e(url('/public/files/SAP Despatch.xlsx')); ?>'" >Download Template</button>
</div>
</div>
<br/>
    	<div class="row">
     	<div class="col-1"> Step 1: </div>
		<div class="col-11 border-bottom blueborder">

			<div class="row">
			<div class="col-md-2 form-inline">
			 <span class="btn bluebox border btn-file w-75" >Upload<input  v-model="file" type="file" name="file"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @click="clickUp($event)" @change="onSapchange($event)"></span>
			</div>
			<div class="col-md-2 form-inline pr-0">
			<label>Month: </label>&nbsp;
			<input type="text"  name="date" id="date"  class="float-left p-0 col-8 form-control border-0"> 
			</div>
			
		</div><br/>
		<div class="row mb-2">
			<div class="col-md-12">
			<span class="pl-3 " v-show="filename" ><strong class="bluetext">{{filename}} </strong>uploaded. Please proceed to Step 2.</span>
			</div>
         </div> 



		</div>
		</div><br/>
		<div class="row" >
     	<div class="col-1"> Step 2: </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row form-group">
			<div class="col-md-2">
			<button class="btn  btn-light border w-75" type="button"  :disabled="sapvalidate" @click="validateSap($event)" :class="buttonvalidate">Validate</button>
			</div>
		</div>
		<p v-if="messege"><span  class="bluetext font-weight-bold">{{messege}}</span>
			<span v-if="wraninglist">, but with warnings. Please proceed to Step 3.</span>
			<span v-else>. Please proceed to Step 3.</span>
		</p>
		<div class="col-md-11" v-show="showerror">
		<p v-show="showerror" style="font-weight: bold">Found the below errors. Please correct the errors and repeat Step 1.</p>
			<div style="height: auto;overflow: auto;max-height: 330px; "> 
		      <div v-for="(errors, key) in error " >
		  	 <span><i class='fa fa-remove text-danger'></i>&nbsp;&nbsp;{{errors.error}}</span><br/>
		      </div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-12 message">
			
			</div>
		</div>

		</div>
		</div><br/>

		<div class="row" >
     	<div class="col-1"> Step 3: </div> 
     	<div class="col-11 "> 
<!-- 			<div v-show="wraninglist">
					 <p >Please add in remarks for the below Validation warnings before submitting</p><br/>
					<div style="height: auto;overflow: auto;max-height: 330px;overflow-x: hidden; "> 
					  	<div class="row  form-group" v-for="(warnings, key) in wraninglist ">
					  		<div class="col-5"> <i class='fa fa-exclamation text-warning'></i>&nbsp;&nbsp;{{warnings.warning}}</div>
					  		<div class="col-5"><input type="text" placeholder="Add remark here" v-model="valsArray[key]"  class=" form-control"> </div>
					  	</div>
					</div>
				</div> -->

					
					  	<div class="row form-group">
					  	<div class="col-md-2">

						<button class="btn  btn-light border w-75" type="button" @click="sendSap" :disabled="disbutton" :class="sapsubmit">Submit</button>
					</div>
				</div>
			</div>
		
		  
			
		</div>
		</div>
		</div>
<?php echo e(Form::close()); ?>

</div>
</div>
</div>

<script src="<?php echo e(VJS); ?>uploads/sapuploads.js"></script>  

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tvs\dev\resources\views/pages/uploads/_sap.blade.php ENDPATH**/ ?>