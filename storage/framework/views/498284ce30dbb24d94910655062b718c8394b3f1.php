<?php $__env->startSection('content'); ?>
<?php echo $__env->make('includes.headerlogin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>




<?php if(env('AZURE_TENANT_ID') != ''): ?>       
   <?php echo $__env->make('includes.activedirectorylogin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php else: ?>
   <?php echo $__env->make('includes.emaillogin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>


<style>
    .error-msg {
        margin: 0;
        color: #f30;
        font-size: 9px;
        font-weight: 500;
        letter-spacing: 1px;
        text-transform: uppercase;
        padding-top: 5px;
    }
</style><?php /**PATH C:\xampp\htdocs\tvs\dev\resources\views/auth/login.blade.php ENDPATH**/ ?>