<?php $__env->startSection('content'); ?>

<div class="page-wrapper container mb-5" id="adstatus">
<div class="main_grid">

<?php echo e(Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data'))); ?>


<div class="row">

<div class="col-lg-12 col-sm-12 p-0"><br/>
<div class="row justify-content-center">
<div class="col-lg-1 col-1 form-inline p-0"><label>Month: </label></div>
<div class="col-lg-2 col-sm-2 form-inline p-0"><input type="text"  name="month" id="month" class="rounded-0 form-control"></div>
</div>
<br/>
<input type="hidden" value="<?php echo asset('storage/app/');?>" id="storage">
<input type="hidden" value="<?php echo asset('storage/app/');?>" id="storageraw">
    <input type="hidden" id="finacial_year" value="<?php echo Session::get('finacial_year');?>">
     <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
<!-- Nav tabs -->
<ul class="nav nav-tabs border-0 nobordertab text-center">
  <li class='nav-item'>
    <a class='nav-link active ' data-toggle='tab' href='#area'>Area Townwise
      
       <div class="box greenbox shadow-none text-white m-0 rounded-0 border border-dark p-3 multi-color-rg" >
            <h5 class="text-center">{{upareacount}} Areas Uploaded</h5>
        </div>
        <span v-html="styleObject"></span>
         
       <!-- <div class="progress box greenbox shadow-none text-white m-0 rounded-0 border border-dark p-4" style="height:65px">
            <div class="progress-bar redbox" role="progressbar" style="height: 25%"></div>
            <span class="progress-text">10/29 Areas Uploaded</span>
        </div>  -->
    </a>
  </li>
  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#sap' @click="getsapValues('sap')">SAP Despatch
       <div class="box   shadow-none text-white m-0 rounded-0 border border-dark p-3" :class="sapstatusbox">
            <h5 class="text-center" v-html="sapdate"></h5></div>
            </a>
   </li>  
     <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#retail' @click="getsapValues('retails')">Retail
       <div class="box  shadow-none text-white m-0 rounded-0 border border-dark p-3" :class='retstatusbox'>
            <h5 class="text-center" v-html="retdate"></h5></div>
            </a>
   </li> 
     <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#siam' @click="getsapValues('siam')">All India SIAM
       <div class="box  shadow-none text-white m-0 rounded-0 border border-dark p-3" :class="siamstatusbox">
            <h5 class="text-center" v-html="siamdate"></h5></div>
            </a>
   </li> 
     <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#pdplan' @click="getsapValues('pdplan')">PD Plan
       <div class="box  shadow-none text-white m-0 rounded-0 border border-dark p-3" :class="pdstatusbox">
            <h5 class="text-center" v-html="pddate"></h5></div>
            </a>
   </li> 
 </ul>



     <br/>

      <!-- Tab panes -->
<div class="tab-content">
    
  <div class='tab-pane container active' id='area'>
     <ul class="nav nav-tabs  border-0 nobordertab">
  <li class='nav-item'>
    <a class='nav-link active' data-toggle='tab' href='#consdata'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#rawdata'>Competition raw data for HO</a>
   </li> 

 </ul>
 <!-- Tab panes -->
<div class="tab-content">
    
  <div class='tab-pane container active' id='consdata'>
  <br/>

  <div class="row position-absolute float-right" style="right: 0;"><div class="col-12 p-0" style=""><strong style="left: 0;">Legend:</strong> <div class="row form-group"><div class="col-12"><div class="row"><div class="col-2 "><div class="box redbox m-0 shadow-none border border-dark rounded-0 p-3"></div></div> <div class="col-10"><div class="row h-100"><div class="col-sm-12 my-auto"><p>Data upload past due date</p></div></div></div></div></div></div> <div class="row form-group"> <div class="col-12"><div class="row"><div class="col-2 "><div class="box orangebox m-0  shadow-none border border-dark rounded-0 p-3"></div></div> <div class="col-10"><div class="row h-100"><div class="col-sm-12 my-auto"><p>Data pending approval by Area Manager</p></div></div></div></div></div></div> <div class="row"> <div class="col-12"><div class="row"><div class="col-2 "><div class="box greenbox m-0 shadow-none border border-dark rounded-0 p-3"></div></div> <div class="col-10"><div class="row h-100"><div class="col-sm-12 my-auto"><p>Data submitted to HO</p></div></div></div></div></div></div></div> </div>

  
     <div class="row">
		<br/>
		
        <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row adstatus" v-for="(areadt, key) in zonearray ">
        <br/>
        	<div class="col-sm-2 col-md-2 col-lg-2 m-0 p-1 ">
            <div class="box shadow-none m-0 rounded-0">
            <h5 class="text-center">{{key}}</h5></div>
            </div>


			<div class="col-sm-2 col-md-2 col-lg-2 m-0 p-2 " v-for="(val, key) in areadt" @click="getCondt(val.areaid,val.status,val.area,val.classstatus)">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark pl-2 pr-2 pt-3 pb-2 text-center" :class="val.classstatus">
            <h5 >{{val.area}} <br/>{{val.up_date}}</h5></div>
            </div>
        
           
		
		</div>
	
		
        </div>

	</div>
    </div>

    <div class='tab-pane container fade' id='rawdata'>
	<br/>
	<div class="row">
        <br/>
        
        <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row adstatus" v-for="(areadt, key) in compzone ">
        <br/>
            <div class="col-sm-2 col-md-2 col-lg-2 m-0 p-1 ">
            <div class="box shadow-none m-0 rounded-0">
            <h5 class="text-center">{{key}}</h5></div>
            </div>


            <div class="col-sm-2 col-md-2 col-lg-2 m-0 p-2 " v-for="(val, key) in areadt" @click="getrawdt(val.areaid,val.area,val.classstatus)">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark pl-2 pr-2 pt-3 pb-2" :class="val.classstatus">
            <h5 class="text-center">{{val.area}} <br/>{{val.up_date}}</h5></div>
            </div>
        
           
        
        </div>
    
        
        </div>

    </div>

	</div>

</div>
</div>
<div class='tab-pane container fade' id='sap'>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <p class="font-weight-bold">SAP Despatch data</p>
        </div>

        <div class="row">
             <div class="col-md-6 text-center p-4">
             <p><span class="font-weight-bold">Status:</span> {{sapstatus}}</p>
             </div>
              <div class="col-md-6 p-4" >
             <p v-show="sapupdate"><span class="font-weight-bold">Uploaded Date:</span> {{sapupdate}}</p>
             </div>
        </div>
         <div class="row" v-show="sapdt">
             <div class="col-md-6 text-right p-4">
                <button class="btn btn-light border   "  type="button" @click="onDownload('sap')">Download</button>
            </div>
        </div>
    
    </div>
    </div>
    <div class='tab-pane container fade' id='retail'>
         <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <p class="font-weight-bold">Retail data</p>
        </div>

        <div class="row">
             <div class="col-md-6 text-center p-4">
             <p><span class="font-weight-bold">Status:</span> {{retailstatus}}</p>
             </div>
              <div class="col-md-6 p-4">
             <p v-show="retupdate"><span class="font-weight-bold">Uploaded Date:</span> {{retupdate}}</p>
             </div>
        </div>
         <div class="row" v-show="retdt">
             <div class="col-md-6 text-right p-4">
                <button class="btn btn-light border   "  type="button" @click="onDownload('retails')">Download</button>
            </div>
        </div>
 
    </div>
    </div>
    <div class='tab-pane container fade' id='siam'>
         <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <p class="font-weight-bold"> ALL India SIAM data</p>
        </div>
      
        <div class="row">
             <div class="col-md-6 text-center p-4">
             <p><span class="font-weight-bold">Status:</span> {{siamstatus}}</p>
             </div>
              <div class="col-md-6 p-4">
             <p><span class="font-weight-bold" v-show="siamupdate">Uploaded Date:</span> {{siamupdate}}</p>
             </div>
        </div>
         <div class="row"  v-show="siamdt">
             <div class="col-md-6 text-right p-4">
                <button class="btn btn-light border   "  type="button" @click="onDownload('siam')">Download</button>
            </div>
        </div>
 
    </div>
    </div>
    <div class='tab-pane container fade' id='pdplan'>
         <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <p class="font-weight-bold">PD plan data</p>
        </div>
       
        <div class="row">
             <div class="col-md-6 text-center p-4">
             <p><span class="font-weight-bold">Status:</span> {{pdstatus}}</p>
             </div>
              <div class="col-md-6 p-4">
             <p><span class="font-weight-bold" v-show="pdupdate">Uploaded Date:</span> {{pdupdate}}</p>
             </div>
        </div>
         <div class="row" v-show="pdplandt">
             <div class="col-md-6 text-right p-4">
                <button class="btn btn-light border   "  type="button" @click="onDownload('pdpaln')">Download</button>
            </div>
        </div>
   
    </div>
    </div>
</div>





<div id="dataexpModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        
    <div class="modal-content border border-dark rounded-0 pb-3 ">
    <div class="modal-body">
	    <div class="row justify-content-center" v-show="showdata">
	    <div class="col-8 p-4">
	    <p><strong>{{areaname}}: </strong>Townwise data</p>
	    <p>Status: {{areastatus}}</p>
	    <p>Uploaded Date: {{areaupdate}}</p>
	    <p>Approved date: {{areaapprove}}</p>
	    </div>
	    </div>

	    <div class="row justify-content-center" v-show="showdata">
	  	<div class="col-12 text-center" v-show="arealink">
		 <button class="btn btn-dark bluebox" style="font-weight: bold;" type="button" @click="onAreaDownload()">Download As Excel</button>
		</div>
	    </div>

		<div class="row justify-content-center" v-show="!showdata">
		<div class="col-md-6 p-4">
		<p><strong>{{areaname}}: </strong>Townwise data</p>
		<p>Status:  {{areastatus}}</p>

		</div>
		</div>
    </div>
	</div>
             
    </div>

    </div>
</div>

<div id="datarawModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        
    <div class="modal-content border border-dark rounded-0 ">
    <div class="modal-body">
    	<div class="row justify-content-center" v-show="rawshowdata">
    	<div class="col-md-6 p-4">
        <p><strong>{{rawareaname}}: </strong>Townwise data</p>
        <p>Status: {{rawareastatus}}</p>
        <p>Uploaded Date:{{rawareaupdate}}</p>
        <p>Month: {{rawareamonth}}</p>
      	</div>
      	</div>
        <br/>
        <div class="row" v-show="rawshowdata">
        <div class="col-12">
           <div class="row form-group"  v-for='data in filelist' >
            <div class="col-lg-8 col-md-8 col-sm-8 text-left">
              {{data}}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <button class="btn btn-sm bluebox" type="button" @click="onrawareaDownload(data)"><i class="fa fa-download" data-toggle="tooltip" data-placement="top" title="Download"></i></button>
            </div>
            </div>
        </div>
    	</div>
          
        <div class="row justify-content-center" v-show="!rawshowdata">
           <div class="col-md-6 p-4">
            <p><strong>{{rawareaname}}: </strong>Townwise data</p>
            <p>Status:  {{rawareastatus}}</p>
        	</div>
        </div>
    </div>
    </div>

    </div>
</div>

</div>
<?php echo e(Form::close()); ?>

</div>
</div>
<script src="<?php echo e(VJS); ?>status/adminstatus.js"></script>  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tvs\dev\resources\views/pages/status/_adminstatus.blade.php ENDPATH**/ ?>