<?php $__env->startSection('content'); ?>
<div class="page-wrapper container" id="conupload">
<div class="main_grid pb-5">

<?php echo e(Form::open(array('url' => '/downloadapprove', 'method' => 'post','enctype'=>'multipart/form-data'))); ?>


<br/>
<input type="hidden" id="area" value="<?php echo Session::get('area');?>">
  <input type="hidden" id="areaid" name="areaid" value="<?php echo Session::get('areaid');?>">
   <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
    <input type="hidden" id="user_role" value="<?php echo Session::get('user_role');?>">
    <input type="hidden" value="<?php echo asset('storage/app/');?>" id="storage">

<?php if(session()->get('approved')): ?>
    <input type="hidden" id="approved" value=" <?php echo e(session()->get('approved')); ?>" >
 
<?php endif; ?>
<?php if(session()->get('rejected')): ?>
    <input type="hidden" id="rejected" value=" <?php echo e(session()->get('rejected')); ?>" >
 
<?php endif; ?>
    
  <div  v-show="monthlist">
    
  <div class="row form-group">
  <div class="col-12 text-center">
 <!-- added by chethan -->
 1 file is pending your approval: <strong class="bluetext"> {{filename}}</strong>
 <!-- end  -->
  </div>
  </div>
  <div class="row form-group">

  <div class="col-md-3 form-inline pr-0">
  <label>Month: </label>&nbsp;
  <!-- <input type="text" name="month" id="month" class="float-left form-control border-0 p-0 font-weight-bold "> -->
         <select class="form-control border-0" id="month" name="month" v-model="month"  class="p-2" @change="getApprove()" >
              <option v-for='data in monthlist' :value="data.month" >{{ data.month }}</option>
         </select>
  </div>
  <div class="col-lg-4 col-sm-4 form-inline"><label>Uploaded Date: </label>
  <strong> {{update}}</strong></div>
  </div>

<div class="row form-group">
  <div class="col-lg-6 col-sm-6 bluetext"><strong style="font-size-adjust: 1;">Summary View</strong></div>
  </div> 
<div class="row">
<div class="col-lg-12 col-sm-12">

<div class="tab-content p-0">
  <div class='tab-pane container fade in show active p-0' id='hero'>
     
     <table class="table table-bordered " >
      <thead >
          <tr class="bluebox">
              <th class="text-center" v-for="(column, index) in manufacturelist" :key="index" > {{column}}</th>
          </tr>
      </thead>
      <tbody>
           <tr v-for="(item, index) in approvelist" :key="index" >
              <td class="text-left">{{item.town}}</td>
               <td class="text-left">{{item.district}}</td>
               <td class="text-right" v-for="(column, indexColumn) in manufacturelist" :key="indexColumn" v-if="indexColumn>1">{{item[column] ? item[column] : 0 }}</td>
          </tr>
      </tbody>
    </table>
  </div>
</div>


</div>

</div>
<div class="row form-group pb-2">
  <div class="col-lg-6 col-sm-6" ><strong v-show="wraninglist">Warnings with Remarks</strong></div>
  </div> 
<div class="row form-group">
  <div class="col-lg-12 col-sm-12" v-for="(warnings, key) in wraninglist "><span class="text-warning">!</span> {{warnings.warning}}<span v-if="warnings.remark"> - </span> {{warnings.remark}}</div>
  </div>
  <div class="row form-group">
  <div class="col-lg-8 col-sm-8"><button class="btn btn-light border"  type="button" @click="onDownload()" >Download File</button></div>
  <div class="col-lg-4 col-sm-4"><div class="row ">
  <div class="col-lg-6 col-sm-6"><button class="btn btn-light border float-right"  type="button" @click="onSubmit(2)">Reject</button></div><div class="col-lg-6 col-sm-6"><button class="btn bluebox border float-right" type="button" @click="onSubmit(1)">Approve & Submit</button></div></div>
  </div>
</div>
</div>

<div v-show="!monthlist">
 <div class="row ">
 <div class="col-12 text-center">
 <h2> No file pending approval</h2>
 </div>
 </div>
 </div>





<?php echo e(Form::close()); ?>



</div></div>
<script src="<?php echo e(VJS); ?>/approve.js"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\tvs\dev\resources\views/pages/_approve.blade.php ENDPATH**/ ?>