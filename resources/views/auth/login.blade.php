@section('content')
@include('includes.headerlogin')




@if (env('AZURE_TENANT_ID') != '')       
   @include('includes.activedirectorylogin')
@else
   @include('includes.emaillogin')
@endif


<style>
    .error-msg {
        margin: 0;
        color: #f30;
        font-size: 9px;
        font-weight: 500;
        letter-spacing: 1px;
        text-transform: uppercase;
        padding-top: 5px;
    }
</style>