@section('content')
@include('includes.headerlogin')

<div class="container-fluid p-0">
    <!-- Login Form Container-->
    <div class="container-login100">
        <div class="wrap-login25">
            <div class="row" style="margin:auto">

                <div class="col-12">

                    <div class="login100-pic text-center">
                        <img src="{{url('public/images/TVS_Motor_logo.png')}}" class="img img-responsive" alt="logo" width="250" height="90">
                        <h2 class="text-dark">Townwise Data Automation</h2>
                    </div>

                    <form id="login" class="form-group" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <hr />
                        <span class="login100-form-title txt1">Login</span><br />

                        <div class="wrap-input100 validate-input form-group">
                            <!-- <label class="field-label" >User Name</label> -->
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @if ($errors->has('email'))
                            <p class="error-msg">
                                <strong>{{ $errors->first('email') }}</strong>
                            </p>
                            @endif
                        </div>

                        <div class="wrap-input100 validate-input form-group">
                            <!--  <label class="field-label" >Password</label> -->
                            <input class="form-control" placeholder="Enter Password" id="password" type="password" name="password" required>
                            @if ($errors->has('password'))
                            <p class="error-msg">
                                <strong>{{ $errors->first('password') }}</strong>
                            </p>
                            @endif
                            <span class="glyphicon glyphicon-lock"></span>
                            <span class="bar"></span>
                        </div>


                        <div class="container-login100-form-btn">
                           
                            <button type="submit" class="btn btn-md btn-danger login100-form-btn">
                                Login
                            </button>
                           
                           
                        </div>

                        <div class=" clearfix"></div>
                        <div class="text-center p-t-12">
                            <span class="txt1">
                                ©2020 TVS Motors
                            </span>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .error-msg {
        margin: 0;
        color: #f30;
        font-size: 9px;
        font-weight: 500;
        letter-spacing: 1px;
        text-transform: uppercase;
        padding-top: 5px;
    }
</style>