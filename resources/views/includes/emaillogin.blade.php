

<div class="container-fluid p-0">
    <!-- Login Form Container-->
    <div class="container-login100">
        <div class="wrap-login25">
            <div class="row" style="margin:auto">

                <div class="col-12">

                    <div class="login100-pic text-center">
                        <img src="{{url('public/images/TVS_Motor_logo.png')}}" class="img img-responsive" alt="logo" width="250" height="90">
                        <h2 class="text-dark">Townwise Data Automation</h2>
                    </div>

                    <form id="login" class="form-group" role="form" method="POST" action="{{ url('/loginemail') }}">
                        {{ csrf_field() }}
                        <hr />
                        <span class="login100-form-title txt1">Login</span><br />

                        <div class="wrap-input100 validate-input form-group">
                            <!-- <label class="field-label" >User Name</label> -->
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @if ($errors->has('email'))
                            <p class="error-msg">
                                <strong>{{ $errors->first('email') }}</strong>
                            </p>
                            @endif
                        </div>

                      


                        <div class="container-login100-form-btn">
                           
                            <button type="submit" class="btn btn-md btn-danger login100-form-btn">
                                Login
                            </button>
                           
                           
                        </div>

                        <div class=" clearfix"></div>
                        <div class="text-center p-t-12">
                            <span class="txt1">
                                ©2020 TVS Motors
                            </span>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div> 