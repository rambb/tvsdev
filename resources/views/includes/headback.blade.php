<!-- Style.css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- Font-awesome  Css-->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
<link href="{{url('public/css/style.css')}}" rel="stylesheet">
<!-- Datepicker.css-->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="{{JS}}moment.min.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{url('public/js/daterangepicker.js')}}"></script>
<script src="{{url('public/js/bootstrap-datepicker.js')}}"></script>   
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.0.0/dist/vee-validate.js"></script>
<script src="https://unpkg.com/vee-validate@2.0.0"></script>
<script src="https://unpkg.com/vuetable-2@1.6.0"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3"></script>
<link href="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3/dist/vue-loading.css" rel="stylesheet">
<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
<script src="{{JS}}sweetalert-dev.js"></script>

<!-- <script src="https://unpkg.com/vue-multiselect@2.0.0-beta.13"></script> -->


<script type="text/javascript">
// ------------------------- DATE PICKER SCRIPT -------------------------------
$(function () {
    $('#sub_from').datepicker({ format: 'DD/MM/YYYY',});
    $('#sub_to').datepicker({ format: 'DD/MM/YYYY',});
      
});

    var APP_URL = {!! json_encode(url('/').'/'.env('BACKEND_PATH')) !!}
    var url = APP_URL.substring(0, APP_URL.lastIndexOf("/") + 1)


     //Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();

    if (element.is('li')) {
        element.addClass('active');
    }
});
      
</script>