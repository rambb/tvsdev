<br/>
<div class="row">
<div class="col-10 pr-0">
<ul class="nav nav-tabs nav-border">
  <?php if(Session::get('user_role')=='3'){?>
  <li class="nav-item ml-2 mr-2 ">
    <a class="nav-link rounded-0 pl-4 pr-4 pt-0 pb-2" href="{{url('/approve')}}">Approve</a>
  </li>
<?php } ?>
  <?php if(Session::get('user_role')=='4'){?>
  <li class="nav-item ml-2 mr-2 ">
    <a class="nav-link rounded-0 pt-0 pb-2 "   id="uploads" href="{{url('/consolidateData')}}">Upload</a>
  </li>
  <?php } ?>
   <?php if(Session::get('user_role')=='1'){?>
  <li class="nav-item ml-2 mr-2 " >
    <a class="nav-link rounded-0 pt-0 pb-2 "  id="uploads" href="{{url('/sapDispatch')}}">Upload</a>
  </li>
  <?php } ?>
  <li class="nav-item ml-2 mr-2 ">
    <a class="nav-link rounded-0 pt-0 pb-2 "  href="{{url('/status')}}">Status</a>
  </li>
   <li class="nav-item ml-2 mr-2 ">
    <a class="nav-link rounded-0 pt-0 pb-2 "  href="{{url('/downloadTownwise')}}">Townwise</a>
  </li>
  </ul>
  </div>
  
<div class="col-2 p-0">
  <?php   if(Session::get('user_role')=='1'){?>
<ul class="nav nav-tabs nav-border">
   <li class="nav-item ml-2 mr-2 ">
    <a class="nav-link rounded-0 pt-0 pb-2 " id="adminmenu" href="{{url('/settings')}}">Administration</a>
  </li>
</ul>
<?php } ?>

</div>
</div>