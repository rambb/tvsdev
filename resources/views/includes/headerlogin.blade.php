<!DOCTYPE HTML>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta http-equiv="Pragma" content="no-cache">
    <link rel="shortcut icon" href="{{url('public/images/FaviconTVS_16.png')}}" type="image/x-icon">
    <title>Townwise Data Automation</title>
    <!-- Style.css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <!-- Font-awesome  Css-->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('public/css/util.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/css/main.css')}}" />
<!--     // <script type="text/javascript" src="{{url('js/custom.js')}}"></script> -->

    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
    <link href="https://fonts.googleapis.com/css?family=Domine&display=swap" rel="stylesheet">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
</head>

<body>