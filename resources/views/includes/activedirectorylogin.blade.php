

<div class="container-fluid p-0">
    <!-- Login Form Container-->
    <div class="container-login100">
        <div class="wrap-login25">
            <div class="row" style="margin:auto">

                <div class="col-12">

                    <div class="login100-pic text-center">
                        <img src="{{url('public/images/TVS_Motor_logo.png')}}" class="img img-responsive" alt="logo" width="250" height="90">
                        <h2 class="text-dark">Townwise Data Automation</h2>
                    </div>

                   <form id="login" class="form-group" role="form" method="POST" action="{{ url('/login/azure') }}">
                        {{ csrf_field() }}
                        <hr />
                       

                      


                        <div class="container-login100-form-btn">
                           
                            <button type="submit" class="btn btn-md btn-danger login100-form-btn">
                                Login
                            </button>
                           
                           
                        </div>

                        <div class=" clearfix"></div>
                        <div class="text-center p-t-12">
                            <span class="txt1">
                                ©2020 TVS Motors
                            </span>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div> 