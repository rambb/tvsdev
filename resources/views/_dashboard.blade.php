@extends('layouts.master')
@section('content')

<?php $curmnth = date('F'); ?>

<div class="content-fluid" id="app">
    <br />
    <div class="row p-2">

        <div class="col-lg-3 col-sm-3 ml-5">
            <!-- Left Col -->
            <div class="subscriber1">
                <div class="row subscriber color">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <!-- Left Col -->
                        <div class="row pt-2 pb-2 text-center">
                            <div class="col-lg-11 col-sm-11 float-left">
                                <h3 class="text-danger">{{ $curmnth }} Upload Summary </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row no-subscriber">
                    <div class="col-lg-6 col-sm-6 border-right">
                        <a href="viewSubscriber?sub_status=1">
                            <div class="dashboard-widget">
                                <h6 class="text-info">Uploaded</h6>
                                <h3 v-html="active"></h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <a href="viewSubscriber?sub_status=2">
                            <div class="dashboard-widget">
                                <h6 class="text-info">Pending</h6>
                                <h3 v-html="inactive"></h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>



    </div><!-- row end -->
</div>
<script src="{{VIEWS}}/dashboard.js"></script>

@endsection