@extends('layouts.master')
@section('content')
<script src="https://unpkg.com/v-tooltip"></script>

 
<div class="page-wrapper container mb-5" id="adduser">
<div class="main_grid">

{{ Form::open(array('url' => 'subscriber_form', 'method' => 'POST','enctype'=>'multipart/form-data','v-on:submit.prevent' => 'send()')) }}
<div class="row">
<div class="col-lg-12 col-sm-12">
<br>

<!-- Nav tabs -->
<ul class="nav nav-tabs border-0 uploadtab">
    <li class='nav-item'>
    <a class='nav-link ' href='{{url("/settings") }}'>Settings</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link active ' href='{{url("/manageUsers") }}'>Manage Users</a>
  </li>
    
  <li class='nav-item'>
    <a class='nav-link ' href='{{url("/userLogs") }}'>Usage Logs</a>
   </li>  
 </ul>
    <br/>
    <div class="row">
    <div class="col-lg-8 col-sm-8 border-right border-dark">
       
        <br/>
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-2 " >
                <div class="form-inline form-group d-none">
                <label>Show:</label>&nbsp;&nbsp;
                <select class="form-control" v-model="perPage" @change="pagePerpage()">
                <option value=15>15</option>
               
                </select>
                </div>
            </div> 
             <div class="col-3">
                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 float-right">
                <div class="form-inline form-group">
                <label>Search:</label>&nbsp;&nbsp;
                <input v-model="searchFor" class="col-9 search-query form-control" @keyup.enter="onFilterSet" v-on:keyup="onFilterSet">
                </div>
            </div> 
                 
  
            </div>      
        </div>
        </div>

        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="ui" v-if="apiUrl != ''">
            <vuetable ref="vuetable"
                :api-url="apiUrl"
                :fields="fields"
                :sort-order="sortOrder"
                :css="css.table"
                pagination-path=""
                :per-page="perPage"
                @vuetable:loading="onLoading"        
                @vuetable:loaded="onLoaded"
                @vuetable:pagination-data="onPaginationData"
                :append-params="moreParams"
                @vuetable:row-clicked="expandAdditionalInfo" 
                 >
            </vuetable>
             <vuetable-pagination-info ref="paginationInfo" class="text-center m-2 float-left"></vuetable-pagination-info>
            <vuetable-pagination ref="pagination" :css="css.pagination" @vuetable-pagination:change-page="onChangePage"></vuetable-pagination>
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-4 ">
        <h4 class="pl-4"><u>Add / Modify Users</u></h4>   
        <br/>        
        <div class="row form-group"> 
    
            <label class="col-sm-4">Email id <span class="text-danger">*</span></label>
            <div class="col-sm-8">                    
            <input class="form-control" v-on:blur="onBlur()"  v-validate.disable="'required|email'" id="user_email" type="text" placeholder="E-mail" name="user_email"  v-model="user_email">
            <span v-if="errors.has('user_email')" style="color:red" >@{{ errors.first('user_email') }}</span>
            </div>
     
        </div>
        <!--
        <div class="col-lg-12 col-md-12 col-sm-12 row form-group">
            <label class="col-sm-3">Password <span class="text-danger">*</span></label>
            <div class="col-sm-9">                    
            <input class="form-control"  id="user_pass" type="password" v-validate="{ required: true, min: 6 }" placeholder="Password" name="user_pass"  v-model="user_pass">
            <span v-if="errors.has('user_pass')" style="color:red" >@{{ errors.first('user_pass') }}</span>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 row form-group">
            <label class="col-sm-3">ReType Password <span class="text-danger">*</span></label>
            <div class="col-sm-9">                    
            <input class="form-control"  id="user_repass" v-validate="'required|confirmed:user_pass'" type="password" placeholder="Re-Type Password" name="user_repass"  v-model="user_repass">
            <input type="hidden" id="chkpass" name="chkpass"  />
            <span v-if="errors.has('user_repass')" style="color:red" >@{{ errors.first('user_repass') }}</span>
            </div>
        </div>
        -->
         <div class="row form-group">
      
            <label class="col-sm-4">Role <span class="text-danger">*</span></label>
            <div class="col-sm-8"> 
            <select  id="role" name="role" v-model="role" class="p-2 w-100"   v-validate="'required'"  @change="changearea()">
            <option value="" >Select Role </option>
            <option v-for='data in rolelist' :value="data.id">@{{ data.role }}</option>
            </select>
            <span v-if="errors.has('role')" style="color:red" >@{{ errors.first('role') }}</span>
            </div>
       
        </div>
         <div class="row form-group" >
      
            <label class="col-sm-4">Area <span class="text-danger">*</span></label>
            <div class="col-sm-8"  v-show="role != 2"> 
            <select  id="area" name="area" v-model="area" v-validate="'required'" class="p-2 w-100"  >
            <option value="0"  v-show="role == 1">All </option>
            <option v-show="role != 1" v-for='data in locationlist' :value="data.id">@{{ data.area }}</option>
            </select>
            <span v-if="errors.has('area')" style="color:red" >@{{ errors.first('area') }}</span>
            </div>
            <div class="col-sm-8" v-show="role == 2"> 
                <select class="mult form-control" id="hoarea" @input="hoareaChange()" name="hoarea" multiple="multiple" v-if="locationlist">
                   <option v-for="param in locationlist" v-bind:value="param.id" >@{{param.area}}</option>
                </select>
     
            <span v-if="errors.has('hoarea')" style="color:red" >@{{ errors.first('hoarea') }}</span>
             </div>
       
        </div>
         <div class="row form-group ">
            <div class="col-md-12 float-right"> 
           
            <button v-show="!edit" class="btn btn-light border float-right" :disabled="errors.any()" > Add</button>
            </div>
            <div class="col-5"> </div>
            
            <div class="col-2"> 
            <button v-show="edit"   class="btn btn-light border float-right"  type="button" @click="Delete"> Delete</button> 
            </div>

            <div class="col-2 p-0"> 
            <button v-show="edit" class="btn btn-light border float-right" type="button" @click="Refresh"> Cancel</button> 
            </div>

            <div class="col-3"> 
            <button v-show="edit" class="btn btn-light border float-right" :disabled="errors.any()"> Update</button> 
            </div>

           </div>
    </div>
    </div><!-- Row end-->
</div>
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}administration/user.js"></script> 
<script type="text/javascript">
    $(document).ready(function() {

        $('#adminmenu').addClass('active');
});

</script> 
<style type="text/css">
 .bootstrap-select .bs-ok-default::after {
    width: 0.3em;
    height: 0.6em;
    border-width: 0 0.1em 0.1em 0;
    transform: rotate(45deg) translateY(0.5rem);
}

.btn.dropdown-toggle:focus {
    outline: none !important;
}
.multiselect__tag {
  background-color: #283895;
}
.multiselect__option--highlight {
    background : #283895;
    outline    : none;
    color      : white;
    content    : '';
}
.multiselect__option--selected:after {
    content : '';
    color   : silver;
}
.multiselect__option--selected.multiselect__option--highlight {
    background : #283895;
    color      : white;
}

.multiselect__option--selected.multiselect__option--highlight:after {
    background : $vue-multiselect-option-selected-highlight-bg;
    content    : '';
    color      : $vue-multiselect-option-selected-highlight-color;
}

</style>
@endsection