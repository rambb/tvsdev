@extends('layouts.master')
@section('content')

<div class="page-wrapper container"  id="uploadsetting">
<div class="main_grid">

{{ Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data')) }}

<div class="row form-group">
<div class="col-lg-12 col-sm-12">
    <br/>
<!-- Nav tabs -->
<ul class="nav nav-tabs border-0 uploadtab">
    <li class='nav-item'>
    <a class='nav-link active' href='{{url("/settings") }}'>Settings</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link  ' href='{{url("/manageUsers") }}'>Manage Users</a>
  </li>
    
  <li class='nav-item'>
    <a class='nav-link ' href='{{url("/userLogs") }}'>Usage Logs</a>
   </li>    
</ul>

<br/>
 <div class="row">
<div class="col-lg-12 col-sm-12">
 <div class="row form-group">

    <div class="col-lg-2 col-sm-2">
    <label style="font-weight: bold;">Location Master:</label>
    </div>
    <div class="col-lg-3 col-sm-3">
    <label v-show="locup">Last Updated On @{{ locup}}</label>
    </div>
    <div class="col-lg-2 col-sm-2">
    <button class="btn btn-light border w-75  "  type="button" @click="onDownload('location')">Download</button>
    </div>
    <div class="col-lg-2 col-sm-2">
    <span class="btn  btn-light border    btn-file">
    Upload & Update<input  type="file" ref="files" @change="onLocationChange($event)">
    </span>
    </div>

  </div>
</div>
</div>
 <div class="row">
<div class="col-lg-12 col-sm-12">
  <div class="row form-group">

    <div class="col-lg-2 col-sm-2">
    <label style="font-weight: bold;">Product Master:</label>
    </div>
    <div class="col-lg-3 col-sm-3">
    <label v-show="proup">Last Updated On @{{proup}} </label>
    </div>
    <div class="col-lg-2 col-sm-2">
    <button class="btn btn-light border   w-75 " type="button" @click="onDownload('product')">Download</button>
    </div>
    <div class="col-lg-2 col-sm-2">
    <span class="btn  btn-light border   btn-file">
    Upload & Update<input  type="file" ref="files" @change="onProductChange($event)">
    </span>
    </div>

  </div>
</div>
</div>
 <div class="row">
<div class="col-lg-12 col-sm-12">
    <div class="row form-group">
    <div class="col-lg-2 col-sm-2">
    <label style="font-weight: bold;">Dealer Master:</label>
    </div>
    <div class="col-lg-3 col-sm-3">
    <label v-show="delup">Last Updated On @{{delup}} </label>
    </div>
    <div class="col-lg-2 col-sm-2">
    <button class="btn btn-light border  w-75  " type="button" @click="onDownload('dealer')">Download</button>
    </div>
    <div class="col-lg-2 col-sm-2">
    <span class="btn  btn-light border   btn-file">
    Upload & Update<input type="file" ref="files" @change="onDealerChange($event)">
    </span>
    </div>
  </div>
</div>
</div>
<hr/>

  <div class="row form-group">

    <div class="col-3">
    <label style="font-weight: bold;">Disable File Upload for MIS Users: </label>
    <span v-if="errors.has('fupload')" style="color:red" >@{{ errors.first('fupload') }}</span>
    </div>
    <div class="col-3">
    <div class="row">
    <div class="col-9 p-0">   
    <input type="radio" name="fupload" v-model="fupload" value="date"> Auto disable every month on   
    </div>
    <div class="col-2 p-0">   
    <select  id="date" name="date" v-model="date"  v-validate="'required'" >
    <option v-for='data in datelist' :value="data">@{{ data}}</option>
    </select>
    </div>
    <div class="row">
    <span v-if="errors.has('date')" style="color:red" >@{{ errors.first('date') }}
    </span>
    </div>
    </div>
    </div>
    <div class="col-1 p-0">   
    <input type="radio" name="fupload" v-model="fupload" value="Yes"  v-validate="'required'"> Enable Now<br>
    </div>
    <div class="col-1 p-0">   
    <input type="radio" name="fupload" v-model="fupload" value="No"> Disable Now<br>
    </div>
            
  </div> 
    <div class="row form-group">
    <div class="col-3">
    <label style="font-weight: bold;">Minimum Threshold : </label>
    </div>
     
    <div class="col-1 p-0">   
        <input type="text" name="mthreshold" class="form-control" v-model="mthreshold"  v-validate="'required'"> 
         <span v-if="errors.has('mthreshold')" style="color:red" >@{{ errors.first('mthreshold') }}</span>
    </div> 
    </div>
    <div class="row form-group">
    <div class="col-3">
    <label style="font-weight: bold;">Variance Threshold (%) : </label>
    </div>
    <div class="col-1 p-0">   
        <input type="text" class="form-control" name="vthreshold" v-model="vthreshold"  v-validate="'required'">
         <span v-if="errors.has('vthreshold')" style="color:red" >@{{ errors.first('vthreshold') }}</span>
    </div>
    </div>
    <div class="row form-group">
    <div class="col-md-4">
      <button class="btn btn-light border float-right" type="button" @click="send()">Save</button>
    </div>
    </div>
<hr/>
</div>
</div>
{{ Form::close() }}
</div>
</div>

<script src="{{VJS}}administration/uploadsetting.js"></script>  
@endsection
