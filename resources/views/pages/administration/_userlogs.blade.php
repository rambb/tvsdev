@extends('layouts.master')
@section('content')

<div class="page-wrapper container" id="userlog">
<div class="main_grid">

{{ Form::open(array('url' => '/downloadlog', 'method' => 'post','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12 p-0">
  <br>
<!-- Nav tabs -->
<ul class="nav nav-tabs border-0 uploadtab">
  <li class='nav-item'>
    <a class='nav-link ' href='{{url("/settings") }}'>Settings</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link  ' href='{{url("/manageUsers") }}'>Manage Users</a>
  </li>
    
  <li class='nav-item'>
    <a class='nav-link active' href='{{url("/userLogs") }}'>Usage Logs</a>
   </li>   
 </ul>
</div>
</div>
 <br/>
 <div class="row form-group ml-0">
    <div class="col-3 form-inline pr-0">
    <label class="col-sm-2 col-form-label">Search:</label>&nbsp;&nbsp;
    <input v-model="searchFor" class="col-8 search-query form-control" @keyup.enter="onFilterSet" v-on:keyup="onFilterSet">
    </div> 

    <div class="col-2 form-inline pl-0" >
     <label class="col-3 col-form-label">From</label>
     <input type="text" placeholder="From" name="sub_from" id="sub_from" class="col-8 form-control"> 
    </div>
    <div class="col-2 p-0 form-inline" >
      <label class="col-3 col-form-label">To</label>
      <input type="text" placeholder="To" name="sub_to" id="sub_to" class="col-8 form-control">
    </div>
    <div class="col-1 p-0" >  
        <button class="btn btn-md col-12 bluebox" type="button" @click="onFilterSet()">Filter </button>    
    </div>
    <div class="col-4" >    
    	<button class="btn btn-light border float-right" >Download </button>  
    </div>

</div>

<div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12" >
<div class="ui" v-if="apiUrl != ''">
            <vuetable ref="vuetable"
                :api-url="apiUrl"
                :fields="fields"
                :sort-order="sortOrder"
                :css="css.table"
                pagination-path=""
                :per-page="perPage"
                @vuetable:loading="onLoading"        
                @vuetable:loaded="onLoaded"
                @vuetable:pagination-data="onPaginationData"
                :append-params="moreParams"
              
                :append-params="moreParams"
                 >

            </vuetable>
           <vuetable-pagination-info ref="paginationInfo" class="text-center m-2 float-left"></vuetable-pagination-info>
            <vuetable-pagination ref="pagination" :css="css.pagination" @vuetable-pagination:change-page="onChangePage"></vuetable-pagination>
            </div>
   

     </div>
</div>​
</div>
</div>
<script src="{{VJS}}administration/userlog.js"></script>  
<script type="text/javascript">
  $(document).ready(function() {

        $('#adminmenu').addClass('active');
});

</script>
@endsection