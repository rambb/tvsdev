 Vue.use(Vuetable);

VeeValidate.Validator.localize('en', {
  custom: {
   user_email: {
      required: 'Enter Email',
      email:'Enter Valid Email'
    },
   
     user_repass: {
      required: 'Enter Retype Password',
      confirmed:'Re Password Must Match Password'
    },
     user_pass: {
      required: 'Enter Password',
      min :'Password Must Be 6 Charecter'
    },
     crpass : {
      required: 'Enter Retype Password',
      confirmed:'Re Password Must Match Password'
    },
     cpass : {
      required: 'Enter Password',
      min :'Password Must Be 6 Charecter'
    },
    
 
    }
})
Vue.directive('tooltip', VTooltip.VTooltip)
Vue.directive('close-popover', VTooltip.VClosePopover)
Vue.component('v-popover', VTooltip.VPopover)
Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#adduser',
      components: {
   'vuetable-pagination': Vuetable.VuetablePagination,
   'vuetable-pagination-info': Vuetable.VuetablePaginationInfo,
   Multiselect: window.VueMultiselect.default
  },
    data:{
        options: [],
    hoarea:[],
      edit:false,
      tablelist:[],
      rolelist:'',
      role:'',
      locationlist:'',
      area:[],
      upid:'',
      erole:'',
      earea:'',
      email:'',
      message : '',
      search:'',
      comments: [],
      showModal:false,
      cpass:'',
      crpass:'',
      newid:'',
      apiUrl:'',
      user_repass:'',
      user_pass:'',
      user_email:'',
      searchFor :'',
      moreParams:[],
      perPage :15,
      moreSearch:[],
      loader_image:'',
      mandala:'',
      type:'',
      valaya:'',
      sub_status:'',
      notifyemail:true,
      notifysms:false,
      fields: [
         {
         name: 'email',
          title: 'Email Id',
          sortField: 'email',
          titleClass: 'text-center  bluebox text-white '
        },
      
        {
          name: 'role',
          title: 'User Role',
          sortField: 'r.role',
          titleClass: 'text-center bluebox text-white '
        },
        {
          name: 'areaname',
          title: 'Area',
          sortField: 'areaname',
          titleClass: 'text-center bluebox text-white '
        },
         {
          name: 'created_at',
          title: 'Date Added',
          sortField: 'created_at',
          titleClass: 'text-center bluebox text-white '
        },
       ],
    sortOrder: [
      { field: 'created_at', direction: 'desc' }
    ],
    css: {
      table: {
         tableWrapper: '',
    tableHeaderClass: 'fixed',
    tableBodyClass: 'vuetable-semantic-no-top fixed',
    tableClass: 'ui selectable unstackable celled table table-bordered',
        loadingClass: 'loading',
        ascendingIcon: 'fa fa-chevron-up',
        descendingIcon: 'fa fa-chevron-down',
        handleIcon: '',
        },
      pagination: {
        infoClass: 'float-left',
        wrapperClass: 'pagination float-right',
        activeClass: 'active',
        disabledClass: 'disabled',
        pageClass: 'page-link',
        linkClass: 'btn btn-border',
        icons: {
          first: '',
          prev: '',
          next: '',
          last: '',
        },
      }
    }
            },

methods:{


        pagePerpage(){
                this.moreParams.filter = this.searchFor
               this.$nextTick( () => this.$refs.vuetable.refresh() )
          },
       onFilterSet (searchFor) {
          this.moreParams.filter = this.searchFor
          //this.moreParams['filter'] = filterText
        this.$nextTick( () => this.$refs.vuetable.refresh() )
      },
      clear(){
          if(this.searchele==''){
              this.$nextTick( () => this.$refs.vuetable.refresh() )
          }
      },
      onFilterReset () {
        delete this.moreParams.filter
        this.searchFor = ''
        this.$nextTick( () => this.$refs.vuetable.refresh() )
      },
        onPaginationData (paginationData) {
          this.$refs.pagination.setPaginationData(paginationData)
             this.$refs.paginationInfo.setPaginationData(paginationData) 
        },
        onChangePage (page) {
          this.$refs.vuetable.changePage(page)
        },
       
        onLoading() {
           this.loader_image = "<img src='"+url+"public/images/loader.GIF'/>";
        },
        onLoaded() {
           this.loader_image = "";
        },
        
        
      
UserAdd(){ 
     
    var vm =this;
   
          vm.new_user = '';
          vm.user_type = 0
          vm.mobilenum = '';
          vm.user_email = '';
          vm.user_mandala = '';
          vm.user_valaya = '';
          vm.id='';
            vm.edit=false;
  $('#myModal').modal('show');
  this.errors.clear();
  
},
Refresh(){
  window.location.href = APP_URL+'manageUsers';
},

      getUserrole:function(){

              var vm = this;
            axios.get(APP_URL+'/getrole', {
                  }).then(function (response) {
                  vm.rolelist = response.data;
              })
     },
     changearea(){
       if(this.role==1){
         this.area = 0;
       }else{
         this.area = this.locationlist[0].id;
       }
     },
     getArea:function(){

              var vm = this;
            axios.get(APP_URL+'/getarea', {
                  }).then(function (response) {
                  vm.locationlist = response.data;
                  vm.options = response.data
              })
     },
   
//load the datatable function

  send(){
   var self = this;
    
     this.$validator.validate('user_email', this.user_email)
     this.$validator.validate('role', this.role) 
      this.$validator.validate('area', this.area)
         .then(() => {
    
 if (this.errors.all() =='') {
      if(this.edit==false){
          var text ="Save";
      }else{
          var text ="Update"
      }
   //alert(this.errors.all())

 swal({
    title: "Are you sure?",
    text: 'You want to '+text+'!!',
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
         axios.post(url+'/saveuser',self.$data)
      .then(response => { 
          var id = response.data.messege;

       // alert(response.data.messege);
       if(id){
            swal("Success!", id, "success"); 
           window.location.href = APP_URL+'manageUsers';
       }
      });
       
    
    } else {
      swal("Cancelled", "", "error");
         e.preventDefault();
    }
 });
    
    
    
    }
  });
  
  
    },
    onBlur(){
          this.$validator.validate('user_email', this.user_email)
        .then(() => {
    
 if (this.errors.all() =='') {
           //place delete ajax call here
            axios.get(url+'/checkemail', {
                params: {
              email: this.user_email,
              id:this.upid,
              }

            }) .then(function (response) {
                if(response.data){
                  swal("Email Exists");
                }
            });
          }
        });

    },
   
//on delete click
Delete: function(){
    var self=this;
    id = this.upid;
   // / alert(id)
     //apply json flash message content to variable
          var title                 = 'Delete';
          var text                  = 'Are You Sure';
          var confirmButtonText     = 'Delete';
          var cancelButtonText      = 'Cancel';
          var delete_status_message = 'Delete Sucessful';
          var sorry_status_message  = 'Sorry';
          var status_prefix_message = '';
          var deleted               = 'Deleted';
          var delete_msg            = 'Deleted';
          var cancelled             ='Cancelled';
          var your_record_is_safe   = 'Record Safe';
          //to display sweet alert
          swal({

              title: title+"?",

              text: text+"!",

              type: "warning",

              showCancelButton: true,

              confirmButtonClass: "btn-danger",

              confirmButtonText: confirmButtonText+"!",

              cancelButtonText: cancelButtonText+"!",

              closeOnConfirm: false,

              closeOnCancel: false

          },
          function(isConfirm) {

            if (isConfirm) {
              //place delete ajax call here
            axios.get(url+'/deleteuser', {
                params: {
              id: id,
              }

            }) .then(function (response) {
             self.$nextTick( () => self.$refs.vuetable.refresh() )
              var deleted_status = response.data;
              var status_message = deleted;
              var status_symbox = 'success';
              if(deleted_status != '1'){
                status_message = sorry_status_message;
                status_pre_message = status_prefix_message;
                status_symbox = 'info';
             }
             else{
              status_pre_message = '';
             }
             swal(status_message+"!", status_pre_message, status_symbox);    
            });
          } else {

              swal(cancelled, your_record_is_safe+" ", "error");
            
            }

          });  
     },
Changepassword: function(email,id,earea,erole){
  this.cpass ='';
  this.crpass='';
 $("#changepass").modal('show');
 this.email = email;
 this.earea = earea;
 this.erole=erole;
 this.newid = id;
},
/*Edit: function(id){
   let vm = this;
   this.edit=true;
$("#LoadingImage").show();
    axios.get(APP_URL+'/getuseredit', {
      params:{
        id:id
      }
      }).then(function (response) {
          $("#LoadingImage").hide();
          vm.errors.clear();
          $('#myModal').modal('show');
          var dt = response.data[0];
           vm.id=dt.id;
           vm.role =dt.role;
           vm.area=dt.area;
           vm.user_email=dt.email;
                   
              })
      },*/

  expandAdditionalInfo(record, index){
    let vm = this;
    this.edit=true;
    $("#LoadingImage").show();
    axios.get(APP_URL+'/getuseredit', {
      params:{
        id:record.id
      }
      }).then(function (response) {
          $("#LoadingImage").hide();
          vm.errors.clear();

          var dt = response.data;
        //  alert(dt.area)
           vm.upid=dt.id[0];
           vm.role =dt.role[0];
           if(vm.role != 2){
           vm.area=dt.area[0];
             }else{
              vm.area=dt.area;
             }
           vm.user_email=dt.email[0];
                   
              })
         
      

      },
   
  
savePassword(){
    var self = this;
    console.log(this.errors);
     this.$validator.validate('cpass', this.cpass)
     this.$validator.validate('crpass', this.crpass) 
         .then(() => {
    
 if (self.errors.all() =='') {
        swal({
            title: "Are you sure?",
            text: 'You want to change status!!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
         function(isConfirm){
        
           if (isConfirm){
                  axios.get(url+'/savenewpassword', {
                params: {
                  id: self.newid,
                  password:self.cpass,
                 
                  
                  }
    
                }) .then(function (response) {
                  var id = response.data.messege;
        
               // alert(response.data.messege);
               if(id){
                    swal("Success!", id, "success"); 
                   window.location.href = APP_URL+'manageUsers';
               }
              });
               
            
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
         });
 }
         });
},
  },

mounted(){
 // / alert('s');
    this.apiUrl = APP_URL+'/getuser';
    this.getUserrole();
    this.getArea();
    this.area=0;
}
 
});

