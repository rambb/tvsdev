 Vue.use(Vuetable);

VeeValidate.Validator.localize('en', {
  custom: {
   user_email: {
      required: 'Enter Email',
      email:'Enter Valid Email'
    },
   
     user_repass: {
      required: 'Enter Retype Password',
      confirmed:'Re Password Must Match Password'
    },
     user_pass: {
      required: 'Enter Password',
      min :'Password Must Be 6 Charecter'
    },
     crpass : {
      required: 'Enter Retype Password',
      confirmed:'Re Password Must Match Password'
    },
     cpass : {
      required: 'Enter Password',
      min :'Password Must Be 6 Charecter'
    },
    
 
    }
})
Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#userlog',

    data:{
      sub_from:'',sub_to:'',
       apiUrl:'',
      user_repass:'',user_pass:'',user_email:'', searchFor :'',moreParams:[],perPage :10,moreSearch:[],loader_image:'',mandala:'',type:'',valaya:'',sub_status:'',notifyemail:true,notifysms:false,
    fields: [
       
       
     
      {
       name: 'DatePart',
        title: 'Date',
      titleClass: 'text-center bluebox text-white '
      },
    
      {
        name: 'TimePart',
        title: 'Time',
        titleClass: 'text-center bluebox text-white '
      },
      {
        name: 'name',
        title: 'Username',
        titleClass: 'text-center bluebox text-white '
      },
       {
        name: 'role',
        title: 'User Role', 
        titleClass: 'text-center bluebox text-white '
      },
     
      {
        name: 'note',
        title: 'Action',
        titleClass: 'text-center bluebox text-white '
      },
     
    ],
   sortOrder: [
      { field: 'u.id', direction: 'asc' }
    ],
    css: {
       table: {
        tableClass: 'table table-bordered vuetable ui selectable stackable dataTable',
        loadingClass: 'loading',
        ascendingIcon: 'fa fa-chevron-up',
        descendingIcon: 'fa fa-chevron-down',
        handleIcon: '',
      },
       pagination: {
        infoClass: 'float-left',
        wrapperClass: 'pagination float-right',
        activeClass: 'active',
        disabledClass: 'disabled',
        pageClass: 'page-link',
        linkClass: 'btn btn-border',
        icons: {
          first: '',
          prev: '',
          next: '',
          last: '',
        },
      }
    }
            },


methods:{
        pagePerpage(){
                this.moreParams.filter = this.searchFor
               this.$nextTick( () => this.$refs.vuetable.refresh() )
          },
       onFilterSet () {
          this.moreParams.from = this.sub_from
         this.moreParams.to = this.sub_to
          this.moreParams.filter = this.searchFor
        this.$nextTick( () => this.$refs.vuetable.refresh() )
      },
      clear(){
          if(this.searchele==''){
              this.$nextTick( () => this.$refs.vuetable.refresh() )
          }
      },
      onFilterReset () {
        delete this.moreParams.filter
        this.searchFor = ''
        this.$nextTick( () => this.$refs.vuetable.refresh() )
      },
        onPaginationData (paginationData) {
          this.$refs.pagination.setPaginationData(paginationData)
             this.$refs.paginationInfo.setPaginationData(paginationData) 
        },
        onChangePage (page) {
          this.$refs.vuetable.changePage(page)
        },
       
        onLoading() {
           this.loader_image = "<img src='"+url+"public/images/loader.GIF'/>";
        },
        onLoaded() {
           this.loader_image = "";
        },
        
        onDownload() {
            axios.get(APP_URL+'/getuserlogs', {
              params: {
              download: 'Yes',
               from: this.sub_from,to:this.sub_to

              }
                  }).then(function (response) {
                  vm.rolelist = response.data;
              })
        },


     
   
  

  },

mounted(){
var date = new Date();
var firstDay = new Date(date.getFullYear(), date.getMonth(), date.getDate()-15);
//var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

var lastDayWithSlashes = (firstDay.getDate()) + '-' + (firstDay.getMonth()) + '-' + firstDay.getFullYear();

//alert(lastDayWithSlashes);
  $('#sub_from').datepicker({autoclose: true,format: 'dd-M-yyyy',}).on(
        'changeDate', () => { this.sub_from = $('#sub_from').val();  }
      ).datepicker("update",lastDayWithSlashes);
     
  this.sub_from = $('#sub_from').val(); 
  $('#sub_to').datepicker({autoclose: true,format: 'dd-M-yyyy',}).on(
        'changeDate', () => { this.sub_to = $('#sub_to').val();  }
      ).datepicker("setDate", new Date());
   this.sub_to = $('#sub_to').val();
    this.moreParams.from = this.sub_from
    this.moreParams.to = this.sub_to
    this.apiUrl = APP_URL+'/getuserlogs';
   
}
 
});

