Vue.use(VueLoading);
new Vue({
    el: '#conupload',
    data: {monthlist:'', storagepath:'',tabledt:'',wraninglist:'',id:'',activetab: '0',datelist:'',tab:'',areaid:'',month:'',approvelist:'',manufacturelist:'',filename:'',update:'',showtable:false},
    methods:{
    onTabclick(id,index){
      var id = (id=='') ? this.datelist[index] : id;
         },
    getApprove:function(){
               var self = this;
  //alert(self.compmonth)
 // this.tabledt = ;
  //alert(this.tabledt);
    let loader = this.$loading.show({
                loader: 'dots'
            });
     axios.get(url+'/getapprove', {
                params: {
              areaid: self.areaid,
              month:this.month,
              type:''
              }
            }) .then(function (response) {
              loader.hide();
              if(response.data){
              self.approvelist = response.data.valuelist;
              self.manufacturelist = response.data.manufacture;
              self.filename = response.data.filename;
              self.link = response.data.link;
              self.update = response.data.update_at;
              self.id = response.data.id;
              self.showtable =true;
              if(response.data.warnings != ''){
              self.wraninglist = response.data.warnings
            }else{
              self.wraninglist ='';
            }
            }else{
              self.showtable =false;
               self.filename = '';
            }

            });
              
     },

     onSubmit(status){
       var self = this;
       if(status == 2){
         swal({
    title: "Are you sure?",
    text: '',
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, Reject!',
    cancelButtonText: "Cancel",
    closeOnConfirm: false,
    closeOnCancel: false,
    showLoaderOnConfirm: true
 },
 function(isConfirm){

   if (isConfirm){
    axios.get(url+'/saveapprove', {
                params: {
              id: self.id,
              status:status,
              month:self.month,
              area:self.areaid,

            
                  }
            }) .then(function (response) {
               if(response.data){
                var messege = (status == 1) ? "Approved" : "Rejected"
              swal('Success', messege+'Successfully', 'success'); 
            window.location.href = APP_URL+'approve';
            }
              

            });
    } else {
         swal("Cancelled", "", "error");
         e.preventDefault();
    }
 });

       }else{
            let loader = this.$loading.show({
                loader: 'dots'
            });
         axios.get(url+'/saveapprove', {
                params: {
              id: self.id,
              status:status,
              month:self.month,
              area:self.areaid,
            
                  }
            }) .then(function (response) {
               loader.hide();
               if(response.data){
                var messege = (status == 1) ? "Approved" : "Rejected"
              swal('Success', messege+'Successfully', 'success'); 
          window.location.href = APP_URL+'approve';
            }
              

            });

       }
      

     },
     onDownload(){
      window.open(this.storagepath+'/'+this.link);

     },
     checkApprove(){
      var self =this;
      axios.get(url+'/checkapprove', {
                params: {
              areaid: self.areaid,
              month:this.month,
              type:''
              }
            }) .then(function (response) {
               if(response.data != ''){
               self.monthlist = response.data;
               self.month = response.data[0].month;
               if(self.month){
                  self.getApprove();
               }
             }
            });
     },

  },
  mounted(){
var self = this;
this.area    = $('#area').val();
this.areaid  = $('#areaid').val();

// var date  = new Date();
// var year=date.getFullYear(); //get year
// var month=date.getMonth()-1; //get month

//     var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
//     var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
//     $('#month').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',endDate: new Date(year, month, '31')}).on(
//       'changeDate', () => { this.month = $('#month').val();self.getApprove() }
//     ).datepicker("update",lastDayWithSlashes);
//    this.month =$('#month').val(); 
//    
//   var numberArray = [];
//   for(var i = 1; i <= 3; i++){
//       numberArray.push(i);
//   }
//   this.datelist=numberArray;
  this.onTabclick('',0);
  this.storagepath = $('#storage').val();
  this.checkApprove();
 var apr = $('#approved').val();
 var rejected = $('#rejected').val();
 //alert(apr)
  if(apr){
     swal('Success', 'Approved Successfully', 'success'); 
  }
  if(rejected){
     swal('Success', 'Rejected Successfully', 'success'); 
  }
//alert(this.monthlist)
  //this.getManufacture();

  }
 
});