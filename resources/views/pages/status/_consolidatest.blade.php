@extends('layouts.master')
@section('content')

<div class="page-wrapper container mb-5" id="constatus">
<div class="main_grid">

{{ Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data')) }}
<div class="row">
<div class="col-lg-12 col-sm-12">
<input type="hidden" id="area" value="<?php echo Session::get('area');?>">
    <input type="hidden" id="areaid" value="<?php echo Session::get('areaid');?>">
     <input type="hidden" id="finacial_year" value="<?php echo Session::get('finacial_year');?>">
     <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
    <input type="hidden" value="<?php echo asset('storage/app/');?>" id="storage">

<br>
<!-- Nav tabs -->
<ul class="nav nav-tabs border-0 uploadtab">
  <li class='nav-item'>
    <a class='nav-link active ' data-toggle='tab' href='#consdata'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#rawdata'>Competition raw data for HO</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
     <div class='tab-pane container active container' id='consdata'>
     <br/>
     <div class="row">
		
        <div class="col-1">
		    <div class="financialblock greybox border border-dark align-middle text-center">
          <div>
   <span> FY</span><br/>
<select class="dropdown greybox border-0  mb-3 p-2 text-center mt-0 font-weight-bold" v-model="cfyear" @change="getCondet()" >
  <option v-for='data in farray' :value='data.dispyear'>@{{data.dispyear}}</option>
 
</select></div>
            </div>
		</div>
		
        <div class="col-lg-5 col-md-5 col-sm-5" v-click-outside="onOutside" >
        <div class="row" >
			<div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser conaprclass"  v-bind:class="[selectedmonth==4 ? conaprclass+' highlightbox' : conaprclass]"  @click="onConBoxclick(4,conapr,conaprclass)">
            <h5 class="text-center">@{{conapr}}</h5>

          </div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==5 ? conmayclass+' highlightbox' : conmayclass]"   @click="onConBoxclick(5,conmay,conmayclass)">
            <h5 class="text-center">@{{conmay}}</h5></div>
            </div>
		
	       	<div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box   shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==6 ? conjunclass+' highlightbox' : conjunclass]" @click="onConBoxclick(6,conjun,conjunclass)">
            <h5 class="text-center ">@{{conjun}}</h5></div>
            </div>
		
		</div>
		<div class="row" >
			
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==7 ? conjulclass+' highlightbox' : conjulclass]"  @click="onConBoxclick(7,conjul,conjulclass)"><h5 class="text-center">@{{conjul}}</h5></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==8 ? conaugclass+' highlightbox' : conaugclass]"  @click="onConBoxclick(8,conaug,conaugclass)"><h5 class="text-center">@{{conaug}}</h5></div>
            </div>
		
	        <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==9 ? consepclass+' highlightbox' : consepclass]"  @click="onConBoxclick(9,consep,consepclass)"><h5 class="text-center">@{{consep}}</h5></div>
            </div>
				
		</div>
		<div class="row" >

		  	<div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  shadow-none  m-0 rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==10 ? conoctclass+' highlightbox' : conoctclass]" @click="onConBoxclick(10,conoct,conoctclass)"><h5 class="text-center">@{{conoct}}</h5></div>
            </div>
			<div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box m-0 shadow-none rounded-0 border border-dark p-4 boxcurser"  v-bind:class="[selectedmonth == 11 ? connovclass+' highlightbox' : connovclass]"  @click="onConBoxclick(11,connov,connovclass)" ><h5 class="text-center ">@{{connov}}</h5></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==12 ? condecclass+' highlightbox' : condecclass]"  @click="onConBoxclick(12,condec,condecclass)"><h5 class="text-center ">@{{condec}}</h5></div>
            </div>
	    </div>
	    <div class="row" >
	  	
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==1 ? conjanclass+' highlightbox' : conjanclass]"  @click="onConBoxclick(1,conjan,conjanclass)"><h5 class="text-center ">@{{conjan}}</h5></div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
                <div class="box m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[selectedmonth==2 ? confebclass+' highlightbox' : confebclass]"  @click="onConBoxclick(2,confeb,confebclass)"><h5 class="text-center">@{{confeb}}</h5></div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
                <div class="box m-0 shadow-none rounded-0 border border-dark p-4 boxcurser"  v-bind:class="[selectedmonth==3 ? conmarclass+' highlightbox' : conmarclass]"  @click="onConBoxclick(3,conmar,conmarclass)"><h5 class="text-center">@{{conmar}}</h5></div>
            </div>
        </div>
        </div>
 <div class="col-lg-2 col-md-2 col-sm-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 text-center  border-left border-dark" v-show="showcon">

           	<div class="info pb-2">

                <h5 class="text-center text-dark">@{{selmonth}}</h5>
                <p><span class="font-weight-bold">Status:</span> @{{selectedstatus}}</p>
                <p><span class="font-weight-bold">Uploaded Date:</span> @{{consoliupdate}}</p>
                <p><span class="font-weight-bold">Approved Date:</span>  @{{approvedate}}</p>
            </div>
            	<br/><br/>
    		<button class="btn btn-dark bluebox" style="font-weight: bold;" type="button" @click="conDownload()">Download As Excel</button>
    	</div>
       <div class="col-lg-4 col-md-4 col-sm-4 text-center  border-left border-dark" v-show="nodata">
            <div class="info pb-2">
                        <div class="row h-100">
        <div class="col-sm-12 my-auto">
            <h3 class="text-dark">No data uploaded</h3>
        </div>
    </div>
              
             </div>
         
      </div>
	</div>


	<div class="row">
	
    <div class="col-lg-12 col-md-12 col-sm-12">
    <br/><strong>Legend:</strong>


		<div class="row">
      
      <div class="col-3">
        <div class="row">
        <div class="col-2 ">
		      <div class="box redbox m-0 shadow-none border border-dark rounded-0 p-3"></div>
        </div>
        <div class="col-10">
         <div class="row h-100">
        <div class="col-sm-12 my-auto">
             <p>Data upload past due date</p>
        </div>
    </div>
       
        </div>
        </div>
      </div>
      
      <div class="col-4">
        <div class="row">
       <div class="col-2 ">
		      <div class="box orangebox m-0  shadow-none border border-dark rounded-0 p-3"></div>
         </div> <div class="col-10">
        <div class="row h-100">
        <div class="col-sm-12 my-auto"><p >Data pending approval by Area Manager</p></div></div>
        </div>
      </div>
      </div>
      
      <div class="col-4">
        <div class="row">
       <div class="col-2 ">
		      <div class="box greenbox m-0 shadow-none border border-dark rounded-0 p-3"></div>    
        </div>
         <div class="col-10">
       <div class="row h-100">
        <div class="col-sm-12 my-auto"> <p >Data submitted to HO</p></div></div>
        </div>
      </div>
      </div>

    </div>
	</div>
	</div>

    </div>

    <div class='tab-pane container fade' id='rawdata'>
	<br/>
	<div class="row">
        
         <div class="col-1">
            <div class="financialblock greybox border border-dark align-middle text-center">
          <div>
   <span> FY</span><br/>
<select class="dropdown greybox border-0 mb-3 p-2 text-center mt-0 font-weight-bold" v-model="fyear" @change="getAlldet()" >
  <option v-for='data in farray' :value='data.dispyear'>@{{data.dispyear}}</option>
 
</select></div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6"   v-click-outside="onRawdataout" >
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box  text-center m-0 shadow-none rounded-0 border border-dark p-4 boxcurser"  v-bind:class="[rowselctedmonth==4 ? aprclass+' highlightbox' : aprclass]" @click="onBoxclick(4,apr)"><h5 class="text-center mb-0">@{{apr}}</h5>
             <span>@{{aprcnt}} Files Uploaded</span></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==5 ? mayclass+' highlightbox' : mayclass]"  @click="onBoxclick(5,may)"><h5 class="text-center mb-0">@{{may }}</h5>
             <span>@{{maycnt}} Files Uploaded</span></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==6 ? junclass+' highlightbox' : junclass]"  @click="onBoxclick(6,jun)"><h5 class="text-center  mb-0">@{{jun}}</h5>
             <span>@{{juncnt}} Files Uploaded</span></div>
            </div>
        
        </div>
        <div class="row">
            
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center  m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==7 ? julclass+' highlightbox' : julclass]"  @click="onBoxclick(7,jul)"><h5 class="text-center mb-0">@{{jul}}</h5>
             <span>@{{julcnt}} Files Uploaded</span></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="text-center m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth== 8? augclass+' highlightbox' : augclass]"  @click="onBoxclick(8,aug)"><h5 class="text-center mb-0">@{{aug}}</h5>
             <span>@{{augcnt}} Files Uploaded</span></div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center  m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==9 ? sepclass+' highlightbox' : sepclass]" @click="onBoxclick(9,sep)"> <h5 class="text-center mb-0 ">@{{sep}}</h5>
             <span>@{{sepcnt}} Files Uploaded</span></div>
            </div>
                
        </div>
        <div class="row">

            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center  m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==10 ? octclass+' highlightbox' : octclass]"  @click="onBoxclick(10,oct)">
                <h5 class="text-center mb-0">@{{oct}}</h5>
            <span>@{{octcnt}} Files Uploaded</span>
            </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth== 11? novclass+' highlightbox' : novclass]"  @click="onBoxclick(11,nov)">
                <h5 class="text-center  mb-0">@{{nov}}</h5>
              <span>@{{novcnt}} Files Uploaded</span>
          </div>
            </div>
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center  m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==12 ? decclass+' highlightbox' : decclass]"  @click="onBoxclick(12,dec)">
                <h5 class="text-center  mb-0">@{{dec }}</h5>
                 <span>@{{deccnt}} Files Uploaded</span>
            </div>
            </div>
        </div>
        <div class="row">
        
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
            <div class="box text-center  m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==1 ? janclass+' highlightbox' : janclass]" @click="onBoxclick(1,jan)" >
                <h5 class="text-center mb-0" >@{{jan}}</h5>
             <span>@{{jancnt}} Files Uploaded</span>
         </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
                <div class="box text-center  m-0 shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==2 ? febclass+' highlightbox' : febclass]" @click="onBoxclick(2,feb)">
                    <h5 class="text-center mb-0" >@{{feb}}</h5>
                 <span>@{{febcnt}} Files Uploaded</span>
             </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 m-0 p-2">
                <div class="box text-center  m-0  shadow-none rounded-0 border border-dark p-4 boxcurser" v-bind:class="[rowselctedmonth==3 ? marclass+' highlightbox' : marclass]"  @click="onBoxclick(3,mar)">
                    <h5 class="text-center mb-0">@{{mar}}</h5>
                 <span>@{{marcnt}} Files Uploaded</span>
             </div>
            </div>
        </div>
        </div>
<div class="col-lg-1 col-md-1 col-sm-1"></div>
        <div class="col-lg-4 col-md-4 col-sm-4  text-center  border-left border-dark" v-show="showraw">
        
            <div class="info">
            <h5 class="text-center">@{{mname}}</h5>
            <p><span class="font-weight-bold">Status:</span> @{{fcount}} files submitted to HO</p>
            <p><span class="font-weight-bold">Uploaded Date:</span> @{{upddate}}</p>
              <h5 class="text-center mb-0">Uploaded Files </h5></br>
                <div class="row form-group"  v-for='data in filelist' >
                <div class="col-lg-8 col-md-8 col-sm-8 text-left">
                  @{{data}}
                </div>
                  <div class="col-lg-4 col-md-4 col-sm-4">
                    <button class="btn btn-sm bluebox" type="button" @click="onDownload(data)"><i class="fa fa-download" data-toggle="tooltip" data-placement="top" title="Download"></i></button>
                    <button class="btn btn-sm redbox" type="button" @click="onDelete(data)" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-trash"></i></button>
                   <!--<button class="btn btn-dark bluebox" type="button" @click="onDownload(data)" >Download As Excel</button>!-->
                 </div>
                </div>
              
           
            <br/>
            </div>
            <br>
           
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4  text-center  border-left border-dark" v-show="norowdata">
            <div class="info">
            <div class="row h-100">
        <div class="col-sm-12 my-auto">
            <h3 class="text-dark">No data uploaded</h3>
        </div>
    </div>
             </div>
            
        </div>
    </div>

	</div>

</div>
</div>
</div>

{{ Form::close() }}
</div>
</div>
<style type="text/css">
.boxcurser{
    cursor: pointer;

}
</style>
<script type="text/javascript">$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})</script>
<script src="{{VJS}}status/consolidatestatus.js"></script>  

@endsection
