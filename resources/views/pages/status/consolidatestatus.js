 Vue.use(Vuetable);

 Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        window.event = function (event) {
            if (!(el == event.target || el.contains(event.target))) {
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', window.event)
    },
    unbind: function (el) {
        document.body.removeEventListener('click', window.event)
    },
})
Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#constatus',
    
    data:{storagerawpath:'',dispatchid:'',nodata:false,norowdata:false,rowselctedmonth:'',selectedmonth:'',disdownload:true,showraw:false,showcon:false,cfyear:'',link:'',fcount:0,filename:'',filelist:'',upddate:'',mname:'',fyear:'',farray:'',finacial_year:'',area:'',areaid:'',stausvalue:'',montharray:[],jan:'',janclass:'',jancnt:'',feb:'',febclass:'',febcnt:'',mar:'',marclass:'',marcnt:'',apr:'',aprclass:'',aprcnt:'',may:'',mayclass:'',maycnt:'',jun:'',junclass:'',juncnt:'',jul:'',julclass:'',julcnt:'',aug:'',augclass:'',augcnt:'',sep:'',sepclass:'',sepcnt:'',oct:'',octclass:'',octcnt:'',nov:'',novclass:'',novcnt:'',dec:'',decclass:'',deccnt:'',conjan:'',conjanclass:'',confeb:'',confebclass:'',conmar:'',conmarclass:'',conapr:'',conaprclass:'',conmay:'',conmayclass:'',conjun:'',conjunclass:'',conjul:'',conjulclass:'',conaug:'',conaugclass:'',consep:'',consepclass:'',conoct:'',conoctclass:'',consep:'',consepclass:'',conoct:'',conoctclass:'',connov:'',connovclass:'',condec:'',condecclass:'',upload_enable:'',selmonth:'',selectedstatus:'',consoliupdate:'',consolilink:'',approvedate:'',confile:''},

methods:{
  getsapValues(type){
    if(type=='sap'){
    var sap = this.statusdt.sap;
    if(sap){
    this.sapupdate = moment(sap.created_at).format("DD-MMM-YY hh:mm A");
  }
    }else  if(type=='retails'){
       var retails = this.statusdt.retails;
     //  alert(retails.created_at);
       if(retails){
      this.retupdate = moment(retails.created_at).format("DD-MMM-YY hh:mm A");
       }

    }else  if(type=='pdpaln'){
      var pdpaln = this.statusdt.pdpaln;
      if(pdpaln){
      this.pdupdate = moment(pdpaln.created_at).format("DD-MMM-YY hh:mm A");
    }
    }else  if(type=='siam'){
      var siam = this.statusdt.siam;
      if(siam){
      this.siamupdate = moment(siam.created_at).format("DD-MMM-YY hh:mm A");
    }
    }

   },
   onDownload(type){
      if(type=='sap'){
       var sap = this.statusdt.sap;
      var upid  = sap.uploadid;
    }else  if(type=='retails'){
       var retails = this.statusdt.retails;
       var upid  = retails.uploadid;
     
    }else  if(type=='pdpaln'){
      var pdpaln = this.statusdt.pdpaln;
      var upid  = pdpaln.uploadid;
    }else  if(type=='siam'){
      var siam = this.statusdt.siam;
       var upid  = siam.uploadid;
    }
    //alert(this.storagepath);
     var self = this;
         axios.get(APP_URL+'/ondownloadadstatus', {
                 params: {'uploadid':upid}
                  }).then(function (response) {
                     window.open(self.storagepath+'/'+response.data[0].link);

                  });
    

   },

  getAlldet(){
    var self = this;
    var test  = this.fyear;
   var lastDigit = test.toString().slice(-2);
   var cyear = lastDigit;
   var nyear = parseFloat(lastDigit)+parseFloat(1)
//OR
//var lastDigit = (test + '').slice(-1);

//alert(lastDigit);
    axios.get(APP_URL+'/rawstatusdisplay', {
                 params: {areaid:this.areaid,year:this.fyear,type:'Raw Data'}
                  }).then(function (response) {
                    var stval = response.data;
                      stval.forEach(function(key,val) {
                           switch(key.month){
                              case 'JAN': 
                                          self.jan = key.month+' ' +nyear;
                                          self.jancnt = key.count;  
                                          if(self.jancnt>0) { self.janclass='box greybox';}else{self.janclass='';}
                                          break;
                              case 'FEB': self.feb =key.month+' ' +nyear;
                                          self.febcnt = key.count;  
                                          if(self.febcnt>0) { self.febclass='box greybox';}else{self.febclass='';}
                                           break;
                              case 'MAR': self.mar = key.month+' ' +nyear;
                                          self.marcnt = key.count;  
                                          if(self.marcnt>0) { self.marclass='box greybox';}else{self.marclass='';}
                                           break;
                              case 'APR': self.apr = key.month+' ' +cyear;
                                          self.aprcnt = key.count;  
                                          if( self.aprcnt>0) {  self.aprclass='box greybox';}else{self.aprclass='';}
                                           break;
                              case 'MAY':  self.may = key.month+' ' +cyear;
                                           self.maycnt = key.count;
                                          if( self.maycnt>0) {  self.mayclass='box greybox';}else{self.mayclass='';}
                                           break;
                              case 'JUN':  self.jun = key.month+' ' +cyear;
                                           self.juncnt = key.count;  
                                          if(self.juncnt>0) { self.junclass='graybox';}else{self.junclass='';}
                                           break;
                              case 'JUL': self.jul = key.month; 
                                          self.julcnt = key.count;  
                                          if(self.julcnt>0) { self.julclass='box greybox';}else{self.julclass='';}
                                           break;
                              case 'AUG': self.aug =key.month+' ' +cyear;
                                          self.augcnt = key.count;  
                                          if(self.augcnt>0) { self.augclass='box greybox';}else{self.augclass='';}
                                           break;
                              case 'SEP': self.sep = key.month+' ' +cyear;
                                          self.sepcnt = key.count;  
                                          if(self.sepcnt>0) { self.sepclass='box greybox';}else{self.sepclass='';}
                                           break;
                              case 'OCT': self.oct =key.month+' ' +cyear;
                                          self.octcnt = key.count;  
                                          if(self.octcnt>0) { self.octclass='box greybox';}else{self.octclass='';}
                                           break;
                              case 'NOV': self.nov =key.month+' ' +cyear;
                             // alert(key.count);
                                          self.novcnt = key.count;  
                                          if(self.novcnt>0) { self.novclass='box greybox';}else{self.novclass='';}
                                           break;
                              case 'DEC': self.dec = key.month+' ' +cyear;
                                          self.deccnt = key.count;  
                                          if(self.deccnt>0) { self.decclass='box greybox';}else{self.decclass='';}
                                           break;
                           }
                          
                      });

                 });


  },
   getCondet(){
    var self = this;
   var fullyear  = this.cfyear;
   var lastDigit = fullyear.toString().slice(-2);
   var cyear = lastDigit;
   var nyear = parseFloat(lastDigit)+parseFloat(1);

   var nextfullyear = parseFloat(fullyear)+parseFloat(1);

    axios.get(APP_URL+'/consolidateddisplay', {
                 params: {areaid:this.areaid,year:this.cfyear,type:'Consolidated'}
                  }).then(function (response) {
                    var stval = response.data;
                      stval.forEach(function(key,val) {
                           switch(key.month){
                              case 'JAN': 
                                          self.conjan = key.month+' '+nyear;
                                         // alert(key.status)
                                         if(key.status == '0') { 
                                             self.conjanclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                             self.conjanclass='greenbox text-white';
                                          }else {
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                                 // (YYYY-MM-DD) 
                                              var duedate = new Date(nextfullyear+"-1-"+self.upload_enable); 
                                             //  alert(currentdate)
                                                if (currentdate < duedate){
                                                  self.conjanclass = '';
                                                }else{
                                                  //alert('b')
                                                   self.conjanclass='redbox text-white';
                                                }
                                              }
                                           
                                          }
                                          break;
                              case 'FEB': self.confeb = key.month+' '+nyear; 
                                        if(key.status == '0') { 
                                             self.confebclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.confebclass='greenbox text-white';
                                          }else{
                                            if(self.upload_enable){
                                              //alert(self.upload_enable)
                                              var currentdate = new Date(); 
                                                // (YYYY-MM-DD) 
                                              var duedate = new Date(nextfullyear+"-2-"+self.upload_enable); 
                                               // /alert(g2)
                                                if (currentdate < duedate){
                                                  //alert('a');
                                                    self.confebclass = '';
                                                }else{
                                                  //alert('b')
                                                   self.confebclass='redbox text-white';
                                                }
                                              }
                                          }
                                           break;
                              case 'MAR': self.conmar = key.month+' '+nyear; 

                              if(key.status == '0') { 
                                             self.conmarclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conmarclass='greenbox text-white';
                                          }else{
                                            if(self.upload_enable){
                                              //alert(self.upload_enable)
                                              var currentdate = new Date(); 
                                                // (YYYY-MM-DD) 
                                              var duedate = new Date(nextfullyear+"-3-"+self.upload_enable); 
                                               // /alert(g2)
                                                if (currentdate < duedate){
                                                  //alert('a');
                                                    self.conmarclass = '';
                                                }else{
                                                  //alert('b')
                                                   self.conmarclass='redbox text-white';
                                                }
                                              }
                                       
                                           
                                          }
                                           break;
                              case 'APR': self.conapr = key.month+' '+cyear; 
                              //alert( self.conapr)
                                        if(key.status == '0') { 
                                             self.conaprclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conaprclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                                // (YYYY-MM-DD) 
                                              var duedate = new Date(fullyear+"-4-"+self.upload_enable); 
                                               // /alert(g2)
                                                if (currentdate < duedate){
                                                 // alert('s')
                                                    self.conaprclass = '';
                                                }else{
                                                 // alert('as')
                                                  self.conaprclass = 'redbox text-white';
                                                }
                                              }
                                          }
                                           break;
                              case 'MAY':  self.conmay = key.month+' '+cyear; 
                                        if(key.status == '0') { 
                                             self.conmayclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conmayclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                                // (YYYY-MM-DD) 
                                              var duedate = new Date(fullyear+"-5-"+self.upload_enable); 
                                               // /alert(g2)
                                                if (currentdate < duedate){
                                                 // alert('s')
                                                    self.conmayclass = '';
                                                }else{
                                                 // alert('as')
                                                  self.conmayclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                              case 'JUN':  self.conjun = key.month+' '+cyear; 
                                        if(key.status == '0') { 
                                             self.conjunclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conjunclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                             var duedate = new Date(fullyear+"-6-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.conjunclass = '';
                                                }else{
                                                  self.conjunclass = 'redbox text-white';
                                                }
                                              }
                                          }
                                           break;
                              case 'JUL': self.conjul = key.month+' '+cyear; 
                                         if(key.status == '0') { 
                                             self.conjulclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conjulclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                             var duedate = new Date(fullyear+"-7-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.conjulclass = '';
                                                }else{
                                                  self.conjulclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                              case 'AUG': self.conaug = key.month+' '+cyear;  
                                          if(key.status == '0') { 
                                             self.conaugclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conaugclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                              var duedate = new Date(fullyear+"-8-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.conaugclass = '';
                                                }else{
                                                  self.conaugclass = 'redbox text-white';
                                                }
                                              }
                                           
                                           
                                          }
                                           break;
                              case 'SEP': self.consep = key.month+' '+cyear; 
                                        if(key.status == '0') { 
                                             self.consepclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.consepclass='greenbox text-white';
                                          }else{
                                            if(self.upload_enable){
                                              var currentdate = new Date(); 
                                              var duedate = new Date(fullyear+"-9-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.consepclass = '';
                                                }else{
                                                  self.consepclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                              case 'OCT': self.conoct = key.month+' '+cyear; 
                                        if(key.status == '0') { 
                                             self.conoctclass='orangebox text-white';
                                         }else  if(key.status == '1') { 
                                            //alert(key.status)
                                            self.conoctclass='greenbox text-white';
                                          }else{
                                             if(self.upload_enable){
                                              var currentdate = new Date(); 
                                              var duedate = new Date(fullyear+"-10-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.conoctclass = '';
                                                }else{
                                                  self.conoctclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                              case 'NOV': self.connov = key.month+' '+cyear; 
                                        if(key.status == '0') { 
                                             self.connovclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.connovclass='greenbox text-white';
                                          }else{
                                            if(self.upload_enable){
                                              var currentdate = new Date(); 
                                              var duedate = new Date(fullyear+"-11-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.connovclass = '';
                                                }else{
                                                  self.connovclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                              case 'DEC': self.condec = key.month+' '+cyear;
                                        if(key.status == '0') { 
                                             self.condecclass='orangebox text-white';
                                         }else if(key.status == '1') { 
                                            //alert(key.status)
                                            self.condecclass='greenbox text-white';
                                          }else{ 
                                          if(self.upload_enable){
                                              var currentdate = new Date(); 
                                              var duedate = new Date(fullyear+"-12-"+self.upload_enable); 
                                                 if (currentdate < duedate){
                                                    self.condecclass = '';
                                                }else{
                                                  self.condecclass = 'redbox text-white';
                                                }
                                              }
                                           
                                          }
                                           break;
                           }
                          
                      });

                 });


  },
  onBoxclick(month,name){
   // alert(name);
    var values = name.split(" ");
    var year = '20'+values[1];
   // alert(values[1]);
   this.rowselctedmonth = month;
     var self = this;
     this.filename ='';
    axios.get(APP_URL+'/monthrawstatus', {
                 params: {areaid:this.areaid,month:month,year:year}
                  }).then(function (response) {
                    if(response.data){
                      self.showraw = true;
                      self.norowdata =false;
                    self.mname = name;
                    self.fcount = response.data.countfile;
                    self.upddate = moment(response.data.date).format("DD-MM-YYYY");
                    self.filelist = response.data.filename;
                    self.link = response.data.link;
                    self.dispatchid = response.data.dispatchid;
                    //self.countfile = response.data.countfile;
                  }else{
                    self.showraw = false;
                    self.norowdata =true;
                  }

                  });
  },
 onDelete:function(data){
   var vm = this;
      swal({
    title: "Are you sure?",
    text: 'want to delete the file?',
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
        
            axios.get(APP_URL+'/deleterawfile', {
                params: {fname:data,'dispatchid':vm.dispatchid,'fcount':vm.fcount,'link':vm.link,filename:vm.filelist}
                  }).then(function (response) {
                     window.location.href = APP_URL+'status';
                  
              })
       
    
    } else {
      swal("Cancelled", "", "error");
         e.preventDefault();
    }
 });

     },
  onRawdataout(){
     this.rowselctedmonth = '';
     this.showraw = false;
     this.norowdata =false;

  },
  onOutside(){
  //  alert('dd')
    this.showcon = false;
    this.selectedmonth = '';
    this.nodata = false;
//alert('df')
  },
  onConBoxclick(month,name,status){
    this.selectedmonth = month;
    var values = name.split(" ");
    var year = '20'+values[1];
   //alert(year);
   var self = this;
   self.consolilink = '';
   self.confile     = '';
   //alert(status)
   if(status=='orangebox text-white' || status == 'greenbox text-white' ){
    console.log(status);

   // if(month == 1){self.conjanclass =status+'   '; }
   

     //this.status = status+' highlightbox';
     this.showcon = true;
      this.nodata = false;
     this.filename ='';
    axios.get(APP_URL+'/monthconstatus', {
                 params: {areaid:this.areaid,month:month,year:year}
                  }).then(function (response) {
                    if(response.data){
                    if(response.data.date){
                     self.consoliupdate = moment(response.data.date).format("DD-MM-YYYY");
                     }
                    if(response.data.approve_at){
                    self.approvedate = moment(response.data.approve_at).format("DD-MM-YYYY");
                    }
                    //alert(response.data.path)
                    self.consolilink = response.data.path;
                    self.confile     = response.data.filename;

                    }
                    self.selmonth = name;
                    if(status == 'redbox text-white'){
                      self.selectedstatus = 'Data upload past due date';
                       this.disdownload =true;

                    }else if(status == 'orangebox text-white'){
                      self.selectedstatus = 'Data pending approval by Area Manager';
                       this.disdownload =false;

                    }else if(status == 'greenbox text-white'){
                      self.selectedstatus = 'Data submitted to HO';
                      this.disdownload =false;

                    }


                  });
                }else{
                  this.showcon = false;
                  this.nodata = true;
                }
  },
  getDispyear(){
    var self =this;
    axios.get(APP_URL+'/getdispyear', {
                   }).then(function (response) {
                    if(response.data == ''){
                     for(i=0;i<3;i++){
                   arr.push({
                   key: 'dispyear', 
                   value:  parseFloat(this.finacial_year)-parseFloat(i)
                   });
                   }
                    self.farray =arr;
                    }else{
                      self.farray = response.data;
                    }

                  });

  },
  onDownload(data){
  //  alert(data)
   // console.log(this.storagepath+'/'+this.link+'/'+this.filename+'.xlsx');
     window.open(this.storagerawpath+'/'+this.link+'/'+data);
  },
  conDownload(){
    console.log(this.consolilink);
    if(this.consolilink){
       window.open(this.storagepath+'/'+this.consolilink);
   }
  },
  

  },

mounted(){
  var arr = [];
  this.finacial_year = $('#finacial_year').val();
  this.upload_enable = $('#upload_enable').val();
  //alert( this.upload_enable);

  // for(i=0;i<3;i++){

  //  arr.push({
  //           key: parseFloat(this.finacial_year)-parseFloat(i), 
  //           value:  parseFloat(this.finacial_year)-parseFloat(i)
  //       });
  //   }
    this.getDispyear();
  
   console.log(this.farray);
   this.area    = $('#area').val();
   this.areaid  = $('#areaid').val();
   this.storagepath = $('#storage').val();
   var year = this.finacial_year ;
   this.fyear = year;
   this.cfyear = year;
   this.getAlldet();
   this.getCondet();
   this.storagerawpath = $('#storage').val();


   


}

 
});

