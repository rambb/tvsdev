@extends('layouts.master')
@section('content')
<br/>
<div class="page-wrapper container" id="conupload">
<div class="main_grid">

{{ Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12 p-0">
<!-- /.Main Heading -->
<div class="row page-header  pt-0 "> 
<div class="col-lg-12 col-md-12 col-sm-12 text-right">
    <button class="btn btn-dark btn-sm" type="button">Download Template</button>
</div>
</div>
<div class="row justify-content-center">
			<div class="col-md-2"><label>Month: </label></div>
			<div class="col-md-4">   
			 <input type="text" placeholder="From" name="sub_mnth" id="sub_mnth" v-validate="'required'"  v-model="sub_mnth"  class=" form-control ">  
            </div>
         </div>
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class='nav-item'>
    <a class='nav-link active' data-toggle='tab' href='#s1'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#s2'>Competition raw data for HO</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
     <div class='tab-pane container fade in show active' id='consdata'>
     <br/>
     	<div class="row justify-content-center">
		<div class="col-6">
		 
		<div class="row form-group">
			<div class="col-md-10">
			<input type="file" class="form-control" v-on:change="onImageChange">
			</div>
			<div class="col-md-2 m-0 p-0">
			<button class="btn btn-success">Submit</button>
			</div>
		</div>
		</div>
		</div>
	</div>
    <div class='tab-pane container fade' id='rawdata'>
	<br/>
		<div class="row justify-content-center">
		<div class="col-6">
		<div class="row form-group">
			<div class="col-md-6"><label>Select Month: </label></div>
			<div class="col-md-6">   
			 <input type="text" placeholder="From" name="sub_mnth" id="sub_mnth" v-validate="'required'"  v-model="sub_mnth"  class=" form-control ">  
            </div>
         </div> 
		<div class="row form-group">
			<div class="col-md-10">
			<input type="file" class="form-control" v-on:change="onImageChange">
			</div>
			<div class="col-md-2 m-0 p-0">
			<button class="btn btn-success">Submit</button>
			</div>
		</div>
		</div>
		</div>	
	</div>
</div>
</div>
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}uploads/uploads.js"></script>  
@endsection
