 Vue.use(Vuetable);


Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#adstatus',
    
    data:{storagerawpath:'',filelist:'',rawshowdata:false,styleObject:'',upareacount:'',compzone:'',showdata:true,areaname:'',areaupdate:'',areaapprove:'',areamonth:'',arealink:'',fyear:'',farray:'',finacial_year:'',zonearray:'',sapstatus:'',retailstatus:'',pdstatus:'',siamstatus:'',storagepath:'',pdupdate:'',pdplandt:false,siamupdate:'',siamdt:false,retupdate:'',retdt:false,sapupdate:'',statusdt:'',month:'',sapdate:'',sapstatusbox:'',retstatusbox:'',pdstatusbox:'',siamstatusbox:'',pddate:'',retdate:'',siamdate:'',sapdt:'false',rawshowdata:true,rawareaname:'',rawareaupdate:'',rawareaapprove:'',rawareamonth:'',rawarealink:'',areastatus :'',rawareastatus:'' },

methods:{
    onAreaDownload(){
  //  alert(data)
   // console.log(this.storagepath+'/'+this.link+'/'+this.filename+'.xlsx');
     window.open(this.storagepath+'/'+this.arealink);
  },
   onrawareaDownload(data){
    //alert(data)
   // console.log(this.storagepath+'/'+this.link+'/'+this.filename+'.xlsx');
     window.open(this.storagerawpath+'/'+this.rawarealink+'/'+data);
  },
 
  getCondt(areaid,status,area,classstatus){
    var self = this;
    if(status !=''){
   
      axios.get(url+'/getstatecondt', {
                 params: {'areaid':areaid,month:this.month,type:'Consolidated'}
                  }).then(function (response) {
                    $('#dataexpModal').modal('show');
                    var dt = response.data.dispatch[0];
                    self.areaname = area;
                    self.showdata =true;
                    self.areaupdate = dt.updated_at;
                    self.areaapprove = dt.approve_at;
                    self.areamonth = self.month;
                    self.arealink = dt.link;

                    if(status == 0){self.areastatus='Data pending approval by Area Manager';}else if(status == 1){self.areastatus='Data submitted to HO';}
                    
                 
                  });


    }else {
       if(classstatus =='redbox text-white'){self.areastatus='Data upload past due date';}else{self.areastatus='No data';}
       self.showdata =false;
        self.areaname = area;
        self.areamonth = self.month;
       $('#dataexpModal').modal('show');


    }

  },
  getrawdt(areaid,area,classstatus){
    var self = this;
    if(classstatus !=''){
   
      axios.get(url+'/getstatecondt', {
                 params: {'areaid':areaid,month:this.month,type:'Raw Data'}
                  }).then(function (response) {
                    $('#datarawModal').modal('show');
                    var dt = response.data.dispatch[0];
                    self.rawareaname = area;
                    self.rawshowdata =true;
                    self.rawareaupdate = dt.updated_at;
                    self.rawareamonth = self.month;
                    self.rawarealink = dt.link;
                     self.filelist = response.data.filelist;
                    self.rawareastatus='Data Uploaded';

                   
                    
                 
                  });


    }else {
        self.rawareastatus='Data not uploaded';
        self.rawshowdata =false;
        self.rawareaname = area;
        self.rawareamonth = self.month;
       $('#datarawModal').modal('show');


    }

  },
  getsapValues(type){
   // alert(type)
    if(type=='sap'){
    var sap = this.statusdt.sap;
    if(sap){
    this.sapupdate = moment(sap.created_at).format("DD-MMM-YY hh:mm A");
  }
    }else  if(type=='retails'){
       var retails = this.statusdt.retails;
     //  alert(retails.created_at);
       if(retails){
      this.retupdate = moment(retails.created_at).format("DD-MMM-YY hh:mm A");
       }

    }else  if(type=='pdplan'){
      var pdpaln = this.statusdt.pdpaln;
      //alert(this.statusdt.pdpaln)
      if(pdpaln){
      this.pdupdate = moment(pdpaln.created_at).format("DD-MMM-YY hh:mm A");
    }
    }else  if(type=='siam'){
      var siam = this.statusdt.siam;
      if(siam){
      this.siamupdate = moment(siam.created_at).format("DD-MMM-YY hh:mm A");
    }
    }

   },
   onDownload(type){
      if(type=='sap'){
       var sap = this.statusdt.sap;
      var upid  = sap.uploadid;
    }else  if(type=='retails'){
       var retails = this.statusdt.retails;
       var upid  = retails.uploadid;
     
    }else  if(type=='pdpaln'){
      var pdpaln = this.statusdt.pdpaln;
      var upid  = pdpaln.uploadid;
    }else  if(type=='siam'){
      var siam = this.statusdt.siam;
       var upid  = siam.uploadid;
    }
    //alert(this.storagepath);
     var self = this;
         axios.get(url+'/ondownloadadstatus', {
                 params: {'uploadid':upid}
                  }).then(function (response) {
                    console.log(self.storagepath+'/'+response.data[0].link)
                     window.open(self.storagepath+'/'+response.data[0].link);

                  });
    

   },

  getAlldet(){
    var self = this;
    axios.get(url+'/getadstatusall', {
                 params: {month:this.month}
                  }).then(function (response) {
                 var dt = response.data[0];
                 self.statusdt = response.data[0];
                 var sap = dt.sap;
                 var ret = dt.retails;
                 var pdpaln = dt.pdpaln;
                 var siam = dt.siam;
                
                 if(sap){
                    self.sapdt =true;
                    self.sapdate  = 'Uploaded <br/>'+sap.cdate;
          
                     self.sapstatus = 'Data submitted to HO';
                     self.sapstatusbox = 'greenbox';
                   

                 }else{
                        self.sapstatus = 'Past the due date for upload';
                        self.sapdt =false;
                        self.sapstatusbox = 'redbox';
                        self.sapdate  = 'Not Uploaded';
                 }

                 if(ret){
                     self.retdt =true;
                     self.retdate  = 'Uploaded <br/>'+ret.cdate;
                 
                      self.retailstatus = 'Data submitted to HO';
                      self.retstatusbox = 'greenbox';
                  

                 }else{
                   self.retailstatus = 'Past the due date for upload';
                   self.retdt =false;
                   self.retdate  = 'Not Uploaded';
                   self.retstatusbox = 'redbox';
                 }


                 if(pdpaln){
                    self.pdplandt =true;
                    self.pddate  = 'Uploaded <br/>'+pdpaln.cdate;
                 
                      self.pdstatus = 'Data submitted to HO';
                      self.pdstatusbox = 'greenbox';
                  

                 }else{
                    self.pdstatus = 'Past the due date for upload';
                    self.pdplandt =false;
                    self.pddate  = 'Not Uploaded';
                    self.pdstatusbox = 'redbox';
                 }

                 if(siam){
                   self.siamdt=true;
                   self.siamdate  = 'Uploaded <br/>'+siam.cdate;
             
                     self.siamstatus = 'Data submitted to HO';
                     self.siamstatusbox = 'greenbox';
                

                 }else{
                  self.siamstatus = 'Past the due date for upload';
                  self.siamdt =false;
                  self.siamdate  = 'Not Uploaded';
                  self.siamstatusbox = 'redbox';
                 }

              })

  },
   userconUpload(){
    var self = this;
    axios.get(url+'/userconupload', {
                 params: {month:this.month,}
                  }).then(function (response) {
                    self.zonearray = response.data.zonedt;
                    self.upareacount = response.data.dispatchcount+'/'+response.data.areacount
                    self.styleObject="<style>.multi-color-rg:after{height:"+response.data.percentage+"%;}</style>"

                  });
                  },
   userrawUpload(){
    var self = this;
    axios.get(url+'/userrawupload', {
                 params: {month:this.month,}
                  }).then(function (response) {
                    self.compzone = response.data.zonedt;
                   // console.log(self.compzone);
                  });
                  },
                  


  },

mounted(){
  this.finacial_year = $('#finacial_year').val();
  //alert(this.finacial_year)
  
var date=new Date();
var year=date.getFullYear(); //get year
var month=date.getMonth()-1; //get month
var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
  $('#month').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',  endDate: new Date(year, month, '31')}).on(
      'changeDate', () => { this.month = $('#month').val();   this.getAlldet();this.userconUpload();
   this.userrawUpload();}
    ).datepicker("update",lastDayWithSlashes);
 
  this.storagepath = $('#storage').val();
  var arr = [];
  for(i=0;i<3;i++){
   arr.push({
            key: parseFloat(this.finacial_year)+parseFloat(i), 
            value:  parseFloat(this.finacial_year)+parseFloat(i)
        });
     }
   this.farray =arr;
   //console.l0g()
   this.month =$('#month').val();
   this.getAlldet();
   this.userconUpload();
   this.userrawUpload();
   var year =this.finacial_year ;
   this.fyear = year;
  this.storagepath = $('#storage').val();
  this.storagerawpath = $('#storage').val();
  


}
 
});

