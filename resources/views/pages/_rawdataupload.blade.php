@extends('layouts.master')
@section('content')

<div class="page-wrapper" id="rawupload">
<div class="main_grid">
<div class="container-fluid container_grid">

{{ Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12 p-0">
<!-- /.Main Heading -->
<div class="row page-header  pt-0 "> 

<div class="col-lg-4 col-md-4 col-sm-12">
<h1>Upload Raw Data</h1>
</div>
</div>
</div>
</div>


<div class="container">

<div class="row justify-content-center">

<div class="col-md-6">
	<div class="card">
		<div class="card-body">
		<div class="row form-group">
		<div class="col-md-6">
		<label>Select Month: </label>
            </div>
				<div class="col-md-6">   
		 <input type="text" placeholder="From" name="sub_mnth" id="sub_mnth" v-validate="'required'"  v-model="sub_mnth"  class=" form-control ">  
             </div></div> 
			<div class="row form-group">
				<div class="col-md-10">
				<input type="file" class="form-control" v-on:change="onImageChange">
				</div>
				<div class="col-md-2 m-0 p-0">
				<button class="btn btn-success">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
<br/>

</div>

{{ Form::close() }}
</div>

</div></div>
<script src="{{VJS}}uploads/uploads.js"></script> 
@endsection