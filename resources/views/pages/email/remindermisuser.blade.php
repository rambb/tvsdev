<!DOCTYPE html>
<html>
<body>
<p>Hello <?php echo @$name;?>,</p>
<p>This is a gentle reminder to upload the consolidated Townwise data & competition raw data files for <?php echo @$month;?> <?php echo @$area;?>.</p>

<p>To see the past data uploaded, please login into the below link:</p>
<p>The option to upload data will be disabled from <?php echo $duedate;?> 24:00 hours.</p>
<a href="http://townwise.azurewebsites.net/testblog/login">http://townwise.azurewebsites.net/testblog/login</a>
<p>Regards,</p>
<p>Data Analytics Team</p>
</body>
</html>
