<!DOCTYPE html>
<html>
<body>
<p>Hello <?php echo @$managername;?>,</p>
<p> <?php echo @$managername;?> has submitted the consolidated Townwise data for your approval. </p>
<p>Please find below a summary of this data of   <?php echo @$month;?> <?php echo @$area;?> </p>


<table class="table table-bordered " >
      <thead >
          <tr class="bluebox">
           <?php foreach($manufacture as $key=> $value){?>
              <th class="text-center"> <?php echo $value;?></th>
              <?php }?>
          </tr>
      </thead>
      <tbody>
      <?php foreach($valuelist as $key=> $value){?>
           <tr v-for="(item, index) in approvelist" :key="index" >
              <td class="text-left"><?php echo $value['town'];?></td>
               <td class="text-left"><?php echo $value['district'];?></td>
                <?php foreach($manufacture as $mkey=> $mvalue){?>
                <?php if($mkey > 1){
                	$val = ($value[$mvalue] != '') ? $value[$mvalue] : 0;
                	?>
                <td class="text-right"><?php echo $val;?></td>
                 <?php }}?>
          </tr>
        
          <?php }?>
      </tbody>
    </table>
    <div class="row float-left">
    <div class="col-lg-6">
      <button type="button" class="btn" href="http://townwise.azurewebsites.net/testblog/approve/<?php echo $encrypted;?>/<?php echo $enid;?>" style="background:#283895;color: #fff !important;float: left;padding: 7px;" >Approve</button>
      </div>
      <div class="col-lg-6 float-right">
      <button  type="button" class="btn" href="http://townwise.azurewebsites.net/testblog/reject/<?php echo $encrypted;?>/<?php echo $enid;?>"  style="background: #a7a9ab;margin: 47;float: right;margin-right: 585px;margin-top: -33px;padding: 7px;">Reject</button>
      </div>
    </div>

<p style="margin-top: 47px;">To see the past data uploaded, please login into the below link:</p>
<a href="http://townwise.azurewebsites.net/testblog/login">http://townwise.azurewebsites.net/testblog/login</a>
<p>Regards,</p>
<p>Data Analytics Team</p>
</body>
</html>
