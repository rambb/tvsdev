<!DOCTYPE html>
<html>
<body>
<p>Hello <?php echo @$name;?>,</p>

<p>The consolidated Townwise data for <?php echo @$month;?> <?php echo @$area;?> has been rejected by the Area Manager. Please speak to the Area Manager and make necessary changes and re-upload (Note: You can do this only before <?php echo @$duedate;?> 24:00 hours).</p>

<p>To see the past data uploaded, please login into the below link:</p>
<a href="http://townwise.azurewebsites.net/testblog/login">http://townwise.azurewebsites.net/testblog/login</a>
<p>Regards,</p>
<p>Data Analytics Team</p>
</body>
</html>
