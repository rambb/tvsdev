<!DOCTYPE html>
<html>
<body>
<p>Hello <?php echo @$adminname;?>,</p>
<p> <?php echo @$name;?> has submitted the consolidated Townwise data to the Area Manager for approval. </p>
<p>Please find below a summary of this data of   <?php echo @$month;?> <?php echo @$area;?> </p>


<table class="table table-bordered " >
      <thead >
          <tr class="bluebox">
           <?php foreach($manufacture as $key=> $value){?>
              <th class="text-center"> <?php echo $value;?></th>
              <?php }?>
          </tr>
      </thead>
      <tbody>
      <?php foreach($valuelist as $key=> $value){?>
           <tr v-for="(item, index) in approvelist" :key="index" >
              <td class="text-left"><?php echo $value['town'];?></td>
               <td class="text-left"><?php echo $value['district'];?></td>
                <?php foreach($manufacture as $mkey=> $mvalue){?>
                <?php if($mkey > 1){
                	$val = ($value[$mvalue] != '') ? $value[$mvalue] : 0;
                	?>
                <td class="text-right"><?php echo $val;?></td>
                 <?php }}?>
          </tr>
        
          <?php }?>
      </tbody>
    </table>



<p>To see the past data uploaded, please login into the below link:</p>
<a href="http://townwise.azurewebsites.net/testblog/login">http://townwise.azurewebsites.net/testblog/login</a>
<p>Regards,</p>
<p>Data Analytics Team</p>
</body>
</html>
