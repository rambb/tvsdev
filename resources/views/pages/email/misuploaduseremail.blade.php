<!DOCTYPE html>
<html>
<body>
<p>Hello <?php echo @$name;?>,</p>

<p>Thank you for uploading the consolidated Townwise data & competition raw data files for <?php echo @$month;?> <?php echo @$area;?>. It is now pending approval from the Area Manager.</p>

<p>To see the past data uploaded, please login into the below link:</p>
<a href="http://townwise.azurewebsites.net/testblog/login">http://townwise.azurewebsites.net/testblog/login</a>
<p>Regards,</p>
<p>Data Analytics Team</p>

</body>
</html>
