@extends('layouts.master')
@section('content')

<div class="page-wrapper container" id="towndata">
<div class="main_grid">

{{ Form::open(array('url' => '/downloaddata', 'method' => 'post','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12">
    <br>


<!-- Nav tabs -->
<ul class="nav nav-tabs  border-0 uploadtab">
    <input type="hidden" value="<?php echo Session::get('user_role');?>" id="role">
 
  <li class='nav-item' v-show="role==1">
    <a class='nav-link' data-toggle='tab' href='#generate' :class="[role==1 ? 'active' : '']">Generate</a>
  </li>

  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#download' :class="[role==1 ? '' : 'show active']">Download</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class='tab-pane container fade in ' id='generate' :class="[role==1 ? 'show active' : '']">
    <br/><br/><br/>
      

    <div class="row">


      <div class="bluegradient justify-content-center p-5 text-white col-2 border rounded text-center" @click="onGenerate()"> 
         Generate</div>
         <div class="col-4 text-bottom pt-5">Last run:  @{{update_at}}</div>
       </div><br/><br/>
       <div class="row">
        <strong>Note:</strong> The Townwise Despatch data will be generated only with those files which are green in the Status tab.
      </div>
  </div>

  <div class='tab-pane container fade' id='download' :class="[role==1 ? '' : 'show active']">
      <br/>
    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col"></div>
    </div>
    <div class="col-lg-3 col-sm-3">
    <div class="col p-0">Product Level:</div>

    </div>
    <div class="col-lg-3 col-sm-3">
    <div class="col p-0">Product:</div>
    </div>
    </div>

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col ">Product:</div>
    </div>
    <div class="col-lg-3 col-sm-3">
  
    <select v-model="ptype" name="producttype" @change="getProduct()"  class="p-2 w-100">
     <option value="" >Select Level </option>
      <option value="category">Category</option>
      <option value="segment">Segment </option>
      <option value="manufacturer">Manufacturer </option>
      <option value="brand">Brand</option>
      
    </select>
    
    </div>
    <div class="col-lg-3 col-sm-3">
    <select v-model="product" name="product"  class="p-2 w-100" >
     <option value="" >All </option>
     <option v-for='data in productlist' :value="data.val" v-if="data.val">@{{ data.val }}</option>
    </select>
    </div>
    </div> 

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col"></div>
    </div>
    <div class="col-lg-3 col-sm-3">
    <div class="col p-0">Market Level:</div>
    </div>
    </div>

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col">Market:</div>

    </div>
    <div class="col-lg-3 col-sm-3">
     <select v-model="mtype"  name="markettype" @change="getMarket()"  class="p-2 w-100">
    <option value="" >Select Market Level </option>
      <option value="Zone">Zone </option>
      <option value="State">State  </option>
     <option value="Area">Area  </option>
     <option value="District">District</option>
   
      <option value="Town">Town</option>
    </select>
  
    </div>
      <div class="col-lg-3 col-sm-3">
    <select v-model="market" name="market"  class="p-2 w-100">
     <option value="" >All </option>
     <option v-for='data in marklist' :value="data.val" v-if="data.val">@{{ data.val }}</option>
    </select>
    </div>
    </div>

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col"></div>
    </div>
    <div class="col-lg-3 col-sm-3">
    <div class="col p-0">Time Level:</div>
    </div>
    <div class="col-lg-2 col-sm-2">
    <div class="col p-0">From:</div>
    </div>
    <div class="col-lg-2 col-sm-2">
    <div class="col p-0">To:</div>
    </div>
    </div>

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col">Time:</div>
    </div>
    <div class="col-lg-3 col-sm-3">
 
    <select id="date" name="date"  class="p-2 w-100">
    <option value="" >Monthly</option>
    </select>
  
    </div>
    <div class="col-lg-2 col-sm-2">
   <input type="text" placeholder="From" name="sub_from" id="sub_from"   class=" form-control float-left"> 
    </select>
    </div>
    <div class="col-lg-2 col-sm-2">
    <input type="text" placeholder="To" name="sub_to" id="sub_to" class=" form-control float-left">
    
    </select>
    </div>
    </div> 

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col"></div>
    </div>
    <div class="col-lg-3 col-sm-3">
    <div class="col p-0">TVS Data:</div>
    </div>
    </div>

    <div class="row form-group">
    <div class="col-lg-1 col-sm-1">
    <div class="col">Type:</div>
    </div>
    <div class="col-lg-3 col-sm-3">

    <select id="type" name="type" class="p-2 w-100" >
    <option value="Despatch" >Despatch </option>
      <option value="Retail" >Retail </option>
    </select>
 
    </div>
    </div><br/>
 <div class="row form-group">
    <div class="col-lg-3 col-sm-3">
    <button class="btn btn-light border float-right" >Download</button>
  </div>
</div>
  </div>

</div>


</div>
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}townwisereport/townwisereport.js"></script>  
@endsection