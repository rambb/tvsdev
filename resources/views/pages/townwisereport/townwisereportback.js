Vue.use(Vuetable);
Vue.use(VueLoading);

var app  =new Vue({
      el:'#towndata',
      
    data:{lastupdate:'',count:'',gmonth:'',gendisable:'',sub_from:'',sub_to:'',update_at:'',productlist :'',segment:'',product:[],area:'',arealist:'',role:'',ptype:'category',mtype:'Zone',market:[],marketlist:'',buttoncls:'bluebox',detlist:'',tcount:'',selectedmarket:[],selectedproduct:[],  params: ''},
    methods:{
 
    onGenerate (){
      
     var self = this;
      swal({
            title: "Are you sure?",
            text: 'Will generate the latest Townwise data until '+self.gmonth+'.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#283895',
            confirmButtonText: 'Yes',
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
         },
         function(isConfirm){
        
           if (isConfirm){
            swal.close();
       let loader = self.$loading.show({
                loader: 'dots'
            });
         axios.get(APP_URL+'/ongenerate', {
                       }).then(function (response) {
                       	 loader.hide();
                       	 var title = response.data.title;
                       	 var id = response.data.messege;
                         var type = response.data.type;
					       if(id){
					            swal(title, id, type); 
					           window.location.href = APP_URL+'downloadTownwise';
					        }
                         });
                       } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
         });
             },
     getDt(){
     	var self = this;
           let loader = this.$loading.show({
                loader: 'dots'
            });
         axios.get(APP_URL+'/getgeneratedt', {
                       }).then(function (response) {
                       	 loader.hide();
                       	 self.update_at = moment(response.data.update_at).format("DD-MMM-YY  h:mm:ss a");
                       	})
                   },
    updateSelected(value) {
      this.buttoncls ='bluebox';
        this.selectedmarket=[];
        //alert(value)
        value.forEach((resource) => {
        //  alert(resource.val)
         this.selectedmarket.push(resource.val)
         })
       },
       updateSelectedproduct(value) {
        this.buttoncls ='bluebox';
        this.selectedproduct=[];
        value.forEach((resource) => {
         this.selectedproduct.push(resource.val)
         })
       },
      getProduct(){
      	if(this.ptype){
      	var self = this;
     	       axios.get(APP_URL+'/productlist', {
	     	     params: {
                 type: this.ptype,
                        }
	                  }).then(function (response) {
                       	self.productlist = response.data;
                                        
                     });
                  }
                   },
       
        getMarket(){
        	 var self = this;
        	 if(self.mtype){
   
     	       axios.get(APP_URL+'/marketlist', {
     	       	 params: {
                 type: self.mtype,
                        }
                       }).then(function (response) {
                        self.marketlist = response.data;
                        $('#market').multiselect('refresh')

                      })
                       	
                       // self.market = 'All';
                      
                       	
                   }
                      
    


        },
        
        buttonclr(){
          this.buttoncls ='bluebox';
        },
        show(){
         // alert($('#market').val())
          var self = this;
           let loader = self.$loading.show({
                loader: 'dots'
            });
           axios.get(APP_URL+'/getgeneratedata', {
             params: {
                 producttype: this.ptype,
                 product: $('#product').val(),
                 markettype: this.mtype,
                 market: $('#market').val(),
                 sub_to: this.sub_to,
                 sub_from: this.sub_from,
                       }
                    }).then(function (response) {
                         loader.hide();
                      var det = response.data.valuelist;
                       self.detlist = det;
                      if(det != ''){
                       self.buttoncls ='greybox';
                       self.tcount = response.data.count[0].val;
                       self.count = response.data.count[0].val;
                      
                      }
                      
                      
                        })
          
        },
        checkGenerate(){
          var self = this;
           axios.get(APP_URL+'/checkgenerate', {
             }).then(function (response) {
          
               self.gendisable = response.data.disable;
               if(response.data.dt){
               self.gmonth = response.data.dt[0].month;
               }

              
              });

        },

      lastUpdate(){
      var vm =this;
       axios.get(APP_URL+'/getlastdate', {
        params:{
          type:'generate'
        }
                  }).then(function (response) {
                    alert('ssa')
                    if(response.data[0].maxdate){
                      alert(response.data[0].maxdate)
                    vm.lastupdate = moment(response.data[0].maxdate).format("MM-YYYY");
                    var year = moment(response.data[0].maxdate).format("YYYY-MM-DD");
                    //alert(year)
                    var monthDayto = new Date(year);
                    //  Var todate = moment(response.data[0].maxdate).format("MM-YYYY");
                   $('#sub_to').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',  endDate: monthDayto}).datepicker("update",monthDayto);
                   vm.sub_to = $('#sub_to').val();
                  }else{
alert('s')
                  }
                });
     },

  },
   updated: function(){
   $('.mult') .multiselect({
      allSelectedText: 'All',
      maxHeight: 200,
      includeSelectAllOption: true
    })
    .multiselect('selectAll', false)
    $('.mult').multiselect('deselectAll', false);    
    $('.mult').multiselect('updateButtonText');
    
  },

mounted(){

	this.role = $("#role").val();
  if(this.role == 1){
      this.checkGenerate();

  }
	this.getDt();
	this.getProduct();
  this.getMarket();
 
	var date=new Date();
  var year=date.getFullYear(); //get year
  var month=date.getMonth()+1; //get month
   if(month>=4){
            var fyear = date.getFullYear();
        }else if(month<4){
            var fyear = date.getFullYear()-1;
        }
       // alert(fyear);

 
    var fromdate = new Date(fyear, 3, date.getDate());
   // var todate = new Date(2021, 5, date.getDate());
	  $('#sub_from').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy'}).on(
      'changeDate', () => { this.sub_from = $('#sub_from').val();this.buttonclr()  }
    ).datepicker("update",fromdate);
    this.sub_from = $('#sub_from').val();
    this.lastUpdate();
   
     var default_date = new Date(2023, 10, 1);
	 $('#sub_to').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',}).on(
      'changeDate', () => { this.sub_to = $('#sub_to').val(); this.buttonclr(); }
    ).datepicker();
   this.sub_to = $('#sub_to').val();
   }
 
});

