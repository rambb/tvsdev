Vue.use(Vuetable);
Vue.use(VueLoading);

var app  =new Vue({
      el:'#towndata',
     
    data:{tablelist:'',showcount:false,lastupdate:'',count:'',gmonth:'',gendisable:'',sub_from:'',sub_to:'',update_at:'',productlist :'',segment:'',product:[],area:'',arealist:'',role:'',ptype:'category',mtype:'Zone',market:[],marketlist:'',buttoncls:'bluebox',detlist:'',tcount:''},
    methods:{
 
    onGenerate (){
     
     var self = this;
      swal({
            title: "Are you sure?",
            text: 'Will generate the latest Townwise data until '+self.gmonth+'.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#283895',
            confirmButtonText: 'Yes',
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
         },
         function(isConfirm){
       
           if (isConfirm){
            swal.close();
       let loader = self.$loading.show({
                loader: 'dots'
            });
         axios.get(APP_URL+'/ongenerate', {
                       }).then(function (response) {
                        loader.hide();
                        var title = response.data.title;
                        var id = response.data.messege;
                         var type = response.data.type;
      if(id){
           swal(title, id, type);
          window.location.href = APP_URL+'downloadTownwise';
       }
                         });
                       } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
         });
             },
     getDt(){
      var self = this;
           let loader = this.$loading.show({
                loader: 'dots'
            });
         axios.get(APP_URL+'/getgeneratedt', {
                       }).then(function (response) {
                        loader.hide();
                         if(response.data){
                        self.update_at = moment(response.data).format("DD-MMM-YY  h:mm:ss a");
                         }
                        })
                   },
    // updateSelected(value) {
    //   this.buttoncls ='bluebox';
    //     this.selectedmarket=[];
    //     //alert(value)
    //     value.forEach((resource) => {
    //     //  alert(resource.val)
    //      this.selectedmarket.push(resource.val)
    //      })
    //    },
    //    updateSelectedproduct(value) {
    //     this.buttoncls ='bluebox';
    //     this.selectedproduct=[];
    //     value.forEach((resource) => {
    //      this.selectedproduct.push(resource.val)
    //      })
    //    },
      getProduct(){
         
      if(this.ptype){
      var self = this;
            axios.get(APP_URL+'/productlist', {
        params: {
                 type: this.ptype,
                        }
                 }).then(function (response) {
                        self.productlist = response.data;

                                  setTimeout(function(){
                     self.productChange();
                  },200)    
                                       
                     });
                  }
               
                   },
       
        getMarket(){
           
        var self = this;
        if(self.mtype){
     
            axios.get(APP_URL+'/marketlist', {
            params: {
                 type: self.mtype,
                        }
                       }).then(function (response) {
                       
                 // alert('list')
                        self.marketlist = response.data;
                setTimeout(function(){
                     self.marketChange();
                  },200)
                        // self.marketChange();
   //$('.mult').multiselect('refresh');
                      })
                       
                       // self.market = 'All';
                     
                       
                   }
                   
                     
   


        },
       
        buttonclr(){
          this.buttoncls ='bluebox';
        },
        show(){
         // alert($('#market').val())
          var self = this;
           let loader = self.$loading.show({
                loader: 'dots'
            });
             let formData = new FormData();
               formData.append('producttype', this.ptype);
            formData.append('product',$('#product').val());
            formData.append('markettype', this.mtype);
            formData.append('market',$('#market').val());
            formData.append('sub_to', this.sub_to );
            formData.append('sub_from', this.sub_from );
           
            axios.post(APP_URL+'/getgeneratedata',formData,
                 {
              headers: {
              'Content-Type': 'multipart/form-data'
            }
              }).then(function (response) {
                         loader.hide();
                      self.tablelist = response.data.valuelist;
                 
                       if(self.tablelist){
                        self.buttoncls ='greybox';
                       }
                       if( self.tablelist != ''){
                           self.getCount();
                       }
                   
                     
                     
                        })
         
        },
        checkGenerate(){
          var self = this;
           axios.get(APP_URL+'/checkgenerate', {
             }).then(function (response) {
         
               self.gendisable = response.data.enable;
               if(response.data.dt){
               self.gmonth = response.data.dt[0].month;
               }

             
              });

        },
        getCount(){
          var self = this;
           
           //axios.post(url+'/getcountdata',{
          let formData = new FormData();
               formData.append('producttype', this.ptype);
            formData.append('product',$('#product').val());
            formData.append('markettype', this.mtype);
            formData.append('market',$('#market').val());
            formData.append('sub_to', this.sub_to );
            formData.append('sub_from', this.sub_from );
           
            axios.post(APP_URL+'/getcountdata',formData,
                 {
              headers: {
              'Content-Type': 'multipart/form-data'
            }
              }).then(function (response) {
         
               if(self.detlist != ''){
                       
                       self.tcount = response.data.count[0].val;
                       if(self.tcount > 1 ){
                           self.showcount =true;
                       }else{
                           self.showcount =false;
                       }
                   
                     
                      }
              self.count = response.data.count[0].val;
              self.tcount = response.data.count[0].val;

             
              });

        },

      lastUpdate(){
      var vm =this;
       axios.get(APP_URL+'/getlastdate', {
        params:{
          type:'generate'
        }
                  }).then(function (response) {
                  //  alert('dd')
                    if(response.data[0].maxdate){

                    vm.lastupdate = moment(response.data[0].maxdate).format("MM-YYYY");
                    var year = moment(response.data[0].maxdate).format("YYYY-MM-DD");
                    //alert(year)
                    var monthDayto = new Date(year);
                    //  Var todate = moment(response.data[0].maxdate).format("MM-YYYY");
                   $('#sub_to').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',  endDate: monthDayto}).datepicker("update",monthDayto);
                   vm.sub_to = $('#sub_to').val();
                  }else{
                     var monthDayto = $('#sub_from').val();
                     $('#sub_to').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',  endDate: monthDayto}).datepicker("update",monthDayto);
                      vm.sub_to = $('#sub_to').val();
                  }
                });
     },

     marketChange(){
   // alert('change')
     
    $('#market').multiselect('destroy');
     $('#market').multiselect({
      allSelectedText: 'All',
      maxHeight: 200,
       enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true
    }).multiselect('selectAll', false)
    .multiselect('updateButtonText');
    $('#market').multiselect('rebuild');
  },

  productChange(){
    //  alert(abc)
     
     $('#product').multiselect('destroy');
   $('#product').multiselect({
      allSelectedText: 'All',
      maxHeight: 200,
       enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true
    }).multiselect('selectAll', false)
    .multiselect('updateButtonText');
    $('#product').multiselect('rebuild');

  }

 

  },


mounted(){
  this.getDt();
  this.getProduct();
  this.getMarket();
  var self =this;
  // setTimeout(function(){
  //    self.marketChange();
  //    self.productChange();
  //  },1000)
 
  $('#product').multiselect({
      allSelectedText: 'All',
      maxHeight: 200,
       enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true
    }).multiselect('updateButtonText');
  // this.marketChange();

this.role = $("#role").val();
  if(this.role == 1){
      this.checkGenerate();

  }

 
var date=new Date();
  var year=date.getFullYear(); //get year
  var month=date.getMonth()+1; //get month
   if(month>=4){
            var fyear = date.getFullYear();
        }else if(month<4){
            var fyear = date.getFullYear()-1;
        }
       // alert(fyear);

 
    var fromdate = new Date(fyear, 3, date.getDate());
   // var todate = new Date(2021, 5, date.getDate());
 $('#sub_from').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy'}).on(
      'changeDate', () => { this.sub_from = $('#sub_from').val();this.buttonclr()  }
    ).datepicker("update",fromdate);
    this.sub_from = $('#sub_from').val();
    this.lastUpdate();
   
     var default_date = new Date(2023, 10, 1);
$('#sub_to').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',}).on(
      'changeDate', () => { this.sub_to = $('#sub_to').val(); this.buttonclr(); }
    ).datepicker();
   this.sub_to = $('#sub_to').val();
   }
 
});