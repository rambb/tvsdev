FV Vue.use(Vuetable);
Vue.use(VueLoading);

VeeValidate.Validator.localize('en', {
  custom: {
  
   
     fupload: {
      required: 'Disable File Upload For MIS Users Is Required',
         },
     allowprev: {
      required: 'Previous Months Upload Is Required',
   
    },
    vthreshold: {
      required: 'Variance Threshold Is Required',
   
    },
    mthreshold: {
      required: 'Mininmum Threshold Is Required',
   
    },
    
 
    }
})
Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#uploadsetting',
    
    data:{vthreshold:'',mthreshold:'',locup:'',proup:'',delup:'',edit:false, datelist:'',date:1,  fupload:'',allowprev:'',button:'Save',file: '',
            },

methods:{
       
   
getSettings(){
 var vm = this;
            axios.get(APP_URL+'/getuploadsettings', {
                  }).then(function (response) {
                   var upload_enable =  response.data.upload_enable.metavalue;
                 //  console.log(response.data.upload_enable[0].metavalue)
                   var upload_prev =  response.data.upload_prev.metavalue;
                    if(upload_enable =='Yes' || upload_enable =='No'){
                      vm.fupload = upload_enable;
                    }else{
                      if(upload_enable){
                            vm.fupload = 'date';
                            vm.date = upload_enable
                            vm.button = 'Update'
                      }
                    }
                // alert(upload_prev)
                  vm.allowprev = upload_prev;
                  vm.locup =  response.data.locupdate;
                  vm.proup =  response.data.proupdate;
                  vm.delup =  response.data.delupdate;
                  vm.mthreshold = response.data.minimum_threshold.metavalue;
                   vm.vthreshold = response.data.variance_threshold.metavalue;
              })
},
onDownload($type){
 var vm = this;
            axios.get(APP_URL+'/getmasterdownloads', {
                params: {type:$type}
                  }).then(function (response) {
                    if(response.data){
                    window.location.href = APP_URL+'/storage/app/'+response.data;
                  }
              })
},

  send(){
   var self = this;
     this.$validator.validate('fupload', this.fupload)
   
     .then(() => {
    
 if (this.errors.all() =='') {
      if(this.edit==false){
          var text ="Save";
      }else{
          var text ="Update"
      }
  let loader = this.$loading.show({
                loader: 'dots'
            });
         axios.post(url+'/saveuploadsetting',self.$data)
      .then(response => { 
        loader.hide();

          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;

       // alert(response.data.messege);
       if(id){
            swal(title, id, type); 
           window.location.href = APP_URL+'settings';
       }
      });
       
       
    }
  });
  
  
    },
    onLocationChange(e){
      this.file = e.target.files[0];
        let formData = new FormData();
        formData.append('file', this.file);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/savelocation', formData, config)
      .then(response => { 
         loader.hide();
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;
          var load = response.data.load;

       // alert(response.data.messege);
       if(id){
             swal(title, id, type); 
           window.location.href = APP_URL+load;
       }
      });
    },
      onProductChange(e){
      this.file = e.target.files[0];
        let formData = new FormData();
        formData.append('file', this.file);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/saveproduct', formData, config)
      .then(response => { 
         loader.hide();
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;
          var load = response.data.load;

       // alert(response.data.messege);
       if(id){
             swal(title, id, type); 
           window.location.href = APP_URL+load;
       }
      });
    },
    onDealerChange(e){
      this.file = e.target.files[0];
        let formData = new FormData();
        formData.append('file', this.file);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/savedealer', formData, config)
      .then(response => { 
         loader.hide();
          var error = response.data.error;
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;
          var load = response.data.load;

       // alert(response.data.messege);
       if(error){
             swal('', error, 'warning'); 
       }else{
          swal(title, id, type); 
           window.location.href = APP_URL+load;
       }
      });
    }
   

  },

mounted(){
  this.getSettings()
  this.datelist = new Array(31);
 var numberArray = [];

for(var i = 1; i <= 31; i++){
    numberArray.push(i);
}
this.datelist=numberArray;
}
 
});

