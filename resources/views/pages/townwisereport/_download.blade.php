@extends('layouts.master')
@section('content')
<style type="text/css">
	.multiselect.btn-default{
		border:solid thin #000;
	}
</style>
<script type="text/javascript">

  const monthNames = ["","January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];

  
</script>

<div class="page-wrapper container" id="towndata">
<div class="main_grid">

{{ Form::open(array('url' => '/downloaddata', 'method' => 'post','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12">
    <!-- Nav tabs -->
<ul class="nav nav-tabs  border-0 uploadtab">
    <input type="hidden" value="<?php echo Session::get('user_role');?>" id="role">
 
  <li class='nav-item' v-show="role==1">
    <a class='nav-link' data-toggle='tab' href='#generate' :class="[role==1 ? 'active' : '']">Generate</a>
  </li>

  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#download' :class="[role==1 ? '' : 'show active d-none']">Download</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class='tab-pane container fade in ' id='generate' :class="[role==1 ? 'show active' : '']">
    <br/><br/><br/>
      

    <div class="row">


      <div class="btn  bluegradient justify-content-center p-5 text-white col-2 border rounded text-center" @click="onGenerate()"  v-bind:class="[gendisable == 0 ? 'disabledbutton disabled' : '']"> 
         Generate</div>
         <div class="col-4 text-bottom pt-5">Last run:  @{{update_at}}</div>
       </div><br/><br/>
       <div class="row">
       <h4 class="redtext" v-show="!gendisable"><p>No new data has been added.</p><p>or</p><p>One or more of the following files have not been uploaded: SAP Despatch, Retail, All India SIAM or Townwise data for at least one Area.</p></h4></br>
        <span  v-show="gendisable"><strong>Note:</strong> The Townwise Despatch data will be generated only with those files which are green in the Status tab.</span>
      </div>
  </div>

  <div class='tab-pane container fade' id='download' :class="[role==1 ? '' : 'show active']">
  <h3><span class="bluetext">Filter By:</span></h3>
      <br/>

    <div class="row form-group">

    <div class="col-4">
  
    <label>From:</label>
    <input type="text" placeholder="From" name="sub_from" id="sub_from"  class="border border-dark rounded-0 form-control float-left"> 

    </div>

    <div class="col-4">
      <label>Market Level:</label>
      <select v-model="mtype"   name="markettype" @change="getMarket('yes');buttonclr()"  class="p-2 w-100 border border-dark ">
      
      <option value="Zone">Zone </option>
      <option value="State">State  </option>
      <option value="Area">Area  </option>
      <option value="District">District</option>
      <option value="Town">Town</option>
      </select>
      
      
   </div>

    <div class="col-4">
      <label>Product Level:</label>
      <select v-model="ptype" name="producttype" @change="buttonclr();getProduct('yes');"  class="p-2 w-100 border border-dark ">
        
      <option value="category" >Category</option>
      <option value="segment">Segment </option>
      <option value="manufacturer">Manufacturer </option>
      <option value="brand">Brand</option>
      </select>
    </div>

    </div>

    <div class="row form-group">

    <div class="col-4">
    <label>To:</label>
        <input type="text" placeholder="To" name="sub_to" id="sub_to" class="border rounded-0 border-dark form-control float-left">
    </div>


    <div class="col-4">
        <label>Market:</label>
        <select class="mult form-control" id="market"   name="market[]" multiple="multiple" >
            <option v-for="param in marketlist" v-bind:value="param.id">@{{param.val}}</option>
        </select>
    
    </div>


    <div class="col-4">
        <label>Product:</label>
        <select class="form-control" id="product"   name="product[]" multiple="multiple" >
          
          <option v-for="param in productlist" v-bind:value="param.id">@{{param.val}}</option>
        </select>
       
    
    </div>

    </div> 

     <div class="row form-group">
        <div class="col-12">
        <button class="btn border float-right rounded-0" :class="buttoncls" type="button" @click="show()">Generate Preview</button>
      </div>    
    </div>
<br/>
<hr/>
<div v-show="buttoncls != 'bluebox'">
<div  v-show="tablelist != ''">
<div class="row form-group">
  <div class="col-4"><strong>A sample 10 rows is as below</strong></div><div class="col-4"></div><div class="col-4"><strong>Total Row Count: @{{tcount}} </strong><input type="hidden" name="count" v-model="count"></div>
</div>
<div class="row">
<!-- <div v-show="tablelist"> -->
<table class="table table-bordered" >
<thead class=" bluebox">
<tr><th>Zone</th> <th> State</th><th> Area </th><th> District</th><th>  Town</th> <th> Town Class</th><th>  Manufacturer</th> <th> Category </th><th> Segment </th><th> Sub-Segment</th><th>Brand</th><th> Month</th><th> Despatch </th><th> Retail</th><</tr>
</thead>
<tbody>

          <tr  v-for="(data,key) in tablelist"  >
              <td class="text-left">@{{data.ZONE}}</td>
              <td class="text-left">@{{data.STATE}}</td>
               <td class="text-left">@{{data.AREA}}</td>
              <td class="text-left">@{{data.DIST}}</td>
               <td class="text-left">@{{data.TOWN}}</td>
               <td class="text-left">@{{data.TOWN_CLASS}}</td>
              <td class="text-left">@{{data.MANF}}</td>
               <td class="text-left">@{{data.PROD_CATG}}</td>
              <td class="text-left">@{{data.SEGMENT}}</td>
               <td class="text-left">@{{data.SUBSEGMENT}}</td>
			  <td class="text-left">@{{data.BRAND}}</td>
              <td class="text-left">   @{{monthNames[data.MONTH_TAG]}} - @{{data.MONTH_TXT.substring(2)}} </td>              
               <td class="text-left">@{{data.DISPATCH_QTY}}</td>
              <td class="text-left">@{{data.RETAIL_QTY}}</td>              
          </tr>
          
      </tbody>
</table>
<!-- </div>
<div v-show="tablelist == '' ">
<span>No Data Available</span>
  
</div> -->

</div>

<div class="row form-group ">
 <div class="col-10 text-center">
<h4 class="redtext" v-show="showcount">You can download a maximum of 10 Lakh rows. Please use the filter above and try again.</h4>
</div>
<div class="col-2"> <button class="btn bluebox border float-right rounded-0" :disabled="showcount">Download as Excel</button> </div>
</div>
</div>
<div v-show="tablelist == ''">
<div class="col-10 text-center">
<h4 class="redtext" >No Data Available</h4>
</div>
</div>
</div>
  </div><!-- tab ends -->

</div><!-- tab content -->


</div> <!-- col-12 end -->
</div> <!-- row end -->
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}townwisereport/townwisereport.js"></script>  

@endsection
<style>
.disabledbutton{
  cursor: not-allowed;
  pointer-events: none;
}
  
</style>