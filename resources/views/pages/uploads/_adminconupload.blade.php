@extends('layouts.master')
@section('content')
<div class="page-wrapper container" id="conupload">
<div class="main_grid">

{{ Form::open(array('url' => '/downloadcontemplate', 'method' => 'post','enctype'=>'multipart/form-data','id'=>'adminup')) }}
<div class="row">
<div class="col-lg-12 col-sm-12 pr-0">

	 <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
	  <input type="hidden" id="user_role" value="<?php echo Session::get('user_role');?>">
	 <br>


<!-- Nav tabs -->
<ul class="nav nav-tabs  border-0 uploadtab">
	

	<li class='nav-item'>
    <a class='nav-link '   href='{{url("/sapDispatch") }}'>SAP Despatch</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='{{url("/retails") }}'>Retail</a>
   </li>  
    <li class='nav-item'>
    <a class='nav-link'   href='{{url("/allSiam") }}'>All India SIAM</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='{{url("/pdPlan") }}'>PD Plan</a>
   <li class='nav-item'>
    <a class='nav-link active' href='{{url("/consolidateUpload") }}'>Consolidated Townwise for HO</a>
  </li>
 </ul>

<!-- Tab panes -->

     <br/>
     <!-- /.Main Heading -->
<div class="row  pt-0 "> 
<div class="col-lg-12 col-md-12 col-sm-12 text-right pr-0 ">
   <!--  <button class="btn greybox border border-dark btn-sm " :disabled="templatedownload">Download Template</button> -->
</div>
</div>
<br/>
    	<div class="row">
     	<div class="col-1"> Step 1 </div>
		<div class="col-11">
			<!-- <div class="row">
			<div class="col-2 form-inline">
			 <span class="btn btn-md border btn-file w-75" v-bind:class="[uploaddisable ? 'warmgreybox disabled' : 'bluebox']">Upload<input type="file" v-model="confile" ref="confile"  name="confile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" :disabled="uploaddisable" @change="onchange" ></span>
			</div>
			<div class="col-2 form-inline pr-0">
			<label>Month: </label>
			<input type="text"  name="conmonth" id="conmonth"  class="float-left col-8 form-control border border-dark rounded-0 ml-1"> 
			</div>
			<div class="col-2 form-inline pr-0"> 
			<label>Area:</label>
			<select v-validate="'required'" id="area" name="area" @change="areachange()"  class="ml-2 col-9 p-2 border  border-dark" v-bind:class="{ 'is-invalid': errors.has('area') }" v-model="area" >
            <option v-for='(data,key) in locationlist' :value="data.id"    >@{{ data.area }}</option>
            </select>
            <span class="error d-none" v-if="errors.has('area')" style="color:red">Area is required</span>
            </div>


		</div>

		<br/>

		<div class="row mb-2">
			<div class="col-md-12">
			<span class="pl-3 " v-show="filename" ><strong class="bluetext">@{{filename}} </strong>uploaded. Please proceed to Step 2.</span>
			</div>
         </div> -->

			<div class="row border-bottom blueborder">
		
			<div class="col-8">
		      <div class="row"> 
		        <div class="col-3 pr-5 h-25">
		         <span class="btn btn-md border btn-file btn-light w-100 bluebox" v-bind:class="[uploaddisable ? 'warmgreybox disabled' : 'bluebox']">Upload<input type="file" v-model="confile" ref="confile"  name="confile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" :disabled="uploaddisable" @click="clickUp($event)" @change="onchange" ></span>
		         </div> 
			 <div class="col-7 pl-0 h-50">
			
			 <div class="row">
			 	<div class="col-5 form-inline pr-0">
				<label>Month: </label>
				<input type="text"  name="conmonth" id="conmonth"  class="float-left col-8 form-control border border-dark rounded-0 ml-1"> 
				</div>
				<div class="col-5 form-inline pr-0"> 
				<label>Area:</label>
				<select v-validate="'required'" id="area" name="area" @change="areachange()"  class="ml-2 col-9 p-2 border  border-dark" v-bind:class="{ 'is-invalid': errors.has('area') }" v-model="area" >
	            <option v-for='(data,key) in locationlist' :value="data.id"    >@{{ data.area }}</option>
	            </select>
	            <span class="error d-none" v-if="errors.has('area')" style="color:red">Area is required</span>
	            </div>
			 </div>
			 
			</div>
			</div>
			<div class="row mb-2 mt-2"><div class="col-md-12"><span class="pl-3 " v-show="filename" ><strong class="bluetext">@{{filename}} </strong>uploaded. Please proceed to Step 2.</span></div></div> 
		
        </div>
		<div class="col-4">
			<div class="row mb-2">
					<div class="col-6 text-right"><span class="text-right"><strong>Help:</strong></span> </div>
					<div class="col-6"><span><button class="btn greybox border border-dark btn-sm ">Download Template</button></span></div>
				</div> 
			<div class="row mt-2">
				<div class="col-6 text-right"> <strong class="text-right">Last updated:</strong></div><div class="col-6 text-left"> <span class="ml-2 text-left"><small class="bluetext">@{{lastupdate}}</small></span>
				</div>
			</div>
		</div>
	</div>

		</div>

		</div><br/>
		<div class="row" >
     	<div class="col-1"> Step 2 </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row form-group">
			<div class="col-md-2">
			<button class="btn btn-md btn-light border w-75" type="button" @click="Validate()"  :disabled="validatebutton" :class="convalidate">Validate</button>
			</div>
		</div>
		<p v-if="messege"><span  class="bluetext font-weight-bold">@{{messege}}</span>
			<span v-if="wraninglist">, but with warnings. Please proceed to Step 3.</span>
			<span v-else>. Please proceed to Step 3.</span>
		</p>

		<div class="col-md-11" v-show="showerror">
		<p v-show="showerror" style="font-weight: bold">Found the below errors. Please correct the errors and repeat Step 1.</p>
			<div style="height: auto;overflow: auto;max-height: 330px; "> 
		      <div v-for="(mft, key) in error " >
		      <div v-for="(verrors, vkey) in mft" >
		       <div v-for="(errors, ekey) in verrors" >
		  	 <span><i class='fa fa-remove text-danger'></i>&nbsp;&nbsp;@{{errors.error}}</span><br/>
		  	 </div>
		  	 </div>
		      </div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-12 message">
			
			</div>
		</div>

		</div>
		</div><br/>
		 <div class="row" >
	     	<div class="col-1"> Step 3 </div>
				<div class="col-11 pb-5"> 
				<div v-show="!showerror">
					<div v-show="wraninglist">
					  <p >Please add in remarks for the below Validation warnings before submitting</p><br/>
					  <div style="height: auto;overflow: auto;max-height: 330px;overflow-x: hidden; "> 
					  <div class="row  form-group" v-for="(warnings, key) in wraninglist ">
					  	<div class="col-5"> <i class='fa fa-exclamation text-warning'></i>&nbsp;&nbsp;@{{warnings.warning}}</div>
					  	<div class="col-5"><input v-on:keyup="remarkChange()" type="text" :name="'remark'+key" v-validate="'required'" placeholder="Add remark here" v-model="valsArray[key]"  class=" form-control">   <span class="error" v-if="errors.has('remark'+key)" style="color:red">Remark is required</span></div>
					  </div>
					</div>
					 </div>	
					 </div>
					<div class="row form-group">
					<div class="col-md-2">
						<button class="btn btn-md btn-light border w-75" type="button" @click="sendConsolidate" :disabled="disbutton" :class="consubmit">Submit</button>
					</div>
							</div>
			</div>
			</div>
	</div>
   

</div>
<div id="datarawModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      
    <div class="modal-content border border-dark rounded-0 ">
    <div class="modal-body">
    <h2 class="text-center bluetext">Summary View</h2>
     <div id="overlay" v-show="loading">
        <img src="{{IMAGES}}loader.GIF"  width="100" height="100">
        </div>
         <table class="table table-bordered ">
      <thead>
          <tr class="bluebox">
              <th v-for="(column, index) in manufacturelist" :key="index" > @{{column}}</th>
          </tr>
      </thead>
      <tbody>
  
           <tr v-for="(item, index) in approvelist" :key="index" >
              <td ><span  class="float-left">@{{index}}</span></td>
               <td ><span  class="float-left">@{{item[0].towngroup}}</span></td>
               <td v-for="(column, indexColumn) in item"  ><span  class="float-right">@{{column[column.brand] ? column[column.brand] : 0 }}</span></td>
          </tr>
          <tr>
              <td v-show="!approvelist"> No data found</td>
             
          </tr>
      </tbody>
    </table>
    </div>
  <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
         <button type="button" class="btn btn-md border  bluebox"  @click="save()">Submit for AM approval </button>
      </div>
          
    </div>
  

    </div>
     
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}uploads/uploads.js"></script>  
<script type="text/javascript">
$(document).ready(function() {

        $('#uploads').addClass('active');
});
</script>
<style type="text/css">
#overlay {
  background: #ffffff;
  color: #666666;
  position: fixed;
  height: 100%;
  width: 100%;
  z-index: 5000;
  top: 0;
  left: 0;
  float: left;
  text-align: center;
  padding-top: 25%;
  opacity: .80;
}</style>
@endsection