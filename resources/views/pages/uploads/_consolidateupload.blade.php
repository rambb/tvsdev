@extends('layouts.master')
@section('content')
<div class="page-wrapper container" id="conupload">
<div class="main_grid">

{{ Form::open(array('url' => '/downloadcontemplate', 'method' => 'post','enctype'=>'multipart/form-data')) }}


<div class="row">
<div class="col-lg-12 col-sm-12 pr-0">
	<input type="hidden" id="area" name ="area" value="<?php echo Session::get('area');?>">
	<input type="hidden" id="areaid" value="<?php echo Session::get('areaid');?>">
	 <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
	  <input type="hidden" id="user_role" value="<?php echo Session::get('user_role');?>">
	 <br>


<!-- Nav tabs -->
<ul class="nav nav-tabs  border-0 uploadtab" id="uploadtab">
  <li class='nav-item'>
    <a class='nav-link active' data-toggle='tab' href='#consdata'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    
    <a class='nav-link ' data-toggle='tab' href='#rawdata' >Competition raw data for HO</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
     <div class='tab-pane container fade in show active' id='consdata'>
     
     <!-- /.Main Heading -->
<div class="row  pt-0 "> 
<div class="col-lg-12 col-md-12 col-sm-12 text-right pr-0 ">
    <button class="btn greybox border border-dark btn-sm ">Download Template</button>
</div>
</div>
<br/>
    	<div class="row">
     	<div class="col-1"> Step 1 </div>
		<div class="col-11 border-bottom blueborder">
			<div class="row">
			<div class="col-md-2 form-inline">
			 <span class="btn btn-md border btn-file w-75" v-bind:class="[uploaddisable ? 'warmgreybox disabled' : 'bluebox']">Upload<input type="file" v-model="confile" ref="confile"  name="confile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" :disabled="uploaddisable" @change="onchange" ></span>
			</div>
			<div class="col-md-2 form-inline pr-0">
			<label>Month: </label>&nbsp;
			<input type="text"  name="conmonth" id="conmonth"  class="float-left p-0 col-8 form-control border-0"> 
			</div>
			<div class="col-md-2 form-inline pr-0"> <label>Area: @{{area}}</label></div>
		</div><br/>
		<div class="row mb-2">
			<div class="col-md-12">
			<span class="pl-3 " v-show="filename" class="float-left"><strong class="bluetext">@{{filename}} </strong>uploaded. Please proceed to Step 2.</span>
			</div>
         </div> 

		</div>
		</div><br/>
		<div class="row" >
     	<div class="col-1"> Step 2 </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row form-group">
			<div class="col-md-2">
			<button class="btn btn-md btn-light border w-75" type="button" @click="Validate()"  :disabled="validatebutton" :class="convalidate">Validate</button>
			</div>
		</div>

		<p v-if="messege"><span  class="bluetext font-weight-bold">@{{messege}}</span>
			<span v-if="wraninglist">, but with warnings. Please proceed to Step 3.</span>
			<span v-else>. Please proceed to Step 3.</span>
		</p>

		<div class="col-md-11" v-show="showerror">
		<p v-show="showerror" style="font-weight: bold">Found the below errors. Please correct the errors and repeat Step 1.</p>
			<div style="height: auto;overflow: auto;max-height: 330px; "> 
		      <div v-for="(mft, key) in error " >
		      <div v-for="(verrors, vkey) in mft" >
		       <div v-for="(errors, ekey) in verrors" >
		  	 <span><i class='fa fa-remove text-danger'></i>&nbsp;&nbsp;@{{errors.error}}</span><br/>
		  	 </div>
		  	 </div>
		      </div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-12 message">
			
			</div>
		</div>

		</div>
		</div><br/>
		 <div class="row" >
	     	<div class="col-1"> Step 3 </div>
				<div class="col-11 pb-5"> 
					<div v-show="!showerror">
					<div v-show="wraninglist">
					  <p >Please add in remarks for the below Validation warnings before submitting</p><br/>
					  <div style="height: auto;overflow: auto;max-height: 330px;overflow-x: hidden; "> 
					  <div class="row  form-group" v-for="(warnings, key) in wraninglist ">
					  	<div class="col-5"> <i class='fa fa-exclamation text-warning'></i>&nbsp;&nbsp;@{{warnings.warning}}</div>
					  	<div class="col-5"><input type="text" :name="'remark'+key" v-validate="'required'" placeholder="Add remark here" v-model="valsArray[key]"  class=" form-control">   <span class="error" v-if="errors.has('remark'+key)" style="color:red">Remark is required</span></div>
					  </div>
					</div>
					 </div>	
					 </div>
					<div class="row form-group">
					<div class="col-md-2">
						<button class="btn btn-md btn-light border w-75" type="button" @click="sendConsolidate" :disabled="disbutton" :class="consubmit">Submit</button>
					</div>
							</div>
			</div>
			</div>
			<div v-show="uploaddisable">
		
			
			
			<p v-show="consuploaded" class="font-weight-bold redtext">Data upload has been disabled. To upload data, please contact the admin. </p></div>
	</div>
    <div class='tab-pane container fade' id='rawdata'>
	<br/>
   	<div class="row">
     	<div class="col-1"> Step 1 </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row">
		   <div class="col-md-2 form-inline">
			 <span class="btn btn-md border btn-file w-75" v-bind:class="[buttondisable ? 'warmgreybox disabled' : 'bluebox']">Upload<input type="file"  ref="file" name="file" multiple="multiple"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @change="onrowchange" :disabled="buttondisable"  ></span>
			</div>
			
			<div class="col-md-2 form-inline pr-0">
			<label>Month: </label>&nbsp;
			<input type="text"  name="compmonth" id="compmonth"  class="float-left p-0 col-8 form-control border-0"> 
			</div>
			<!--<div class="col-md-3 form-inline mt-4 mb-4"><label>Month:&nbsp;   
			<input type="text"   id="compmonth" v-validate="'required'"   class="float-left form-control  border-0"  > </label></div>-->
			<div class="col-md-3 form-inline mt-4 mb-4"> <label>Area:&nbsp;@{{area}}</label></div>
		</div>

		<div class="row mb-2">
			<div class="col-md-12">

			<span  v-show="rawfilename" class="bluetext"> Below files were uploaded: <br/><div class="pl-4" v-html="rawfilename"></div> Please proceed to Step 2.</span>
			</div>
         </div> 

		</div>
		</div><br/>


	<div class="row" >
     	<div class="col-1"> Step 2 </div>
		<div class="col-11 pb-5"> 
		<div class="row form-group">
					<div class="col-md-2">
			<button class="btn btn-md btn-light border w-75" type="button" :disabled="rowdisable" @click="sendrow()" :class="rowsubmit">Submit</button>
			</div></div>
		</div>
		</div>

		</div>
	</div>
</div>
</div>



<!-- Modal for Are Summary before submit -->
<div id="datarawModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      
    <div class="modal-content border border-dark rounded-0 ">
    <div class="modal-body">
    <h2 class="text-center bluetext">Summary View</h2>
       <div id="overlay" v-show="loading">
        <img src="{{IMAGES}}loader.GIF"  width="100" height="100">
        </div>
         <table class="table table-bordered ">
      <thead>
          <tr class="bluebox">
              <th v-for="(column, index) in manufacturelist" :key="index" > @{{column}}</th>
          </tr>
      </thead>
      <tbody>

           <tr v-for="(item, index) in approvelist" :key="index" >
              <td ><span  class="float-left">@{{index}}</span></td>
               <td ><span  class="float-left">@{{item[0].towngroup}}</span></td>
               <td v-for="(column, indexColumn) in item"  ><span  class="float-right">@{{column[column.brand] ? column[column.brand] : 0 }}</span></td>
          </tr>
          <tr>
              <td v-show="!approvelist"> No data found</td>
             
          </tr>
      </tbody>
    </table>
    </div>
  <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
         <button type="button" class="btn btn-md border  bluebox"  @click="save()">Submit for AM approval</button>
      </div>
          
    </div>
  

    </div>
     
</div>

{{ Form::close() }}
</div>
</div>
<style type="text/css">
#overlay {
  background: #ffffff;
  color: #666666;
  position: fixed;
  height: 100%;
  width: 100%;
  z-index: 5000;
  top: 0;
  left: 0;
  float: left;
  text-align: center;
  padding-top: 25%;
  opacity: .80;
}</style>
<script src="{{VJS}}uploads/uploads.js"></script>  
<script type="text/javascript">
	$(function() {

    $('a[data-toggle="tab"]').on('click', function(e) {
        window.localStorage.setItem('activeTab', $(e.target).attr('href'));
       console.log(activeTab);
    });
    var activeTab = window.localStorage.getItem('activeTab');
    console.log(activeTab);
    if (activeTab) {
        $('#uploadtab a[href="' + activeTab + '"]').tab('show');
        window.localStorage.removeItem("activeTab");
    }

    

	});


</script>

@endsection