@extends('layouts.master')
@section('content')

<div class="page-wrapper container" id="pd">
<div class="main_grid">

{{ Form::open(array('url' => '', 'method' => 'get','enctype'=>'multipart/form-data')) }}


 <br>

<ul class="nav nav-tabs  border-0 uploadtab">
	
 
	<li class='nav-item'>
    <a class='nav-link '   href='{{url("/sapDispatch") }}'>SAP Despatch</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='{{url("/retails") }}'>Retail</a>
   </li>  
    <li class='nav-item'>
    <a class='nav-link'   href='{{url("/allSiam") }}'>All India SIAM</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link active '  href='{{url("/pdPlan") }}'>PD Plan</a>
 </li>
   <li class='nav-item'>
    <a class='nav-link ' href='{{url("/consolidateUpload") }}'>Consolidated Townwise for HO</a>
  </li>
 </ul>

<div class="container p-0">
<div class="row">
<div class="col-lg-12 col-sm-12">
<br/><br/>
    	<div class="row pt-2">
     	<div class="col-1"> Step 1: </div>
		<div class="col-10 border-bottom blueborder">
		<div class="row">
			<div class="col-md-2 form-inline">
			 <span class="btn bluebox border btn-file w-75">Upload<input  v-model="file" type="file" name="file" v-validate="'required'" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @change="onchange($event)"></span>
			</div>
			<div class="col-md-2 form-inline pr-0">
			<label>Month: </label>&nbsp;
			<input type="text"  name="date" id="date"  class="float-left p-0 col-8 form-control border-0"> 
			</div>
			
		</div><br/>
		<div class="row mb-2">
			<div class="col-md-12">
			<span  v-show="filename" ><strong class="bluetext">@{{filename}} </strong>uploaded. Please proceed to Step 2.</span>
			</div>
         </div> 



		</div>
		</div><br/>
		 <div class="row" >
     	<div class="col-1"> Step 2: </div>
		<div class="col-10 pb-5"> 
		<div>
						<div class="row form-group">
					  	<div class="col-md-2">	
			<button class="btn  btn-light border w-75" type="button" @click="sendPd" :disabled="buttondisable" :class="submit">Submit</button>
		</div>
	</div>
			
		</div>
		</div>
		</div>
		</div>
</div></div>
{{ Form::close() }}

</div></div>
<script src="{{VJS}}uploads/pduploads.js"></script>  
<script type="text/javascript">
		$(document).ready(function() {

        $('#uploads').addClass('active');
});

</script>
@endsection
