 Vue.use(Vuetable);

VeeValidate.Validator.localize('en', {
  custom: {
  
   
     fupload: {
      required: 'Disable File Upload For MIS Users Is Required',
         },
     allowprev: {
      required: 'Previous Months Upload Is Required',
   
    },
    
 
    }
})
Vue.use(VeeValidate)//vue-validator use 
Vue.use(VueLoading);
var app  =new Vue({
    el:'#sap',
    
    data:{final_arr:'',validatebutton:true,messege:'',validateclass:'lightgreytext',submit:'lightgreytext',filename:'',disbutton:true,warningarray:[],indexarray:[],valsArray: [],wraninglist:'',showerror:false,error:'',sapfile:'',file:'',sub_mnth :'',sub_frm :'',dateview:'false',edit:false, datelist:'',date:1,  fupload:'',allowprev:'',button:'Save',file: '',
            },

methods:{
   clickUp(e){
    //alert('dd')
     if(typeof e.target.files[0] != "undefined"){
    if(e.target.files[0].name == this.filename){
       this.validatebutton = false;
       this.wraninglist='';
       this.showerror =false;
       this.error  = '';
       this.messege='';
       this.submit = 'lightgreytext';
       this.disbutton = true;


     }
   }



  },
  onchange(e){
    if(typeof e.target.files[0] != "undefined"){
      this.sapfile = e.target.files[0];
      this.filename = e.target.files[0].name;
    if(this.filename){
       this.validatebutton = false;
       this.validateclass = 'bluebox';
       this.submit = 'lightgreytext';
       this.wraninglist='';
        this.showerror =false;
        this.error  = '';
        this.messege='';
     }else{
       this.validatebutton = true;
       this.validateclass = 'lightgreytext';
     }
   }
  },
   validate(e){
    var self =this;
    self.showerror =false;
    self.error  = '';
   let loader = this.$loading.show({
                loader: 'dots'
            });
        let formData = new FormData();
        formData.append('file', self.sapfile);
        formData.append('type', 'siam');
         formData.append('month', self.sub_mnth);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        
        axios.post(url+'/validatesiamupload', formData, config)
        .then(response => { 
          loader.hide();
          var error = response.data.error;
         if(error == ''){
            self.final_arr = response.data.final_arr;
          }
      //    alert(error);
          if(response.data.warning != ''){
            self.wraninglist= response.data.warning;
          

          response.data.warning.forEach(function(value, index) {
             self.indexarray.push(value.index);
            });
           response.data.warning.forEach(function(value, index) {
             self.warningarray.push(value.warning);
            });
         }
          if(error == ''){
            self.showerror =false;
            self.error  = '';
            self.submit = 'bluebox';
            self.disbutton =false;
            self.messege = 'Validation successful';
            
          }else{
            self.showerror =true;
            self.error  = error;         
         }
  })
    },
       
   


  send(){
    var self =this;
       let formData = new FormData();
        formData.append('file', self.sapfile);
        formData.append('month', self.sub_mnth);
        formData.append('remark', JSON.stringify(self.valsArray));
        formData.append('index',  JSON.stringify(self.indexarray));
         formData.append('final_arr',  JSON.stringify(self.final_arr));
        formData.append('warning',  JSON.stringify(self.warningarray));        const config = {headers: { 'content-type': 'multipart/form-data' }}
          let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/uploadSIAM', formData, config)
        .then(response => { 
          loader.hide();
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;

       // alert(response.data.messege);
       if(id){
            swal(title, id, type); 
           window.location.href = APP_URL+'sapDispatch';
        }
      });
  
  
    },
    Download(){
       var vm = this;
            axios.get(APP_URL+'/getsapdownload', {
                  }).then(function (response) {
                  vm.locationlist = response.data;
              })
    }
    
   
   

  },

mounted(){
  var self = this;
  //alert($('#allow_prev').val());
  if($('#allow_prev').val()=='Yes'){
    this.dateview =true;
  }else{
     this.dateview =false;
    
  }
  var date=new Date();
var year=date.getFullYear(); //get year
var month=date.getMonth(); //get month
var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
    
      $('#date').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',endDate: monthDay}).on(
      'changeDate', () => { this.sub_mnth = $('#date').val();  }
    ).datepicker("update",lastDayWithSlashes);
  var date1 = new Date();
  date1.setMonth(date1.getMonth() - 1, 1);
             // alert(date1);
  $("#date").datepicker();
  $("#date").datepicker().datepicker("setDate", date1);
   this.sub_mnth =$('#date').val();

}
 
});

