@extends('layouts.master')
@section('content')
<div class="page-wrapper container" id="conupload">
<div class="main_grid">

{{ Form::open(array('url' => '/downloadcontemplate', 'method' => 'post','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12 pr-0">
	<input type="hidden" id="area" name ="area" value="<?php echo Session::get('area');?>">
	<input type="hidden" id="areaid" value="<?php echo Session::get('areaid');?>">
	 <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
	  <input type="hidden" id="user_role" value="<?php echo Session::get('user_role');?>">
	 <br>


<ul class="nav nav-tabs  border-0 uploadtab">
	  <li class='nav-item'>
    <a class='nav-link ' href='{{url("/consolidateUpload") }}'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link active' href='{{url("/rawUpload") }}'>Competition raw data for HO</a>
   </li> 
	<li class='nav-item'>
    <a class='nav-link '   href='{{url("/sapDispatch") }}'>SAP Despatch</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='{{url("/retails") }}'>Retail</a>
   </li>  
    <li class='nav-item'>
    <a class='nav-link'   href='{{url("/allSiam") }}'>All India SIAM</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link '  href='{{url("/pdPlan") }}'>PD Plan</a>
 
 </ul>


	<br/>
		 <br>

   	<div class="row">
     	<div class="col-1"> Step 1 </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row">
		   <div class="col-md-2 form-inline pr-0">
			 <span class="btn  border btn-file" v-bind:class="[buttondisable ? 'warmgreybox disabled' : 'bluebox']">Upload<input type="file"  ref="file" name="file" multiple="multiple"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @change="onrowchange" :disabled="buttondisable"  ></span>
			</div>
			
			<div class="col-md-2 form-inline pr-0">
			<label>Month: </label>&nbsp;
			<input type="text"  name="compmonth" id="compmonth"  class="float-left p-0 col-8 form-control border-0"> 
			</div>
			<!--<div class="col-md-3 form-inline mt-4 mb-4"><label>Month:&nbsp;   
			<input type="text"   id="compmonth" v-validate="'required'"   class="float-left form-control  border-0"  > </label></div>-->
			<div class="col-md-4 form-inline pr-0"> 
             <div class="row form-group">
      
            <label class="col-sm-4">Area:</label>
            <div class="col-sm-8"> 
            <select class="form-control" v-model="areaid"   >
              <option v-for='data in locationlist' :value="data.id">@{{ data.area }}</option>
            </select>
           
            </div>
       
        </div></div>
		</div>

		<div class="row mb-2">
			<div class="col-md-12">
			<span  v-show="rawfilename" > <span   v-html="rawfilename"></span> files uploaded. Please proceed to Step 2.</span>
			</div>
         </div> 

		</div>
		</div><br/>


	<div class="row" >
     	<div class="col-1"> Step 2 </div>
		<div class="col-11 pb-5"> 
		<div >
			<button class="btn  btn-light border" type="button" :disabled="buttondisable" @click="sendrow()" :class="rowsubmit">Submit</button>
			
		</div>
		</div>
		</div>
	
</div>
</div>
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}uploads/uploads.js"></script>  
@endsection