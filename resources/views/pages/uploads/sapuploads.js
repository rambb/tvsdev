 Vue.use(Vuetable);

VeeValidate.Validator.localize('en', {
  custom: {
  
   
     fupload: {
      required: 'Disable File Upload For MIS Users Is Required',
         },
     allowprev: {
      required: 'Previous Months Upload Is Required',
   
    },
    
 
    }
})
Vue.use(VeeValidate)//vue-validator use 
Vue.use(VueLoading);
var app  =new Vue({
    el:'#sap',
     components: {
        Loading: VueLoading
    },
    data:{final_arr:'',messege:'',sapvalidate:true,buttonvalidate:'lightgreytext',sapsubmit:'lightgreytext',filename:'',isLoading: false,disbutton:true,warningarray:[],indexarray:[],valsArray: [],wraninglist:'',showerror:false,error:'',sapfile:'',file:'',sub_mnth :'',sub_frm :'',dateview:'false',edit:false, datelist:'',date:1,  fupload:'',allowprev:'',button:'Save',file: '',
            },

methods:{
  clickUp(e){
    //alert('dd')
  if(typeof e.target.files[0] != "undefined"){
    if(e.target.files[0].name == this.filename){
     // alert('d')
       this.sapvalidate = false;
       this.wraninglist='';
       this.showerror =false;
       this.error  = '';
       this.messege='';
       this.sapsubmit = 'lightgreytext';
       this.disbutton = true;


     }
   }



  },
  onSapchange(e){

//alert('d')
       
     if(typeof e.target.files[0] != "undefined"){
        this.filename = e.target.files[0].name;
        this.sapfile = e.target.files[0];
      if(this.filename){
       this.sapvalidate = false;
       this.buttonvalidate = 'bluebox';
       this.sapsubmit = 'lightgreytext';
       this.wraninglist='';
       this.showerror =false;
       this.error  = '';
       this.messege='';
     }else{
       this.sapvalidate = true;
       this.buttonvalidate = 'lightgreytext';
     }
   }
     // alert(fileName)
  },
   validateSap(e){
    var self =this;
    self.showerror =false;
    self.error  = '';
    
        let formData = new FormData();
        formData.append('file', self.sapfile);
        formData.append('month', self.sub_mnth);
        formData.append('type', 'sap');
        const config = {headers: { 'content-type': 'multipart/form-data' }}
         let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/validatesapupload', formData, config)
        .then(response => { 
         loader.hide()
          var error = response.data.error;
          if(error == ''){
            self.final_arr = response.data.final_arr;
          }
          
           if(response.data.warning != ''){
           self.wraninglist= response.data.warning;
          response.data.warning.forEach(function(value, index) {
             self.indexarray.push(value.index);
            });
           response.data.warning.forEach(function(value, index) {
             self.warningarray.push(value.warning);
            });
         }



          if(error == ''){
            self.showerror =false;
            self.error  = '';
            self.sapsubmit = 'bluebox';
            self.disbutton =false;
            self.messege = 'Validation successful';
            
          }else{
            self.showerror =true;
            self.error  = error;         
         }
  })
    },
       
   


  sendSap(){
    var self =this;
       let formData = new FormData();
       var values = [];
       var indexval=[];
   
        formData.append('file', self.sapfile);
        formData.append('month', self.sub_mnth);
        formData.append('remark', JSON.stringify(self.valsArray));
        formData.append('index',  JSON.stringify(self.indexarray));
        formData.append('warning',  JSON.stringify(self.warningarray));
         formData.append('final_arr',  JSON.stringify(self.final_arr));
       
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/uploadsp', formData, config)
        .then(response => { 
           loader.hide()
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;

       // alert(response.data.messege);
       if(id){
            swal('Success!', id, type); 
           window.location.href = APP_URL+'sapDispatch';
        }
      });
  
  
    },
    
    
   
   

  },

mounted(){
 
  var self = this;
  //alert($('#allow_prev').val());
  if($('#allow_prev').val()=='Yes'){
    this.dateview =true;
  }else{
     this.dateview =false;
    
  }
var today='';
var date=new Date();
var year=date.getFullYear(); //get year
var month=date.getMonth() - 1; //get month

 var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
    
      $('#date').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',endDate: monthDay}).on(
      'changeDate', () => { this.sub_mnth = $('#date').val();  }
    ).datepicker("update",lastDayWithSlashes);
   this.sub_mnth =$('#date').val();
}


 
});

