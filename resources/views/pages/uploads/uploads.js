 Vue.use(Vuetable);
Vue.use(VueLoading);

Vue.use(VeeValidate)//vue-validator use 
var app  =new Vue({
    el:'#conupload',
  
    data:{lastupdate:'',rawsave:false,selected:'selected',loading:false,templatedownload:false,manufacturelist:'',approvelist:'',locationlist:'',messege:'',convalidate:'lightgreytext',consubmit:'lightgreytext',rowsubmit:'lightgreytext',rawfilename:'',filename:'', uploaddisable:false,rawuploaded:false, consuploaded:true, upload_enable:'',validatebutton:true,conmonth:'',disbutton:true,warningarray:[],indexarray:[],valsArray: [],wraninglist:'',showerror:false,error:'',confile:'',
      conbuttondisable:true,compmonth :'',file: '',area:'',areaid:'',sub_mnth:'',buttondisable:false, postFormData: new FormData(),rowdisable:true,user_role:''
            },

methods:{
   clickUp(e){
    //alert('dd')
     if(typeof e.target.files[0] != "undefined"){
    if(e.target.files[0].name == this.filename){
       this.validatebutton = false;
       this.wraninglist='';
       this.showerror =false;
       this.error  = '';
       this.messege='';
       this.consubmit = 'lightgreytext';
       this.disbutton = true;


     }
   }



  },
  onchange(e){
    if(typeof e.target.files[0] != "undefined"){
     this.filename = e.target.files[0].name;
      if(this.filename && this.area){
       this.validatebutton = false;
       this.convalidate = 'bluebox';
       this.consubmit = 'lightgreytext';
       this.wraninglist='';
        this.showerror =false;
        this.error  = '';
        this.messege='';
     }else{
       this.validatebutton = true;
       this.convalidate = 'lightgreytext';
     }
   }
  },
 
   onrowchange(e){
    var arr =[];
    var str1 = '';
   for (var i = 0; i < e.target.files.length; ++i) {
        //arr.push(e.target.files[0].name); commented by chethan 
        str1 = str1 + e.target.files[i].name + '<br/>'  ;
    }
    //var str1 = arr.toString();
    //str1 = str1.replace(/,/g, ", ");
    this.rawfilename = str1;
    if(this.rawfilename){
     this.rowsubmit = 'bluebox';
     this.rowdisable = false;
    }else{
      this.rowsubmit = 'lightgreytext';
      this.rowdisable =true;
    }
  },
checkrawUpload(){
  var self = this;
 // alert(self.compmonth);
     axios.get(url+'/checkuploads', {
                params: {
              areaid: self.areaid,
              month:this.conmonth,
              type:'Raw Data'
              }

            }) .then(function (response) {
            //alert(response.data);
              if(response.data[0]){
              //  alert('dd')
               self.uploaddisable =  false;
               self.rawuploaded   = false;
               
              }else{
                //alert('s')
              self.uploaddisable = true;
              self.rawuploaded   = true;
              self.consuploaded  = false;
              }

            })

},
   checkconsolidateUpload(){
  var self = this;
    this.validatebutton = true;
    this.convalidate = 'lightgreytext';
    this.consubmit = 'lightgreytext';
    this.wraninglist='';
    this.showerror =false;
    this.error  = '';
    this.messege='';
  //alert(self.compmonth)
     axios.get(url+'/checkuploads', {
                params: {
              areaid: self.areaid,
              month:this.conmonth,
              type:'Consolidated'
              }

            }) .then(function (response) {
            //alert(response.data);
              if(response.data == 'Enabled Upload') {
                  self.uploaddisable = false;
                }
              else if(response.data == 'Disabled Upload' || response.data == 'Already Uploaded') {
               // alert(self.user_role)
                self.uploaddisable = true;
                self.consuploaded  = false;
              }
              else{
                // alert(self.user_role)
               if(self.user_role != 1){
                self.enable();
               }
               self.checkrawUpload()
               // self.uploaddisable = false              
              }
              
            })

},
   areachange(){
     this.areaid  = this.area;
   //  alert(this.areaid);
   // this.templatedownload = false; 
        axios.get(url+'/changesessiontown', {
              params:{
               area:this.areaid
              }
                  }).then(function (response) {
                
              })
   
    if(this.filename && this.area){
       this.validatebutton = false;
       this.convalidate = 'bluebox';
       this.consubmit = 'lightgreytext';
       this.wraninglist='';
        this.showerror =false;
        this.error  = '';
        this.messege='';

        
     }else{
       this.validatebutton = true;
       this.convalidate = 'lightgreytext';
     }
      if(this.area){this.templatedownload = false; }
  },
  remarkChange(){
  
        this.consubmit = 'bluebox';
        this.disbutton =false;
   
  },
Validate(){
    var self = this;
   
    self.showerror =false;
    self.error  = '';
     self.wraninglist='';
       
    let formData = new FormData();
     // this.conbuttondisable =true;


        
        formData.append('file', this.$refs.confile.files[0]);
        formData.append('month', this.conmonth);
        formData.append('area',  this.areaid);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
        let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'validateconsolidate', formData, config)
        .then(response => { 
           var error = response.data.error;
           self.manufacturelist = response.data.final_manufacture;
           self.approvelist = response.data.final_arr;
        loader.hide()
           if(response.data.warning != ''){
            //alert('d')
            self.wraninglist = response.data.warning;
            response.data.warning.forEach(function(value, index) { self.indexarray.push(value.index); });
            response.data.warning.forEach(function(value, index) { self.warningarray.push(value.warning); });
          }

          if(error == ''){
            self.showerror =false;
            self.error  = '';
            self.disbutton =false;
            if(response.data.warning == ''){
            self.consubmit = 'bluebox';
                 self.disbutton =false;
            }else{
                self.consubmit = 'lightgreytext';
                    self.disbutton =true;
            }


             self.messege = 'Validation successful';
            
           
          }else{
            self.disbutton =true;
            self.showerror =true;
            self.error  = error;
          }
        })
      
     
  },
   
   


  sendrow(){
    var self =this;
    var count = this.$refs.file.files.length;
    //alert(count)
    for( var i = 0; i < this.$refs.file.files.length; i++ ){
        let file = this.$refs.file.files[i];
        console.log(file);
        this.postFormData.append('files[' + i + ']', file);
        }
    
        this.postFormData.append('area', self.areaid);
        this.postFormData.append('month', self.conmonth);
         this.postFormData.append('areaname', self.area);
        const config = {headers: { 'content-type': 'multipart/form-data' }}
         let loader = this.$loading.show({
                loader: 'dots'
            });
        axios.post(url+'/uploadraw', this.postFormData, config)
        .then(response => { 
          loader.hide()
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;

       // alert(response.data.messege);
       if(id){
           self.rawsave =count+' files uploaded';
           //self.uploaddisable =  false;
             if(self.user_role != 1){
                self.checkconsolidateUpload();
               }
           
             
        }
      });
  
  
    },
    save(){
      var self =this;
       let formData = new FormData();
       var values = [];
       var indexval=[];
       var status = (this.user_role == 1) ? 1 : 0;
   

   
         formData.append('file', this.$refs.confile.files[0]);
        formData.append('month', self.conmonth);
        formData.append('area', self.areaid);
        formData.append('remark', JSON.stringify(self.valsArray));
        formData.append('index',  JSON.stringify(self.indexarray));
        formData.append('warning',  JSON.stringify(self.warningarray));
         formData.append('status',  status);
        // formData.append('area', self.areaid);
       
        const config = {headers: { 'content-type': 'multipart/form-data' }}
      this.loading =true;
        axios.post(url+'/uploadconsolidate', formData, config)
        .then(response => { 
            self.loading =false;
          var id = response.data.messege;
          var title = response.data.title;
          var type = response.data.type;

       // alert(response.data.messege);
       if(id){
            swal(title, id, type); 
            if($('#user_role').val() == 1){
            window.location.href = APP_URL+'consolidateUpload';
            }else{
           window.location.href = APP_URL+'consolidateData';
            }
        }
      });
    },
   sendConsolidate(){
          this.$validator.validateAll()
         .then(() => {
    
    if (this.errors.all() =='') {
   $('#datarawModal').modal('show');
    }
})
   },

   getArea:function(){

              var vm = this;
            axios.get(url+'/getarea', {
                  }).then(function (response) {
                  vm.locationlist = response.data;
                  vm.area = response.data[0].id;
                  vm.areachange();
              })
     },
     enable:function(){
  //    alert('szsd')
 // alert(this.upload_enable)
      var er = /^-?[0-9]+$/;
    //  alert(this.upload_enable)
     if(er.test(this.upload_enable)){

        var currentdate = new Date();  //02/02/2020
        var mnth =  this.conmonth;
       
      // alert(mnth);
       var arr = mnth.split('-');
       var monthn = arr[0];
       var yearn = '20'+arr[1];
       var mnth = moment().month(monthn).format("M");
       var duedate = new Date(yearn,mnth,this.upload_enable); //13/02/2020
        var monthDay = new Date(yearn, mnth, currentdate.getDate());
      //alert(currentdate)
       if (monthDay < duedate){
          console.log(monthDay)
           console.log(duedate)
         // alert('s')
         this.uploaddisable = false;
         this.checkrawUpload();

         }else if (monthDay > duedate){
           //alert('b')
           console.log(monthDay)
           console.log(duedate)
          this.uploaddisable = true;
         }
     
    }else{
      // alert(this.upload_enable)
      if(this.upload_enable == 'No'){
       this.uploaddisable = true;
      }else{
       this.uploaddisable = false;
       this.checkrawUpload();
        
       }
     
    }

     },
     lastUpdate(){
      var vm =this;
       axios.get(url+'/getlastdate', {
        params:{
          type:'master'
        }
                  }).then(function (response) {
                    if(response.data[0].maxdate){
                   vm.lastupdate = moment(response.data[0].maxdate).format("DD-MMM-YY");
                  }
                });
     },

    
 
   

  },
 computed:{
  total: function(){
      if(this.approvelist != ''){
       
     
       }
      return 0;


      }

    },
mounted(){
 
  var self = this;
 
this.upload_enable = $('#upload_enable').val();
this.lastUpdate();

var date=new Date();
var year=date.getFullYear(); //get year
var month=date.getMonth()-1; //get month
var role = $('#user_role').val();
this.user_role = role;
 if(role != 1 ){
this.area    = $('#area').val();
this.areaid  = $('#areaid').val();
}
var today = '';
if(role == 1){
   this.getArea();
   //this.area=1;
  
  
}
//alert(this.areaid)
if(role == 4 ){
  var today  = new Date(year, month, '31');
}
//startDate: today
   //  var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
   //  var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
   //  $('#compmonth').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',endDate: monthDay}).on(
   //    'changeDate', () => {
   //     this.compmonth = $('#compmonth').val();
      
   //   }
   //  ).datepicker("update",lastDayWithSlashes);


   // this.compmonth =$('#compmonth').val(); 


   var monthDay = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    var lastDayWithSlashes = moment(monthDay).format("MM-YYYY");
    $('#conmonth').datepicker({autoclose: true,minViewMode: 1,format: 'M-yy',endDate: monthDay}).on(
      'changeDate', () => { this.conmonth = $('#conmonth').val();   if($('#user_role').val()!=1){this.checkconsolidateUpload();this.checkrawUpload(); this.rawsave ='' }}
    ).datepicker("update",lastDayWithSlashes);
   this.conmonth =$('#conmonth').val();
    if($('#user_role').val()!=1){ 
         this.checkconsolidateUpload();

    }

   
   //alert(moment(this.conmonth).format("mm-YYYY"))
 }
 
});

