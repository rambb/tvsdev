@extends('layouts.master')
@section('content')
<div class="page-wrapper container" id="conupload">
<div class="main_grid">

{{ Form::open(array('url' => '/downloadcontemplate', 'method' => 'post','enctype'=>'multipart/form-data')) }}

<div class="row">
<div class="col-lg-12 col-sm-12 pr-0">
	<input type="hidden" id="area" value="<?php echo Session::get('area');?>">
	<input type="hidden" id="areaid" value="<?php echo Session::get('areaid');?>">
	 <input type="hidden" id="upload_enable" value="<?php echo Session::get('upload_enable');?>">
	  <input type="hidden" id="user_role" value="<?php echo Session::get('user_role');?>">
	 <br>


<!-- Nav tabs -->
<ul class="nav nav-tabs  border-0 uploadtab">
  <li class='nav-item'>
    <a class='nav-link active' data-toggle='tab' href='#consdata'>Consolidated Townwise for HO</a>
  </li>
  <li class='nav-item'>
    <a class='nav-link ' data-toggle='tab' href='#rawdata'>Competition raw data for HO</a>
   </li>  
 </ul>

<!-- Tab panes -->
<div class="tab-content">
     <div class='tab-pane container fade in show active' id='consdata'>
     
     <!-- /.Main Heading -->
<div class="row pt-0 "> 
<div class="col-lg-12 col-md-12 col-sm-12 text-right pr-0 ">
    <button class="btn btn-light border  btn-sm">Download Template</button>
</div>
</div>
<br/>
    	<div class="row">
     	<div class="col-1"> Step 1: </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row">
			<div class="col-md-10">
			 <span class="btn  btn-light border btn-file">Upload<input type="file" v-model="confile" ref="confile"  name="confile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" :disabled="uploaddisable" @change="onchange"></span>
			   <span class="pl-3 bluetext" v-show="filename" >@{{filename}} uploaded</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-inline mt-4 mb-4 pr-0"><label>Month: </label>  
			<input type="text"  name="conmonth" id="conmonth"  class="float-left form-control border-0"> </div>
			<div class="col-md-3 form-inline mt-4 mb-4"> <label>Area: @{{area}}</label></div>
         </div> 

		</div>
		</div><br/>
		<div class="row" >
     	<div class="col-1"> Step 2: </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row form-group">
			<div class="col-md-2">
			<button class="btn  btn-light border bluebox" type="button" @click="Validate()" :disabled="validatebutton">Validate</button>
			</div>
		</div>
		<p v-if="messege"><span  class="bluetext font-weight-bold">@{{messege}}</span><span v-if="wraninglist">, but with warnings. Please proceed to Step 3</span>
			<span v-else>. Please proceed to Step 3</span>
		</p>
		<div class="col-md-10" v-show="showerror">
		<p v-show="showerror" style="font-weight: bold">Found the below errors. Please correct the errors and repeat Step 1.</p>
			<div style="height: auto;overflow: auto;max-height: 330px;"> 
		   <div v-for="(errors, key) in error " >
		  	 <span><i class='fa fa-remove text-danger'></i> @{{errors.error}}</span><br/>
		  </div>
			</div>
			</div>
		<div class="row form-group">
			<div class="col-md-12 message">
			
			</div>
		</div>
		</div>
		</div><br/>
		    	<div class="row" >
     	<div class="col-1"> Step 3: </div>
			<div class="col-11 pb-5"> 
		<div>
		  <p>Please add in remarks for the below Validation warnings before submitting</p><br/>
		  <div style="height: auto;overflow: auto;max-height: 330px;overflow-x: hidden;"> 
		  <div class="row  form-group" v-for="(warnings, key) in wraninglist ">
		  	<div class="col-5"> <i class='fa fa-exclamation text-warning'></i>&nbsp;&nbsp;@{{warnings.warning}}</div>
		  	<div class="col-5"><input type="text" placeholder="Add remark here" v-model="valsArray[key]"  class=" form-control"> </div>
		  </div>
		</div>
		  	<br/>	
			<button class="btn  btn-light border bluebox" type="button" @click="sendConsolidate" :disabled="disbutton">Submit</button>
			
		</div>
		</div>
		</div>
	</div>
    <div class='tab-pane container fade' id='rawdata'>
	<br/>
   	<div class="row">
     	<div class="col-1"> Step 1: </div>
		<div class="col-11 border-bottom blueborder">
		<div class="row">
			<div class="col-md-10">
			 <span class="btn  btn-light border btn-file">Upload<input type="file"  ref="file" name="file" multiple="multiple"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @change="onrowchange"  ></span>
			  <span class="pl-3 bluetext" v-show="rawfilename">@{{rawfilename}} </span>

			</div><br/>
			<div class="col-md-3 form-inline mt-4 mb-4"><label>Month:   
			<input type="text"   id="compmonth" v-validate="'required'"   class="float-left form-control  border-0"  > </label></div>
			<div class="col-md-3 form-inline mt-4 mb-4"> <label>Area: @{{area}}</label></div>
         </div> 

		</div>
		</div><br/>
	<div class="row" >
     	<div class="col-1"> Step 2: </div>
		<div class="col-11 pb-5"> 
		<div >
			<button class="btn  btn-light border" type="button" :disabled="buttondisable" @click="sendrow()">Submit</button>
			
		</div>
		</div>
		</div>
	</div>
</div>
</div>
</div>
{{ Form::close() }}
</div>
</div>
<script src="{{VJS}}uploads/uploads.js"></script>  
@endsection