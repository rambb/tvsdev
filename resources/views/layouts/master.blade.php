<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('public/images/FaviconTVS_32.png')}}" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('includes.head')
</head>

<body>
    <div id="wrapper" class="container-fluid">
        <header>
        <div class="row">
        <div class="col-3"></div>
        <div class="col-5 text-center pt-3 pb-3">
        <h1>Townwise Data Automation</h1>
        </div>
        <div class="col-4 pt-3 pb-3">
            <nav class="navbar navbar-expand-lg navbar-light bg-white float-left">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo Session::get('email'); ?> 
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    @if (env('AZURE_TENANT_ID') != '')       
                    <a class="dropdown-item" href="{{url('/logout/azure')}}">Logout</a>
                    @else
                     <a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
                   @endif
                    
                    </div>
                    </li>
                </ul>
                </div>
            </nav>
        
             <img src="{{url('public/images/TVS_Motor_logo.png')}}" height="50" width="150" alt="Logo">
                 
        </div>
        </div>
                <div class="row tvs-line">
       
        </div>
        <div class="container">
        <div class="row">
        <div class="col-12">
            @include('includes.mainmenu')
        </div>
        </div>
        </div>

        </header>
        <main>
            <!-- <div id="preloader" ></div> -->
           <!-- <div class='preloader' id="mainloader">
                <div class='preloader-container'>
                    <div style="margin:10px;overflow: hidden;" class="d-none">
                        <img src="{{url('public/images/TVS-motors-logo.png')}}" alt="tvs" style="width: 75%;height: 50%;">
                    </div>
                    <div class='progress progress-small'>
                        <div class='progress-bar progress-bar-colorful bg-danger' id='bar' role='progressbar' style='width: 0%;'></div>
                    </div>
                </div>
            </div>-->
            @yield('content')
        </main>

    </div>
</body>

</html>