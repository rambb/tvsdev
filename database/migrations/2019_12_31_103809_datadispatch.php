<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datadispatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('datadispatch', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('month');
            $table->string('value')->nullable();
            $table->string('uploadid');
            $table->string('type');
            $table->string('delaerid');
            $table->string('createdby');
            $table->string('modifiedby');
            $table->string('status')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datadispatch', function (Blueprint $table) {
            //
        });
    }
}
