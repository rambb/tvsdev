<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductDt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_dt', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('productid');
            $table->string('townwisemodel');
            $table->string('industry');
            $table->string('category');
            $table->string('segment');
            $table->string('brand');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_dt');
    }
}
