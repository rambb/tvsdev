<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dispatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dispatch', function (Blueprint $table) {
             $table->string('month');
            $table->string('count')->nullable();
            $table->string('uploadid');
            $table->string('type');
            $table->string('dealerid');
            $table->string('createdby');
            $table->string('modifiedby');
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dispatch', function (Blueprint $table) {
            //
        });
    }
}
