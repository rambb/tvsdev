<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class district extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('district', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('district');
            $table->string('zoneid');
            $table->string('stateid');
            $table->string('areaid');
            $table->date('update_at');
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
