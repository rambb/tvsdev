<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatadispatchDt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datadispatch_dt', function (Blueprint $table) {
            $table->bigIncrements('id');
               $table->string('ddid');
            $table->string('value');
            $table->string('productid');
            $table->string('warning')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datadispatch_dt', function (Blueprint $table) {
            //
        });
    }
}
