<?php
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/clear-config', function() {
    Artisan::call('config:cache');
    return "Config is cleared";
});

Route::get('/clear-view', function() {
    Artisan::call('view:clear');
    return "View is cleared";
});

Route::post('/login/azure', '\App\Http\Middleware\AppAzure@azure');
Route::get('/login/azurecallback', '\App\Http\Middleware\AppAzure@azurecallback');
Route::post('/loginemail', 'Auth\LoginController@doLogin');
Route::get('/logout/azure','\RootInc\LaravelAzureMiddleware\Azure@azurelogout');
Route::group(['middleware' => 'preventBackHistory'], function(){

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard')->middleware('auth');
Route::get('/', 'HomeController@index')->name('dashboard')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout','Auth\LoginController@logout');

/*MODULE:ADMINISTRATION ADDED ON:19-12-19 ADDED BY:*/

//MANAGE USERS
Route::get('/manageUsers',function()
 {
   return view('pages.administration._user');
 })->middleware('auth');

//SETTINGS
Route::get('/settings',function()
 {
   return view('pages.administration._settings');
 })->middleware('auth');

//SETTINGS
Route::get('/uploadSettings',function()
 {
   return view('pages.administration._upsettings');
 })->middleware('auth');

//USER LOGS
Route::get('/userLogs',function()
 {
   return view('pages.administration._userlogs');
 })->middleware('auth');
//GET USER ROLE
Route::get('/checkemail','UserController@check_unique');
//GET USER ROLE
Route::get('/getrole','UserController@get_user_role');
//GET user details
Route::get('/getuser','UserController@list_all');
//save user
Route::post('/saveuser','UserController@store');
//GET area
Route::get('/getarea','MasterSettings@listLocation');
//GET user details
Route::get('/getuseredit','UserController@get_user_details');
//delete user
Route::get('/deleteuser','UserController@delete_user');
//save uploadsettings
Route::post('/saveuploadsetting','MasterSettings@saveUploadsetting');
//GET uploadsettings
Route::get('/getuploadsettings','MasterSettings@getUploadsettings');
//save user
Route::get('/savenewpassword','UserController@change_password');
//save location
Route::post('/savelocation','MasterSettings@saveLocation');
//get download
Route::get('/getmasterdownloads','MasterSettings@masterDownloads');
//save product
Route::post('/saveproduct','MasterSettings@saveProduct');
//save dealer
Route::post('/savedealer','MasterSettings@saveDealer');
//get userlogs
Route::get('/getuserlogs','MasterSettings@getActivity');
//get userlogs
Route::post('/downloadlog','MasterSettings@downloadLogs');
//get userlogs
Route::get('/getdispyear','MasterSettings@getDispyear');
/***************************************END OF ADMINSTATRION******************************


/*MODULE:UPLOADS ADDED ON:19-12-19 ADDED BY:*/
//SAP DISPATCH
Route::get('/sapDispatch',function()
 {
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   return view('pages.uploads._sap');
 })->middleware('auth');
//validate sap
Route::post('/validatesapupload','AdminUploadsController@validatesapUploads');
//validate sap
Route::post('/uploadsp','AdminUploadsController@saveSap');
//validate sap
Route::get('/getsapdownload','AdminUploadsController@sapDownload');
//download template
Route::post('/downloadallindia','AdminUploadsController@DownloadAllindiaTemplate');
//REATILS
Route::get('/retails',function()
 {
   return view('pages.uploads._retail');
 })->middleware('auth');
//validate sap
Route::post('/validateretailsupload','AdminUploadsController@validateretailsUploads');
//save retails
Route::post('/uploadretails','AdminUploadsController@saveRetails');
//download template
Route::post('/downloadretails','AdminUploadsController@DownloadRetailTemplate');
//ALL INDIA SIAM
Route::get('/allSiam',function()
 {
   return view('pages.uploads._allsiam');
 })->middleware('auth');
//validate SIAM
Route::post('/validatesiamupload','AdminUploadsController@validatesiamUploads');
//validate SIAM
Route::post('/uploadSIAM','AdminUploadsController@saveSiam');
//PD PLAN
Route::get('/pdPlan',function()
 {
   return view('pages.uploads._pdplan');
 })->middleware('auth');
//validate sap
Route::post('/uploadpd','AdminUploadsController@savePdplan');
//REATILS
Route::get('/consolidateUpload',function()
 { 
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   return view('pages.uploads._adminconupload');
 })->middleware('auth');
Route::get('/rawUpload',function()
 {
   return view('pages.uploads._adminraw');
 })->middleware('auth');
//consolidate
Route::get('/consolidateData',function()
 {
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   return view('pages.uploads._misuserupload');
 })->middleware('auth');
/*
Route::get('/consolidateDataold',function()
 {
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   return view('pages.uploads._consolidateupload');
 })->middleware('auth');
 */
//get admin status
Route::get('/checkuploads','UsersUploadController@checkUpload');
//save raw data
Route::post('/uploadraw','UsersUploadController@saveCompetition');
//get raw data
Route::get('/rawstatusdisplay','UsersUploadController@rowStatus');
//get perticular raw data
Route::get('/monthrawstatus','UsersUploadController@getrowmonthStatus');
//get validation for con data
Route::post('/validateconsolidate','UsersUploadController@ValidateCon');
//get save for con data
Route::post('/uploadconsolidate','UsersUploadController@SaveConsolidate');
//get raw data
Route::get('/consolidateddisplay','UsersUploadController@consolidateStatus');
//get perticular consolidate data
Route::get('/monthconstatus','UsersUploadController@getconsolimonthStatus');
//download template
Route::post('/downloadcontemplate','UsersUploadController@DownloadTemplate');
//download template
Route::get('/deleterawfile','UsersUploadController@DeleteRaw');
//download template
Route::get('/changesessiontown','HomeController@TownSessionchange');
//getupload date
Route::get('/getlastdate','AdminUploadsController@getLastupdate');
//END OF UPLOADS

/*MODULE:reports ADDED ON:19-12-19 ADDED BY:*/
//CONSOLIDATED DATA
Route::get('/status',function()
 {
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   if(Session::get('user_role')=='4' || Session::get('user_role')=='3' ){
   return view('pages.status._consolidatest');
   }else if(Session::get('user_role')=='1' ||  Session::get('user_role')=='2'){
  /// return view('pages.status._adminstatus');
  
    
  
         return view('pages.status._adminstatus');
   }
 })->middleware('auth');
//get admin status
Route::get('/getadstatus','AdminStatusController@getStatusdt');
//get admin all status
Route::get('/getadstatusall','AdminStatusController@getStatusdtAll');
//get admin all status
Route::get('/ondownloadadstatus','AdminStatusController@getUploadsdt');
//get admin all status
Route::get('/userconupload','AdminStatusController@getconsolidateUpload');
//get perticular constatus statewise
Route::get('/getstatecondt','AdminStatusController@getStatestatus');
//get raw all status
Route::get('/userrawupload','AdminStatusController@getrawUpload');
//get raw download
Route::get('/rawzip','AdminStatusController@getrawZip');
//COMPETATION DATA
Route::get('/competitionData',function()
 {
   return view('pages.reports._competitionst');
 })->middleware('auth');
//END OF reports

/*MODULE:TOWNWISE ADDED ON:19-12-19 ADDED BY:*/

//DOWNLOPAD
Route::get('/downloadTownwise',function()
 {
    // $app            = app();
    // $controller1     = $app->make('\App\Http\Controllers\HomeController');
    // $getUsers       = $controller1::index();
   return view('pages.townwisereport._download');
 })->middleware('auth');
//on generate
Route::get('/ongenerate','TownwiseReportController@genrateTownwise');
Route::post('/getgeneratedata','TownwiseReportController@getTownwiseData');
//get generate details
Route::get('/getgeneratedt','TownwiseReportController@getDetails');
//get product list
Route::get('/productlist','TownwiseReportController@getProduct');
//get area list
Route::get('/arealist','TownwiseReportController@getArea');
//get area list
Route::get('/marketlist','TownwiseReportController@getMarket');
//download data
Route::post('/downloaddata','TownwiseReportController@downloadData');
//check generate
Route::get('/checkgenerate','TownwiseReportController@genrateCheck');
Route::post('/getcountdata','TownwiseReportController@getTownwiseDataCount');

//END OF TOWNWISE

/*MODULE:APPROVE  ADDED ON:19-12-19 ADDED BY:*/
//GENERATE
Route::get('/approve',function()
 {
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
   return view('pages._approve');
 })->middleware('auth');
//get admin all status
Route::get('/getapprove','ApproveController@Approve');
Route::post('/downloadapprove','ApproveController@downloadApprove');
//save approve
Route::get('/saveapprove','ApproveController@SaveApprove');
//save approve
Route::get('/checkapprove','ApproveController@CheckAprrove');
//END OF APPROVE

Route::get('approve/{key}/{id}' ,function($key,$id){
    
$user_email =Crypt::decryptString($key);
// now find the user
$user = User::where('email',$user_email)->first();
//print_r($user);exit;
if($user){
    Auth::login($user); // login user automatically
    $app            = app();
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
    $home_controller     = $app->make('\App\Http\Controllers\ApproveController');
    $getUsers       = $home_controller::approveEmail($key,$id,1);
    return redirect('/approve')->with('approved', "Yes");

}else {
      return "User not found!";
}

});
Route::get('reject/{key}/{id}' ,function($key,$id){
    
$user_email =Crypt::decryptString($key);
// now find the user
$user = User::where('email',$user_email)->first();
//print_r($user);exit;
if($user){
    Auth::login($user); // login user automatically
    $app            = app();
    $app            = app();
    $controller1     = $app->make('\App\Http\Controllers\HomeController');
    $getUsers       = $controller1::index();
    $home_controller     = $app->make('\App\Http\Controllers\ApproveController');
    $getUsers       = $home_controller::approveEmail($key,$id,2);
    return redirect('/approve')->with('rejected', "Yes");

}else {
      return "User not found!";
}

});
});