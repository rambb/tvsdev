<?php

namespace App\Exports;
//ini_set('memory_limit', '512M');
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Session;
use App\TownwiseData;
class FinalOutputExport implements  FromCollection, WithMapping,WithHeadings
{

    public function __construct($data)
    {
      $this->data = $data;
      $markettype = $data['mtype'];
      $market = $data['market'];
      $ptype = $data['ptype'];
      $product = $data['product'];
      $from = $data['from'];
      $to = $data['to'];
      $type = $data['type'];
      $user_role = $data['user_role'];
      $area = $data['area'];
        $headings = array('Zone','State','Area','District','Town','Town Class','Manufacturer','Category','Segment','Brand','Month','Year');
        if($type == ''){
          array_push($headings,'Despatch');
          array_push($headings,'Retail');
        }else if($type == 'Despatch'){
        array_push($headings,'Despatch');
        }else if($type == 'Retail'){
         array_push($headings,'Retail');
        }
        $this->headings = $headings;

       
    }
     public function collection()
    {
      $data = $this->data;
   
      /*$markettype = $data['mtype'];
      $market = $data['market'];
      $ptype = $data['ptype'];
      $product = $data['product'];
      $from = $data['from'];
      $to = $data['to'];
      $type = $data['type'];
      $user_role = $data['user_role'];
      $area = $data['area'];
      $market_cond = '';
      $product_cond = '';
         
          $market_cond = '';
          $product_cond = '';
          
         
                 
              if($markettype == 'Town'){
            $market_cond = " and   TOWN_ID in ($market)";
           }else if($markettype == 'Area'){
            $market_cond = "  and AREA_ID in ($market)";
           } else if($markettype == 'District'){
            $market_cond = " and DIST_ID in ($market)";
           }else if($markettype == 'State'){
            $market_cond = " and STATE_ID in ($market)";
           }else if($markettype == 'Zone'){
          $market_cond = " and ZONE_ID in ($market)";
           }
       
           
           if($ptype == 'category'){
           $product_cond = " and PRO_CATG_ID in ($product)";
           }else if($ptype == 'segment'){
            $product_cond = " and SEGMENT_ID in ($product)";
           }else if($ptype == 'brand'){
            $product_cond = " and BRAND_ID in ($product)";
            }else if($ptype=='manufacturer'){
            $product_cond = " and MANF_ID in ($product)";
            }
         $sap = Session::get('sapitemid');
         $ret = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $con = Session::get('consoliitemid');
        
        $month_cond ="MONTH_TAG  between '".$from."' and '".$to."'" ;
       
	$new_year_month_condition ="MONTY_YEAR  between '".$from."' and '".$to."'" ;

        if($market_cond == ''){
          if($user_role != 1){
            if(!empty($area)){
            $market_cond = "and area='".$area."'";
            }
          }
        }
        if($type == ''){
         $type_cond ="Despatch,Retail";
        }else if($type == 'Despatch'){
        $type_cond ="Despatch";
        }else if($type == 'Retail'){
        $type_cond ="Retail";
        }*/
        
        //print_r($market_cond);exit;

     //$sql =  DB::table(DB::raw("exec generatedetails as sub"))->mergeBindings($sub->getQuery());
    //print_r($sql);exit;
        //echo 'SELECT TOP 1000000 *,Retail,concat("Consolidated","SAP Despatch","SIAM") as  Despatch FROM (SELECT  z.zone,s.state,area.area,dis.district, t.town,d.month,dispyear,p.manufacturer,pdt.brand,pr.category,pr.segment,value,type,pr.subsegment,t.towngroup,t.townclass FROM datadispatch d join datadispatch_dt dt on d.id=dt.ddid left join town t on t.id=dt.townid left join district dis on dis.id=t.districtid left join area area on area.id=dis.areaid left join state s on s.id=area.stateid left join zone z on z.id=s.zoneid left join brand pdt on pdt.id=dt.productid left join manufacture p on p.id=pdt.mftid left join product pr on pr.brandid=pdt.id where generate=1 '.$market_cond.$product_cond.$month_cond.') t PIVOT(sum(value) FOR type IN ("Retails","Consolidated","SAP Despatch","SIAM")) AS pivot_table';exit;
      
      /*$sql =  DB::Select('SELECT  TOP 100000 * from Townwise_Data where  '.$new_year_month_condition.$market_cond.$product_cond);     
	
	   $row = collect($sql);

     return $row;*/

     $query = TownwiseData::query(); 

          $from = $data['from']; 
          $to = $data['to'];
          $markettype = $data['mtype'];
          $market = explode(',', $data['market']);          
          $ptype = $data['ptype'];
          $product = explode(',', $data['product']);
          $from = $data['from']; 
          $to = $data['to'];
          $type = $data['type'];
          $user_role = $data['user_role'];
          $area = $data['area'];
          $market_cond = '';
          $product_cond = '';         
          $market_cond = '';
          $product_cond = '';       
          
           
          if($markettype == 'Town'){
            $query->whereIn('TOWN_ID', $market);
          }else if($markettype == 'Area'){
            $query->whereIn('AREA_ID', $market);
          } else if($markettype == 'District'){
            $query->whereIn('DIST_ID', $market);
          }else if($markettype == 'State'){
            $query->whereIn('STATE_ID', $market);
          }else if($markettype == 'Zone'){
            $query->whereIn('ZONE_ID', $market);
          }
                  
          if($ptype == 'category'){
            $query->whereIn('PRO_CATG_ID', $product);
          }else if($ptype == 'segment'){
            $query->whereIn('SEGMENT_ID', $product);
          }else if($ptype == 'brand'){
            $query->whereIn('BRAND_ID', $product);
          }else if($ptype=='manufacturer'){
            $query->whereIn('MANF_ID', $product);
          }        
       
          if($market_cond == ''){
            if($user_role != 1){
              if(!empty($area)){
                $query->where('AREA', "'".$area."'");
              }
            }
          }

         /* if($type == ''){
            $type_cond ="Despatch,Retail";
          }else if($type == 'Despatch'){
            $type_cond ="Despatch";
          }else if($type == 'Retail'){
            $type_cond ="Retail";
          }*/

          $query->whereBetween('MONTY_YEAR', [$from, $to]);
          $query->select('ZONE','STATE','AREA','DIST','TOWN','TOWN_CLASS','MANF','PROD_CATG','SEGMENT','BRAND','MONTH_TAG','MONTH_TXT','DISPATCH_QTY','RETAIL_QTY');
          $query->take(999999);
          $final_query = $query->cursor();

          //$final_query = $query->count();         
          //echo $test->toSql();
        
         foreach($final_query as $towndata) {
            yield $towndata;
          }
  
    }
public function headings(): array
    {
     // print_r( $this->headings);exit;
        return  $this->headings;
    }
    public function map($farm): array
   {
        return [
            $farm->ZONE,
            $farm->STATE,
            $farm->AREA,
            $farm->DIST,
            $farm->TOWN,
            $farm->TOWN_CLASS,
            $farm->MANF,
            $farm->PROD_CATG,
            $farm->SEGMENT,
            $farm->BRAND,
	    $farm->MONTH_TAG,
            $farm->MONTH_TXT,
            $farm->DISPATCH_QTY,
            $farm->RETAIL_QTY,
         
 
        ];
    }
   
}