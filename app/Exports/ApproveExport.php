<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;

class ApproveExport implements  FromCollection,  WithHeadings
{

    public function __construct($headings,$data)
    {
        $this->headings = $headings;
        $this->data = $data;
        $this->array = [];
    }
     public function collection()
    {
      $headings = $this->headings;
      $data = $this->data;
      $details = [];
      foreach ($data as $key => $value) {
        //print_r($value);exit;
         foreach ($headings as $vkey => $vvalue) {
            if($vkey==0){
            $details[$key]["town"] = $value['town'];
             }
             if($vkey==1){
            $details[$key]['district'] = $value['district'];
             }
             if($vkey > 1){
            
               // $var  = ($value['manufacturer']== $vvalue ) ? $value['sum'] : '0';
                $details[$key][$vvalue] =    $value[$vvalue];
          
             
         }
            
         }
      }
     //print_r($details);exit;
     
     $row = collect($details);
    // /print_r($row);exit;        
     return $row;
    }
public function headings(): array
    {
        return  $this->headings;
    }
   
}
