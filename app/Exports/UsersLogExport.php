<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersLogExport implements  FromCollection, WithMapping, WithHeadings
{

    public function __construct($from,$to)
    {
        $this->from = $from;
        $this->to = $to;
        $this->headings =  '';
    }
     public function collection()
    {
      $from = $this->from;
      $to = $this->to;
    	 $sql = "SELECT u.*,CONVERT(VARCHAR(9),a.createdat,106) as DatePart,
    ctime as TimePart,a.*,r.role FROM activity a left join users u on u.id=a.createdby LEFT JOIN userrole r on r.id=u.role order by createdat,ctime desc";
     $deliveries = DB::select($sql);
     $row = collect($deliveries);
     return $row;
    }
public function headings(): array
    {
        return [
            'Date',
            'Time',
            'Username',
            'User Role',
           
                'Action'
        ];
    }
    public function map($farm): array
   {
        return [
            $farm->DatePart,
            $farm->TimePart,
            $farm->name,
            $farm->role,
           
            $farm->note,
 
        ];
    }
}
