<?php
namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Maatwebsite\Excel\Concerns\WithMapping;

class TemplatePerBrandSheet implements FromCollection, WithTitle,WithHeadings
{
    private $model;
    private $townlist;
    private $brands;

    public function __construct($townlist,$model,$brands)
    {
        $this->townlist = $townlist;
        $this->model  = $model;
        $this->brands  = $brands;
    }

    /**
     * @return Builder
     */
    public function collection()
    {
       // print_r($this->townlist);exit;
        
        $townlist = array_column($this->townlist, 'town');
        $towngrouplist = array_column($this->townlist, 'district');
        $final = array();
        foreach ($townlist as $key => $value) {
            $final[$key]["town"] = $value;
            $final[$key]["towngroup"] = $towngrouplist[$key];
        }
        $row = collect($final);
    // print_r($row);exit;        
        return $row;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        // /print_r($this->model);exit;
        return $this->model;
    }

    public function headings(): array
    {
        $head = array("Town",'Town Group');
        $headings = array_column($this->brands, 'brand');
        foreach ($headings as $key => $value) {
          array_push($head,$value);
        }
        
       // print_r($head);exit;
        return  $head;
    }
}
?>