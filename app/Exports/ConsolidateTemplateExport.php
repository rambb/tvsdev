<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\TemplatePerBrandSheet;
class ConsolidateTemplateExport implements   WithMultipleSheets
{

    use Exportable;

    protected $area;
    
    public function __construct($area,$townlist)
    {
        $this->area = $area;
        $this->townlist = $townlist;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
          $sheets = [];
           $where = " manufacturer !='TVS' ";
          // echo "SELECT id as mftid, manufacturer FROM manufacture WHERE  $where and status=1 order by display_seq ASC";exit;
          $row = DB::SELECT("SELECT id as mftid, manufacturer FROM manufacture WHERE  $where and status=1 order by display_seq ASC");
          $models =  json_decode( json_encode($row), true);
        


//print_r( $models);exit;
        foreach ($models as $key => $value) {
         // print_r($value);exit;
            $where = "manufacturer='".$value['mftid']."'";
            //print_r($where);exit;
            $brands =  DB::SELECT("SELECT d.brand from brand d  join manufacture dt on dt.id=d.mftid  where mftid='".$value['mftid']."' and d.status=1 and dt.status=1 group by d.brand ");
            $brands =  json_decode( json_encode($brands), true);
           // print_r($brands);exit;
            $sheets[] = new TemplatePerBrandSheet($this->townlist, $value['manufacturer'],$brands);
        }

        return $sheets;
    }
   
}
