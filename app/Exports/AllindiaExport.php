<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;

class AllindiaExport implements  FromCollection,  WithHeadings
{

    public function __construct($from)
    {
      $this->from = $from;
      $header = array('0' =>'Manufacturer','1'=>'Models','2'=>'Quantity');
      $retarray = array();
      $this->headings =  $header;
      //print_r(expression)
       
    }
     public function collection()
    {
      $from = $this->from;
     // $where = "m.manufacturer != 'TVS'";
      $sql = "SELECT m.manufacturer,dt.brand FROM brand dt join manufacture m on m.id=dt.mftid join product p on p.brandid=dt.id WHERE m.status=1 and dt.status=1 and p.status=1 group by m.manufacturer,dt.brand";
       $deliveries = DB::select($sql);
       $row = collect($deliveries);
    
      return $row;
    }
public function headings(): array
    {
     // print_r($this->headings);exit;
        return $this->headings;
    }
     public function map($farm): array
   {
        return [
            $farm->manufacturer,
            $farm->brand,
                   ];
    }
}
