  <?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class Product extends Model
    {
         public $fillable = ['id','productid','townwisemodel','industry','category','segment','brand'];
    }
    ?>