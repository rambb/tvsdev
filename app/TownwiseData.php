<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TownwiseData extends Model
{
     protected $table = 'Townwise_Data';
}
