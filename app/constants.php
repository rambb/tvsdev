<?php

 $base = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
 if (isset($_SERVER['HTTP_HOST'])) {
 $base .= '://'.$_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
 }
 

define('PREFIX', $base);


//print_r($base);exit;
define ('DB_PREFIX','db_');
define('IMG_PATH', '_images/');
// dd($_SERVER);
//Design Source File Paths
define('IMAGES', PREFIX.'public/images/');
define('CSS', PREFIX.'public/css/');
define('JS', PREFIX.'public/js/');
define('LIB', PREFIX.'public/lib/');
define('NODE', PREFIX.'node_modules/');
define('VJS', PREFIX.'resources/views/pages/');
define('VIEWS', PREFIX.'resources/views/');
