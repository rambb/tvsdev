<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class dealers extends Model
    {
         public $fillable = ['id','dealername','createdby','modifiedby','status','created_at','updated_at','dealercode','dealertown','towngroup','townid'];
    }
    ?>