<?php

namespace App\Http\Middleware;

use RootInc\LaravelAzureMiddleware\Azure as Azure;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

use Auth;

use App\User;

class AppAzure extends Azure
{

    protected function success($request, $access_token, $refresh_token, $profile)
    {

        $graph = new Graph();
        $graph->setAccessToken($access_token);
        
        $graph_user = $graph->createRequest("GET", "/me")
                      ->setReturnType(Model\User::class)
                      ->execute();
       
        $email = strtolower($graph_user->getUserPrincipalName());
        $email = 'tvsadmin@gmail.com';
        $user = User::where('email', $email)->first();
       /* $user = User::updateOrCreate(['email' => $email], [
            'name' => $graph_user->getGivenName()
        ]);*/

       if(!empty($user)){
        Auth::login($user, true);
          //  print_r(Auth::User());exit();
        //return redirect('/sapDispatch');exit;
            $category  = "Login";
            $note   = 'Login';
            $activity        =   app('App\Http\Controllers\HomeController')->saveActivity($category,'',$note,Auth::user()->id);
            if((Auth::user()->role)=='1'){
            return redirect('/sapDispatch');
            }else if((Auth::user()->role)=='2'){
            return redirect('/status');
            }if((Auth::user()->role)=='3'){
            return redirect('/approve');
            }if((Auth::user()->role)=='4'){
            return redirect('/consolidateData');
            }
        }
        else{
            return redirect('/login');
        }


       // return parent::success($request, $access_token, $refresh_token, $profile);
    }

    //  public function getLogoutUrl()
    // {
    //     return $this->baseUrl . "common" . $this->route . "logout";
    // }
    //  public function azurelogout(Request $request)
    // {
    //     $request->session()->pull('_rootinc_azure_access_token');
    //     $request->session()->pull('_rootinc_azure_refresh_token');

    //     return redirect()->away($this->getLogoutUrl());
    // }
}