<?php

namespace App\Http\Middleware;

use RootInc\LaravelAzureMiddleware\Azure as Azure;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

use Auth;

use App\User;

class AppAzure extends Azure
{

     protected function success($request, $access_token, $refresh_token, $profile)
    {


        $graph = new Graph();
        $graph->setAccessToken($access_token);
        
        $graph_user = $graph->createRequest("GET", "/me")
                      ->setReturnType(Model\User::class)
                      ->execute();
       error_log(print_r($graph_user,true));
        $email = $graph_user->getmail();
      // print_r($graph_user->getdisplayName());exit;
       // $email = 'tvsadmin@gmail.com';
        $user = User::where('email', $email)->first();
       

       if(!empty($user)){
            Auth::login($user, true);
          //   $user = User::update(['email' => $email], [
          //   'name' => $graph_user->getGivenName()
          // ]);

            $uesr = User::where('email',$email)->update(['name' => $graph_user->getdisplayName()]);
          //  print_r(Auth::User());exit();
        //return redirect('/sapDispatch');exit;
            $category  = "Login";
            $note   = 'Login';
            $activity        =   app('App\Http\Controllers\HomeController')->saveActivity($category,'',$note,Auth::user()->id);
            if((Auth::user()->role)=='1'){
            return redirect('/sapDispatch');
            }else if((Auth::user()->role)=='2'){
            return redirect('/status');
            }if((Auth::user()->role)=='3'){
            return redirect('/approve');
            }if((Auth::user()->role)=='4'){
            return redirect('/consolidateData');
            }
        }
        else{

            return redirect('/login')->with('message', 'Sorry!! You dont have access to application');
        }


       // return parent::success($request, $access_token, $refresh_token, $profile);
    }

    //  public function getLogoutUrl()
    // {
    //     return $this->baseUrl . "common" . $this->route . "logout";
    // }
    //  public function azurelogout(Request $request)
    // {
    //     $request->session()->pull('_rootinc_azure_access_token');
    //     $request->session()->pull('_rootinc_azure_refresh_token');

    //     return redirect()->away($this->getLogoutUrl());
    // }
}