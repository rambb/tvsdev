<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Exports\UsersLogExport;

class MasterSettings extends Controller
{
  
    /*==========================================================
    Function: listArea
    Description: To get area
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
        public static function listLocation(Request $request){

          $list = DB::table('area')->where('status','=',1)->orderBy('area', 'asc')->get();
          return $list;
            
          } 

    /*==========================================================
    Function: saveUploadsetting
    Description: To save upload settings
    Author: 
    Created Date: 23-12-2019
    Modification: 
    ==========================================================*/
        public static function saveUploadsetting(Request $request){
               $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $fupload  = $request->get('fupload');
             $allowprev  = $request->get('allowprev');
             $value = ($fupload == 'date') ?  $date : $fupload;
             $vthreshold  = $request->get('vthreshold');
             $mthreshold  = $request->get('mthreshold');
             $check = self::getUploadsettings($request);
//print_r( $value);exit;
             if(!empty($check['upload_enable'])){

                   $row  = DB::table('masters')
                        ->where('metakey','upload_enable')
                        ->update([
                          "metavalue" =>$value,                           
                              ]);
             }else{

                $values1 = array(
                        'metakey'    =>  'upload_enable',
                        'metavalue'  =>  $value,
                        'default'=>'Yes',
                           );
                 $val1 = masters::insert($values1);

             }
              $category  = "Changed MIS upload due date";
              $homecontroller   =   new HomeController;
              $note   = 'Changed MIS upload due date';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              Session::put('upload_enable', $value);

                if(!empty($check['upload_enable'])){
                    $row  = DB::table('masters')
                        ->where('metakey','upload_prev')
                        ->update([
                          "metavalue" =>$allowprev,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'upload_prev',
                            'metavalue'  => $allowprev,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('allow_prev',$allowprev);
              $category  = "Enabled / Disabled past data upload";
              $homecontroller   =   new HomeController;
              $note   = 'Enabled / Disabled past data uploadeded';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);



              if(!empty($check['variance_threshold'])){
                    $row  = DB::table('masters')
                        ->where('metakey','variance_threshold')
                        ->update([
                          "metavalue" =>$vthreshold,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'variance_threshold',
                            'metavalue'  => $vthreshold,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('v_threshold',$vthreshold);
              $category  = "Variance Threshold data upload";
              $homecontroller   =   new HomeController;
              $note   = 'Variance Threshold data upload';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);

              if(!empty($check['minimum_threshold'])){
                    $row  = DB::table('masters')
                        ->where('metakey','minimum_threshold')
                        ->update([
                          "metavalue" =>$mthreshold,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'minimum_threshold',
                            'metavalue'  => $mthreshold,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('m_threshold',$mthreshold);
              $category  = "Minimum Threshold data upload";
              $homecontroller   =   new HomeController;
              $note   = "Minimum Threshold data upload";
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);


             // print_r(   $activity);exit;
              return array('messege'=>'Settings Updated Successfully');

          } 
    /*==========================================================
    Function: getUploadsettings
    Description: To get uploadsettings
    Author: 
    Created Date: 23-12-2019
    Modification: 
    ==========================================================*/
        public static function getUploadsettings(Request $request){
          $deldate =$prodate=$date='';
          $up = DB::table('masters')->where('metakey','=','upload_enable')->get();
          $prev = DB::table('masters')->where('metakey','=','upload_prev')->get();
          $minimum = DB::table('masters')->where('metakey','=','minimum_threshold')->get();
          $variance  = DB::table('masters')->where('metakey','=','variance_threshold')->get();
          $row1        = json_decode( json_encode($up), true);
          $row2        = json_decode( json_encode($prev), true);
          $row3        = json_decode( json_encode($minimum), true);
          $row4        = json_decode( json_encode($variance), true);
          $sql = DB::table('uploads')->where('status','=',1)->where('category','=','location')->distinct()->get();
          $row = json_decode( json_encode($sql), true);
          if(!empty($row)){
          $date =  date("d-M-y",strtotime($row[0]['updated_at']));
           }

          $sql = DB::table('uploads')->where('status','=',1)->where('category','=','product')->distinct()->get();
          $row = json_decode( json_encode($sql), true);
          if(!empty($row[0])){
          $prodate =  date("d-M-y",strtotime($row[0]['updated_at']));
          }

          $sql = DB::table('uploads')->where('status','=',1)->where('category','=','dealer')->distinct()->get();
          $row = json_decode( json_encode($sql), true);
          if(!empty($row)){
          $deldate =  date("d-M-y",strtotime($row[0]['updated_at']));
          }
          return array('upload_enable'=>empty($row1[0]) ? '' : $row1[0] ,'upload_prev'=>  empty($row2[0]) ? '' : $row2[0],'locupdate'=>$date,'proupdate'=>$prodate,'delupdate'=>$deldate,'minimum_threshold'=>empty($row3[0]) ? '' : $row3[0],'variance_threshold'=>empty($row4[0]) ? '' : $row4[0]);
            
          } 
    /*==========================================================
    Function: getValues
    Description: To get values
    Author: 
    Created Date: 27-12-2019
    Modification: 

    ==========================================================*/
       public static function getValues($where,$table){
       // print_r("select * from $table where $where");exit;
        $sql = DB::select("select * from $table where $where and status=1");
        $row        = json_decode( json_encode($sql), true);
        if(!empty($row)){
        return $row[0];
      
      }


       }
  
          public static function saveLocation(Request $request){
           $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               

               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('location',$headings[0][0]);
             $data_arr = $zoneid_arr=$stateid_arr=$areaid_arr=$disid_arr=$townid_arr=array();

          // print_r(  $validate['success']);exit;
          if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $values = $dataarray[0];
          
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');
            foreach ($values as $key => $value) {
             $Town =  $value['Town'];
             $towncond =   str_replace("'", "''", $Town);
             $District =  $value['District'];
             $Districtcond =   str_replace("'", "''", $District);
             $Area =  $value['Area'];
             $State =  $value['State'];
             $Zone =  $value['Zone'];
            
            
             $TownClass =  $value['Town Class'];
            // $data_arr[$Zone] = array($State=>array($Area=>array($District=>array($Town))));
              $data_arr[$Zone][$State][$Area][$District][$Area] =array("town"=>$Town,"townclass"=> $TownClass);


            }
          /* echo '<pre>';
            print_r($data_arr);
            echo '</pre>';exit;*/
            
            foreach ($data_arr as $zonekey => $state) {
              //check zone
             $whereZone  = "zone='".$zonekey."'";
             $zonetable  = "zone"; 
             $zonedt = self::getValues($whereZone,$zonetable);
              if(empty($zonedt)){
                  $zoneid = DB::table('zone')->insertGetId(['zone' => $zonekey,'update_at'=>$cdate,'status'=>1]);
                    array_push($zoneid_arr,$zoneid);
                   foreach ($state as $statekey => $area) {
                  //save state
                  $stateid = DB::table('state')->insertGetId(['zoneid' => $zoneid,'state'=> $statekey,'update_at'=>$cdate,'status'=>1]);
                       array_push($stateid_arr,$stateid);
                      foreach ($area as $areakey => $district) {
                        //save area
                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                         array_push($areaid_arr,$areaid);
                        foreach ($district as $distkey => $town) {
                  
                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                          array_push($disid_arr,$districtid);
                           foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                        array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }
                      # code...
                    }
                  
               }else{

                 $zoneid = $zonedt['id'];
                 array_push($zoneid_arr,$zoneid);
              foreach ($state as $statekey => $area) {
               
                $whereState  = "zoneid='".$zoneid."' and state='".$statekey."'";
                $staetable  = "state"; 
                $statedt = self::getValues($whereState,$staetable);
                if(empty($statedt)){

                     $stateid = DB::table('state')->insertGetId(['zoneid' => $zoneid,'state'=> $statekey,'update_at'=>$cdate,'status'=>1]);
                       array_push($stateid_arr,$stateid);
                      foreach ($area as $areakey => $district) {
                        //save area
                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                        array_push($areaid_arr,$areaid);
                        foreach ($district as $distkey => $town) {

                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                           array_push($disid_arr,$districtid);
                           foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                         array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }

                }else{
                $stateid = $statedt['id'];
                array_push($stateid_arr,$stateid);

                foreach ($area as $areakey => $district) {
                  $whereArea  = "zoneid='".$zoneid."' and stateid='".$stateid."' and area='".$areakey."'";
                    $areatable  = "area"; 
                    $areadt = self::getValues($whereArea,$areatable);
                       if(empty($areadt)){
                        //save area
                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                         array_push($areaid_arr,$areaid);
                        foreach ($district as $distkey => $town) {

                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                           array_push($disid_arr,$districtid);
                            foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                       array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }else{
                          $areaid = $areadt['id'];
                          array_push($areaid_arr,$areaid);
//print_r($district);exit;
                        foreach ($district as $distkey => $town) {
                          //count( $town);exit;
                             $Districtcond =   str_replace("'", "''", $distkey);
                             $whereDist  = "zoneid='".$zoneid."' and stateid='".$stateid."' and areaid='".$areaid."' and district='".$Districtcond."'";
                             $disttable  = "district"; 
                             $distdt = self::getValues($whereDist,$disttable);
                             if(empty($distdt)){

                                $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                                array_push($disid_arr,$districtid);
                                foreach($town as $key=>$value){

                                $town      = $value['town'];
                                $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                                $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                                array_push($townid_arr,$townid);
                              }
                                 }else{

                                $districtid =  $distdt['id'];
                                array_push($disid_arr,$districtid);
                                 

                                foreach($town as $key=>$value){

                                $town      = $value['town'];
                                $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                                $towncond =   str_replace("'", "''", $town);
                               // $towngrpcond =   str_replace("'", "''", $towngroup);
                                $whereTown  = "zoneid='".$zoneid."' and stateid='".$stateid."' and areaid='".$areaid."' and districtid='".$districtid."' and town='".$towncond."'  and townclass='".$townclass."'";
                                   $towntable  = "town"; 

                                   $towndt = self::getValues($whereTown,$towntable);
                                  //
                                  // print_r( $towndt);exit;
                                   if(empty($towndt)){
                                      $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                                    array_push($townid_arr,$townid);
                                    }else{
                                       $tid = $towndt['id'];
                                       array_push($townid_arr,$tid);
                                       

                                    }
                              }
                                

                        }
                      }


                      }
                    }
                 }
               
              }
            }//zone not empty
              # code...
            }
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'location/'.$filename;
            Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('location/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));

           $sql = DB::table('uploads')->where('status','=',1)->where('category','=','location')->distinct()->get();
           $row = json_decode( json_encode($sql), true);
           if(!empty($row)){
           $uploadid = $row[0]['id'];
           }
           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> 'location','link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                 /// DB::table('datadispatch')->where('id',$dispid)->update(['updated_at'=>$time]);
           }
          // $actv_zoneid = implode(',', $zoneid_arr);
        //  print_r(  array($zoneid_arr));exit;
            //zone make inactive
          
           
               DB::table('zone')
               ->whereNotIn('id',$zoneid_arr)
               ->update([
                'status'    =>  2,
                    ]);
        // echo 'dsd';exit;
          // $actv_townid = implode(',', $townid_arr);
           
               DB::table('town')
              ->whereNotIn('id',$townid_arr)
               ->update([
                'status'    =>  2,
                    ]);
             
                //district amke inactive
               //$actv_disid = implode(',', $disid_arr);
             
               DB::table('district')
               ->whereNotIn('id',$disid_arr)
               ->update([
                'status'    =>  2,
                    ]);
            

               //area amke inactive
            //  $actv_areaid = implode(',', $areaid_arr);
               DB::table('area')
              ->whereNotIn('id',$areaid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
               //state amke inactive
                // $actv_stateid = implode(',', $stateid_arr);
               DB::table('state')
               ->whereNotIn('id',$stateid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              $category  = "Locations Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Locations Master Updated';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
            
              return array('title'=>'Success!','messege'=>'Location Updated Successfully','type'=>'success','load'=>'settings');

            }else{
            //  print_r($validate);exit;
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Format is wrong','type'=>'warning');
            }

           
          }
           /*==========================================================
    Function: masterDownloads
    Description: To get Uploads
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
    public function masterDownloads(Request $request){
       $type  = $request->get('type');
       $storage_path = Storage::disk('local')->getAdapter()->getPathPrefix();;
      // print_r( $storage_path);exit;
      if($type=='location'){
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=','location')->distinct()->get();
    
      }else if($type=='product'){
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=','product')->distinct()->get();
         
      }else if($type=='dealer'){
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=','dealer')->distinct()->get();
         
      }
      $link ='';
       $row = json_decode( json_encode($sql), true);
        if(!empty( $row)){
        $link = $row[0]['link'];
        }
    return $link; 
      }
     /*==========================================================
    Function: saveProduct
    Description: To upload  product
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
        public static function saveProduct(Request $request){
              $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('product',$headings[0][0]);
         // print_r( $validate   );exit;
            if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $values = $dataarray[0];
         //print_r(  $values);exit;
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');
           $prid_arr = array();
           $pridtd_arr = array();
            foreach ($values as $key => $value) {
              if(!empty($value['Townwise Models'])){
             $townwise_models =  $value['Townwise Models'];
             $industry =  $value['Industry'];
             $category =  $value['Category'];
             $segment =  $value['Segment'];
             $manufactuer =  $value['Manufacturer'];
             $brand =  $value['Brand'];
             $subsegment =  $value['Sub Segment'];
             $status_type =  $value['Active_Inactive'];
              $status = (strtolower($status_type) == 'active') ? 1 : 2;
             //get zone id
             $wheremanufacture  = "manufacturer='".$manufactuer."'";
             $product  = "product"; 
             $manudt = self::getValues($wheremanufacture,$product);
             if(empty($manudt)){
             // print_r($value);exit;
              //if manufacture id is empty
              $productid = DB::table('product')->insertGetId(['manufacturer' => $manufactuer,'updated_at'=>$cdate,'status'=>1,'createdby'=>$uid,'created_at'=>$cdate,'modifiedby'=>$uid]);
              array_push($prid_arr,$productid);
              $dt = DB::table('product_dt')->insertGetId(['productid' => $productid,'townwisemodel'=>$townwise_models,'industry'=>$industry,'category' => $category,'segment'=>$segment,'subsegment'=>$subsegment,'brand'=>$brand,'status'=>$status,'updated_at'=>$cdate]);
              array_push($pridtd_arr,$dt);
             }else{
             $pid = $manudt['id'];
              array_push($prid_arr,$pid);
             
              
             $whereprd  = "productid='".$pid."' and townwisemodel='".$townwise_models."' and industry='".$industry."' and category='".$category."' and segment='".$segment."' and brand='".$brand."' and subsegment='".$subsegment."' ";
             $productDT  = "product_dt"; 
             $manudt = self::getValues($whereprd,$productDT);
             if(empty($manudt)){
              $dt = DB::table('product_dt')->insertGetId(['productid' => $pid,'townwisemodel'=>$townwise_models,'industry'=>$industry,'category' => $category,'segment'=>$segment,'subsegment'=>$subsegment,'brand'=>$brand,'status'=>$status,'updated_at'=>$cdate]);
              array_push($pridtd_arr,$dt);
              }else{
               array_push($pridtd_arr,$manudt['id']);
             }

             }
              }
             }
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'product/'.$filename;
           Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('product/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));

           $sql = DB::table('uploads')->where('status','=',1)->where('category','=','product')->distinct()->get();
           $row = json_decode( json_encode($sql), true);
           if(!empty($row)){
            $uploadid = $row[0]['id'];
            }

           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> 'product','link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                }
              $category  = "Product Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Product Master Updated';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              //zone make inactive
              DB::table('product')
               ->whereNotIn('id',$prid_arr)
               ->update([
                'status'    =>  2,
                    ]);
             
               //town amke inactive
              //zone make inactive
              DB::table('product_dt')
               ->whereNotIn('id',$pridtd_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
          
              return array('title'=>'Success!','messege'=>'Product Updated Successfully','type'=>'success','load'=>'settings');

            }else{
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Formate is wrong','type'=>'warning');
            }

           
          } 
             /*==========================================================
    Function: saveDealer
    Description: To upload  dealer
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
        public static function saveDealer(Request $request){
              $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('dealer',$headings[0][0]);
             $deaid_arr = array();

           // print_r(  $validate);exit;
         if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
            //print_r(  $dataarray);exit;
           
           $values = $dataarray[0];
       
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');
            foreach ($values as $key => $value) {
             $dcode =  $value['Dealer Code'];
             $dname =  $value['Dealer Name'];
             $dtown =  $value['Dealer Town'];
             $town =  $value['Town'];
             $State =  $value['State'];
             $towncond =   str_replace("'", "''", $town);
            /* $District =  $value['District'];
             $TownClass =  $value['Town Class'];
             $State =  $value['State'];
             $Area =  $value['Area'];
             $Zone =  $value['Zone'];
         
             $towngroup =  $value['Town Group'];
             $towngpcond =   str_replace("'", "''", $towngroup);
             $dtowncond =   str_replace("'", "''", $dtown);
             $namecond =   str_replace("'", "''", $dname);*/
             //get town id
            //  $array  = array("zone"=>$Zone,"state"=>$State,"area"=>$Area,"district"=>$District,"town"=>$towncond,"towngroup"=>$towngpcond,"townclass"=>$TownClass);
             $array  = array("state"=>$State,"town"=>$towncond);
             $towndt = self::getTowndetails($array);
            //print_r( $towndt);exit;
             if(!empty($towndt)){
              $townid=  $towndt[0]['id'];
              //if manufacture id is empty
            //  $wherede  = "dealername='".$namecond."' and townid='".$townid."' and dealercode='".$dcode."' and dealertown='".$dtowncond."' ";
               $wherede  = "townid='".$townid."' and dealercode='".$dcode."' ";
              $deal  = "dealers"; 
              $manudt = self::getValues($wherede,$deal);
              if(empty($manudt)){
               $dealerid = DB::table('dealers')->insertGetId(['dealername' => $dname,'townid'=>$townid,'dealercode'=>$dcode,'dealertown'=>$dtown,'updated_at'=>$cdate,'status'=>1,'createdby'=>$uid,'created_at'=>$cdate,'modifiedby'=>$uid]);
                array_push($deaid_arr,$dealerid);
                }else{
                array_push($deaid_arr,$manudt['id']);
                }
             
               }

             }
           
             
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'dealers/'.$filename;
           Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('dealers/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));

           $sql = DB::table('uploads')->where('status','=',1)->where('category','=','dealer')->distinct()->get();
           $row = json_decode( json_encode($sql), true);
            if(!empty($row)){
            $uploadid = $row[0]['id'];
            }

           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> 'dealer','link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                }
              $category  = "Dealer Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Dealer Master Updated';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              //zone make inactive
               DB::table('dealers')
               ->whereNotIn('id',$deaid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
              
              
          
              return array('title'=>'Success!','messege'=>'Dealers Updated Successfully','type'=>'success','load'=>'settings');

            }else{
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Formate is wrong','type'=>'warning');
            }

           
          } 
           /*==========================================================
    Function: getTowndetails
    Description: To get activity
    Author: 
    Created Date: 09-01-2020
    Modification: 
    ==========================================================*/
        public static function getTowndetails($array){
         
        $zone = (array_key_exists('zone', $array)) ? $array['zone'] : '';
        $state = (array_key_exists('state', $array)) ? $array['state'] : '';
        $area = (array_key_exists('area', $array)) ? $array['area'] : '';
        $district = (array_key_exists('district', $array)) ? $array['district'] : '';
        $town = (array_key_exists('town', $array)) ? $array['town'] : '';
        $townclass = (array_key_exists('townclass', $array)) ? $array['townclass'] : '';
        $towngroup = (array_key_exists('towngroup', $array)) ? $array['towngroup'] : '';


      
        $zonecond = ($zone=='')?' ':" and zone='".$zone."'";
        $statecond = ($state=='')?' ':" and state='".$state."'";
        $districtcond = ($district=='')?' ':" and district='".$district."'";
        $areacond = ($area=='')?' ':" and area ='".$area."'";
        $towncond = ($town=='')?' ':"  town='".$town."'";
        $townclasscond = ($townclass=='')?' ':" and townclass ='".$townclass."'";
        $towngroupcond = ($towngroup=='')?' ':" and towngroup='".$towngroup."'";
     //   print_r($statuscond);exit;
//echo "SELECT * from towndetails  where  ".$towncond.$zonecond.$statecond.$districtcond.$areacond.$townclasscond.$towngroupcond;
        $sql = DB::SELECT("SELECT * from towndetails  where  ".$towncond.$zonecond.$statecond.$districtcond.$areacond.$townclasscond.$towngroupcond);
        $row = json_decode( json_encode($sql), true);
        return $row;

        }
  /*==========================================================
    Function: getActivity
    Description: To get activity
    Author: 
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
        public static function getActivity(Request $request){
          $input = $request->all();
          //  print_r(Session::get('user_id'));exit;
          $uid  = $request->get('user_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
           }
          // print_r($uid);exit;
        $from  = date("Y-m-d",strtotime($request->get('from')));
        $to  = date("Y-m-d",strtotime($request->get('to')));
        $perPage = $request->get('per_page');
        $sort = $request->get('sort');
        $filter = $request->get('filter');
        $page = $request->get('page');
        $download = $request->get('download');
        $sort =  explode('|', $sort);
        $limit = ' ORDER BY '.$sort[0];
        $sql = "SELECT u.*,CONVERT(VARCHAR(9),a.createdat,106) as DatePart,
    CONVERT(VARCHAR(10),a.createdat,108) as TimePart,a.*,r.role FROM activity a left join users u on u.id=a.createdby LEFT JOIN userrole r on r.id=u.role where format(createdat, 'yyyy/MM/dd') between '".$from."' AND '".$to."'";
     $deliveries = DB::select($sql);

   
        
         $row = collect($deliveries);
         $currentPage = $page ?: 1;
         $slice_init = ($currentPage == 1) ? 0 : (($currentPage*$perPage)-$perPage);
         $data = $row->slice($slice_init, $perPage)->all();
         $row = json_decode( json_encode($deliveries  ), true);
          $deliveries = new LengthAwarePaginator($row, count($deliveries), $perPage, $currentPage);
         return $deliveries;
       



          
        }
        /*==========================================================
    Function: downloadLogs
    Description: To get activity
    Author: 
    Created Date: 02-01-2020
    Modification: 
    ==========================================================*/
        public static function downloadLogs(Request $request){
        $from  = date("Y-m-d",strtotime($request->get('from')));
        $to  = date("Y-m-d",strtotime($request->get('to')));
         return Excel::download(new UsersLogExport($from,$to), 'userslog.xlsx');

        }

}
