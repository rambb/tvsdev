<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Session;
class FinalOutputExport implements  FromCollection, WithMapping,WithHeadings
{

    public function __construct($data)
    {
      $this->data = $data;
      $markettype = $data['mtype'];
      $market = $data['market'];
      $ptype = $data['ptype'];
      $product = $data['product'];
      $from = $data['from'];
      $to = $data['to'];
      $type = $data['type'];
      $user_role = $data['user_role'];
      $area = $data['area'];
        $headings = array('Zone','State','Area','District','Town','Town Class','Manufacturer','Category','Segment','Brand','Month');
        if($type == ''){
          array_push($headings,'Despatch');
          array_push($headings,'Retail');
        }else if($type == 'Despatch'){
        array_push($headings,'Despatch');
        }else if($type == 'Retail'){
         array_push($headings,'Retail');
        }
        $this->headings = $headings;

       
    }
     public function collection()
    {
      $data = $this->data;
     // / print_r($data);exit;
      $markettype = $data['mtype'];
      $market = $data['market'];
      $ptype = $data['ptype'];
      $product = $data['product'];
      $from = $data['from'];
      $to = $data['to'];
      $type = $data['type'];
      $user_role = $data['user_role'];
      $area = $data['area'];
      $market_cond = '';
      $product_cond = '';
         
          $market_cond = '';
          $product_cond = '';
          
         
                 
             if($markettype == 'Town'){
            $market_cond = " and   town in ($market)";
           }else if($markettype == 'Area'){
            $market_cond = "  and area.area in ($market)";
           } else if($markettype == 'District'){
            $market_cond = " and district in ($market)";
           }else if($markettype == 'State'){
            $market_cond = " and state in ($market)";
           }else if($markettype == 'Zone'){
          $market_cond = " and zone in ($market)";
           }
       
           
           if($ptype == 'category'){
           $product_cond = " and category in ($product)";
           }else if($ptype == 'segment'){
            $product_cond = " and segment in ($product)";
           }else if($ptype == 'brand'){
            $product_cond = " and brandid in ($product)";
            }else if($ptype=='manufacturer'){
            $product_cond = " and manufacturerid in ($product)";
            }
         $sap = Session::get('sapitemid');
         $ret = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $con = Session::get('consoliitemid');
         //print_r($market_cond);exit;
          //  echo date('m', strtotime($to));exit;
        $month_cond ="convert(date, cast(dispyear*10000 + dispmonth*100 + 1 as varchar(8)), 112)  between '".$from."' and '".$to."'" ;
       
        if($market_cond == ''){
          if($user_role != 1){
            if(!empty($area)){
            $market_cond = "and area='".$area."'";
            }
          }
        }
        if($type == ''){
         $type_cond ="Despatch,Retail";
        }else if($type == 'Despatch'){
        $type_cond ="Despatch";
        }else if($type == 'Retail'){
        $type_cond ="Retail";
        }
        
        //print_r($market_cond);exit;

     //$sql =  DB::table(DB::raw("exec generatedetails as sub"))->mergeBindings($sub->getQuery());
    //print_r($sql);exit;
        //echo 'SELECT TOP 1000000 *,Retail,concat("Consolidated","SAP Despatch","SIAM") as  Despatch FROM (SELECT  z.zone,s.state,area.area,dis.district, t.town,d.month,dispyear,p.manufacturer,pdt.brand,pr.category,pr.segment,value,type,pr.subsegment,t.towngroup,t.townclass FROM datadispatch d join datadispatch_dt dt on d.id=dt.ddid left join town t on t.id=dt.townid left join district dis on dis.id=t.districtid left join area area on area.id=dis.areaid left join state s on s.id=area.stateid left join zone z on z.id=s.zoneid left join brand pdt on pdt.id=dt.productid left join manufacture p on p.id=pdt.mftid left join product pr on pr.brandid=pdt.id where generate=1 '.$market_cond.$product_cond.$month_cond.') t PIVOT(sum(value) FOR type IN ("Retails","Consolidated","SAP Despatch","SIAM")) AS pivot_table';exit;
      $sql =  DB::Select('SELECT TOP 1000000 *,ISNULL(Retails, coalesce(concat("Retails","Consolidated","SAP Despatch","SIAM"),0) ) Retails,coalesce(concat("Retails","Consolidated","SAP Despatch","SIAM"),0) Despatch FROM (SELECT  * from downloaddata where  '.$month_cond.$market_cond.$product_cond.') t PIVOT(sum(value) FOR type IN ("Retails","Consolidated","SAP Despatch","SIAM")) AS pivot_table ORDER BY month,zone,state,district,area,town,manufacturer,category,segment asc');
     $row = collect($sql);
     return $row;
  
    }
public function headings(): array
    {
     // print_r( $this->headings);exit;
        return  $this->headings;
    }
    public function map($farm): array
   {
        return [
            $farm->zone,
            $farm->state,
            $farm->area,
            $farm->district,
            $farm->town,
           // $farm->towngroup,
            $farm->townclass,
            $farm->manufacturer,
            $farm->categoryname,
            $farm->segmentname,
            $farm->brand,
            $farm->month,
            $farm->Despatch,
            $farm->Retails,
         
 
        ];
    }
   
}
