<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Exports\UsersLogExport;
use App\DD_ITEMS;
class MasterSettings extends Controller
{
  
    /*==========================================================
    Function: listArea
    Description: To get area
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
        public static function listLocation(Request $request){

          $list = DB::table('area')->where('status','=',1)->where('area','NOT LIKE','%INDIA%')->orderBy('area', 'asc')->get();
          return $list;
            
          } 

    /*==========================================================
    Function: saveUploadsetting
    Description: To save upload settings
    Author: 
    Created Date: 23-12-2019
    Modification: 
    ==========================================================*/
        public static function saveUploadsetting(Request $request){
               $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $fupload  = $request->get('fupload');
             $allowprev  = $request->get('allowprev');
             $value = ($fupload == 'date') ?  $date : $fupload;
             $vthreshold  = $request->get('vthreshold');
             $mthreshold  = $request->get('mthreshold');
             $check = self::getUploadsettings($request);
//print_r( $value);exit;
             if(!empty($check['upload_enable'])){

                   $row  = DB::table('masters')
                        ->where('metakey','upload_enable')
                        ->update([
                          "metavalue" =>$value,                           
                              ]);
             }else{

                $values1 = array(
                        'metakey'    =>  'upload_enable',
                        'metavalue'  =>  $value,
                        'default'=>'Yes',
                           );
                 $val1 = masters::insert($values1);

             }
              $category  = "Changed MIS upload due date";
              $homecontroller   =   new HomeController;
              $note   = 'Changed MIS upload due date';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              Session::put('upload_enable', $value);

                if(!empty($check['upload_enable'])){
                    $row  = DB::table('masters')
                        ->where('metakey','upload_prev')
                        ->update([
                          "metavalue" =>$allowprev,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'upload_prev',
                            'metavalue'  => $allowprev,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('allow_prev',$allowprev);
              $category  = "Enabled / Disabled past data upload";
              $homecontroller   =   new HomeController;
              $note   = 'Enabled / Disabled past data uploadeded';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);



              if(!empty($check['variance_threshold'])){
                    $row  = DB::table('masters')
                        ->where('metakey','variance_threshold')
                        ->update([
                          "metavalue" =>$vthreshold,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'variance_threshold',
                            'metavalue'  => $vthreshold,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('v_threshold',$vthreshold);
              $category  = "Variance Threshold data upload";
              $homecontroller   =   new HomeController;
              $note   = 'Variance Threshold data upload';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);

              if(!empty($check['minimum_threshold'])){
                    $row  = DB::table('masters')
                        ->where('metakey','minimum_threshold')
                        ->update([
                          "metavalue" =>$mthreshold,                           
                              ]);
                    }else{
                 $values2 = array(
                            'metakey'    =>  'minimum_threshold',
                            'metavalue'  => $mthreshold,
                            'default'=>'Yes',
                               );
                
                  $val2 = masters::insert($values2);
              }
               Session::put('m_threshold',$mthreshold);
              $category  = "Minimum Threshold data upload";
              $homecontroller   =   new HomeController;
              $note   = "Minimum Threshold data upload";
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);


             // print_r(   $activity);exit;
              return array('messege'=>'Settings Updated Successfully');

          } 

          /*==========================================================
    Function: generateTrigger
    Description: To make generate enable or disable
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
      public static function generateTrigger($month,$year){
         $sap = Session::get('sapitemid');
         $ret = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $con = Session::get('consoliitemid');
         $dispatch = DB::SELECT("select count(a.type) as type from (select type from  datadispatch where month=$month and dispyear=$year and type in($sap,$ret,$siam,$con) and status=1 group by type) a");

        $row = json_decode( json_encode($dispatch), true);
        //print_r($row);exit;
        if($row[0]['type'] == 4){

                  $row  = DB::table('masters')
                        ->where('metakey','generate_enable')
                        ->update([
                          "metavalue" =>1,                           
                          ]);
                        Session::put('generate_enable',1);
                       //  echo 's';exit;
        }else{
                   $row  = DB::table('masters')
                        ->where('metakey','generate_enable')
                        ->update([
                          "metavalue" =>0,                           
                          ]);
                        Session::put('generate_enable',0);
                         //echo '455';exit;
        }
       //  echo "select count(a.type) from (select type from  datadispatch where month=$month and dispyear=$year and type in($sap,$ret,$siam,$con) and status=1 group by type) a";exit;
        return;
      }
    /*==========================================================
    Function: getUploadsettings
    Description: To get uploadsettings
    Author: 
    Created Date: 23-12-2019
    Modification: 
    ==========================================================*/
        public static function getUploadsettings(Request $request){
          $deldate =$prodate=$date='';
          $up = DB::table('masters')->where('metakey','=','upload_enable')->get();
          $prev = DB::table('masters')->where('metakey','=','upload_prev')->get();
          $minimum = DB::table('masters')->where('metakey','=','minimum_threshold')->get();
          $variance  = DB::table('masters')->where('metakey','=','variance_threshold')->get();
          $row1        = json_decode( json_encode($up), true);
          $row2        = json_decode( json_encode($prev), true);
          $row3        = json_decode( json_encode($minimum), true);
          $row4        = json_decode( json_encode($variance), true);
          $sql = DB::select("select max(update_at) as updated_at  from zone");
          $row = json_decode( json_encode($sql), true);
          if(!empty($row)){
          $date =  date("d-M-y",strtotime($row[0]['updated_at']));
           }

          $sql = DB::select("select max(updated_at) as updated_at  from product");
          $row = json_decode( json_encode($sql), true);
          if(!empty($row[0])){
          $prodate =  date("d-M-y",strtotime($row[0]['updated_at']));
          }
          $sql = DB::select("select max(updated_at) as updated_at  from dealers");
          
          $row = json_decode( json_encode($sql), true);
          if(!empty($row)){
          $deldate =  date("d-M-y",strtotime($row[0]['updated_at']));
          }
          return array('upload_enable'=>empty($row1[0]) ? '' : $row1[0] ,'upload_prev'=>  empty($row2[0]) ? '' : $row2[0],'locupdate'=>$date,'proupdate'=>$prodate,'delupdate'=>$deldate,'minimum_threshold'=>empty($row3[0]) ? '' : $row3[0],'variance_threshold'=>empty($row4[0]) ? '' : $row4[0]);
            
          } 
    /*==========================================================
    Function: getValues
    Description: To get values
    Author: 
    Created Date: 27-12-2019
    Modification: 

    ==========================================================*/
       public static function getValues($where,$table){
       // print_r("select * from $table where $where");exit;
        //echo "select * from $table where $where and status=1";exit;
        $sql = DB::select("select * from $table where $where");
        $row        = json_decode( json_encode($sql), true);
        if(!empty($row)){
        return $row[0];
      
      }


       }
  
          public static function saveLocation(Request $request){
               $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               

               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('location',$headings[0][0]);
             $data_arr = $zoneid_arr=$stateid_arr=$areaid_arr=$disid_arr=$townid_arr=array();

          // print_r(  $validate['success']);exit;
          if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $values = $dataarray[0];
           $uploadcatid = self::getCategoryid('UPLOAD_TYPE');
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');

            foreach ($values as $key => $value) {
             $Town =  $value['Town'];
             $towncond =   str_replace("'", "'", $Town);
             $District =  $value['District'];
             $Districtcond =   str_replace("'", "''", $District);
             $Area =  $value['Area'];
             $State =  $value['State'];
             $Zone =  $value['Zone'];
            
            
             $TownClass =  $value['Town Class'];
            // $data_arr[$Zone] = array($State=>array($Area=>array($District=>array($Town))));
              $data_arr[$Zone][$State][$Area][$District][] =array("town"=>$Town,"townclass"=> $TownClass);


            }
            $start = date('Y-m-d h:m:i');
           
          /* echo '<pre>';
            print_r($data_arr);
            echo '</pre>';exit;*/
           
            
            foreach ($data_arr as $zonekey => $state) {
             
                   	
              //check zone
             $whereZone  = "zone='".$zonekey."'";
             $zonetable  = "zone"; 
             $zonedt = self::getValues($whereZone,$zonetable);
             $zoneid = $zonedt['id'];
            

            //echo "Zoneid: ".$zoneid; exit;
             //$zonedt = self::getValues($whereZone,$zonetable);

            if(empty($zoneid)){
            	//exit;
                  $zoneid = DB::table('zone')->insertGetId(['zone' => $zonekey,'update_at'=>$cdate,'status'=>1]);
                    array_push($zoneid_arr,$zoneid);
                   foreach ($state as $statekey => $area) {
                  //save state
                  $stateid = DB::table('state')->insertGetId(['zoneid' => $zoneid,'state'=> $statekey,'update_at'=>$cdate,'status'=>1]);
                       array_push($stateid_arr,$stateid);
                      foreach ($area as $areakey => $district) {
                        //save area
                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                         array_push($areaid_arr,$areaid);
                        foreach ($district as $distkey => $town) {
                  
                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                          array_push($disid_arr,$districtid);
                           foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                        array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }
                      # code...
                    }
                  
               }else{

               	array_push($zoneid_arr,$zoneid);
              	foreach ($state as $statekey => $area) {

              	
               	$whereState  = "zoneid='".$zoneid."' and state='".$statekey."'";
                $staetable  = "state"; 
                $statedt = self::getValues($whereState,$staetable);
               	$stateid = $statedt['id'];
               // echo "stateid: ". $stateid; exit;
                //$statedt = self::getValues($whereState,$staetable);
                if(empty($stateid)){

                     $stateid = DB::table('state')->insertGetId(['zoneid' => $zoneid,'state'=> $statekey,'update_at'=>$cdate,'status'=>1]);
                       array_push($stateid_arr,$stateid);
                      foreach ($area as $areakey => $district) {
                        //save area
                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                        array_push($areaid_arr,$areaid);
                        foreach ($district as $distkey => $town) {

                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                           array_push($disid_arr,$districtid);
                           foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                         array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }

                }else{

                //$stateid = $statedt['id'];
                array_push($stateid_arr,$stateid);

                foreach ($area as $areakey => $district) {

                	
                     $whereArea  = "zoneid='".$zoneid."' and stateid='".$stateid."' and area='".$areakey."'";
                    $areatable  = "area"; 
                    $areadt = self::getValues($whereArea,$areatable);
                    $areaid = $areadt['id'];
                	//	print_r($areaid );EXIT;
                    //$areadt = self::getValues($whereArea,$areatable);
                       if(empty($areaid)){
                        //save area

                         $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>$areakey,'update_at'=>$cdate,'status'=>1]);
                         array_push($areaid_arr,$areaid);
                        // print_r($distkey);exit;

                        foreach ($district as $distkey => $town) {
                          
                          $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                           array_push($disid_arr,$districtid);
                            foreach($town as $key=>$value){
                          $town      = $value['town'];
                          $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                        
                          $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                       array_push($townid_arr,$townid);
                        }
                        }
                        # code...
                      }else{

                          //$areaid = $areadt['id'];
                          array_push($areaid_arr,$areaid);
//print_r($district);exit;

                        foreach ($district as $distkey => $town) {
                          //count( $town);exit;
                            // $distkey =   str_replace("'", "''", $distkey);                        	
                        
                            $Districtcond =   str_replace("'", "'+char(39)+'", $distkey );
                             $whereDist  = "zoneid='".$zoneid."' and stateid='".$stateid."' and areaid='".$areaid."' and district='".$Districtcond."'";
                             $disttable  = "district"; 
                             $distdt = self::getValues($whereDist,$disttable);
                             $districtid =  $distdt['id'];
                        
                            // $distdt = self::getValues($whereDist,$disttable);
                            if(empty($districtid)){

                                $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> $distkey,'update_at'=>$cdate,'status'=>1]);
                                array_push($disid_arr,$districtid);
                                foreach($town as $key=>$value){

                                $town      = $value['town'];
                                $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                                $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                                array_push($townid_arr,$townid);
                              	}
                            }else{
   
                               // $districtid =  $distdt['id'];
                                array_push($disid_arr,$districtid);
                                 

                                foreach($town as $key=>$value){

                                $townkey      = $value['town'];
                                $townclass = isset($value['townclass']) ? $value['townclass'] : '' ;
                                //$townkey =   str_replace("'", "'", $townkey);
                                $townkeycond = str_replace("'", "'+char(39)+'", $townkey );

                                
                                   $whereTown  = "zoneid='".$zoneid."' and stateid='".$stateid."' and areaid='".$areaid."' and districtid='".$districtid."' and town='".$townkeycond."'  and townclass='".$townclass."'";
                                   $towntable  = "town"; 

                                   $towndt = self::getValues($whereTown,$towntable);
                                    $tid = $towndt['id'];
                                   //$towndt = self::getValues($whereTown,$towntable);
                                  //
                                  // print_r( $towndt);exit;
                                 //  print_r($tid) ;exit;
                                   if(empty($tid)){
                                     // print_r(array('zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>$town,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass));exit;
                                      $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>   $townkey ,'update_at'=>$cdate,'status'=>1,"townclass"=>$townclass]);
                                    array_push($townid_arr,$townid);
                                    }else{
                                     // echo 'dsd';exit;
                                       //$tid = $towndt['id'];
                                       array_push($townid_arr,$tid);
                                       

                                    }
                              }
                                

                        }
                      }


                      }
                    }
                 }
               
              }
             }//zone not empty

              
            }
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'location/'.$filename;
            Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('location/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));
            $uploadcatid = self::getCategoryid('UPLOAD_TYPE');
            $upcatid = self::getItemid($uploadcatid,'location');
            //print_r($upcatid);exit;
           if(empty($upcatid)){
            $upcatid = self::saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => 'location','DDI_DISP_TEXT'=> 'location','uid'=> $uid));
              }
           $sql = DB::table('uploads')->where('status','=',1)->where('category','=',$upcatid)->get();
           $row = json_decode( json_encode($sql), true);
           if(!empty($row)){
           $uploadid = $row[0]['id'];
           }
           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> $upcatid,'link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                 /// DB::table('datadispatch')->where('id',$dispid)->update(['updated_at'=>$time]);
           }
          // $actv_zoneid = implode(',', $zoneid_arr);
        //  print_r(  array($zoneid_arr));exit;
            //zone make inactive
          
           
               DB::table('zone')
               ->whereNotIn('id',$zoneid_arr)
               ->update([
                'status'    =>  2,
                    ]);

        // echo 'dsd';exit;
          // $actv_townid = implode(',', $townid_arr);
           //print_r($townid_arr);exit;
               DB::table('town')
              ->whereNotIn('id',$townid_arr)
               ->update([
                'status'    =>  2,
                    ]);
             
                //district amke inactive
               //$actv_disid = implode(',', $disid_arr);
             
               DB::table('district')
               ->whereNotIn('id',$disid_arr)
               ->update([
                'status'    =>  2,
                    ]);
            

               //area amke inactive
            //  $actv_areaid = implode(',', $areaid_arr);
               DB::table('area')
              ->whereNotIn('id',$areaid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
               //state amke inactive
                // $actv_stateid = implode(',', $stateid_arr);
               DB::table('state')
               ->whereNotIn('id',$stateid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              $category  = "Locations Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Updated Location Master';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
            
              return array('title'=>'Success!','messege'=>'Location Updated Successfully','type'=>'success','load'=>'settings');

            }else{
            //  print_r($validate);exit;
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Format is wrong','type'=>'warning');
            }

           
          }
           /*==========================================================
    Function: masterDownloads
    Description: To get Uploads
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
    public function masterDownloads(Request $request){
       $type  = $request->get('type');
       $storage_path = Storage::disk('local')->getAdapter()->getPathPrefix();;
         $uploadcatid = self::getCategoryid('UPLOAD_TYPE');
      // print_r( $storage_path);exit;
      if($type=='location'){
         $upcatid = self::getItemid($uploadcatid,'location');
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=',$upcatid)->get();
    
      }else if($type=='product'){
         $upcatid = self::getItemid($uploadcatid,'Product');
       //  print_r( $upcatid);exit;
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=',$upcatid)->get();
         
      }else if($type=='dealer'){
         $upcatid = self::getItemid($uploadcatid,'dealer');
        $sql = DB::table('uploads')->where('status','=',1)->where('category','=', $upcatid)->get();
         
      }
      $link ='';
       $row = json_decode( json_encode($sql), true);
        if(!empty( $row)){
        $link = $row[0]['link'];
        }
    return $link; 
      }
     /*==========================================================
    Function: saveProduct
    Description: To upload  product
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
        public static function saveProduct(Request $request){
              $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('product',$headings[0][0]);
         // print_r( $validate   );exit;
            if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $values = $dataarray[0];
         //print_r(  $values);exit;
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');
           $prid_arr = array();
           $pridtd_arr = array();
           $modeltd_arr = array();
           //get category id 
           $segmentcatid = self::getCategoryid('SEGMENT');
           $subsegmentcatid = self::getCategoryid('SUB_SEGMENT');
           $categorycatid = self::getCategoryid('CATEGORY_PRODUCT');
           $uploadcatid = self::getCategoryid('UPLOAD_TYPE');
          // print_r($uploadcatid);exit;

            foreach ($values as $key => $value) {
              if(!empty($value['Townwise Models'])){
                   $townwise_models =  $value['Townwise Models'];
                   $industry =  $value['Industry'];
                   $categoryname =  $value['Category'];
                   $segmentname =  $value['Segment'];
                   $manufactuer =  $value['Manufacturer'];
                   $brand =  $value['Brand'];
                   $subsegmentname =  $value['Sub Segment'];
                   $status_type =  $value['Active_Inactive'];
                   $status = (strtolower($status_type) == 'active') ? 1 : 2;
                   $segment = self::getItemid($segmentcatid,$segmentname);
                   if(empty($segment)){
                     $string = strtoupper(substr($segmentname,0,8));
                      $segment = DD_ITEMS::insertGetId(['DDI_CATG_ID' => $segmentcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> $segmentname,'status' => 1,'ADD_BY'=> $uid,'ADD_ON' => $cdate,'EDIT_BY'=> $uid,'EDIT_ON'=> $cdate,'DDI_CODE'=>4]);
                   }
                   $subsegment = self::getItemid($subsegmentcatid,$subsegmentname);
                   if(empty($subsegment)){
                      $string = strtoupper(substr($subsegmentname,0,8));
                      $subsegment = DD_ITEMS::insertGetId(['DDI_CATG_ID' => $subsegmentcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> $subsegmentname,'status' => 1,'ADD_BY'=> $uid,'ADD_ON' => $cdate,'EDIT_BY'=> $uid,'EDIT_ON'=> $cdate,'DDI_CODE'=>4]);
                   }
                   $category = self::getItemid($categorycatid,$categoryname);
                   if(empty($category)){
                     $string = strtoupper(substr($categoryname,0,8));
                     $category = DD_ITEMS::insertGetId(['DDI_CATG_ID' => $categorycatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> $categoryname,'status' => 1,'ADD_BY'=> $uid,'ADD_ON' => $cdate,'EDIT_BY'=> $uid,'EDIT_ON'=> $cdate,'DDI_CODE'=>4]);
                   }

             //get zone id
                   

             
              $manudt = DB::table('manufacture')->where('manufacturer','=',$manufactuer)->value('id');
             //print_r($manudt);exit;
             if(empty($manudt)){
             // print_r($value);exit;
              //if manufacture id is empty
                $mftid = DB::table('manufacture')->insertGetId(['manufacturer' => $manufactuer,'updated_at'=>$cdate,'status'=>1,'createdby'=>$uid,'created_at'=>$cdate,'modifiedby'=>$uid]);
                array_push($prid_arr,$mftid);
                //inset brand
                $brandid = DB::table('brand')->insertGetId(['mftid' => $mftid,'brand'=>$brand,'status'=>$status]);
                array_push($pridtd_arr,$brandid);
                //insert models
                 $dt = DB::table('product')->insertGetId(['brandid' => $brandid,'townwisemodel'=>$townwise_models,'industry'=>$industry,'category' => $category,'segment'=>$segment,'subsegment'=>$subsegment,'status'=>$status,'updated_at'=>$cdate]);
                array_push($modeltd_arr,$dt);

             }else{
                         

              $mftid = $manudt;
              array_push($prid_arr,$mftid);
              //check brand exists for manufacture
              $brandid = DB::table('brand')->where('mftid','=',$mftid)->where('brand','=',$brand)->value('id');
              if(empty($brandid)){

                    $brandid = DB::table('brand')->insertGetId(['mftid' => $mftid,'brand'=>$brand,'status'=>1]);
                    array_push($pridtd_arr,$brandid);
                    //insert models
                     $dt = DB::table('product')->insertGetId(['brandid' => $brandid,'townwisemodel'=>$townwise_models,'industry'=>$industry,'category' => $category,'segment'=>$segment,'subsegment'=>$subsegment,'status'=>$status,'updated_at'=>$cdate]);
                    array_push($modeltd_arr,$dt);

              }else{
                 DB::table('brand')
                 ->where('id',$brandid)
                 ->update([
                   'status'=> $status,
                    ]);

                   array_push($pridtd_arr,$brandid);
                   $whereprd  = "brandid='".$brandid."' and townwisemodel='".$townwise_models."' and industry='".$industry."' and category='".$category."' and segment='".$segment."' and subsegment='".$subsegment."'  ";
                  $productDT  = "product"; 
                  $manudt = self::getValues($whereprd,$productDT);
                 // print_r( $manudt);exit;
                  //insert models
                     if(empty($manudt)){
                        $dt = DB::table('product')->insertGetId(['brandid' => $brandid,'townwisemodel'=>$townwise_models,'industry'=>$industry,'category' => $category,'segment'=>$segment,'subsegment'=>$subsegment,'status'=>$status,'updated_at'=>$cdate]);
                        array_push($modeltd_arr,$dt);
                        }else{
                          DB::table('product')
                           ->where('id',$manudt['id'])
                           ->update([
                             'status'=>$status,'updated_at'=>$cdate,
                              ]);
                         array_push($modeltd_arr,$manudt['id']);
                       }

              }
                   
              }
              }
             }
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'product/'.$filename;
           Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('product/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));
           $upcatid = self::getItemid($uploadcatid,'product');

           if(empty($upcatid)){
            $upcatid = self::saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => 'product','DDI_DISP_TEXT'=> 'product','uid'=> $uid));
              }
           $sql = DB::table('uploads')->where('status','=',1)->where('category','=',$uploadcatid)->get();
           $row = json_decode( json_encode($sql), true);
           if(!empty($row)){
            $uploadid = $row[0]['id'];
            }

           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> $uploadcatid,'link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                }
              $category  = "Product Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Updated Product Master';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              //zone make inactive
              DB::table('manufacture')
               ->whereNotIn('id',$prid_arr)
               ->update([
                'status'    =>  2,
                    ]);
            // print_r($pridtd_arr);exit;
               //town amke inactive
              //zone make inactive
              DB::table('brand')
               ->whereNotIn('id',$pridtd_arr)
               ->update([
                'status'    =>  2,
                    ]);

               DB::table('product')
               ->whereNotIn('id',$modeltd_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
          
              return array('title'=>'Success!','messege'=>'Product Updated Successfully','type'=>'success','load'=>'settings');

            }else{
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Formate is wrong','type'=>'warning');
            }

           
          } 

            /*==========================================================
    Function: saveDealer
    Description: To upload  dealer
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
        public static function saveDealer(Request $request){
              $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $cdate  = date('Y-m-d');
             $time  = date('Y-m-d h:m:i'); 
             $data_arr = array();
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('dealer',$headings[0][0]);
             $deaid_arr = array();

           // print_r(  $validate);exit;
         if($validate['success'] == 1){
                 //get array from file
           $uploadcatid = self::getCategoryid('UPLOAD_TYPE');
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           // print_r(  $dataarray);exit;

            $values = $dataarray[0];
            $townarr = array_column($values,'Town');
          //  print_r($townarr)
            $temp = str_replace("'", "'+char(39)+'", $townarr );
          
            $townarr = ("('".implode("'),('", $temp))."')";
            //print_r($townarr);exit;
         
    //    echo "select * from (values $townarr) as t(town) except select town from town";exit;
            $towndt =DB::SELECT("select * from (values $townarr) as t(town) except select town from town");
            $towndt = json_decode( json_encode($towndt), true);
   // echo '<pre>';
   //       print_r( $towndt);
   //        echo '</pre>';exit;
            //print_r($towndt);exit;
            if(empty($towndt)){
            foreach ($values as $key => $value) {
             $dcode =  $value['Dealer Code'];
             $dname =  $value['Dealer Name'];
             $dtown =  $value['Dealer Town'];
             $town  =  $value['Town'];
             $State =  $value['State'];
            // $data_arr[$Zone] = array($State=>array($Area=>array($District=>array($Town))));
              $data_arr[$State][]=array("town"=>$town,"dcode"=> $dcode,"dname"=> $dname,"dtown"=>$dtown);


            }
       
          foreach ($data_arr as $key => $towndt) {

             $State =  $key;
           
            //   count($towndt)
              foreach ($towndt as $town => $value) {
               
              //print_r($value);exit;
             $dcode =  $value['dcode'];
             $dname =  $value['dname'];
             $dtown =  $value['dtown'];
             $town  =  $value['town'];
             $towncond =   str_replace("'", "''", $town);
             $townid  = DB::table('town')->join('state', 'town.stateid', '=', 'state.id')->where("town",$towncond)->value('town.id');
            
              if(!empty($townid)){
                 $dealer_id  = DB::table('dealers')->where("townid",$townid)->where("dealercode",$dcode)->value('id');
               if(empty($dealer_id)){
                 $dealerid = DB::table('dealers')->insertGetId(['dealername' => $dname,'townid'=>$townid,'dealercode'=>$dcode,'dealertown'=>$dtown,'updated_at'=>$cdate,'status'=>1,'createdby'=>$uid,'created_at'=>$cdate,'modifiedby'=>$uid]);
                  array_push($deaid_arr,$dealerid);
                  }else{
                  array_push($deaid_arr,$dealer_id);
                  }
             
               }
             }

             
             }
             $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'dealers/'.$filename;
           Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('dealers/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));
            $upcatid = self::getItemid($uploadcatid,'dealer');
           if(empty($upcatid)){
            $upcatid = self::saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => 'dealer','DDI_DISP_TEXT'=> 'dealer','uid'=> $uid));
            
           }
           $sql = DB::table('uploads')->where('status','=',1)->where('category','=',$upcatid)->get();
           $row = json_decode( json_encode($sql), true);
            if(!empty($row)){
            $uploadid = $row[0]['id'];
            }

           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> $upcatid,'link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                }
              $category  = "Dealer Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Updated Dealer master';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              //zone make inactive
               DB::table('dealers')
               ->whereNotIn('id',$deaid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
              
              
          
              return array('title'=>'Success!','messege'=>'Dealers Updated Successfully','type'=>'success','load'=>'settings');
            }else{

            //  print_r(array_column($towndt,'town'));exit;
              $err_tow = implode(", ",array_map('trim',array_column($towndt,'town')));

              return array("error"=>'The following Towns are not found in the Location Master: '.$err_tow.' please correct and re-upload.');
            }

            }else{
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Formate is wrong','type'=>'warning');
            }

          

     }
             /*==========================================================
    Function: saveDealer
    Description: To upload  dealer
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
       /* public static function saveDealer(Request $request){
              $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
             $data_arr = array();
             $date  = $request->get('date');
             $headings = (new HeadingRowImport)->toArray(request()->file('file'));
             $homecontroller   =   new HomeController;
             $validate        =   $homecontroller->checkHeader('dealer',$headings[0][0]);
             $deaid_arr = array();

           // print_r(  $validate);exit;
         if($validate['success'] == 1){
                 //get array from file
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           // print_r(  $dataarray);exit;
           
            $values = $dataarray[0];

            foreach ($values as $key => $value) {
             $dcode =  $value['Dealer Code'];
             $dname =  $value['Dealer Name'];
             $dtown =  $value['Dealer Town'];
             $town  =  $value['Town'];
             $State =  $value['State'];
            // $data_arr[$Zone] = array($State=>array($Area=>array($District=>array($Town))));
              $data_arr[$State][$town]=array("town"=>$town,"dcode"=> $dcode,"dname"=> $dname);


            }
            echo '<pre>';
       print_r( $data_arr);
       echo '</pre>';exit;
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:m:i');
            foreach ($values as $key => $value) {
             $dcode =  $value['Dealer Code'];
             $dname =  $value['Dealer Name'];
             $dtown =  $value['Dealer Town'];
             $town  =  $value['Town'];
             $State =  $value['State'];
             $towncond =   str_replace("'", "''", $town);
           
             $array  = array("state"=>$State,"town"=>$towncond);
             $towndt = self::getTowndetails($array);
            //print_r( $towndt);exit;
             if(!empty($towndt)){
              $townid=  $towndt[0]['id'];
              //if manufacture id is empty
            //  $wherede  = "dealername='".$namecond."' and townid='".$townid."' and dealercode='".$dcode."' and dealertown='".$dtowncond."' ";
               $wherede  = "townid='".$townid."' and dealercode='".$dcode."' ";
              $deal  = "dealers"; 
              $manudt = self::getValues($wherede,$deal);
              if(empty($manudt)){
               $dealerid = DB::table('dealers')->insertGetId(['dealername' => $dname,'townid'=>$townid,'dealercode'=>$dcode,'dealertown'=>$dtown,'updated_at'=>$cdate,'status'=>1,'createdby'=>$uid,'created_at'=>$cdate,'modifiedby'=>$uid]);
                array_push($deaid_arr,$dealerid);
                }else{
                array_push($deaid_arr,$manudt['id']);
                }
             
               }

             }
           
             
            $file = request()->file('file');
            $filename = $file->getClientOriginalName();
            $path = 'dealers/'.$filename;
           Storage::disk('local')->delete( $path);
           // Storage::disk('local')->makeDirectory('location');
           Storage::disk('local')->put('dealers/'.$filename,  File::get(request()->file('file')));
           //Storage::disk('local')->put(.$request['name'], fopen(request()->file('file'), 'r+'));

           $sql = DB::table('uploads')->where('status','=',1)->where('category','=','dealer')->distinct()->get();
           $row = json_decode( json_encode($sql), true);
            if(!empty($row)){
            $uploadid = $row[0]['id'];
            }

           if(empty($uploadid)){
           $uploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> 'dealer','link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           }else{
               DB::table('uploads')
                ->where('id',$uploadid)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);
                }
              $category  = "Dealer Master Updated";
              $homecontroller   =   new HomeController;
              $note   = 'Dealer Master Updated';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              //zone make inactive
               DB::table('dealers')
               ->whereNotIn('id',$deaid_arr)
               ->update([
                'status'    =>  2,
                    ]);
              
              
              
          
              return array('title'=>'Success!','messege'=>'Dealers Updated Successfully','type'=>'success','load'=>'settings');

            }else{
              return array('load'=>'settings','title'=>'Failed!','messege'=>'Formate is wrong','type'=>'warning');
            }

           
          } */
           /*==========================================================
    Function: getTowndetails
    Description: To get activity
    Author: 
    Created Date: 09-01-2020
    Modification: 
    ==========================================================*/
        public static function getTowndetails($array){
         
        $zone = (array_key_exists('zone', $array)) ? $array['zone'] : '';
        $state = (array_key_exists('state', $array)) ? $array['state'] : '';
        $area = (array_key_exists('area', $array)) ? $array['area'] : '';
        $areaid = (array_key_exists('areaid', $array)) ? $array['areaid'] : '';
        $district = (array_key_exists('district', $array)) ? $array['district'] : '';
        $town = (array_key_exists('town', $array)) ? $array['town'] : '';
        $townclass = (array_key_exists('townclass', $array)) ? $array['townclass'] : '';
        $towngroup = (array_key_exists('towngroup', $array)) ? $array['towngroup'] : '';


      
        $zonecond = ($zone=='')?' ':" and zone='".$zone."'";
        $statecond = ($state=='')?' ':" and state='".$state."'";
        $districtcond = ($district=='')?' ':" and district='".$district."'";
        $areacond = ($area=='')?' ':" and area ='".$area."'";
        $areaidcond = ($areaid=='')?' ':" and areaid ='".$areaid."'";
        $towncond = ($town=='')?' ':"  town='".$town."'";
        $townclasscond = ($townclass=='')?' ':" and townclass ='".$townclass."'";
        $towngroupcond = ($towngroup=='')?' ':" and towngroup='".$towngroup."'";
     //   print_r($statuscond);exit;
// /echo "SELECT * from towndetails  where  ".$towncond.$zonecond.$statecond.$districtcond.$areacond.$townclasscond.$towngroupcond;exit;
        $sql = DB::SELECT("SELECT * from towndetails  where  ".$towncond.$zonecond.$statecond.$districtcond.$areacond.$townclasscond.$towngroupcond.$areaidcond);
        $row = json_decode( json_encode($sql), true);
        return $row;

        }
  /*==========================================================
    Function: getActivity
    Description: To get activity
    Author: 
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
        public static function getActivity(Request $request){
          $input = $request->all();
          //  print_r(Session::get('user_id'));exit;
          $uid  = $request->get('user_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
           }
          // print_r($uid);exit;
        $where ='';
        $from  = date("Y-m-d",strtotime($request->get('from')));
        $to  = date("Y-m-d",strtotime($request->get('to')));
        $perPage = $request->get('per_page');
        $sort = $request->get('sort');
        $filter = $request->get('filter');
        $page = $request->get('page');
        $download = $request->get('download');
        $sort =  explode('|', $sort);
        $limit = ' ORDER BY '.$sort[0];
        if($filter){
        $where = " and ( note LIKE '" . $filter. "%') OR ( r.role LIKE '" . strtoupper($filter) . "%') OR ( createdat LIKE '" . $filter . "%')";
        }
        $sql = "SELECT u.*,replace(CONVERT(VARCHAR(9),a.createdat,106),' ','-') as DatePart,
 convert(VARCHAR(12), a.createdat, 108) as TimePart,a.*,r.role FROM activity a left join users u on u.id=a.createdby LEFT JOIN userrole r on r.id=u.role where format(createdat, 'yyyy-MM-dd') between '".$from."' AND '".$to."' $where order by createdat desc ";
     $deliveries = DB::select($sql);
//print_r($sql);exit;
   
        
           $row = collect($deliveries);
         $currentPage = $page ?: 1;
         $slice_init = ($currentPage == 1) ? 0 : (($currentPage*$perPage)-$perPage);
         $data = $row->slice($slice_init, $perPage)->all();
         $deliveries = new LengthAwarePaginator($data, count($deliveries), $perPage, $currentPage);
         
            return $deliveries;
       



          
        }
        /*==========================================================
    Function: downloadLogs
    Description: To get activity
    Author: 
    Created Date: 02-01-2020
    Modification: 
    ==========================================================*/
        public static function downloadLogs(Request $request){
        $from  = date("Y-m-d",strtotime($request->get('from')));
        $to  = date("Y-m-d",strtotime($request->get('to')));
         return Excel::download(new UsersLogExport($from,$to), 'useagelog.xlsx');

        }
            /*==========================================================
    Function: downloadLogs
    Description: To get activity
    Author: 
    Created Date: 02-01-2020
    Modification: 
    ==========================================================*/
        public static function getDispyear(Request $request){
         $sql = DB::SELECT("select distinct dispyear from datadispatch order by dispyear DESC");
         $row = json_decode( json_encode($sql  ), true);
         return $row;


        }
    /*==========================================================
    Function: getCategoryid
    Description: To get cat is
    Author: 
    Created Date: 11-04-2020
    Modification: 
    ==========================================================*/
        public static function getCategoryid($catname){
         $sql = DB::SELECT("select DDI_CATG_ID from DD_CATEGORIES where DDI_CATG_NAME='".$catname."'");
         $row = json_decode( json_encode($sql  ), true);
         return $row[0]['DDI_CATG_ID'];
       }

       /*==========================================================
    Function: getItemid
    Description: To get item id
    Author: 
    Created Date: 11-04-2020
    Modification: 
    ==========================================================*/
        public static function getItemid($catid,$cname){
         //echo "select DDI_ID from DD_ITEMS where DDI_CATG_ID='".$catid."' and DDI_DISP_TEXT='".$cname."'";exit;
         $sql = DB::SELECT("select DDI_ID from DD_ITEMS where DDI_CATG_ID='".$catid."' and DDI_DISP_TEXT='".$cname."'");
         $row = json_decode( json_encode($sql), true);
       //  print_r($sql);exit;
         if(empty($row)){
          return ;
         }else{
         return $row[0]['DDI_ID'];
         }


        }
        /*==========================================================
    Function: saveItems
    Description: To get item id
    Author: 
    Created Date: 11-04-2020
    Modification: 
    ==========================================================*/
        public static function saveItems($array){
            $uid = $array['uid'];
            $DDI_CATG_ID = $array['DDI_CATG_ID'];
            $DDI_CODE_TXT = $array['DDI_CODE_TXT'];
            $DDI_DISP_TEXT = $array['DDI_DISP_TEXT'];
            $uploadcatid = $array['DDI_CATG_ID'];
            $time  = date('Y-m-d h:i:s');
          
            $upcatid = DD_ITEMS::insertGetId(['DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $DDI_CODE_TXT,'DDI_DISP_TEXT'=>  $DDI_DISP_TEXT,'status' => 1,'ADD_BY'=> $uid,'ADD_ON' => $time,'EDIT_BY'=> $uid,'EDIT_ON'=> $time,'DDI_CODE'=>4]);
            return $upcatid;
 
        }

}
