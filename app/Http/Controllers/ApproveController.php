<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;
use App\Imports\ConsolidateImport;
use App\Exports\ApproveExport;
use View;
use Mail;
use App\datadispatch;
use Illuminate\Support\Facades\Crypt;
class ApproveController  extends Controller
{
	/*==========================================================
    Function: Approve
    Description: get APPROVELIST
    Author:
    Created Date: 17-01-2020
    Modification: 
    ======================================================*/
    public static function Approve(Request $request){
    //	$type = $request->get('type');
    	 $month = $request->get('month');
       if(empty($month)) { $month = date("m-Y", strtotime("-1 months")); }
      //echo $month;exit;
      	$mdata  = explode("-",$month);
        $month = $mdata[0];
        $year =  '20'.$mdata[1];
        $date = date_parse($month);
        $month =  $date['month'];
        $manulist = array(0=>'Town',1=>'Town Group');
      	$areaid = ($request->get('areaid') != '') ? $request->get('areaid') : $request->get('area') ;
     	  $UploadsSettings   =   new AdminUploadsController;
         $MasterSettings   =   new MasterSettings;
        $concatid = Session::get('consoliitemid');
     	  $discond  = array('type'=> $concatid,'month'=>$month,'status'=>0,'year'=>$year,'area'=>$areaid);
        $disvalue =  $UploadsSettings->getDispatch($discond);
        if(!empty($disvalue )){
          $uploads = DB::table('uploads')->where('id',  $disvalue[0]['uploadid'])->get();
          $updetails = json_decode( json_encode($uploads), true);
          $filename = $updetails[0]['filename'];
          $link = $updetails[0]['link'];
        $manusql = DB::SELECT("Select p.manufacturer,p.display_seq from datadispatch d join datadispatch_dt dt on d.id=dt.ddid join brand pdt on pdt.id=dt.productid join manufacture p on p.id=pdt.mftid where type='". $concatid."' and month='".$month."' and dispyear='".$year."' and d.area='".$areaid."' and d.status=0 group by p.manufacturer,p.display_seq order by p.display_seq");
       	$manufacture = json_decode( json_encode($manusql), true);
       	foreach ($manufacture as $key => $value) {
       		array_push($manulist, $value['manufacturer']);
       	}
       // echo "exec approve_details $month,$areaid,$year";exit;
      //echo "exec approve_details $month,$areaid,$year,$concatid";exit;
     	$sql =  DB::Select("exec approve_details $month,$areaid,$year,$concatid");
    	$row = json_decode( json_encode($sql), true);
    //  print_r(    $row);exit;
      if(!isset($row[0]['ErrorMessage'])){
      $final_arr = array();
      foreach ($row as $key => $value) {
        $final_arr[$value['town']][$key] =  $value;
      }
    	$warnings  = '';
    	//print_r($disvalue[0]['warning']);exit;
    	if(!empty($disvalue[0]['warning'])){
    	$warnings = json_decode($disvalue[0]['warning']);
         }
       // print_r($row);exit;
    	if(!empty($row)){
    		return array( "link"=> $link,"filename"=>$filename,"valuelist"=>$row,"manufacture"=>$manulist,'update_at'=> date("d-M-y H:i:s",strtotime($disvalue[0]['updated_at'])),'id'=>$disvalue[0]['id'],'warnings'=>$warnings);
    	}
    }else{
      return;
    }

    	
        
    }
}

    
     /*==========================================================
    Function: downloadApprove
    Description: To download approve
    Author: 
    Created Date: 17-01-2020
    Modification: 
    ==========================================================*/
        public static function downloadApprove(Request $request){
        	//print_r($request->all());exit;		
        $data = self::Approve($request);
        $time  = date('Y-m-d h:i:s');
        //print_r($values);exit;
        return Excel::download(new ApproveExport($data['manufacture'],$data['valuelist']), 'approve.xlsx');

        }

    /*==========================================================
    Function: approveEmail
    Description: To approve from email
    Author: 
    Created Date: 12-03-2020
    Modification: 
    ==========================================================*/
        public static function approveEmail($email,$id,$status){

            $id = Crypt::decryptString($id);
            $email = Crypt::decryptString($email);
            $det = DB::SELECT("SELECT dispyear,month from datadispatch where id=$id");
            $yearmonth = date('M', mktime(0, 0, 0, $det[0]->month, 10)).'-'.$det[0]->dispyear;
            $request = new \Illuminate\Http\Request();
            $request->replace(['id' => $id,'status'=>$status,'month'=>$yearmonth]);
            self::SaveApprove($request);
            }
         /*==========================================================
    Function: SaveApprove
    Description: To save approve
    Author: 
    Created Date: 17-01-2020
    Modification: 
    ==========================================================*/
        public static function SaveApprove(Request $request){
        	//print_r(self::Approve($request));exit;		
        $areaid =  $request->get('areaid');
        $area =  $request->get('area');
        $user_role  = $request->get('user_role');
        $user_name  = $request->get('user_name');
        $uid  = $request->get('user_id');
        $upload_enable  = $request->get('upload_enable');
        if(empty($areaid)){
            $areaid = Session::get('areaid');
            $area = Session::get('area');
            $user_role = Session::get('user_role');
            $user_name = Session::get('user_name');
            $uid = Session::get('user_id');
            $fid = Session::get('finacial_id');
            $email = Session::get('email');
            $upload_enable = Session::get('upload_enable');
             $concatid = Session::get('consoliitemid');

        }
        $duedate = '';
        //echo is_numeric($upload_enable);exit;
        if(is_numeric($upload_enable)){
          $duedate = date($upload_enable.'-m-Y');
        }
       // print_r($duedate);exit;
        $status     = $request->get('status');
        $yearmonth  = $request->get('month');
        $time       = date('Y-m-d h:i:s');
        $id         = $request->get('id');
        
       // print_r( $approvelist);exit;
        $val = DB::table('datadispatch')->where('id',$id)->update(['updated_at'=>$time,'status'=> $status,'approve_at'=>$time]);
        if($status == 1){
          $note ='Approved Consolidated Townwise';
         }else{
          $note ='Rejected  Consolidated Townwise';
        }
        $mdata  = explode("-",$yearmonth);
        $month = $mdata[0];
        $year =  '20'.$mdata[1];
        $nmonth = date("m", strtotime($month));
        /* email*/
       /*MIS User – Approval successful / rejection email*/
      // echo "select email from users where area='".$areaid."' and role='4'";exit;
       $userdt = DB::select("select email from users where area='".$areaid."' and role='4'");
       if(!empty($userdt)){
        $miseremail = $userdt[0]->email ;
        
       }
       
        if($status == 1){
           $subject =  $yearmonth.' '.$area.' Townwise data: Approved';
           $type="approved";
           $view = 'pages.email.approvemisuser';
        }else{
           $subject =  $yearmonth.' '.$area.' Townwise data: Rejected';
           $type="rejected";

           $view = 'pages.email.rejectmisuser';
        }
        if(!empty( $miseremail)){
        $emailcontroller   =   new EmailController;
        $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view,'type'=> $type,'duedate'=>$duedate);
        $emailcontroller->sendEmail($args);
        }
      /* end of MIS User – Approval successful / rejection email*/
        /*AM User – Thank you for Approving*/
       
        if($status == 1){
        $subject =  $yearmonth.' '.$area.'  Townwise data: Thank you for Approving';
        $emailcontroller   =   new EmailController;
        $view = 'pages.email.approveemail';
        $args = array('email'=>$email,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
        $emailcontroller->sendEmail($args);
        }
        /*end of AM User – Thank you for Approving*/
         /*HO Admin – Approved by AM*/
       
         if($status == 1){
           $sql =  DB::Select("exec approve_details $nmonth,$areaid,$year,$concatid");
           $valuelist = json_decode( json_encode($sql), true);
           $manusql = DB::SELECT("Select p.manufacturer,p.display_seq from datadispatch d join datadispatch_dt dt on d.id=dt.ddid join brand pdt on pdt.id=dt.productid join manufacture p on p.id=pdt.mftid where type=$concatid and month='".$nmonth."' and dispyear='".$year."' and d.area='".$areaid."' and d.status=0 group by p.manufacturer,p.display_seq order by p.display_seq");
           $manufacture = json_decode( json_encode($manusql), true);
            $datafile = DB::SELECT("SELECT link from datadispatch d join uploads u on d.uploadid=u.id where d.id=$id");
            $pathToFile =  $datafile[0]->link; 
           // print_r($pathToFile);exit;
           $userdt = DB::select("select email from users where role=1");
           if(!empty($userdt)){
            $adminemail = $userdt[0]->email ;
            
           }
            $subject =  $yearmonth.' '.$area.'  HO Admin – Approved by AM';
            $emailcontroller   =   new EmailController;
            $view = 'pages.email.adminapproveemail';
          
            //  print_r($valuelist);exit;
            $data = array('pathToFile'=>Storage::path($pathToFile),'manufacture'=>$manufacture,'valuelist'=>$valuelist,'email'=>$adminemail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
 
         }
        /*end of AM User – Thank you for Approving*/
        /*HO Admin – Rejected by AM*/
         if($status == 2){
           $userdt = DB::select("select email from users where role=1");
           if(!empty($userdt)){
            $adminemail = $userdt[0]->email ;
            
           }
            $subject =  $yearmonth.' '.$area.'  Townwise data: Rejected';
            $emailcontroller   =   new EmailController;
            $view = 'pages.email.adminrejectemail';
            $args = array('email'=>$adminemail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
              $emailcontroller->sendEmail($args);
            }

              $MasterSettings   =   new MasterSettings;
              $MasterSettings->generateTrigger($nmonth,$year);
              $category  = "Approve";
              $homecontroller   =   new HomeController;
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              return $activity;

        }
          /*==========================================================
    Function: CheckAprrove
    Description: To checkapprove
    Author: 
    Created Date: 19-02-2020
    Modification: 
    ==========================================================*/
        public static function CheckAprrove(Request $request){
           $area = $request->get('areaid');
           $sql = DB::SELECT("SELECT concat(FORMAT(DATEFROMPARTS(1900, month, 1), 'MMM', 'en-US'),'-',FORMAT(DATEFROMPARTS(dispyear, 1, 1), 'yy ', 'en-US')) as month FROM datadispatch where area='".$area."' and status=0 order by month asc");
           $row = json_decode( json_encode($sql), true);
         return $row;

        }
        
    
}
?>