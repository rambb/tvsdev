<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;

class AdminStatusController extends Controller
{


    /*==========================================================
    Function: getStatusdtAll
    Description: To get details to display status 
    Author:
    Created Date: 01-01-2020
    Modification: 
    ======================================================*/
    public static function getStatusdtAll(Request $request){
         $month = $request->get('month');
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $year =  '20'.$mdata[1];
      
      //   print_r($year);exit;
         $date = date_parse($month);
         $month =  $date['month'];
         $details = array();
         $sap = Session::get('sapitemid');
         $retail = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $pdplan = Session::get('pditemid');
      	 $AdminUploads   =   new AdminUploadsController;
      	 $discond1  = array('type'=>$sap,'month'=>$month,'year'=> $year,'status'=>'');
      	 $sapvalue  =  $AdminUploads->getDispatch($discond1);
      	 $sap = (empty($sapvalue)) ? '' : $sapvalue[0];
      	// print_r( $sapvalue);exit;
      	 $discond2  = array('type'=> $retail,'month'=>$month,'year'=> $year,'status'=>'');
      	 $retvalue  =  $AdminUploads->getDispatch($discond2);
      	 $ret = (empty($retvalue)) ? '' : $retvalue[0];
      	  //print_r( $value);exit;
      	 $discond3  = array('type'=> $pdplan,'month'=>$month,'year'=> $year,'status'=>'');
      	 $pdvalue  =  $AdminUploads->getDispatch($discond3);
      	 $pd = (empty($pdvalue)) ? '' : $pdvalue[0];
      	  //print_r( $value);exit;
      	 $discond4  = array('type'=>$siam,'month'=>$month,'year'=> $year,'status'=>'');
      	 $siamvalue  =  $AdminUploads->getDispatch($discond4);
      	 $siam = (empty($siamvalue)) ? '' : $siamvalue[0];
      	 array_push($details,array("sap"=>$sap,"retails"=>$ret,"pdpaln"=>$pd,"siam"=>$siam,'status'=>''));
      	 return $details;


     }
         /*==========================================================
    Function: getUploadsdt
    Description: To get details to uploads status 
    Author:
    Created Date: 01-01-2020
    Modification: 
    ======================================================*/
    public static function getUploadsdt(Request $request){

// Path to the 'storage/app' folder    
       
    	$upid = $request->get('uploadid');
    	$dt = DB::table('uploads')->where('id', $upid)->get();
    	$row = json_decode( json_encode($dt), true);
      return $row;


    }
    /*==========================================================
    Function: getUserupload
    Description: To get details to display status of consolidate and raw status
    Author:
    Created Date: 29-01-2020
    Modification: 
    ======================================================*/
    public static function getconsolidateUpload(Request $request){
       $uploadenable = $request->get('upload_enable');
        if(empty($uploadenable)){
          $uploadenable = Session::get('upload_enable');
          $user_role = Session::get('user_role');
          $areaid = Session::get('areaid');
          $consoli  =  Session::get('consoliitemid');
         
         }

         $month = $request->get('month');
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $year =  '20'.$mdata[1];
         $date = date_parse($month);
         $month =  $date['month'];
         $duedate =  date( 'Y-m-'.$uploadenable); 
         $current_date = date('Y-m-d');
         $zonearray = array();
         $details = array();
          $area_cond = $area_cnd = '';
          if($user_role == 2){
              if(!empty($areaid)){
                $area_cond = " and s.id in($areaid)";
                $area_cnd = " and a.id in($areaid)";
                $areacount = DB::SELECT("select count(id) as cnt from area where status=1 and id in($areaid)");
                $areacount = $areacount[0]->cnt;
              }else{
                $area_cnd = "";
               $areacount = DB::SELECT("select count(s.id) as cnt from zone z join area s on z.id=s.zoneid  where s.status=1 and area NOT LIKE '%INDIA%'");
                $areacount = $areacount[0]->cnt;
              }
          }else{
              $areacount = DB::SELECT("select count(s.id) as cnt from zone z join area s on z.id=s.zoneid  where s.status=1 and area NOT LIKE '%INDIA%'");
              $areacount = $areacount[0]->cnt;
          }
        
         $dt = DB::SELECT("SELECT s.area,z.zone,z.id zoneid,s.id as areadid from zone z join area s on z.id=s.zoneid where s.status=1 and z.status=1 and s.area NOT LIKE '%INDIA%'  $area_cond order by s.area ASC");
         $row = json_decode( json_encode($dt), true);
         $dispatch = DB::SELECT("SELECT format(d.approve_at, 'dd-MM-yyyy') as approve_at ,d.area,d.status from datadispatch d left join datadispatch_dt dt on d.id=dt.ddid join area a on a.id=d.area where type='".$consoli."'  and month='".$month."' and dispyear='".$year."' and a.status=1 and d.status!=3  $area_cnd group by d.area,approve_at,d.status ");
         $dispatch = json_decode( json_encode($dispatch), true);
         $dispatchcount = count($dispatch);
         if(!empty($areacount)){
         $percentage =  ($dispatchcount*100)/ $areacount;
         $percentage = round($percentage);
       }else{
        $percentage =0;
       }
       
         foreach ($row as $key => $value) {
            if(array_search($value['areadid'], array_column($dispatch, 'area')) !== false) {
                $dispatchkey = array_search($value['areadid'], array_column($dispatch, 'area'));
                $status = $dispatch[$dispatchkey]['status'];
                if($status==1 ){$date = ($dispatch[$dispatchkey]['approve_at'] != '') ? $dispatch[$dispatchkey]['approve_at'] : ''; $class_status='greenbox text-white';}else if($status==0){$date ='';$class_status='orangebox text-white';}else if($status==2){$date ='';$class_status='';}
                $zonearray[$value['zone']][$key] = array('areaid'=>$value['areadid'],'area'=>$value['area'],'up_date'=>$date,'classstatus'=>$class_status,'zoneid'=>$value['zoneid'],'status'=>$status);
             }else {
                if(date('Y-m-d',strtotime($duedate)) < date('Y-m-d',strtotime($current_date))){
                 $class_status ='redbox text-white';
                }else{
                 $class_status ='';
                }
                $zonearray[$value['zone']][$key] = array('area'=>$value['area'],'up_date'=>'','classstatus'=>$class_status,'areaid'=>'','zoneid'=>$value['zoneid'],'status'=>'');
             }
          }
        return array("zonedt"=>$zonearray,'areacount'=>$areacount,'dispatchcount'=>$dispatchcount,'percentage'=>$percentage  );

         }
           /*==========================================================
    Function: getStatestatus
    Description: To get details to display status of consolidate statewise
    Author:
    Created Date: 29-01-2020
    Modification: 
    ======================================================*/
    public static function getStatestatus(Request $request){
         $areaid = $request->get('areaid');
         $type = $request->get('type');
         $month = $request->get('month');
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $year =  '20'.$mdata[1];
         $date = date_parse($month);
         $month =  $date['month'];
         $filearray = array();
         if($type == 'Raw Data'){
           $cattype   = Session::get('rawitemid');
         }else{
           $cattype  =  Session::get('consoliitemid');
         }
         //echo "SELECT d.area,u.filename,u.link,CONVERT(VARCHAR(9),d.approve_at ,106) as approve_at,CONVERT(VARCHAR(9),d.updated_at ,106) as updated_at,d.dispyear from datadispatch d left join datadispatch_dt dt on d.id=dt.ddid join uploads u on u.id=d.uploadid where type='". $cattype."' and month='".$month."' and dispyear='".$year."' and d.area='".$areaid."' ";exit;
       
         $dt = DB::SELECT("SELECT d.area,u.filename,u.link,CONVERT(VARCHAR(9),d.approve_at ,106) as approve_at,CONVERT(VARCHAR(9),d.updated_at ,106) as updated_at,d.dispyear from datadispatch d left join datadispatch_dt dt on d.id=dt.ddid left join uploads u on u.id=d.uploadid where type='". $cattype."' and month='".$month."' and dispyear='".$year."' and d.area='".$areaid."' ");
         $dispatch = json_decode( json_encode($dt), true);
         if($type == 'Raw Data'){
           $sql = DB::select("SELECT  month,countfile,link,filename,d.updated_at,d.id as dispatchid FROM datadispatch d join uploads u on d.uploadid=u.id  where type='".$cattype."'  and dispyear='".$year."' and area='".$areaid."' and month='".$month."' ");
           $row = json_decode( json_encode($sql), true);    
         if(!empty($row)){
            $filename =   $row[0]['filename'];
           $filearray = explode(",",$filename);
          }
        }
        return  array('dispatch'=>$dispatch,'filelist'=>$filearray);
       }

    /*==========================================================
    Function: getrawUpload
    Description: To get details to display  raw status
    Author:
    Created Date: 29-01-2020
    Modification: 
    ======================================================*/
    public static function getrawUpload(Request $request){
      $uploadenable = $request->get('upload_enable');
        if(empty($uploadenable)){
          $uploadenable = Session::get('upload_enable');
           $user_role = Session::get('user_role');
          $areaid = Session::get('areaid');
         }
         $area_cond = '';
         $month = $request->get('month');
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $year =  '20'.$mdata[1];
         $date = date_parse($month);
         $month =  $date['month'];
         $duedate =  date( 'Y-m-'.$uploadenable); 
         $current_date = date('Y-m-d');
         $zonearray = array();
         $details = array();
         $area_cond ='';
        if($user_role == 2){
          if(!empty($areaid)){
            $area_cond = "and s.id in($areaid)";
            }
          }
         $raw   = Session::get('rawitemid');
         $dt = DB::SELECT("SELECT s.area,z.zone,z.id zoneid,s.id as areadid from zone z join area s on z.id=s.zoneid where s.status=1 and z.status=1 and s.area NOT LIKE '%INDIA'  $area_cond");
         $row = json_decode( json_encode($dt), true);
         $dispatch = DB::SELECT("SELECT CONVERT(VARCHAR(9),d.updated_at ,106) as updated_at,d.area,d.status from datadispatch d left join datadispatch_dt dt on d.id=dt.ddid join area a on a.id=d.area where type='".$raw."'  and month='".$month."' and dispyear='".$year."' and d.status=1 group by d.area,updated_at,d.status");
         $dispatch = json_decode( json_encode($dispatch), true);
         foreach ($row as $key => $value) {
           if(array_search($value['areadid'], array_column($dispatch, 'area')) !== false) {
              $dispatchkey = array_search($value['areadid'], array_column($dispatch, 'area'));
              $class_status='box greybox';
              $date=$dispatch[$dispatchkey]['updated_at'];
              $zonearray[$value['zone']][$key] = array('areaid'=>$value['areadid'],'area'=>$value['area'],'up_date'=>$date,'classstatus'=>$class_status,'zoneid'=>$value['zoneid']);
             }else {
              $class_status='';
              $zonearray[$value['zone']][$key] = array('area'=>$value['area'],'up_date'=>'','classstatus'=>$class_status,'areaid'=>'','zoneid'=>$value['zoneid']);
             }
          }
         return array("zonedt"=>$zonearray);
         }
    }
?>