<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use App\TownwiseData;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;
use App\Exports\FinalOutputExport;
Use App\datagenerate;
use App\Jobs\Generate;
use App\datadispatchdet;
use App\datadispatch;

use Rap2hpoutre\FastExcel\FastExcel;
//use App\FastExcel;

class TownwiseReportController extends Controller
{

  /*==========================================================
    Function: genrateCheck
    Description: To enable generate button
    Author:
    Created Date: 26-02-2020
    Modification: 
    ==========================================================*/
      public static function genrateCheck(Request $request){
      //   $dispatch = DB::SELECT("SELECT month,area,dispyear,type from datadispatch where generate=0 and status=1 order by dispyear,month ASC");
      //   $row = json_decode( json_encode($dispatch), true);
      // // print_r( $row);exit;
      //    $sap = Session::get('sapitemid');
      //    $ret = Session::get('retailitemid');
      //    $siam = Session::get('siamitemid');
      //    $con = Session::get('consoliitemid');
      //   $data = array();
      //   if(!empty($row)){
      //     foreach ($row as $key => $value) {
      //      $area = $value['area'];
      //      $dispyear = $value['dispyear'];
      //      $month = $value['month'];
      //      $type_arr = array("SAP Despatch","Retails","SIAM");
      // //    echo  "SELECT concat(FORMAT(DATEFROMPARTS(1900, month, 1), 'MMM', 'en-US'),'-',FORMAT(DATEFROMPARTS(dispyear, 1, 1), 'yy ', 'en-US')) as month from datadispatch where  status=1 and month='".$month."' and dispyear='".$dispyear."' and type in('SAP Despatch','Retails','SIAM','Consolidated')";exit;
      //       $dis = DB::SELECT("SELECT concat(FORMAT(DATEFROMPARTS(1900, month, 1), 'MMM', 'en-US'),'-',FORMAT(DATEFROMPARTS(dispyear, 1, 1), 'yy ', 'en-US')) as month from datadispatch where  status=1 and month='".$month."' and dispyear='".$dispyear."' and type in($sap,$ret,$siam,$con)");
      
      //       if(count($dis) < 4){
      //           $data =array("disable"=>0);
      //       }else{
      //            $data = array("disable"=>1,'dt'=>$dis);
      //          // $data = array("disable"=>1,'dt'=>$dis);
      //       }
      //     }

      //    }else{
      //      array_push($data,array("disable"=>0));
      //    }
       //print_r($data);exit;
	$generate_status = TownwiseData::where('generate', 0)->count();

       //$data = Session::get('generate_enable');
      // print( $data);exit;
       if($generate_status == 1){
        return array("enable"=>1);
       }else{
        return array("enable"=>0);
       }



      }
	/*==========================================================
    Function: storageSave
    Description: To store file in client server
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
      public static function genrateTownwise(Request $request){

      $time  = date('Y-m-d h:i:s');
        // $sql = DB::select('select id from datadispatch where status=3');
        // $data = json_decode( json_encode($sql), true);
        // $dispid =array();
     
        //  $dispatchid = array_column($data,'id');
        //   DB::table('datadispatch_dt')->whereIn('ddid',$dispatchid)->delete();
        //   DB::table('datadispatch')->whereIn('id',$dispatchid)->delete();
            
             // $datadispatchdet = datadispatchdet::where('ddid',$id);
             // $datadispatchdet->delete();
             // $datadispatch = datadispatch::where('ddid',$id);
             // $datadispatch->delete();
         
         $sql = DB::select('select id,area from datadispatch where generate=2 and status=1');
         $data = json_decode( json_encode($sql), true);
         $area_arr = array_column($data,'area');
 
      DB::statement('EXEC SP_Generate');
     
    // echo 'd';exit;
  $emailcontroller   =   new EmailController;
 /*AM User – Latest Townwise file is available for download*/
 if(!empty($area_arr)){
 foreach ($area_arr as $key => $value) {
       $userdt = DB::select("select email,name from users where area='".$value."' and role='3'");
       if(!empty($userdt)){
        $miseremail = $userdt[0]->email ;
        $user_name = $userdt[0]->name ;
        $subject =  'Latest Townwise data is ready';
       
        $view = 'pages.email.generateemail';
        $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>$value,'month'=>'','name'=>$user_name,'view'=>$view);
        $emailcontroller ->sendEmail($args);
        
       }
     }
        
 }
    
 /*end AM User – Latest Townwise file is available for download*/
 /*HO Admin – Latest Townwise file is available for download*/
  $userdt = DB::select("select email,name from users where  role='1'");
       if(!empty($userdt)){
        $miseremail = $userdt[0]->email ;
        $user_name = $userdt[0]->name ;
         $subject =  'Latest Townwise data is ready';
       
        $view = 'pages.email.generateemail';
        $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>'','month'=>'','name'=>$user_name,'view'=>$view);
        $emailcontroller->sendEmail($args);
        
       }
       
 /* end HO Admin – Latest Townwise file is available for download*/

 //  /*HO User – Latest Townwise file is available for download*/
 //  // $userdt = DB::select("select email,a.area from users u join area a on a.id=u.area where  role='4' and u.status=1 and a.status=1");
            
 // /* end HO Admin – Latest Townwise file is available for download*/
 //  Session::put('generate_enable',0);
 //  $row  =  DB::table('masters')->where('metakey','generate_enable')->update(["metavalue" =>0,]);
  return array('title'=>'Success!','messege'=>'Generated','type'=>'success');


        	
           
        }
    // /*==========================================================
    // Function: storageSave
    // Description: To store file in client server
    // Author:
    // Created Date: 07-01-2020
    // Modification: 
    // ==========================================================*/
    //    public static function genrateTownwise(Request $request){
    //      $time  = date('Y-m-d h:i:s');
    //      DB::table('datadispatch')->where('status',1)->update(['updated_at'=>$time,'generate'=>1]);
    //      return array('title'=>'Success!','messege'=>'Generated','type'=>'success');
    //   }

      /*==========================================================
    Function: getDetails
    Description: To get details
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
        public static function getDetails(Request $request){
         $id =  DB::SELECT('SELECT MAX(updated_at) udate from datadispatch where generate=1 and status=1');
        
          $row = json_decode( json_encode($id), true);
       return $row[0]['udate'];
         
        }
   /*==========================================================
    Function: getProducts
    Description: To get details from datagenerate
    Author:
    Created Date: 27-01-2020
    Modification: 
    ==========================================================*/
        public static function getProduct(Request $request){
        $type = $request->get('type');
        $groubby =$typecond ='';
        if($type == 'category'){
           
          $sql = DB::SELECT("SELECT DDI_DISP_TEXT as val,DDI_ID as id from product p join DD_ITEMS i on p.category =i.DDI_ID join DD_CATEGORIES c on c.DDI_CATG_ID=i.DDI_CATG_ID where p.status=1 and c.DDI_CATG_NAME='CATEGORY_PRODUCT' group by DDI_DISP_TEXT,DDI_ID");

         

        }else if($type == 'segment'){
         
         $sql = DB::SELECT("SELECT DDI_DISP_TEXT as val,DDI_ID as id from product p join DD_ITEMS i on p.segment =i.DDI_ID join DD_CATEGORIES c on c.DDI_CATG_ID=i.DDI_CATG_ID where p.status=1 and c.DDI_CATG_NAME='SEGMENT' group by DDI_DISP_TEXT,DDI_ID");


        }else if($type == 'brand'){
         
         $sql = DB::SELECT("SELECT brand as val,id from brand where status=1");

        }else if($type == 'manufacturer'){
         
         $sql = DB::SELECT("SELECT manufacturer as val,id from manufacture where status=1");

        }
    // echo "SELECT $select as val from manufacture m join brand b on b.mftid=b.id join product p on p.brandid=b.id   where m.status=1 and p.status=1 and b.status=1";exit;
        
        
          $row = json_decode( json_encode($sql), true);
         return $row;
       
       
        }


    /*==========================================================
    Function: getArea
    Description: To get area
    Author:
    Created Date: 27-01-2020
    Modification: 
    ==========================================================*/
        public static function getMarket(Request $request){

        $type = $request->get('type');
        $area = $request->get('areaid');
        $user_role = $request->get('user_role');
        if($area==''){
           $area= Session::get('areaid');
           $user_role = Session::get('user_role');
        }
        if($user_role == 1){
          //echo "SELECT $type as val,".$type.".id from $type where status=1 order by $type ";exit;
           $sql = DB::SELECT("SELECT $type as val,".$type.".id from $type where status=1 order by $type ");

        }else{
          $area_cond =$area_cond1='';
          if(!empty($area)){
            $area_cond = " and areaid in ($area)";
            $area_cond1 = " and a.id in($area)";
          }

           if($type == 'Town'){
            $sql = DB::SELECT("SELECT town as val,$type.id from $type where status=1 $area_cond order by town ASC");
           }else if($type == 'Area'){
           // echo "SELECT area as val from $type as a where status=1 $area_cond1 order by area ASC";exit;
             $sql = DB::SELECT("SELECT area as val, a.id from $type as a where status=1 $area_cond1 order by area ASC");
           } else if($type == 'District'){
            $sql = DB::SELECT("SELECT district as val,$type.id from $type where status=1 $area_cond order by district ASC");
           }else if($type == 'State'){
            $sql = DB::SELECT("SELECT state as val,s.id from $type s join area a on s.id=a.stateid where s.status=1 $area_cond1 group by s.state,s.id order by state ASC");
           }else if($type == 'Zone'){
            //echo "SELECT zone as val from $type z join area a on z.id=a.zoneid where z.status=1 and a.id in($area) group by z.zone";exit;
            $sql = DB::SELECT("SELECT zone as val,z.id from $type z join area a on z.id=a.zoneid where z.status=1 $area_cond1 group by z.zone,z.id order by zone ASC");
           }

//print_r($sql);exit;
        }
     
        $row = json_decode( json_encode($sql), true);
       return $row;
        }
  /*==========================================================
    Function: downloadData
    Description: To data download
    Author: 
    Created Date: 27-01-2020
    Modification: 
    ==========================================================*/
        public static function downloadData(Request $request){
           $data = $request->all();
               //print_r($input);exit;
                $uid  = $request->get('user_id');
                $fid  = $request->get('finacial_id');
                $user_role  = $request->get('user_role');
                 $area  = $request->get('area');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
                 $user_role = Session::get('user_role');
                 $area = Session::get('area');
               }
        $from  = $data['sub_from'];
        $mdata  = explode("-",$from);
        $month = $mdata[0];
        $fmonth = date("n", strtotime($month));
        $fyear = '20'.$mdata[1];
        $from_cond = $fyear.$fmonth;
//print_r($mdata[1]);exit;
        $to  = $data['sub_to'];
        $mdata  = explode("-",$to);
        $tomonth = $mdata[0];
        $tomonth = date("n", strtotime($tomonth));
        $toyear = '20'.$mdata[1];
        $to_cond = $toyear.$tomonth;
        $market = $request->get('market');
        $type = $request->get('type');
        $product = $request->get('product');
        $producttype = $request->get('producttype');
        $markettype = $request->get('markettype');
        $market = implode ( ",", $market );
        $product = implode ( ",", $product );
        $count = $request->get('count');
        //print_r($markettype);exit;
        $array = array("market"=>$market,"mtype"=>$markettype,"ptype"=>$producttype,"product"=>$product,'from'=>$from_cond,'to'=>$to_cond,'type'=>$type,'user_role'=> $user_role,'area'=>$area);
        $category  = "File Uploaded";
        $homecontroller   =   new HomeController;
        $note   = 'Downloaded '.$count.' rows of Townwise data';
        $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);

        //return Excel::download(new FinalOutputExport($array), 'Final_Report_Output.xlsx');

        $townwisedata = self::townDataGenerator($array); 
               
        return (new FastExcel($townwisedata))->download('Final_Report_Output.xlsx');
        
         // return FastExcel::data($townwisedata)->download('Final_Report_Output.xlsx');
        }


        public static function townDataGenerator($data = array()){          
          $query = TownwiseData::query(); 

          $from = $data['from']; 
          $to = $data['to'];
          $markettype = $data['mtype'];
          $market = explode(',', $data['market']);          
          $ptype = $data['ptype'];
          $product = explode(',', $data['product']);
          $from = $data['from']; 
          $to = $data['to'];
          $type = $data['type'];
          $user_role = $data['user_role'];
          $area = $data['area'];
          $market_cond = '';
          $product_cond = '';         
          $market_cond = '';
          $product_cond = '';       
          
           
          if($markettype == 'Town'){
            $query->whereIn('TOWN_ID', $market);
          }else if($markettype == 'Area'){
            $query->whereIn('AREA_ID', $market);
          } else if($markettype == 'District'){
            $query->whereIn('DIST_ID', $market);
          }else if($markettype == 'State'){
            $query->whereIn('STATE_ID', $market);
          }else if($markettype == 'Zone'){
            $query->whereIn('ZONE_ID', $market);
          }
                  
          if($ptype == 'category'){
            $query->whereIn('PRO_CATG_ID', $product);
          }else if($ptype == 'segment'){
            $query->whereIn('SEGMENT_ID', $product);
          }else if($ptype == 'brand'){
            $query->whereIn('BRAND_ID', $product);
          }else if($ptype=='manufacturer'){
            $query->whereIn('MANF_ID', $product);
          }        
       
          if($market_cond == ''){
            if($user_role != 1){
              if(!empty($area)){
                $query->where('AREA', "'".$area."'");
              }
            }
          }

         /* if($type == ''){
            $type_cond ="Despatch,Retail";
          }else if($type == 'Despatch'){
            $type_cond ="Despatch";
          }else if($type == 'Retail'){
            $type_cond ="Retail";
          }*/

          $query->whereBetween('MONTY_YEAR', [$from, $to]);
          //$query->select('ZONE','STATE','AREA','DIST','TOWN','TOWN_CLASS','MANF','PROD_CATG','SEGMENT','BRAND','MONTH_TAG','MONTH_TXT','DISPATCH_QTY','RETAIL_QTY', 'SUBSEGMENT');
          $query->select('ZONE','STATE','AREA','DIST','TOWN','TOWN_CLASS','MANF','PROD_CATG','SEGMENT','SUBSEGMENT','BRAND','MONTH_TAG','MONTH_TXT','DISPATCH_QTY','RETAIL_QTY');
          $query->take(999999);
          $final_query = $query->cursor();

          //$final_query = $query->count();         
          //echo $test->toSql();
        
         foreach($final_query as $towndata) {
            yield $towndata;
          }
        }

           /*==========================================================
    Function: getTownwiseData
    Description: To display data
    Author: 
    Created Date: 25-02-2020
    Modification: 
    ==========================================================*/
        public static function getTownwiseDataCount(Request $request){
          $data = $request->all();
         // print_r($data);exit;
            //  $data = $input['params'];
            //  print_r($input);exit;
                // $uid  = $data['user_id'];
                // $fid  =$data['finacial_id'];
                // $user_role  = $data['user_role'];
                // $area  =$data['area'];
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
                 $user_role = Session::get('user_role');
                 $area = Session::get('area');
               }
              $from  = $data['sub_from'];
        $mdata  = explode("-",$from);
        $month = $mdata[0];
        $fmonth = date("n", strtotime($month));
        $fyear = '20'.$mdata[1];
        $from_cond = $fyear.$fmonth;

//print_r($mdata[1]);exit;
        $to  = $data['sub_to'];
        $mdata  = explode("-",$to);
        $tomonth = $mdata[0];
        $tomonth = date("n", strtotime($tomonth));
        $toyear = '20'.$mdata[1];
        $to_cond = $toyear.$tomonth;
        $market = $data['market'];
        $product = $data['product'];
        $ptype = $data['producttype'];
        $markettype = $data['markettype'];

         // $market = "'" . implode ( "','", $market_arr ) . "'";
         //  $product = "'" . implode ( "','", $product_arr ) . "'";
        
        //print_r( $market);exit;
          $market_cond = '';
          $product_cond = '';
          
          if($markettype == 'Town'){
            $market_cond = " and   TOWN_ID in ($market)";
           }else if($markettype == 'Area'){
            $market_cond = "  and AREA_ID in ($market)";
           } else if($markettype == 'District'){
            $market_cond = " and DIST_ID in ($market)";
           }else if($markettype == 'State'){
            $market_cond = " and STATE_ID in ($market)";
           }else if($markettype == 'Zone'){
          $market_cond = " and ZONE_ID in ($market)";
           }
       
           
           if($ptype == 'category'){
           $product_cond = " and PRO_CATG_ID in ($product)";
           }else if($ptype == 'segment'){
            $product_cond = " and SEGMENT_ID in ($product)";
           }else if($ptype == 'brand'){
            $product_cond = " and BRAND_ID in ($product)";
            }else if($ptype=='manufacturer'){
            $product_cond = " and MANF_ID in ($product)";
            }
         $sap = Session::get('sapitemid');
         $ret = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $con = Session::get('consoliitemid');
         //print_r($market_cond);exit;
          //  echo date('m', strtotime($to));exit;
         $month_cond ="MONTH_TAG  between '".$from_cond."' and '".$to_cond."'" ;

          $new_year_month_condition ="MONTY_YEAR  between '".$fyear.$fmonth."' and '".$toyear.$tomonth."'" ;
       
        if($market_cond == ''){
          if($user_role != 1){
            if(!empty($area)){
            $market_cond = "and area='".$area."'";
            }
          }

        }
     
//echo 'SELECT COUNT(*) AS val FROM (SELECT  * from downloaddata where '.$month_cond.$market_cond.$product_cond.') t PIVOT(sum(value) FOR type IN ("Retails","Consolidated","SAP Despatch","SIAM")) AS pivot_table ';exit;
        //$sqlcount =  DB::Select('SELECT COUNT(*) AS val FROM (SELECT  * from Townwise_Data where '.$new_year_month_condition.$market_cond.$product_cond.')');

          $sqlcount =  DB::Select('SELECT count(id) AS val from Townwise_Data where  '.$new_year_month_condition.$market_cond.$product_cond);


        $rowcount = json_decode( json_encode($sqlcount), true);
       
        return array('count'=>$rowcount);

      }
               /*==========================================================
    Function: getTownwiseData
    Description: To display data
    Author: 
    Created Date: 25-02-2020
    Modification: 
    ==========================================================*/
        public static function getTownwiseData(Request $request){            
              $data = $request->all();
         //  print_r($data);exit;
            //  $data = $input['params'];
            //  print_r($input);exit;
                // $uid  = $data['user_id'];
                // $fid  =$data['finacial_id'];
                // $user_role  = $data['user_role'];
                // $area  =$data['area'];
                 if(empty($uid)){
                   $uid = Session::get('user_id');
                   $fid = Session::get('finacial_id');
                   $user_role = Session::get('user_role');
                   $area = Session::get('area');
                 }

                $query = TownwiseData::query();

                $from  = $data['sub_from'];
                $mdata  = explode("-",$from);
                $month = $mdata[0];
                $fmonth = date("n", strtotime($month)); 
                $fyear = '20'.$mdata[1]; 
                $from_cond = $fyear.$fmonth;
        //print_r($mdata[1]);exit;
                $to  = $data['sub_to'];
                $mdata  = explode("-",$to);
                $tomonth = $mdata[0];
                $tomonth = date("n", strtotime($tomonth));
                $toyear = '20'.$mdata[1];
                $to_cond = $toyear.$tomonth;
                $market_array = $data['market'];
                $product_array = $data['product'];

                /*$market_implode = implode( ",", $data['market']);
                $product_implode = implode( ",", $data['product']);
                $market = explode( ",", $market_implode );
                $product = explode( ",", $product_implode );*/

                $market = explode(",",$data['market']);
                $product = explode(",",$data['product']);

                $ptype = $data['producttype'];
                $markettype = $data['markettype'];

         // $market = "'" . implode ( "','", $market_arr ) . "'";
         //  $product = "'" . implode ( "','", $product_arr ) . "'";
        
        //print_r( $market);exit;
          $market_cond = '';
          $product_cond = '';
          
          if($markettype == 'Town'){
            $query->whereIn('TOWN_ID', $market);
          }else if($markettype == 'Area'){
            $query->whereIn('AREA_ID', $market);
          } else if($markettype == 'District'){
            $query->whereIn('DIST_ID', $market);
          }else if($markettype == 'State'){
            $query->whereIn('STATE_ID', $market);
          }else if($markettype == 'Zone'){
            $query->whereIn('ZONE_ID', $market);
          }
                  
          if($ptype == 'category'){
            $query->whereIn('PRO_CATG_ID', $product);
          }else if($ptype == 'segment'){
            $query->whereIn('SEGMENT_ID', $product);
          }else if($ptype == 'brand'){
            $query->whereIn('BRAND_ID', $product);
          }else if($ptype=='manufacturer'){
            $query->whereIn('MANF_ID', $product);
          }        
       
          if($market_cond == ''){
            if($user_role != 1){
              if(!empty($area)){
                $query->where('AREA', "'".$area."'");
              }
            }
          }

         $sap = Session::get('sapitemid');
         $ret = Session::get('retailitemid');
         $siam = Session::get('siamitemid');
         $con = Session::get('consoliitemid');
         //print_r($market_cond);exit;
          //  echo date('m', strtotime($to));exit;
        //$month_cond ="MONTH_TAG  between '".$from_cond."' and '".$to_cond."'" ;

        //$new_year_month_condition ="MONTY_YEAR  between '".$fyear.$fmonth."' and '".$toyear.$tomonth."'" ;
       

//$townwisecount = TownwiseData::count();
        
     
          // $sql =  DB::Select('SELECT TOP 10 *,ISNULL("Consolidated",0)+ISNULL("SAP Despatch",0)+ISNULL("SIAM",0) Despatch,ISNULL(Retails, (ISNULL("Retails",0)+ISNULL("Consolidated",0)+ISNULL("SAP Despatch",0)+ISNULL("SIAM",0))) Retail FROM (SELECT   * from downloaddata where  '.$month_cond.$market_cond.$product_cond.' ) t PIVOT(sum(value) FOR type IN ("Retails","Consolidated","SAP Despatch","SIAM")) AS pivot_table ORDER BY month,zone,state,district,area,town,manufacturer,category,segment asc');

//echo 'SELECT * from Townwise_Data where  '.$new_year_month_condition.$market_cond.$product_cond; die;

          //$sql =  DB::Select('SELECT  TOP 10 * from Townwise_Data where  '.$new_year_month_condition.$market_cond.$product_cond);
        

          $query->select('ZONE','STATE','AREA','DIST','TOWN','TOWN_CLASS','MANF','PROD_CATG','SEGMENT','SUBSEGMENT','BRAND','MONTH_TAG','MONTH_TXT','DISPATCH_QTY','RETAIL_QTY');
          $query->whereBetween('MONTY_YEAR', [$fyear.$fmonth, $toyear.$tomonth]);         
          $townwiseFilterData = $query->take(10)->get();
          //$townwiseTotalCount = $query->count();
          //, monthname(str_to_date('MONTH_TAG', '%m')) as 'MonthName'
        //var_dump($townwiseFilterData); die;
        // print_r($sql);exit;
          $townDataRow = json_decode( json_encode($townwiseFilterData), true);
         // $townDataCount = json_decode( json_encode($townwiseTotalCount), true);
         // print_r( $row);
         // exit;


          return array("valuelist"=> $townDataRow,);
        //  return $row;
    
      }
   


}
?>