<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;
use App\Imports\ConsolidateImport;
use App\Exports\ConsolidateTemplateExport;
use App\datadispatchdet;
use App\datadispatch;
use App\uploads;
use Mail;
use Illuminate\Support\Facades\Crypt;
class UsersUploadController  extends Controller
{
 /*==========================================================
    Function: checkUpload
    Description: check uploading is done for month or not 
    Author:
    Created Date: 10-01-2020
    Modification: 
    ======================================================*/
    public static function checkUpload(Request $request){
     // check upload status of masters- added by chethan on 17-02-2020
      $upload_enable = Session::get('upload_enable');
      if($upload_enable == 'Yes'){
        $row  =  "Enabled Upload";
      }
      else if($upload_enable == 'No'){
         $row  =  "Disabled Upload";
      }
      else{
      	$dtype = $request->get('type');
      	$month = $request->get('month');
      	$mdata  = explode("-",$month);
        $month = trim($mdata[0]);
        $year = '20'.$mdata[1];
        //$month = date("m", strtotime($month));
         $date = date_parse($month);
         $month =  $date['month'];
        // print_r( $month);exit;
       	$areaid = $request->get('areaid');
        if($dtype == 'Consolidated'){
          $type = Session::get('consoliitemid');
        }else{
        $type = Session::get('rawitemid');
        }
      //  echo "SELECT * from datadispatch where type='".$type."' and month='".$month."' and year='".$year."' and area='".$areaid."'";exit; 
      	$sql = DB::SELECT("SELECT * from datadispatch where type='".$type."' and month='".$month."' and dispyear='".$year."' and area='".$areaid."' and status != '2'");
      	$row = json_decode( json_encode($sql), true);
        if(!empty($row)){
          $row = "Already Uploaded";
        }
      }
        return $row;
    }
    /*==========================================================
    Function: savePdplan
    Description: To upload  pdplan 
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
        public static function saveCompetition(Request $request){
              $input = $request->all();
             // /  print_r($input);exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
                $user_role  = $request->get('user_role');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
                 $user_role = Session::get('user_role');
               }
             $date  = $request->get('date');
             $homecontroller   =   new HomeController;
             $MasterSettings   =   new MasterSettings;
             $uploadscontroller   =   new AdminUploadsController;
             $fy        =   $homecontroller->getFinacilaYear();
            //get array from file
           $month  = $request->get('month');
           $area  = $request->get('area');
           $areaname  = $request->get('areaname');
                 
           $cdate  = date('Y-m-d');
           $time   = date('Y-m-d h:i:s');
           $mdata  = explode("-",$month);
           $month = $mdata[0];
             $date = date_parse($month);
           $month =  $date['month'];
           $year = '20'.$mdata[1];
          
           $files   = request()->file('files');
           //print_r($files);exit;
            //to store file in server
            $upcontroller   =   new UploadsController;
            $filecnt =  count($files);
            $destinationPath = base_path().'/storage/app/'.$fy.'/'.$month.'/'.$areaname;
             /*if(file_exists($destinationPath)){
             File::deleteDirectory($destinationPath);
              }*/
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid =  Session::get('rawcatid');
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $rawcatid = Session::get('rawitemid');
       //  print_r($sapcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('Raw Data',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uptype,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Raw Data','uid'=> $uid));
           
           }

           if(empty($rawcatid)){
            $string = strtoupper(substr('Raw Data',0,8));
         //   print_r(expression)
             $rawcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Raw Data','uid'=> $uid));
          
            }
            foreach ($files as $key => $file) {
            	//print_r($areaname);exit;
             $filename = $file->getClientOriginalName();
              $path     =   $fy.'/'.$month.'/'.$areaname.'/'.$filename;
             $destinationPath = base_path().'/storage/app/'.$fy.'/'.$month.'/'.$areaname.'/'.$filename;
             if(file_exists($destinationPath)){
               $path     =   $fy.'/'.$month.'/'.$areaname.'/'.date('d-m-y').'_'.$filename;
               }
            
             $storage  = $upcontroller->storageSave($file,$month,$path);
             $file_arr[] = $filename;
            }
           //print_r($file_arr);exit;
            $filenames = implode(",",$file_arr);
          //  print_r(  $filenames );exit;

             $path     =   $fy.'/'.$month.'/'.$areaname;
          
             //get dispatch data for the month
            $discond  = array('type'=>$rawcatid,'month'=>$month,'status'=>1,'year'=>$year,'area'=>$area);
            $disvalue = $uploadscontroller->getDispatch($discond);
          // print_r($disvalue);exit;
           if(empty($disvalue)){

                //uploads save
                $uparray   = array('filename'=>$filenames,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
                $newuploads = $upcontroller->saveUplaods($uparray);

                //dispatch save
                $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$rawcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'countfile'=>$filecnt,'area'=>$area);
                $dispid   =  $uploadscontroller->saveDispatch($disarray);

            }else{

               $up_id = $disvalue[0]['uploadid'];
               $dispid = $disvalue[0]['id'];
               $countfile = $disvalue[0]['countfile'];
               $fname  =uploads::where('id',$up_id)->value('filename');
                //print_r($fname);exit;

               //print_r( $filenames);exit;
             
               $finalfile = $filenames.','.$fname;
               $countfile = $countfile+$filecnt;

              //echo $dispid.'asdf';exit;

              //  print_r($countfile);exit;
                DB::table('uploads')
                ->where('id',$up_id)
                ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename'=> $finalfile]);

               

                DB::table('datadispatch')->where('id',$dispid)->update(['updated_at'=>$time,'countfile'=>$countfile]);
            }
           
          
            
             
              $category  = "File Uploaded";
              $homecontroller   =   new HomeController;
              $note   = 'Uploaded Competition Raw data';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              
              
               return array('title'=>'Success!','messege'=>'Raw Data Uploaded Successfully','type'=>'success');
           } 

/*==========================================================
    Function: DeleteRaw
    Description: To delete raw file 
    Author:
    Created Date: 31-01-2020
    Modification: 
    ==========================================================*/
        public static function DeleteRaw(Request $request){
         // print_r($request->all());exit;
          $filename = $request->get('fname');
          $dispatchid = $request->get('dispatchid');
          $time   = date('Y-m-d h:i:s');
          $fcount = $request->get('fcount');
          $link = $request->get('link');
          $fid = Session::get('finacial_id');
          $uid = Session::get('user_id');
          $fcount =  $fcount - 1;
          $filesname = $request->get('filename');
        
           // print_r($parts);exit;
            while(($i = array_search($filename, $filesname)) !== false) {
                unset($filesname[$i]);
            }

           $file  =  implode(',', $filesname);
         
          $upid = datadispatch::where('id',$dispatchid)->value('uploadid');
        //  print_r( $upid);exit;
          $destinationPath = base_path().'/storage/app/'.$link.'/'.$filename.'.xlsx';
          if(file_exists($destinationPath)){
            //print_r($destinationPath);exit;
            unlink($destinationPath);
             }

  //unlink($destinationPath);
            if($fcount == 0){$status =2;}else{$status =1;}
             $val =DB::table('datadispatch')->where('id',$dispatchid)->update(['updated_at'=>$time,'countfile'=>$fcount]);
             $val =DB::table('uploads')->where('id',$upid)->update(['filename'=>$file]);
              $category  = "File Deleted";
              $homecontroller   =   new HomeController;
              $note   = 'Deleted Competition Raw file';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              
          return $val;


        }
 /*==========================================================
    Function: rowStatus
    Description: To display status raw data 
    Author:
    Created Date: 10-01-2020
    Modification: 
    ==========================================================*/
        public static function rowStatus(Request $request){
        	//print_r($request->all());exit;
        	$fy = Session::get('finacial_year');
        	$fyto = $fy + 1;
          
          $raw = Session::get('rawitemid');
        	//print_r($fyto);exit;
        	$area = $request->get('areaid');
          $year = $request->get('year');
          $fyto = $year + 1;
        	$from = $year.'-04'.'-01';
        	$to   = $fyto.'-03'.'-31';
        	$type = $request->get('type');
        	$details = array();
        //	echo "SELECT  month,countfile  FROM datadispatch where type='".$type."'  and convert(date, cast(year*10000 + month*100 + 1 as varchar(8)), 112) > '".$from."' AND convert(date, cast(year*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' group by month,countfile";exit;
        	$sql = DB::select("SELECT  month,countfile  FROM datadispatch where type=$raw  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) >= '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' and status=1 group by month,countfile");
        	$row = json_decode( json_encode($sql), true);
        	//print_r($row);
        	$montharray = array('1'=>'JAN','2'=>'FEB','3'=>'MAR','4'=>'APR','5'=>'MAY','6'=>'JUN','7'=>'JUL','8'=>'AUG','9'=>'SEP','10'=>'OCT','11'=>'NOV','12'=>'DEC');
			
			$retval = array();

        	foreach ($montharray  as $key => $value) {
        		if(array_search($key, array_column($row, 'month')) !== false) {
        			$rowkey = array_search($key, array_column($row, 'month'));//exit;
        			array_push($retval,array('count'=> $row[$rowkey]['countfile'],'month'=> $montharray[$key]));
					}
					else {
					   array_push($retval,array('count'=> 0,'month'=> $montharray[$key]));
					}
        	}

         	return $retval;



        	}
          /*==========================================================
    Function: consolidateStatus
    Description: To display status raw data 
    Author:
    Created Date: 10-01-2020
    Modification: 
    ==========================================================*/
        public static function consolidateStatus(Request $request){
          //print_r($request->all());exit;
          $fy = Session::get('finacial_year');
          $fyto = $fy + 1;
          //print_r($fyto);exit;
          $area = $request->get('areaid');
          $year = $request->get('year');
           //  $year = substr( $year, -2);
          $fyto = $year + 1;
          $from = $year.'-04'.'-01';
          $to   = $fyto.'-03'.'-31';
          $type = $request->get('type');
           $consolidated = Session::get('consoliitemid');
         //  print_r($consolidated);exit;
          $retval = array();
     //echo "SELECT  month,status  FROM datadispatch where type=$consolidated  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) > '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' and status!=3  group by month,status";exit;
         // echo "SELECT  month,status  FROM datadispatch where type=$consolidated  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) > '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' and status!=3  group by month,status";exit;
          //echo "SELECT  month,status  FROM datadispatch where type=$consolidated  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) > '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' and status!=3  group by month,status";exit;
          $sql = DB::select("SELECT  month,status  FROM datadispatch where type=$consolidated  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) >= '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) <= '".$to."' and area='".$area."' and status!=3  group by month,status");
          $row = json_decode( json_encode($sql), true);
          //print_r($row);
          $montharray = array('1'=>'JAN','2'=>'FEB','3'=>'MAR','4'=>'APR','5'=>'MAY','6'=>'JUN','7'=>'JUL','8'=>'AUG','9'=>'SEP','10'=>'OCT','11'=>'NOV','12'=>'DEC');

          
          foreach ($montharray  as $key => $value) {

            if(array_search($key, array_column($row, 'month')) !== false) {

              $rowkey = array_search($key, array_column($row, 'month'));//exit;
              array_push($retval,array('status'=> $row[$rowkey]['status'],'month'=> $montharray[$key]));
           }
          else {
              array_push($retval,array('status'=> '','month'=> $montharray[$key]));
            }
          }

          return $retval;



          }
           /*==========================================================
    Function: getrowmonthStatus
    Description: To display status raw data  details for perticular month
    Author:
    Created Date: 13-01-2020
    Modification: 
    ==========================================================*/
        public static function getrowmonthStatus(Request $request){
          //print_r($request->all());exit;
          $fy = Session::get('finacial_year');
          //print_r($fyto);exit;
          $area = $request->get('areaid');
          $month = $request->get('month');
          $year = $request->get('year');
          //print_r( $year);exit;
          $fyto = $year + 1;
          $from = $fy.'-04'.'-01';
          $to   = $fyto.'-03'.'-31';
          $type =  Session::get('rawitemid');;
             $rawcatid =   Session::get('rawcatid');;
          $details = array();
          $filearray = array();
         // echo "SELECT  month,countfile,link,filename,d.updated_at FROM datadispatch d join uploads u on d.uploadid=u.id  where type='".$type."'  and year='".$year."' and area='".$area."' and month='".$month."' group by month,countfile,uploadid,link,filename,d.updated_at";exit;
          $sql = DB::select("SELECT  month,countfile,link,filename,d.updated_at,d.id as dispatchid FROM datadispatch d join uploads u on d.uploadid=u.id  where type='".$type."' and u.category=$rawcatid  and dispyear='".$year."' and area='".$area."' and month='".$month."' and d.status=1 ");
          $row = json_decode( json_encode($sql), true);

          
        if(!empty($row)){
                     $filename =   $row[0]['filename'];
          $filename = explode(",",$filename);
          return array("filename"=>$filename,"countfile"=>$row[0]['countfile'],"date"=>$row[0]['updated_at'],'link'=>$row[0]['link'],"dispatchid"=>$row[0]['dispatchid']);
            }else{
              return;
            }
          }
             /*==========================================================
    Function: getrowmonthStatus
    Description: To display status consolidate data  details for perticular month
    Author:
    Created Date: 16-01-2020
    Modification: 
    ==========================================================*/
        public static function getconsolimonthStatus(Request $request){
          //print_r($request->all());exit;
          $fy = Session::get('finacial_year');
     
          //print_r($fyto);exit;
          $area = $request->get('areaid');
          $month = $request->get('month');
          $year = $request->get('year');
          $fyto = $year + 1;
          $from = $fy.'-04'.'-01';
          $to   = $fyto.'-03'.'-31';
          $type =   Session::get('consoliitemid');;
           $consolicatid =   Session::get('consolicatid');;
          $details = array();
          $filearray = array();
       //   echo "SELECT  month,link,filename,d.updated_at,approve_at,filename FROM datadispatch d join uploads u on d.uploadid=u.id  where type='".$type."'  and convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) > '".$from."' AND convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112) < '".$to."' and area='".$area."' and u.category=$consolicatid and month=$month";exit;
         // echo "SELECT  month,link,filename,d.updated_at,approve_at,filename FROM datadispatch d left join uploads u on d.uploadid=u.id  where type='".$type."'  and dispyear = '".$year."' and month=$month and area='".$area."' and u.category=$consolicatid and month=$month";exit;
          $sql = DB::select("SELECT  month,link,filename,d.updated_at,approve_at,filename FROM datadispatch d left join uploads u on d.uploadid=u.id  where type='".$type."'  and dispyear = '".$year."' and month=$month and area='".$area."' and u.category=$consolicatid and month=$month");
          $row = json_decode( json_encode($sql), true);         
        if(!empty($row)){
                   
          return array("filename"=>$row[0]['filename'],"date"=>$row[0]['updated_at'],'path'=>$row[0]['link'],"approve_at"=>$row[0]['approve_at']);
            }else{
              return;
            }
          }
/*==========================================================
  Function: ValidateCon
  Description: To validate Consolidated data upload
  Author:
  Created Date: 13-01-2020
  Modification: 
  ==========================================================*/
public static function ValidateCon(Request $request){
  $input = $request->all();
 //print_r($input);exit;
  $t_threshold  = $request->get('t_threshold');
  $m_threshold  = $request->get('m_threshold');
  $townlist  = $request->get('townlist');
  $uid  = $request->get('user_id');
  $brandNames  = $request->get('brandlist');


  //print_r(Session::get('m_threshold'));exit;
  if(empty($uid)){
    $t_threshold = Session::get('v_threshold');
    $m_threshold = Session::get('m_threshold');
    $townlist = Session::get('townlist');
    $brandNames = Session::get('brandlist');

  }
 
//print_r( $brandNames );exit;
 
 //print_r($townlist);exit;
  $Import = new ConsolidateImport(); 
  $ts = Excel::import( $Import,  request()->file('file'));
  $data = [];
  $headings = (new HeadingRowImport)->toArray(request()->file('file'));
  $month  = $request->get('month');
  $area  = $request->get('area');
  $mdata  = explode("-",$month);
  $month = $mdata[0];
  $date = date_parse($month);
  $month =  $date['month'];
  $year  = $mdata[1];
  $warningarray = array();
  $MasterSettings   =   new MasterSettings;
  $homecontroller   =   new HomeController;
  $UploadsSettings   =   new AdminUploadsController;
  $data = [];
  $error_array = array();
  $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
  $warindex = $skiploop = 0;
  //$skiploop = 1;
  $conarray = array();
  $newArray = $validate = array();
  $final_manufacture = array('Town','Towngroup');
  $final_arr = array();
  $brandlist=array();
  $town_arr[] = array();
  $dataarray = array_filter($dataarray);
  $trimarrvalue =array();
  
  $final_err = array();
   $concatid = Session::get('consoliitemid');
   
// Return an import object for every sheet
foreach ($Import->getSheetNames() as $index => $sheetName) {
   
$schemaerr  =  $branderr = $locerr = $blankerr = $wholenumerr = $sheettown=$sheetbrand=$brandlist=array();
$k = 0;
$data[$index] = $sheetName;
$manufact = self::getManufacture($sheetName);
$brandlist = [];
 //print_r($manufact);exit; 
if(!empty($manufact)){
 
  array_push($final_manufacture,$manufact[0]['manufacturer']);
  if(!empty($brandNames)){
 
 foreach ($brandNames as $key => $value) {
   if($value['mftid'] == $manufact[0]['id']){
    $brandlist[] = $value['brand'];
   }
 }
}
  // //print_r($brandNames);exit;
  // $brands =  DB::SELECT("SELECT d.brand from brand d  join product dt on dt.brandid=d.id  where mftid='".$manufact[0]['id']."' and d.status=1 and dt.status=1 group by d.brand");
  // $brands =  json_decode( json_encode($brands), true);
  // foreach ($brands as $key => $value) {
  //  array_push($brandlist,strtoupper(trim($value['brand'])));
  // }

  //brand header row validation
  if(!empty($headings[$index])){
    foreach ($headings[$index] as $key => $header){
    // print_r($header);
      if(!empty($data[$key])){
        $validate = $homecontroller->consolidateHeaderCheck($header, $sheetName);
    //   print_r(  $validate );exit;
        if(!isset($validate['success'])){
         // print_r( $validate );exit;
          foreach ($validate as $bkey => $value) {
            // array_push($error_array,array("error"=>$value['error']));
            array_push($branderr,array("error"=>$value['error']));
            
          }
        }
      }
    }
  }


//validate cell values  sheetwise loop
 $rowcount = 0;
 $m= 0;
 $rcnt = 2;
 
//print_r($townlist);exit;
    foreach ($dataarray[$index] as $key => $value) {
    //  echo 'd';exit;
    $j =  0;
    // print_r($value );
    $keys = array_map( 'trim', array_keys($value) );
    $value = array_combine( $keys, $value );
   // print_r($value );exit;
    if(!empty($value['Town'])){
      $district =   str_replace("'", "''", $value['Town Group']);
      $twn =   str_replace("'", "''", $value['Town']);

     // $townval = $MasterSettings->getTowndetails(array("town"=>$twn,"district"=>$district,"areaid"=>$area));
   //print_r(Session::get('townlist'));exit;
  // print_r( $townlist);exit;
    if(!in_array($twn, $townlist)){
       $column = 'A';
        //array_push($error_array,array( 'error'=>'Sheet '.$manufact[0]['manufacturer'].' - Town "'.$value['Town'].'" and Town Group "'.$value['Town Group'].'" is not found in the template'));
       array_push($schemaerr,array( 'error'=>'Sheet '.$manufact[0]['manufacturer'].' - Town "'.$value['Town'].'" in Town Group "'.$value['Town Group'].'" is not found in the template')); 
    }
     
    array_push($sheettown,$twn);
   
    //echo 'ss';exit;
    foreach ($value as $vkey => $vvalue) {
    if(is_string($vkey)){
      $cell = $j;
      $towncol = $k+2;
      //skip town and towngroup column from integer check
    
      if(trim($vkey) != 'Town' && $vkey != 'Town Group' && !empty($vkey)){
         //whole number check 
        if(!is_int($vvalue) && !empty($vvalue) ){
              $column =  $UploadsSettings->getAlphabest($cell);
              array_push($wholenumerr,array("error"=>'Sheet '.$manufact[0]['manufacturer'].' - Cell '.$column.$rcnt.' should be a whole number'));
               
               }   //check value is empty
        else if(!isset($vvalue) ){
                $column =  $UploadsSettings->getAlphabest($cell);
                array_push($blankerr,array("error"=>'Sheet '.$manufact[0]['manufacturer'].' - Cell '.$column.$rcnt.' cannot be left blank'));
              
             }
       
         $b_id  = DB::SELECT("SELECT p.id from brand p join manufacture pd on pd.id=p.mftid where manufacturer='".$manufact[0]['manufacturer']."' and brand='".$vkey."'");
         if(!empty($b_id)){
         $brand_id = $b_id[0]->id;
         }

     if(!empty( $brand_id)){
       // print_r( $brand_id);exit;
        $sum = array_sum(array_column($dataarray[$index], $vkey));//get total value of each brand in all town
        //check value greater than threshold value ex. 50 should check once in a loop---------------------------------------------------
       if($rowcount == 0){
        array_push($sheetbrand,strtoupper(trim($vkey)));
        //check for variance threshold ex. 200%
        $discond  = array('type'=> $concatid,'area'=> $area,'product'=>$brand_id,'month'=>$month,'status'=>'','year'=>$year);
        //echo "SELECT AVG(a.avg) AS sum from (SELECT sum(value) from threshold_data where  type=$concatid  and productid=$brand_id and area=$area group by month)";exit;
        $sql = DB::SELECT("SELECT AVG(a.avg) AS sum from (SELECT sum(value) as avg from threshold_data where  type=$concatid  and productid=$brand_id and area=$area group by month) as a");
        //$disvalue = $UploadsSettings->checkThreshold($discond);
        $disvalue =  json_decode( json_encode($sql), true);
    
        $olddispatch =  $percentage = '';


        if(!empty($disvalue[0]['sum'])){
          // / print_r($disvalue);exit;
          if($disvalue[0]['sum'] > $m_threshold){
          $olddispatch = $disvalue[0]['sum'];//1607
      
          $percentage = ($olddispatch * $t_threshold)/100;//6428
           // print_r( $disvalue);exit;
          $gtval = $percentage + $olddispatch;//
          $lsval = $olddispatch - $percentage  ;
 //print_r( $percentage );exit;
          if($sum > $gtval){
            $column =  $UploadsSettings->getAlphabest($cell);
            // $war = 'Cell'.$column.$cell.': Value is more than 200% of previous 6 month value';
            $war = $manufact[0]['manufacturer'].' '.$vkey.': total value of '.$sum.' is greater than '.$t_threshold.'% of previous 6 month average value of '.$olddispatch;
            array_push($warningarray,array("index"=>$key,"warning"=>$war));
          }
          else if($sum < $lsval){
            $column =  $UploadsSettings->getAlphabest($cell);
            //$war = 'Cell'.$column.$cell.': Value is less than 200% of previous 6 month value';
            $war = $manufact[0]['manufacturer'].' '.$vkey.': total value of '.$sum.' is lesser than '.$t_threshold.'% of previous 6 month average value of '.$olddispatch;
            array_push($warningarray,array("index"=>$key,"warning"=>$war));
          }
         }
        }
      }
         }   
        //end of oncechek loop--------------------------------------------------------------------------------------
         //print_r($sheetbrand);exit;

}

        // print_r($warningarray);
      //end of town/town group column skip loop
     //print_r($sheetbrand);exit;
   }//end of empty value check loop

    $j++;

 
    } //end of foreach of each row of a sheet

 $rcnt ++;
     $rowcount++;
   // exit;
    $k++;
    $m++;
    } //end of each sheet loop 
   
//print_r($town_arr);exit;
}//end of manufacture empty check
//if manufacture empty
   
}
else{
 array_push($error_array,array("error"=>"Manufacturer '".$sheetName."' is not found in the template"));
} 

$result_brand=array_diff($brandlist,$sheetbrand);
// print_r($brandlist);
// echo 'dsdsd';
// print_r($sheetbrand);exit;
 //unset($sheetbrand); 
foreach ($result_brand as $key => $value) {
 array_push($schemaerr,array( 'error'=>'Sheet '.$manufact[0]['manufacturer'].' - Model "'.$value.'"  is not found in the uploaded data')); 
}


//print_r('ds');exit;
$result_town=array_diff($townlist,$sheettown);
foreach ($result_town as $key => $value) {
 array_push($schemaerr,array( 'error'=>'Sheet '.$manufact[0]['manufacturer'].' - Town "'.$value.'"  is not found in the uploaded data')); 
}
$res = array_merge($schemaerr,$branderr,$locerr,$blankerr,$wholenumerr);
//print_r($res);exit;
//array_push($final_err,$manufact[0]['manufacturer']);
if(!empty($res)){
array_push($final_err,array($manufact[0]['manufacturer']=>$res));
}

}//main foreach end 
//print_r($schemaerr);echo "<br/>";print_r($branderr);echo "<br/>";print_r($locerr);echo "<br/>";print_r($blankerr);echo "<br/>";print_r($wholenumerr);

/*echo '<pre>';
print_r($final_err);
echo '</pre>';exit;*/
if(!empty($final_err)){ $error_array = $final_err; }
// /print_r($error_array);exit;
if(empty($error_array)){

  $final_arr = self::getFinalarr($dataarray,$Import->getSheetNames());
   //print_r($final_arr);EXIT;
}

//asort($warningarray);
return array("warning"=>$warningarray,"error" => $error_array,'final_manufacture'=>$final_manufacture,'final_arr'=>$final_arr);

}
 /*==========================================================
    Function: getFinalarr
    Description: To save Consolidated data upload
    Author:
    Created Date: 14-01-2020
    Modification: 
    ==========================================================*/
        public static function getFinalarr($dataarr,$sheetarr){
            $final_arr =array();
          $data_arr=array();
              $trimarrvalue = array();

          foreach ($sheetarr as $key => $vvalue) {
            
           //print_r($vvalue);exit;
           // $sum = array_sum(array_column($dataarr[$key], $key));
            foreach ($dataarr[$key] as $vkey => $value) {
              array_walk_recursive($value, function($vkey, $value) use (&$trimarrvalue){
                $trimarrvalue[trim($value)] = trim($vkey);
              });
             // / print_r($value);exit;
              if(!empty($trimarrvalue['Town'])){
             // print_r( array_sum($value));exit;
            // array_push($final_arr, array("town"=>$value['Town'],"towngroup"=>$value['Town Group'],$vvalue=>array_sum($value)));
                $final_arr[$trimarrvalue['Town']][]=array("town"=>$trimarrvalue['Town'],"towngroup"=>$trimarrvalue['Town Group'],$vvalue=>array_sum($trimarrvalue),"brand"=>$vvalue);


             }
            }
          
            
          }
       /*   echo '<pre>';
          print_r($final_arr);
          echo'</pre>';exit;*/
          return $final_arr;

          }


         /*==========================================================
    Function: SaveConsolidate
    Description: To save Consolidated data upload
    Author:
    Created Date: 14-01-2020
    Modification: 
    ==========================================================*/
        public static function SaveConsolidate(Request $request){
               $input = $request->all();
             // print_r($input);exit;
               $t_threshold  = $request->get('t_threshold');
               $m_threshold  = $request->get('m_threshold');
               $townlist  = $request->get('townlist');
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               $email  = $request->get('email');
               $areaname  = $request->get('areaname');
               $user_role  = $request->get('user_role');
               $user_name  = $request->get('user_name');
               
                //print_r(Sessio;n::get('m_threshold'));exit;
               if(empty($uid)){
                 $t_threshold = Session::get('v_threshold');
                 $m_threshold = Session::get('m_threshold');
                 $townlist = Session::get('townlist');
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
                 $email = Session::get('email');
                 $areaname = Session::get('area');
                 $user_role = Session::get('user_role');
                 $user_name = Session::get('user_name');
          
               }
     
           $Import = new ConsolidateImport(); 
           $ts = Excel::import( $Import,  request()->file('file'));
           $data = [];
           $headings = (new HeadingRowImport)->toArray(request()->file('file'));
          // print_r( $headings);exit;
           $yearmonth  = $request->get('month');
           $area  = $request->get('area');
           $status  = $request->get('status');
           $mdata  = explode("-",$yearmonth);
           $month = $mdata[0];
           $date = date_parse($month);
           $month =  $date['month'];
           $year  = '20'.$mdata[1];
           $warningarray = array();
           $MasterSettings   =   new MasterSettings;
           $homecontroller   =   new HomeController;
           $UploadsSettings   =   new AdminUploadsController;
           $ApproveController   =   new ApproveController;
//print_r( $area);exit;
           $fy        =   $homecontroller->getFinacilaYear();
           $data = [];
           $wararray = array();
           $index  = json_decode($request->get('index'));
           $remark  = json_decode($request->get('remark'));
           $warning  = json_decode($request->get('warning'));
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:i:s');
           $file = request()->file('file');
           $filename = $file->getClientOriginalName();
           $path     =   $fy.'/'.$month.'/'.$filename;
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $details_array =array();
           $emailcontroller   =   new EmailController;

         
         $upcatid = Session::get('consolicatid');
         $concatid = Session::get('consoliitemid');
         // print_r($sapcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('Consolidated',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $upcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Consolidated','uid'=> $uid));
           
           }

           if(empty($concatid)){
            $string = strtoupper(substr('Consolidated',0,8));
         //   print_r(expression)
             $concatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Consolidated','uid'=> $uid));
          
            }

            if($user_role != 1){    
                        /*MIS User – Thank you for uploading and pending approval*/
              $miseremail = $email;
              $subject =  $yearmonth.' '.$areaname.' Townwise data: Thank you for uploading';
              $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$month,'name'=>$user_name,'view'=>'pages.email.misuploaduseremail');
              $emailcontroller->sendEmail($args);
              }
              //print_r($remark);exit;
              foreach ($remark as $key => $value) {
                 array_push($wararray, array('remark'=> $value,'warning'=>$warning[$key]));
              }
          //print_r($areaname);exit;
               //to store file in server
             $upcontroller   =   new UploadsController;
             $storage  = $upcontroller->storageSave(request()->file('file'),$month,$path);
             //get dispatch data for the month
            $discond  = array('type'=>$concatid,'month'=>$month,'year'=>$year,'area'=>$area,'status'=>'');
            //$disvalue =  $UploadsSettings->getDispatch($discond);


	$disvalue = DB::SELECT("SELECT id from datadispatch where type='".$concatid."' and month='".$month."' and dispyear='".$year."' and area='".$area."'");

          // print_r($disvalue);exit;
           if(empty($disvalue)){
                //uploads save
                $uparray   = array('filename' => $filename,'category'=>$upcatid ,'path'=>$path,'uid'=>$uid,'areaid'=>$area);
                $newuploads = $upcontroller->saveUplaods($uparray);

                //dispatch save
                $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$concatid,'uid'=>$uid,'year'=>$year,'status'=>0,'generate'=>0,'warning'=>json_encode($wararray),'area'=>$area);
                $dispid   =  $UploadsSettings->saveDispatch($disarray);

            }else{
                //$arryid = array_column($disvalue,'id');
                // DB::table('datadispatch')->whereIn('id',$arryid)->update(['status'=> 3]);
               
		            DB::table('Datadispatch_DT')->where('ddid',$disvalue[0]->id)->delete();
                DB::table('datadispatch')->where('id',$disvalue[0]->id)->delete();
                DB::table('Townwise_data')->where('AREA_ID',$area)->where('MONTY_YEAR',$year.$month)->where('TYPE',$concatid)->delete();

               $uparray   = array('filename' => $filename,'category'=> $upcatid ,'path'=>$path,'uid'=>$uid,'areaid'=>$area);
                $newuploads = $upcontroller->saveUplaods($uparray);

                //dispatch save
                $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$concatid,'uid'=>$uid,'year'=>$year,'status'=>0,'generate'=>0,'warning'=>json_encode($wararray),'area'=>$area);
                $dispid   =  $UploadsSettings->saveDispatch($disarray);

              
            }
        /*   echo '<pre>';
           print_r($dataarray);
           echo '</pre>';exit;*/
                   // Return an import object for every sheet
          foreach ($Import->getSheetNames() as $index => $sheetName) {
           // print_r($index);
              $manufact = self::getManufacture($sheetName);
              //print_r($manufact);exit;
              if(!empty($manufact)){
                foreach ($dataarray as $key => $value) {
                if(!empty($value)){
                  if($index ==  $key){
                     $headcount = count(array_filter($headings[$key][0]));
                 //  print_r($headcount);exit;
                    foreach ($value as $vkey => $trimarrvalue) {
                      array_walk_recursive($trimarrvalue, function($vkey, $value) use (&$vvalue){
                     $vvalue[trim($value)] = trim($vkey);
                     });
                    //  print_r(expression)
                     // print_r($headings[$vkey]);exit;
                      if(!empty($vvalue)){
                      for($i=2;$i<$headcount;$i++){
                        if(!empty($headings[$key][0][$i])){
                        //  print_r($vvalue);exit;
                       // print_r($vvalue[$headings[$key][0][$i]]);exit;
                        $barndvalue = $vvalue[$headings[$key][0][$i]];
                        $brand      = $headings[$key][0][$i];
                        $town  = $vvalue['Town'];
                      //  $wheretown  = "town='". $town."'";
                       
                            $brand_id  = DB::SELECT("SELECT p.id from brand p join manufacture pd on pd.id=p.mftid where manufacturer='". $manufact[0]['manufacturer']."' and brand='".$brand."'");
                           
                                                            
                                if(!empty($brand_id )){
                                  $brand_id = $brand_id[0]->id;
                                 // $brand_id= $brandid[0]['productid'];
                                 }
                                // / print_r($brand_id);exit;
                                 $townid     = self::getTown($town);
                                if(!empty($townid )){
                                  $town_id= $townid[0]['id'];
                                }else{
                                   $town_id='';
                                }
                                
                                if(!empty($town_id)){
                              //  $cnt = ($value[$i]=='') ? 0 : $value[$i];
                                  datadispatchdet::insertGetId(['ddid' => $dispid,'value'=>$barndvalue,'productid'=>$brand_id,'dealerid'=>'','status'=>1,'townid'=> $town_id ]);
                               // array_push($details_array,array('ddid' => $dispid,'value'=>  $barndvalue,'productid'=>$brand_id,'townid'=> $town_id ,'uid'=>$uid));
                                }
                              }

                      }
                    }
                    
                      


                   
                    }

                  }
                }
                
                             
             }
            }
                   
          

            }
            
                  
          // print_r($details_array);exit;
              //$dtid = $UploadsSettings->saveDispatchdetails($details_array);
             
              $category  = "File Uploaded";
              $homecontroller   =   new HomeController;
              $note   = 'Submitted Consolidated Townwise data for AM approval';
                $approve_dt  = $ApproveController->Approve($request);
              //  print_r($approve_dt);exit;
                 
                if(!empty($approve_dt)){
                  $valuelist = $approve_dt['valuelist'];
                  $manufacture = $approve_dt['manufacture'];

                }
              
              /*AM User – Data submitted and pending approval*/
             // echo "select email,name from users where role=3 and area=$area";exit;
              $userdt = DB::select("select email,name from users where role=3 and area=$area");
               if(!empty($userdt)){
              $miseremail = $userdt[0]->email ;
           
              $mangername = $userdt[0]->name ;
              $subject = 'Townwise data: Pending approval';
              $emailcontroller   =   new EmailController;
              $view = 'pages.email.approvedataemail';
              $encrypted = Crypt::encryptString($miseremail);
              $enid = Crypt::encryptString($dispid);
              $args = array('enid'=>$enid,'encrypted'=>$encrypted,'manufacture'=>$manufacture,'valuelist'=>$valuelist,'email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view,'managername'=> $mangername);
              $emailcontroller->sendEmail($args);
                
               }
                 /*MIS User – HO Admin – Data submitted by MIS user*/
              $admindt = DB::select("select email,name from users where  role=1 and area=$area");
               if(!empty($admindt)){
              $adminemail = $admindt[0]->email ;
                $adminname = $admindt[0]->name ;
              $subject = 'Townwise data: Pending approval';
              $emailcontroller   =   new EmailController;
              $view = 'pages.email.adminuploademail';
              $args = array('adminname'=>$adminname ,'manufacture'=>$manufacture,'valuelist'=>$valuelist,'email'=>$adminemail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
              $emailcontroller->sendEmail($args);
              }
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              
            

           return array('title'=>'Success!','messege'=>'Consolidated Uploaded Successfully','type'=>'success','load'=>'settings');

        }
      
        /*==========================================================
    Function: getManufacture
    Description: To get Manufacturere from product table
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getManufacture($manufact){
      
         $sql = DB::SELECT("SELECT d.* from manufacture d where d.manufacturer='".$manufact."'");
         $row = json_decode( json_encode($sql), true);
         return $row;

       }

  /*==========================================================
    Function: getTown
    Description: To get getTown from town table
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getTown($town){
      
         $sql = DB::SELECT("SELECT d.* from town d where d.town='".$town."'");
         $row = json_decode( json_encode($sql), true);
         return $row;

       }
         /*==========================================================
    Function: DownloadTemplate
    Description: To download template in consolidated uplopad
    Author:
    Created Date: 21-01-2020
    Modification: 
    ==========================================================*/
        public static function DownloadTemplate(Request $request){
         // print_r($request->all()); echo "sess". Session::get('areaid');exit;
               $input = $request->all();
               $areaid  = ($request->get('area') != '') ? $request->get('area') : Session::get('areaid') ;
               $is_api  = $request->get('is_api');
               $user_role  = $request->get('user_role');
              
               $townlist  = $request->get('townlist');
               $conmonth  = $request->get('conmonth');
               $area  = $request->get('area');
               $month = str_replace('-','',$conmonth);
               
             // print_r($month);exit;
               if(empty($is_api)){
                
                 $area = Session::get('area');
                 $townlist = Session::get('townlist');
                 $user_role = Session::get('user_role');
                 }
                 // if($user_role == 1){ commented by chethan as area was not displaying in consolidated template
                 //added by chethan to get area id from session for other than admin
               if($user_role != 1){ $areaid = Session::get('areaid') ; }
                  // $areaid  = $request->get('areaid');
                   $area =  DB::table('area')->where('id','=',$areaid)->get();
                   $area =  json_decode( json_encode($area), true);
                   //print_r($area);exit;
                   $area = $area[0]['area'];
                   $town = DB::SELECT("select * from towndetails where  status=1 and areaid='".$areaid."'");
                   $townlist = json_decode( json_encode($town), true);
                   //print_r($area);exit;
                 //}
               //   print_r(  $area);exit;
               $name = 'Industry_'.$area.'_'.$month.'.xlsx';
           return (new ConsolidateTemplateExport($areaid,$townlist))->download( $name);
        }

}
?>