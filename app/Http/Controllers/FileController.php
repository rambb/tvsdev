<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

    class FileController extends Controller {
        public function importExportExcelORCSV(){
            return view('file_import_export');
        }
        public function importFileIntoDB(Request $request){
          // print_r(request()->file('sample_file'));exit;
            if($request->hasFile('sample_file')){

                 $dataarray = Excel::toArray(new UsersImport,  request()->file('sample_file'));
                 $Import = new UsersImport(); 
                 $ts = Excel::import( $Import,  request()->file('sample_file'));
                 $data = [];
                 // Return an import object for every sheet
                foreach ($Import->getSheetNames() as $index => $sheetName) {
                    $data[$index] = $sheetName;
                }
        
            }
            dd('Request data does not have any files to import.');      
        } 
        public function downloadExcelFile($type){
            $products = Product::get()->toArray();
            return \Excel::create('expertphp_demo', function($excel) use ($products) {
                $excel->sheet('sheet name', function($sheet) use ($products)
                {
                    $sheet->fromArray($products);
                });
            })->download($type);
        }      
    }
    
    ?>