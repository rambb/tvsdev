<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use DB;
use App\activity;
use App\masters;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public static function index()
    {

        $user_id = Auth::id();
        $val = DB::select("SELECT * FROM users WHERE id =".$user_id);

       $result = json_decode( json_encode($val), true);
       if(!empty($result)){
            Session::put('user_id',$result[0]['id']); 
            Session::put('user_name', $result[0]['name']);
            Session::put('user_role',$result[0]['role']);
             Session::put('email',$result[0]['email']);
        // print_r(Session::get('user_name'));exit;
            //update settings
              $MasterSettings   =   new MasterSettings;
              $request = new \Illuminate\Http\Request();
              $check        =   $MasterSettings->getUploadsettings($request);
           // print_r(  $check );exit;
             if(empty($check['upload_enable'])){

                 
                $values1 = array(
                        'metakey'    =>  'upload_enable',
                        'metavalue'  =>  11,
                        'default'=>'Yes',
                           );
                 $val1 = masters::insert($values1);
               Session::put('upload_enable', 11);
              }else{
                 Session::put('upload_enable', $check['upload_enable']['metavalue']);
              }
             // print_r( Session::get('upload_enable'));exit;
                if(empty($check['upload_prev'])){
                  
                 $values2 = array(
                            'metakey'    =>  'upload_prev',
                            'metavalue'  => 'No',
                            'default'=>'Yes',
                               );
                 
                  $val2 = masters::insert($values2);
                   Session::put('allow_prev','No');
              }else{
                 Session::put('allow_prev',$check['upload_prev']['metavalue']);

              }
               if(empty($check['variance_threshold'])){
                  
                 $values2 = array(
                            'metakey'    =>  'variance_threshold',
                            'metavalue'  => '200',
                            'default'=>'Yes',
                               );
                 
                  $val2 = masters::insert($values2);
                   Session::put('v_threshold','200');
              }else{
                 Session::put('v_threshold',$check['variance_threshold']['metavalue']);

              }
             

              if(empty($check['minimum_threshold'])){
                  
                 $values2 = array(
                            'metakey'    =>  'minimum_threshold',
                            'metavalue'  => '50',
                            'default'=>'Yes',
                               );
                 
                  $val2 = masters::insert($values2);
                   Session::put('m_threshold','50');
              }else{
                 Session::put('m_threshold',$check['minimum_threshold']['metavalue']);

              }
              
             
          
        $currentY = date('Y');
        $currentM = date('m');
        if($currentM>=4){
            $fyear = $currentY;
        }else if($currentM<4){
            $fyear = $currentY-1;
        }
        Session::put('finacial_year',  $fyear);
         }
         //print_r($result[0]['role']);exit;
         if($result[0]['role']== 3 || $result[0]['role']== 4){
          $area = DB::SELECT("select area from area where id=".$result[0]['area']." and status=1");
          $arearesult = json_decode( json_encode($area), true);
          if(!empty($arearesult)){

             Session::put('area',  $arearesult[0]['area']);
             Session::put('areaid',  $result[0]['area']);
           }
            if($result[0]['role']== 3 || $result[0]['role']== 4){
            $area_cond = " and areaid='".$result[0]['area']."'";
         }
 //echo "select * from towndetails where  status=1".$area_cond;exit; 
          $town = DB::SELECT("select * from towndetails where  status='1' ".$area_cond." and area NOT LIKE '%INDIA%'");
          $townresult = json_decode( json_encode($town), true);
        //  print_r($townresult);exit;
         Session::put('townlist', array_column($townresult, 'town'));
         // print_r(Session::get('townlist'));exit;

        }else if($result[0]['role']== 2){
          $where        = "u.id =".$user_id." and u.status!=3 ";
          $columns      = "(select ', '+a.area from area as a where ',' +u.area+ ',' LIKE '%,' + cast(a.id as nvarchar(20))  + ',%' for xml path(''), type
       ).value('substring(text()[1], 2)', 'varchar(max)') as areaname,(select ', '+cast(a.id as nvarchar(20)) from area as a where ',' +u.area+ ',' LIKE '%,' + cast(a.id as nvarchar(20))  + ',%' for xml path(''), type
       ).value('substring(text()[1], 2)', 'varchar(max)') as areaid";
        $sql =  "SELECT $columns from  users u left join userrole r on u.role=r.id   where  $where";
        $deliveries = DB::select($sql);
        $row = json_decode( json_encode($deliveries), true);
      //  print_r( $row);exit;
        Session::put('area',  $row[0]['areaname']);
        Session::put('areaid',  $row[0]['areaid']);
        //print_r( $row );exit;

        }
          $brands =  DB::SELECT("SELECT d.brand,d.mftid from brand d join manufacture m on m.id=d.mftid  where d.status=1 and manufacturer !='TVS'");
          $brands =  json_decode( json_encode($brands), true);
          Session::put('brandlist',  $brands);

          /* type of item in session*/
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'SAP Despatch');
         Session::put('sapcatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $sapcatid = $MasterSettings->getItemid($uploadcatid,'SAP Despatch');
         Session::put('sapitemid',  $sapcatid);

         //retails
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'Retails');
         Session::put('retailcatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $retcatid = $MasterSettings->getItemid($uploadcatid,'Retails');
         Session::put('retailitemid',  $retcatid);

         //pdplan
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'SIAM');
         Session::put('siamcatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $pdcatid = $MasterSettings->getItemid($uploadcatid,'SIAM');
         Session::put('siamitemid',  $pdcatid);

         //pdplan
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'PD Plan');
         Session::put('pdcatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $pdcatid = $MasterSettings->getItemid($uploadcatid,'PD Plan');
         Session::put('pditemid',  $pdcatid);

         //consolidated
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'Consolidated');
         Session::put('consolicatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $concatid = $MasterSettings->getItemid($uploadcatid,'Consolidated');
         Session::put('consoliitemid',  $concatid);

         //raw upload
         $uptype        =   $MasterSettings->getCategoryid('UPLOAD_TYPE');
         $upcatid = $MasterSettings->getItemid($uptype,'Raw Data');
         Session::put('rawcatid',  $upcatid);
         $uploadcatid = $MasterSettings->getCategoryid('CATEGORY_UPLOAD');
         $rawcatid = $MasterSettings->getItemid($uploadcatid,'Raw Data');
          Session::put('rawitemid',  $rawcatid);

          $generate_enable = DB::SELECT("select metavalue from masters where metakey='generate_enable'");
          $generate_enable = json_decode( json_encode($generate_enable), true);
          if(empty($generate_enable)){
            $values1 = array(
                        'metakey'    =>'generate_enable',
                        'metavalue'  => 0,
                        'default'=>'Yes',
                           );
                 $val1 = masters::insert($values1);
                 Session::put('generate_enable',0);
          }else{
            Session::put('generate_enable',  $generate_enable[0]['metavalue']);
          }



     
    }
        /*==========================================================
    Function: saveActivity
    Description: To save all activity
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
    public static function saveActivity($category,$fid,$note,$uid) {
      date_default_timezone_set('Asia/Kolkata');
      $createdat = date('Y-m-d h:i:s A');
     // $time =date("h:i:sa");
      //echo date("h:i:sa");exit;
       // / $createdat = date('Y-m-d h:m:s');
      //  print_r($createdat);exit()t;
        $ip    = request()->ip();
        $ip = '103.253.169.202';
        $data = \Location::get($ip);
        $cityname = $data->cityName;
        $SQL = DB::table('activity')->insertGetId([
            'category' => $category,
            'note' => $note,
            'createdat' => $createdat,
            'createdby' => $uid,
            'ipaddress'=>$ip,
            'location'=>$cityname,
           
        ]);
        return $SQL;
    }
     /*==========================================================
    Function: checkHeader
    Description: To check all the haed
    Author: 
    Created Date: 30-12-2019
    Modification: 
    ==========================================================*/
    public static function checkHeader($type,$array) {
       $array = array_filter($array);
//print_r( $array);exit;
          $UploadsSettings   =   new AdminUploadsController;
        $colcount=0;
        $retarray = array();
         switch ($type) {
            case 'location': 
            $header = array('0' =>'town','1'=>'district','2'=>'area','3'=>'state','4'=>'zone','5'=>'town_class');
            $colcount =6;
            break;
            case 'product': 
            $header = array('0' =>'townwise_models','1'=>'industry','2'=>'category','3'=>'segment','4'=>'sub_segment','5'=>'manufacturer','6'=>'brand','7'=>'active_inactive');
            $colcount =8;
            break;
            case 'dealer': 
            $header = array('0' =>'dealer_code','1'=>'dealer_name','2'=>'dealer_town','3'=>'town','4'=>'state');
            $colcount =5;
            break;
            case 'sap': 
            $header = array('0' =>'Dealer Code','1'=>'Model','2'=>'Despatch');
            $colcount =3;
            break;
            case 'siam': 
            $header = array('0' =>'Manufacturer','1'=>'Models','2'=>'Quantity');
            $colcount =3;
            break;
            
            default:
                # code...
            break;
       }
     //  print_r($array);exit;
            // Check for equality 
     
         //print_r(array_intersect($array, $header));exit;
        if (!empty($array)){ 
            if(count(array_filter($array))== $colcount){
          // for($i=0;$i<= $colcount;$i++){
          //   if($array[$i]==$header[$i]){
          //     return array("success"=>true,"error"=>"");
          //   }else{
          //       return array("success"=>false,"error"=>"Template doesnot match");
               
          //    }
          //  }
              if($array == $header) {
              //they are the same
                 return array("success"=>true,"error"=>"");
                }else{
                   return array("success"=>false,"error"=>"Template doesnot match");
                }
         }else{
           return array("success"=>false,"error"=>"Uploaded data does not match the Template");
            
         }
       }

     
   
        
    }
     /*==========================================================
    Function: getFinacilaYear
    Description: To get Finacial Year
    Author: 
    Created Date: 31-12-2019
    Modification: 
    ==========================================================*/
    public static function getFinacilaYear() {
        $currentY = date('Y');
        $currentM = date('m');
        if($currentM>=4){
            $fyear = $currentY;
        }else if($currentM<4){
            $fyear = $currentY-1;
        }
        return  $fyear;
    }
     /*==========================================================
    Function: retailsHeaderCheck
    Description: To Check Header Retails
    Author: 
    Created Date: 31-12-2019
    Modification: 
    ==========================================================*/
   
    public static function retailsHeaderCheck($type,$array) {
            $UploadsSettings   =   new AdminUploadsController;
            $final_arr = array();
            //  print_r($array);exit;
            $where = "manufacturer='TVS'";
            $header = array('0' =>'Dealer Code');
            $retarray = array();

              // print_r($count);exit;
           $row = DB::SELECT("SELECT brand FROM brand b join manufacture m on m.id=b.mftid WHERE  $where and b.status=1 and m.status=1");
         //  print_r($row);exit;
           $models =  json_decode( json_encode($row), true);
           
             $brand = array_column($models,'brand');
          //   print_r($brand);exit;
             $brand = array_filter($brand);
             $brand = array_map('strtoupper', $brand);
             //print_r($brand);exit;
           /* $brand=array();
            foreach ($models as $key => $value) {
               array_push($brand,strtoupper($value['brand']));
            }*/
            $head1 = array();
            $head2 = array();
          
            if(count($array)<1){
                array_push($retarray,array('error'=>"Uploaded data does not match the Template")) ;
            }else{
            for ($i=0;$i<1;$i++) {
                array_push($head1,$array[$i]);
                array_push($head2,$header[$i]);
            }
            
          for ($i=0;$i<1;$i++) {
           //print_r( $head1[$i] );exit;
            if($head1[$i] == $head2[$i]){
            
            //  print_r(in_array(strtoupper('MOPED'),$brand));exit;
            for ($i=2;$i<count($array);$i++) {
                //print_r($brand);exit;
              if(!in_array(strtoupper(trim($array[$i])),$brand)){
                  $cell   = 1;
                  $column =  $UploadsSettings->getAlphabest($i);
                  array_push($retarray,array('error'=>'Brand "'.$array[$i].'" is not found in template')) ;
              }
            }
        }else{
                 array_push($retarray,array('error'=>"Uploaded data does not match the Template")) ;
            }
            
       }
          
       
    }
      if(empty($retarray)){
          return array("success"=>true);

         }else{
          return $retarray;
         }

       
    }
         /*==========================================================
    Function: retailsHeaderCheck
    Description: To Check Header Retails
    Author: 
    Created Date: 31-12-2019
    Modification: 
    ==========================================================*/
    public static function consolidateHeaderCheck($haederarr,$manufacture) {
          //  $array = $array[0];
       array_walk_recursive($haederarr, function($vkey, $value) use (&$array){
                $array[trim($value)] = trim($vkey);
              });
         // print_r($manufacture);exit;
            $type = 'Consolidated';
            $UploadsSettings   =   new AdminUploadsController;
            $header = array('0' =>'town','1'=>'town_group');
            $where = "manufacturer='".$manufacture."'";
          //  print_r($where);exit;
            $brands =  $UploadsSettings->getBrand($where);
            //print_r($brands);exit;
            $brand  = array();
            $retarray = array();
           // print_r($array[0]).'fdf';exit;
           if($array[0] != 'Town' || $array[1] != 'Town Group'){
                  array_push($retarray,array('error'=>"Uploaded data does not match the Template")) ;
                   return $retarray;
                }
            foreach ($brands as $key => $value) {
               array_push($brand,strtoupper(trim($value['brand'])));
            }
            
         //  print_r($brand);
                for ($i=2;$i<count($array);$i++) {
                           
                  if(!empty($array[$i])){
              //   print_r(in_array(strtoupper($array[$i]),$brand));exit;
                    if(!in_array(strtoupper(trim($array[$i])),$brand)){
                      $cell   =  1;
                      $column =  $UploadsSettings->getAlphabest($i);
                      array_push($retarray,array('error'=>'Sheet '.$manufacture.' - Model "'.$array[$i].'" is not found in the template')) ;
                    }
                  }
                }
               // print_r($retarray).'<br/>';
             

         if(empty($retarray)){
          return array("success"=>true);

         }else{
          return $retarray;
         }
        }
         /*==========================================================
    Function: TownSessionchange
    Description: To change town session value
    Author: 
    Created Date: 11-032-2019
    Modification: 
    ==========================================================*/
    public static function TownSessionchange(Request $request) {
      $areaid = $request->get('area');
      
      if(!empty($areaid)){
      $area = DB::SELECT("select area from area where id=$areaid and status='1'");
          $arearesult = json_decode( json_encode($area), true);
           $area_cond = " and areaid=$areaid";
         // echo "select * from towndetails where  status=1 $area_cond and s.area NOT LIKE '%INDIA%'";exit;
          $town = DB::SELECT("select * from towndetails where  status='1' $area_cond and area NOT LIKE '%INDIA%'");
          $townresult = json_decode( json_encode($town), true);
         // print_r( $townresult);exit;
           Session::put('townlist', array_column($townresult, 'town'));
           //print_r(Session::get('townlist'));exit;
          if(!empty($arearesult)){

             Session::put('area',  $arearesult[0]['area']);
             Session::put('areaid',  $areaid);
           }
      //print_r(  $area);exit;
    }

    }


    
    
}
