<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\User;
use App\usermeta;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
  public static function store(Request $request) { 
    $edit = $request->get('edit');
   // print_r($request->get('upid'));exit;
    if($edit == 1){
      $result = self::update_user($request);
 
    }else{
   // /   print_r($request->all());exit;
           $result = self::save_newuser($request);
    }
    return $result;

  }
   /*==========================================================
    Function: get_user_role
    Description: To get userrole
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
public static function get_user_role(Request $request) {
      $input = $request->all();
        //print_r($input);exit;
      $uid  = $request->get('user_id');
       if(!isset($uid)){
         $uid = Session::get('user_id');
       }
       $uroles = DB::table('userrole')->get();
       return $uroles; 
    
  }
  /*==========================================================
    Function: list_all
    Description: To get all users
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
  public static function list_all(Request $request){
    
          $input = $request->all();
       //print_r(Session::get('user_id'));exit;
          $uid  = $request->get('user_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
           }
          // print_r($uid);exit;
        $table ='users';   
        $where        = "u.status!=3";
       
       //$sql =  DB::SELECT("SELECT $columns from  $table where $where");
        $limit = '';
        $perPage = $request->get('per_page');
        $sort = $request->get('sort');
        $filter = $request->get('filter');
        $page = $request->get('page');
        $sort =  explode('|', $sort);
        //print_r($sort);exit;
         $darea = '';
        $limit = ' ORDER BY '.$sort[0].' '.$sort[1];
        if($filter){
          $where = $where . " and (( name LIKE '" . $filter . "%') or (ar.area LIKE '" . $filter . "%') or  (',' + RTRIM((select ', '+a.area from area as a where ',' +u.area+ ',' LIKE '%,' + cast(a.id as nvarchar(20)) + ',%' for xml path(''), type ).value('substring(text()[1], 2)', 'varchar(max)')) + ',') LIKE '%,%$filter%,%') ";
        //  $where = $where . " and (( name LIKE '" . $filter . "%') or (ar.area LIKE '" . $filter . "%')) ";
         // $darea = "a.area like '" . $filter . "%' or";
        }
         $columns      = "u.email,u.created_at,u.status,u.id,u.name,r.role,(select ', '+a.area from area as a where $darea ',' +u.area+ ',' LIKE '%,' + cast(a.id as nvarchar(20)  )  + ',%' for xml path(''), type
       ).value('substring(text()[1], 2)', 'varchar(max)') as areaname,(select ', '+cast(a.id as nvarchar(20)) from area as a where ',' +u.area+ ',' LIKE '%,' + cast(a.id as nvarchar(20))  + ',%' for xml path(''), type
       ).value('substring(text()[1], 2)', 'varchar(max)') as areaid";
         $sql =  "SELECT $columns from  users u left join userrole r on u.role=r.id left join area ar on cast(ar.id as nvarchar(20))=u.area  where  $where   $limit";
        // 
    //  print_r($sql);exit;
         $deliveries = DB::select($sql);

         $row = collect($deliveries);
         $currentPage = $page ?: 1;
         $slice_init = ($currentPage == 1) ? 0 : (($currentPage*$perPage)-$perPage);
         $data = $row->slice($slice_init, $perPage)->all();
         $row = json_decode( json_encode($deliveries  ), true);
         //print_r($row);exit;
          foreach($row as $key=>$value){
         $status = '<span class="text-success">ACTIVE</span>';
            if($value['status'] == "2"){ $status = '<span class="text-secondary">INACTIVE</span>';}
            if($value['status'] == "3"){ $status = '<span class="text-danger">TRASH</span>';}
            if($value['status'] !="3"){
             $statusbutn = '<a style="width:75px;" id="btn'.$value['id'].'" class="btn ng-binding btn-xs inactivebtn" onclick="app.change_status('.$value['id'].','.$value['status'].')"><strong>'.$status.'</strong></a>';
            }else{
                 $statusbutn = '<a  id="btn'.$value['id'].'" class="btn ng-binding  inactivebtn" style="pointer-events: none;cursor: default;width:75px;"><strong>'.$status.'</strong></a>';
            }
          // print_r($value);exit;
            if(count(explode(',', $value['areaid'])) > 3){
             // print_r(explode(',', $value['areaname']));exit;
              $val =explode(',', $value['areaname']);
              $carr =array();
              foreach ($val as $key1 => $value1) {
                if(count($carr) <3){
                $carr[] = $value1;
                }
              }
             $arname = implode(',',$carr);
           //  print_r($arname);exit;
              $area = '<span  data-toggle="tooltip" title="'.$value['areaname'].'">'.$arname.' ,...</span>';
            }else{
              $area =$value['areaname'];
              $area = empty($area) ? 'All' : $area;
            }
                $row[$key]['areaname'] =  $area;
                 $row[$key]['statusbutn'] =  $statusbutn;
                $row[$key]['username'] =  $value['name'];
                $row[$key]['role'] =  $value['role'];
                $row[$key]['id'] =  $value['id'];
                $row[$key]['email'] =  $value['email'];
                $row[$key]['statuskey'] =  $value['status'];
                $row[$key]['created_at'] =  date('d-M-Y',strtotime($value['created_at']));
                          }
      
        $deliveries = new LengthAwarePaginator($row, count($deliveries), $perPage, $currentPage);
         
            return $deliveries;
    
}

/*==========================================================
    Function: save_newuser
    Description: To get save or update users
    Author: 
    Created Date: 06-01-2020
    Modification: 
    ==========================================================*/
    public static function check_unique(Request $request) {
      $email =  Input::get('email');
      $id     =  Input::get('id');
      $idcond =($id=='') ? ' ' : "and id!='".$id."'";
      $user   = DB::SELECT("SELECT * FROM USERS where status =  1 and email ='".$email."' $idcond");
      // $user   = DB::SELECT("SELECT * FROM USERS where email ='".$email."' $idcond");
           if (empty($user)) {
               return 0;
              }else{
                return 1;
              }
      }
 /*==========================================================
    Function: save_newuser
    Description: To get save or update users
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
    public static function save_newuser(Request $request) {
               $input = $request->all();
             
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
      $cdate    =   date('Y-m-d');
      $time  = date('Y-m-d h:m:i');
      $selected = $request->get('selectedResources');
      $status   =   1;
      $encpass  =   bcrypt($request->get('user_pass'));
      $role  =   $request->get('role');
      $area  =   ($role == 2) ? $request->get('hoarea') : $request->get('area');
     // print_r($area);exit;
      if($role == 2){
          $area = implode(',', $area);
      }      
    //print_r($area);exit;
        $dbprefix = DB_PREFIX;
        
                  $values = array(
                        'email'    =>  $request->get('user_email'),
                        'role'   => $role,
                        'area'=>   $area,
                        'created_by' => $uid,
                        'created_at' => $cdate,
                        'modified_by' => $uid,
                         'status' => $status
                        );
    //  print_r($values);exit;
      $part_id = User::insertGetId($values);
      //save to activity
      $category  = "User added";
      $homecontroller   =   new HomeController;
      $note ='User added';
      $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
     
     return array('messege'=>'User Saved Successfully');
    
}

//get user details for edit
public static function get_user_details(Request $request) {
  $uid = $request->get('id');
  $dbprefix   =   DB_PREFIX;
  $tables   =   'users';
  $columns  =   "*";
  $where    = "id=" . $uid;
  $limit    =   "";
  $sql =  DB::SELECT("SELECT $columns from  $tables where $where");
  // echo $sql;exit;
    $row = json_decode( json_encode($sql), true);
    $area_col = array_column($row,"area");
    $arealist = explode(',', $area_col[0]);
    if(in_array(0,  $area_col)){
       $area = DB::select("select id from area where status=1");
       $area = json_decode( json_encode($area), true);
       $arealist = array_column($area,"id");
       //$area = explode(',', $area_col);
     //  $area_col = array_column($arow,"id");
    }
   // print_r( $arealist);exit;

    $final_arra = array("id"=>array_column($row,"id"),"email"=>array_column($row,"email"),"role"=>array_column($row,"role"),"area"=>$arealist);
  return $final_arra;
  }
  
  /*==========================================================
    Function: update_user
    Description: To get update users
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
 public static function update_user(Request $request) {
  // print_r($request->get('hoarea'));exit;
               $input = $request->all();
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
      $cdate    =   date('Y-m-d h:i:s');
      $status   =   1;
      $encpass  =   bcrypt($request->get('user_pass'));
      $role  =   $request->get('role');
      $area  =   $request->get('area');
      $area  =   ($role == 2) ? $request->get('hoarea') : $request->get('area');

      if($role == 2){
        $area = implode(',', $area);
      
      }
      $id  =   $request->get('upid');
 //  print_r($area);exit;
    if (empty($request->get('user_pass'))) {
 //print_r($id);exit;
      DB::table('users')
            ->where('id',$id)
            ->update([
                'email'    =>  $request->get('user_email'),
                'role'   => $role,
                'area'=>   $area,
                'updated_at' => $cdate,
                'modified_by' => $uid,
                 'status' => $status
        ]);
       
      
    } 
  else {
      $encpass = bcrypt($request->get('user_pass'));

       DB::table('users')
            ->where('id',$id)
            ->update([
                'email'    =>  $request->get('user_email'),
                'role'   => $role,
                'area'=>   $area,
                'modified_by' => $uid,
                 'status' => $status,
                "updated_at" => $cdate,
                
        ]);
       
       
    }
    
      $category  = "User updated";
      $homecontroller   =   new HomeController;
      $note ='User updated';
      $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
    
 return array('messege'=> "User detail updated successfully");
  
  

}

/*==========================================================
    Function: delete_user
    Description: To get delete users
    Author: 
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
public static function delete_user(Request $request) {
   //print_r($_POST);exit;
   $id  = $request->get('id');
   $input = $request->all();
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
   $dbprefix = DB_PREFIX;
   $where   = "id=".$id;
   $columns = "user_status=3";
    $row  = DB::table('users')
            ->where('id',$id)
            ->update([
              "status" => 3,
             ]);
   
      $category  = "User removed";
      $homecontroller   =   new HomeController;
      $note ='User removed';
      $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
      return 1;
   
}
//update password
public static function update_password(Request $request) {
    $uid = Session::get('user_id');
    $ret    =   false;
    $dbprefix   =   DB_PREFIX;
    $tables   =   'users';
    $columns  =   "retyped_password";
    $where    =   "id=" . $uid;
    $limit    =   "";
    $user_id     = Session::get('user_id');
    $sql =  DB::SELECT("SELECT $columns from  $tables where $where");
      //  echo $sql;exit;
    $row = json_decode( json_encode($sql), true);
    $pass     = $request->get('prof_opass');
    
    //print_r($pass);exit;;
   // print_r($pass);exit;
    if (trim($row[0]['retyped_password']) == trim($pass)) {
      $newpass  = bcrypt($request->get('prof_npass'));
            $row  = DB::table('users')
            ->where('id',$uid)
            ->update([
              "password" => $newpass,
              "retyped_password"=>$request->get('prof_npass'),
                "modified_by"     => $user_id,
             ]);
         
      
    return array('messege'=>"Password Updated Successfully",'type'=>'success','text'=>'Success');
      //return $ret;
    }else{
      return array('messege'=>"Password Doesnot Match",'type'=>'warning','text'=>'Failure');
    }
    //return $ret; 
     

}
public static function get_uname(Request $request) {
    $uid        = $request->get('uid');
	$dbprefix 	= 	DB_PREFIX;
	$tables 	= 	Users;
	$columns 	= 	"name";
	$where 		=	"id=" . $uid;
	$limit 		= 	"";
    $sql        =  DB::SELECT("SELECT $columns from  $tables where $where");
	$row        = json_decode( json_encode($sql), true);
	$uname 		=	$row[0]['name'];
	return $uname;
}
 /*==========================================================
    Function: change_password
    Description: To SAVE PASSWORD
    Author: 
    Created Date: 23-12-2019
    Modification: 
    ==========================================================*/
public static function change_password(Request $request) {
    $input = $request->all();
              //  print_r(Session::get('user_id'));exit;
               $uid  = $request->get('user_id');
               $fid  = $request->get('finacial_id');
               if(empty($uid)){
                 $uid = Session::get('user_id');
                 $fid = Session::get('finacial_id');
               }
    $limit    =   "";
    $uid        = $request->get('id');
    $pass     = $request->get('password');
 
      $newpass  = bcrypt($request->get('password'));
            $row  = DB::table('users')
            ->where('id',$uid)
            ->update([
              "password" => $newpass,
               "retyped_password"=>$request->get('password'),
               "modified_by"     => $uid,
                  ]);
      $category  = "User modification";
      $homecontroller   =   new HomeController;
      $note ='Password Updated';
      $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
      
    return array('messege'=>"Password Updated Successfully",'type'=>'success','text'=>'Success');
      //return $ret;
    
    //return $ret; 
     

}
//send email link to the users
public static function SendLink(Request $request) {
    $user_id = Session::get('user_id');
    $id        = $request->get('id');
    $email        = $request->get('email');
    $crypt_id          =   sha1($id);
    $crypt_id          =   substr($crypt_id, 0, 8); 
    $Controller = new MessageController;
     $d=date ("d");
            $m=date ("m");
            $y=date ("Y");
            $t=time();
            $dmt=$d+$m+$y+$t;    
            $ran= rand(0,10000000);
            $dmtran= $dmt+$ran;
            $un=  uniqid();
            $dmtun = $dmt.$un;
            $mdun = md5($dmtran.$un);
//echo $crypt_inst_id;exit;
             DB::table('users')
                ->where('id',$id)
                ->update([
                        "passtokenid" => $mdun,
            ]);
   $url = url('/');
   $sms_content = 'Click below link To Change The Password ';
   $sms_content .= $url.'/reset?id='.$crypt_id.'&tokenid='.$mdun;
   error_log($url.'/reset?id='.$crypt_id.'&tokenid='.$mdun);
        $subject    ="DB NEW PASSWORD";
        $sent       = $Controller->sendMail($email,$sms_content,$subject);
	  
		if( $sent =='1'){
                 // error_log("Mail Sent");
                return array('messege'=>"Mail Sent Successfully",'type'=>'success','text'=>'Success');
        }
        else{ 
                  return array('messege'=>"Mail Not Sent",'type'=>'warning','text'=>'Warning');

        }
    
     }
  
    /**GET USER DETAILS FROM USERS TABLE**/
        public static function get_user_type($created_by){
                //$partCond = empty($partid)? '' : 'ms_Id='.$partid;
        $dbprefix = DB_PREFIX;
        $tables = 'users';
        $columns = "user_type";
        $where   =   "id=" . $created_by ;
        $limit = "";
        
          $sql =  DB::SELECT("SELECT $columns from  $tables where $where");
                //  echo "SELECT $columns from  $tables where $where";exit;
          $row = json_decode( json_encode($sql), true);
            return $row;
              
                 
            } 

}
