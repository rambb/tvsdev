<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;

class UploadsController extends Controller
{
 /*==========================================================
    Function: storageSave
    Description: To store file in client server
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
        public static function storageSave($file,$month,$path){
           $homecontroller   =   new HomeController;
           $fy        =   $homecontroller->getFinacilaYear();
           $filename = $file->getClientOriginalName();
           //$path     =   $fy.'/'.$month'/'.$filename;
           //if()
           $str = Storage::disk('local')->put($path,  File::get($file));
           //$str = Storage::disk('custom')->put($path,  File::get($file));
           return $str;
        }
        /*==========================================================
    Function: saveUplaods
    Description: To save to uploads table
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
        public static function saveUplaods($array){
            $uid = $array['uid'];
            $category = $array['category'];
            $filename = $array['filename'];
            $path = $array['path'];
            $time  = date('Y-m-d h:i:s');
            $newuploads = DB::table('uploads')->insertGetId(['filename' => $filename,'category'=> $category,'link'=>$path,'status'=>1,'createdby'=>$uid,'modifiedby'=>$uid,'created_at'=>$time,'updated_at'=>$time]);
           return $newuploads;
        }
}
?>