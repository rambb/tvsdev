<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;
use App\Exports\RetailsExport;
use App\Exports\AllindiaExport;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use App\datadispatchdet;
use App\DD_ITEMS;
HeadingRowFormatter::default('none');
class AdminUploadsController  extends Controller
{

/*==========================================================
    Function: getAlphabest
    Description: To get alphabests 
    Author:
    Created Date: 06-01-2020
    Modification: 
    ==========================================================*/
      public static function getAlphabest($key){
         $aphaarray = Array();

          for( $i = 65; $i < 91; $i++){
                  $aphaarray[] = chr($i);
           }
           $aphabet ='';
           if($key<=25){
            $aphabet = $aphaarray[$key];
           }else if($key>=26 && $key<=51){

            $first = 'A';
            $second = $key - 26;
            $secval = $aphaarray[$second];
            $aphabet =  $first.$secval;

           }else if($key>=52 && $key<=77){
            $first = 'B';
            $second = $key - 52;
            $secval = $aphaarray[$second];
            $aphabet =  $first.$secval;

           }else if($key>=78 && $key<=103){
            $first = 'C';
            $second = $key - 78;
            $secval = $aphaarray[$second];
            $aphabet =  $first.$secval;

           }
       return $aphabet;
            

      }
      /*==========================================================
    Function: checkThreshold
    Description: check checkThreshold for 6 month data
    Author:
    Created Date: 09-01-2020
    Modification: 
    ==========================================================*/
      public static function checkThreshold($array){
   //    print_r($product);exit;
        $type =  $array['type'];
        $dealer = (array_key_exists('dealer', $array)) ? $array['dealer'] : '';
        $product = (array_key_exists('product', $array)) ? $array['product'] : '';
        $month = (array_key_exists('month', $array)) ? $array['month'] : '';
        $status = (array_key_exists('status', $array)) ? $array['status'] : '';
        $year = (array_key_exists('year', $array)) ? $array['year'] : '';
        $area = (array_key_exists('area', $array)) ? $array['area'] : '';
        $groupby = ' group by dealerid,productid';
        $dealercond = ($dealer=='')?' ':" and dealerid='".$dealer."'";
        $productcond = ($product=='')?' ':" and productid='".$product."'";
        $statuscond = ($status=='')?' ':" and d.status ='".$status."' and dt.status=1";
        $areacond = ($area=='')?' ':" and area='".$area."'";
        $select ="sum(dt.value) as avg,dealerid,productid";
        $fselct ="AVG(a.avg) AS sum,dealerid,productid";
        $gtype=' group by month,dealerid,productid';
        $dettable = 'datadispatch_dt';
        $currentY = date('Y');
        $from =  date("Y-m-d", strtotime( date($year.'-'.$month.'-01' )." -6 months"));
        $to = date("Y-m-d",strtotime('20'.trim($year).'-'.$month.'-01'));
//ECHO "SELECT  $fselct from (SELECT $select from datadispatch d left join  $dettable dt on d.id=dt.ddid where d.status!=3 and type='".$type."' and   convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112)  between '".$from."' and '".$to."' ".$dealercond.$productcond.$statuscond.$areacond.$groupby.$gtype.") AS a $groupby";EXIT;
    //    echo "SELECT  $fselct from (SELECT $select from datadispatch d left join  $dettable dt on d.id=dt.ddid where d.status!=3 and type='".$type."' and   convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112)  between '".$from."' and '".$to."' ".$dealercond.$productcond.$statuscond.$areacond.$gtype.") AS a  $groupby";exit;
        $sql = DB::SELECT("SELECT  $fselct from (SELECT $select from datadispatch d left join  $dettable dt on d.id=dt.ddid where d.status!=3 and type='".$type."' and   convert(date, cast(dispyear*10000 + month*100 + 1 as varchar(8)), 112)  between '".$from."' and '".$to."' ".$dealercond.$productcond.$statuscond.$areacond.$gtype.") AS a  $groupby");
         $row = json_decode( json_encode($sql), true);
         return $row;
               

      }
  
    /*==========================================================
    Function: validatesapUploads
    Description: To validate sap uploads 
    Author:
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
      public static function validatesapUploads(Request $request){

          $input = $request->all();
          $t_threshold  = $request->get('t_threshold');
          $m_threshold  = $request->get('m_threshold');
          $uid  = $request->get('user_id');
                //print_r(Session::get('m_threshold'));exit;
           if(empty($uid)){
             $t_threshold = Session::get('v_threshold');
             $m_threshold = Session::get('m_threshold');
           }
          $type = $request->get('type');
          $headings = (new HeadingRowImport)->toArray(request()->file('file'));
          $month  = $request->get('month');
          $mdata  = explode("-",$month);
          $month = $mdata[0];
          $date = date_parse($month);
          $month =  $date['month'];
          $year  = $mdata[1];
          $warningarray = array();
          $MasterSettings   =   new MasterSettings;
          $homecontroller   =   new HomeController;
          $error_array = array();
          $final_arr =  array();
         //header validation
          $validate        =   $homecontroller->checkHeader($type,$headings[0][0]);
           //print_r($validate);exit;
        if($validate['success'] == '1'){

           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $data = $dataarray[0];
         if(isset($data[0]['Dealer Code'])){

          if(!empty($data[0]['Dealer Code'])){

           $final_arr = array();
           $dealercode_arr = array_unique(array_column(array_filter($data),'Dealer Code'));
           $dealercode_arr  = array_filter($dealercode_arr);
           if(count($dealercode_arr) > 1){
              $imp_dealer = ("(".implode("),(", $dealercode_arr).")");
            }else{
              $imp_dealer = "(".$dealercode_arr[0].")";
            }


           $dealercodes =DB::SELECT("select * from (values $imp_dealer) as t(dealercode) except select dealercode from dealers where status=1");
            foreach ($dealercodes as $key => $value) {
              array_push($error_array,array("error"=>'Dealer "'.$value->dealercode.'" is not found in template'));
            }


            $model_arr = array_unique(array_column($data,'Model'));
             if(count(array_filter($model_arr)) > 1){
         //    echo 'sas';exit;
             $imp_model = ("('".implode("'),('", $model_arr))."')";
            }else{
             $imp_model ="('".$model_arr[0]."')";
            }
//echo "select * from (values $imp_model) as t(brand) except select brand from brand where status=1";exit;
           $products =DB::SELECT("select * from (values $imp_model) as t(brand) except select brand from brand where status=1");
            foreach ($products as $key => $value) {
              if(!empty($value->brand)){
              array_push($error_array,array("error"=>'Model "'.$value->brand.'" is not found in template'));
             }
            } 

            foreach ($data as $Dkey => $Dvalue) {

               if(isset($Dvalue['Dealer Code']) &&  isset($Dvalue['Model'])){
                  $dispatch = $Dvalue['Despatch'];
                    if(!isset( $Dvalue['Despatch']) ){
                          $cell = $Dkey+2;
                          $column = 'C';
                          array_push($error_array,array("error"=>'Cell '.$column.$cell.' cannot be left blank'));
                        }else if(!is_int($dispatch) && !empty($dispatch) ){
                          $cell = $Dkey+2;
                          $column = 'C';
                          array_push($error_array,array("error"=>'Cell '.$column.$cell.' should be a whole number'));
                         }   //check value is empty
         
                 $dealercodes = json_decode(json_encode($dealercodes),true);
                 $products = json_decode(json_encode($products),true);
                if(!in_array($Dvalue['Dealer Code'], $dealercodes) && !in_array($Dvalue['Model'], $products) ){
                   $dealerid  = DB::table('dealers')->where("dealercode",$Dvalue['Dealer Code'])->value('id');
                   $productid =  DB::table('brand')->where("brand",$Dvalue['Model'])->value('id');
                   array_push($final_arr,array("dealerid"=> $dealerid,"productid"=> $productid,"count"=>$dispatch));
                     // $discond  = array('type'=>'SAP Despatch','dealer'=> $dealerid,'product'=>$productid,'month'=>$month,'status'=>1,'year'=>$year);
                     // $disvalue = self::checkThreshold($discond);
                     // if(!empty($disvalue[0]['sum'])){
                     //     if($disvalue[0]['sum'] > $m_threshold){
                     //        $olddispatch = $disvalue[0]['sum'];
                     //        $percentage = ($olddispatch *$t_threshold)/100;
                     //       // print_r($percentage);exit;
                     //        if($percentage < $dispatch){
                     //          $cell = $Dkey   +2;
                     //          //print_r($key);exit;
                     //          $column =  self::getAlphabest($cell);
                     //          $war = 'Cell'.$column.$cell.': total value of '.$dispatch.' is greater than 200% of previous 6 month average value of '.$percentage;
                     //          array_push($warningarray,array("index"=>$Dkey,"warning"=>$war));
                     //        }else if($percentage > $dispatch){
                     //          $cell = $Dkey+2;
                     //          //print_r($key);exit;
                     //          $column =  self::getAlphabest($cell);
                     //          $war = 'Cell'.$column.$cell.': total value of '.$dispatch.' is lesser than 200% of previous 6 month average value of '.$percentage;
                     //          array_push($warningarray,array("index"=>$Dkey,"warning"=>$war));
                     //        }
                     //    }
                      
                     //  }
                   
                 }
               }

             }
           }
          }
          
          }else{ 
          array_push($error_array,array("error"=>$validate['error']));
           }
         return array("error" => $error_array,"warning"=>$warningarray,"final_arr"=>$final_arr);
      } 
   /*==========================================================
    Function: validateretailsUploads
    Description: To validate retail uploads 
    Author:
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
        public static function validateretailsUploads(Request $request){
           $input = $request->all();
           $t_threshold  = $request->get('v_threshold');
           $m_threshold  = $request->get('m_threshold');
           $uid  = $request->get('user_id');
           if(empty($uid)){
             $t_threshold = Session::get('v_threshold');
             $m_threshold =  Session::get('m_threshold');
           }
          
           $type = $request->get('type');
           $headings = (new HeadingRowImport)->toArray(request()->file('file'));
           $month  = $request->get('month');
           $mdata  = explode("-",$month);
           $month = $mdata[0];
           $date = date_parse($month);
           $month =  $date['month'];
           $year  = $mdata[1];
           $warningarray = array();
           $MasterSettings   =   new MasterSettings;
           $homecontroller   =   new HomeController;
          //header validation
          $validate        =   $homecontroller->retailsHeaderCheck($type,$headings[0][0]);
        //print_r($headings[0][0]);exit;   
          $updateFile = $request->file('file');
          $error_array = array();
          $bandarr = array();
          $final_arr = array();
          $retcatid = Session::get('retailitemid');
          
          if(isset($validate['success'])){
           // echo 'inside';exit;
               $dataarray = Excel::toArray(new RetailsImport,  request()->file('file'));
               $header = $headings[0][0];
               $discond  = array('type'=>$retcatid,'month'=>$month,'status'=>1,'year'=>$year);
               $disvalue = self::checkThreshold($discond);
               $brands = $headings[0][0]; 
             //print_r( $brands);exit;
               foreach ($brands  as $key => $value) {
                if($key != 0){
                  //echo "select id from brand where brand=$value";exit;
                 $brval  = DB::SELECT("select id from brand where brand='".$value."'");
                  $row = json_decode( json_encode($brval), true);
                 // echo "select id from brand where brand='".$value."'";
             //  print_r($brval);
                 if(!empty($row)){
                   array_push($bandarr,$row[0]['id']);
                  }
                }
               }
//exit;
           foreach ($dataarray[0] as $key => $value) {
            if(!empty($value)){
             //skip first 3 row
            if($key != 0){
            $count = count($value);
            $dealercode = $value[0];
            $dealerid  = DB::table('dealers')->where("dealercode",$dealercode)->value('id');
              if(empty($dealerid)){
                   $cell = $key+1;
                   $column =  'A';
                   array_push($error_array,array("error"=>'Dealer "'.$dealercode.'" is not found in template'));
                           //return array("error"=>'Cell'.$column.$cell.': Value not found in Retail Template');
                  }else{
                             
                   $date = "Y-".$month."-d";
                   $priormonth = date ('m', strtotime ( '-1 month' , strtotime ($date)));
                   $count =  count($value);
                   for($i=1;$i<$count;$i++) {
                      $cell = $key+1;
                      $rval = $value[$i];
                     if(!is_int($rval) && !empty($rval) ){
                         $cell = $key+1;
                         $column = self::getAlphabest($i);
                         $bkey = $cell;
                         array_push($error_array,array("error"=>'Cell '.$column.$cell.' should be a whole number'));
                      // return array("error"=>'Cell'.$column.$cell.': Retail Has an Issue No Whole Number');
                      }else if(!isset($rval) || $rval == ''){
                        if(strlen($rval) == 0){
                         $cell = $key+1;
                         $column = self::getAlphabest($i);
                         $bkey = $cell;
                         array_push($error_array,array("error"=>'Cell '.$column.$cell.' cannot be blank'));
                           }
                          }
                        if($rval != ''){
                            array_push($final_arr,array('dealerid'=> $dealerid,'value'=>$rval,'brand'=>$bandarr[$i-1]));
                            }
                          //  print_r($disvalue);exit;
                  if(!empty($disvalue[0]['sum'])){
                     foreach ($disvalue as $key => $dval) {
                     if($dval['dealerid'] ==  $dealerid && $dval['productid'] ==  $bandarr[$i-1]){
                        if($dval['sum'] > $m_threshold){

                      $olddispatch = $dval['sum'];
                      $percentage = ($olddispatch *$t_threshold)/100;
                     // print_r($percentage);exit;
                      if($percentage < $rval){
                        $cell = $key+2;
                        //print_r($key);exit;
                        $column =  self::getAlphabest($cell);
                        $war = 'Brand "'.$header[$i].'": total value of '.$rval.' is greater than 200% of previous 6 month average value of '.$percentage;
                        array_push($warningarray,array("index"=>$key,"warning"=>$war));
                      }else if($percentage > $rval){
                        $cell = $key+2;
                        //print_r($key);exit;
                        $column =  self::getAlphabest($cell);
                         $war = 'Brand "'.$header[$i].'": total value of '.$rval.' is lesser than 200% of previous 6 month average value of '.$percentage;
                        array_push($warningarray,array("index"=>$key,"warning"=>$war));
                      }
                      }
                      }
                    }
                   }
                                    
                    }//for close
                        
                  }//else close
                       
                  }//key close
                }//empty check
               }//foreach close
                
              }else{
          //  print_r($validate);exit;

             foreach ($validate as $vkey => $vvalue) {
                 array_push($error_array,array("error"=>$vvalue['error']));
                }
              }  
          
            return array("success"=>true,"warning"=>$warningarray,'error'=>$error_array,'final_arr'=>$final_arr );
          
          } 
   
    /*==========================================================
    Function: saveDispatch
    Description: To store dispatch table
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
        public static function saveDispatch($array){

            $uid = $array['uid'];
            $month = $array['month'];
            $year = $array['year'];
            $year = (strlen($year) == 2) ? '20'.$year : $year;
            $uploadid = $array['uploadid'];
            $type = $array['type'];
            $generate = $array['generate'];
            $status = $array['status'];
            $warning = isset($array['warning']) ?  $array['warning'] :' ';
            $countfile = isset($array['countfile']) ? $array['countfile'] : 0 ;
            $area = isset($array['area']) ? $array['area'] : '' ;
            $time  = date('Y-m-d h:i:s');
            $approve  = '';
            if($status == 1){$approve=$time;}
             $dispid = DB::table('datadispatch')->insertGetId(['month' => $month,'uploadid'=>$uploadid,'type'=>$type,'updated_at'=>$time,'status'=>$status,'createdby'=>$uid,'created_at'=>$time,'modifiedby'=>$uid,'dispyear'=>$year,'generate'=> $generate,'countfile'=> $countfile,'area'=>$area,'warning'=> $warning]);

           return $dispid;
        }

    /*==========================================================
    Function: saveDispatchdetails
    Description: To store dispatch details table
    Author:
    Created Date: saveUplaods
    Modification: 
    ==========================================================*/
        public static function saveDispatchdetails($array){
        //print_r($array);exit;
          foreach ($array as $key => $value) {
            $uid          = $value['uid'];
            $dispatch     = $value['value'];
            $dispid       = $value['ddid'];
            $productid    = $value['productid'];
            $dealerid     = isset($value['dealerid']) ? $value['dealerid'] : '' ;
            $townid     = isset($value['townid']) ? $value['townid'] :'';
            $table    = isset($value['table']) ? $value['table'] :'datadispatch_dt';
            $time         = date('Y-m-d h:i:s');
            if(!empty($productid)){
            $dispid  = DB::table($table)->insertGetId(['ddid' => $dispid,'value'=>$dispatch,'productid'=>$productid,'dealerid'=>$dealerid,'status'=>1,'townid'=> $townid ]);
          }
             }
           return $dispid;
        }
    /*==========================================================
    Function: saveSap
    Description: To upload  sapdispatch
    Author:
    Created Date: 31-12-2019
    Modification: 
    ==========================================================*/
      public static function saveSap(Request $request){
          $input = $request->all();
         //  print_r($input);exit;
           $uid  = $request->get('user_id');
           $fid  = $request->get('finacial_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
             $fid = Session::get('finacial_id');
           }
         $date  = $request->get('date');
      
          $month  = $request->get('month');
          $final_arr  = $request->get('final_arr');
       //   print_r($request->all());exit;    
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $date = date_parse($month);
         $month =  $date['month'];
         $year = '20'.$mdata[1];
         $wararray = array();
         $index  = json_decode($request->get('index'));
         $remark  = json_decode($request->get('remark'));
         $warning  = json_decode($request->get('warning'));
         $cdate  = date('Y-m-d');
         $time  = date('Y-m-d h:i:s');
         $details_array = array();
         $file  =  request()->file('file');
         $filename = $file->getClientOriginalName();
         $details_array=array();
          //print_r( $final_arr);exit;
         $homecontroller   =   new HomeController;
         $MasterSettings   =   new MasterSettings;
         $upcontroller   =   new UploadsController;
         $fy        =   $homecontroller->getFinacilaYear();
         $path     =   $fy.'/'.$month.'/'.$filename;
         $dispdetails_array = array();
         $upcatid =  Session::get('sapcatid');
         $sapcatid = Session::get('sapitemid');
         // print_r($sapcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('SAP Despatch',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $upcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'SAP Despatch','uid'=> $uid));
           
           }

           if(empty($sapcatid)){
            $string = strtoupper(substr('SAP Despatch',0,8));
         //   print_r(expression)
             $sapcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'SAP Despatch','uid'=> $uid));
          
            }

        // print_r($sapcatid);exit;
          foreach ($remark as $key => $value) {
             array_push($wararray, array('remark'=> $value,'warning'=>$warning[$key]));
          }
         
         //to store file in server
         $upcontroller   =   new UploadsController;
         $storage  = $upcontroller->storageSave(request()->file('file'),$month,$path);
         //get dispatch data for the sapcatid
        $discond  = array('type'=>$sapcatid,'month'=>$month,'year'=>$year);
        $disvalue = self::getDispatch($discond);
      // print_r($disvalue);exit;
$disvalue = self::getDispatchCount($discond);
     

       if(empty($disvalue)){
            //uploads save
            $uparray   = array('filename' => $filename,'category'=>$upcatid,'path'=>$path,'uid'=>$uid);
            $newuploads =$upcontroller->saveUplaods($uparray);

            //dispatch save
            $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$sapcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
            $dispid   =  self::saveDispatch($disarray);
		
        }else{
           //update sttsus 3 for already existintg data
          foreach ($disvalue as $key => $value) {
           //DB::table('datadispatch')->where('id',$value['id'])->update(['status'=>3]);
 	DB::table('Datadispatch_DT')->where('ddid',$disvalue[0]->id)->delete();             
            DB::table('datadispatch')->where('id',$disvalue[0]->id)->delete();
            DB::table('Townwise_data')->where('MONTY_YEAR',$year.$month)->where('TYPE',$sapcatid)->delete();
          }
           
           $uparray   = array('filename' => $filename,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
           $newuploads =$upcontroller->saveUplaods($uparray);

            //dispatch save
            $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$sapcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
            $dispid   =  self::saveDispatch($disarray);
        }
       
         $sum = 0;
         $final_arr = json_decode($final_arr);
         //added by chethan
         $prevprod = $prevdealerid = '';
    // print_r($dispid);exit;
        if(!empty($final_arr)){
        foreach ($final_arr as $key => $value) {
          //print_r('fdf');exit;
          $dealerid  = $value->dealerid;
          $productid  = $value->productid;
          $dispatch  = $value->count;
          $townid =  DB::table('dealers')->where("id",$dealerid)->value('townid');
          if(!empty($productid)){
          array_push($dispdetails_array,array('ddid' => $dispid,'value'=>$dispatch,'productid'=>$productid,'dealerid'=>$dealerid,'uid'=>$uid,'townid'=>$townid));
           }
        
         }
     //print_r($dispdetails_array);exit;
         if(!empty($dispdetails_array)){
         $dtid = self::saveDispatchdetails($dispdetails_array,$wararray);
       }
        //============================================================================================
        
          $monthName= date('M', mktime(0, 0, 0, $month, 10)); // March
          $category  = "File Uploaded";
          $homecontroller   =   new HomeController;
          $note   = 'Submitted SAP Despatch data';
          $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
          $MasterSettings->generateTrigger($month,$year);
      }
       return array('title'=>'Success!','messege'=>'Sap Dispatch Uploaded Successfully','type'=>'success','load'=>'settings');
  } 

/*==========================================================
    Function: getDispatchCount
    Description: To upload  dispatch
    Author:
    Created Date: 15-05-2020
    Modification: 
    ==========================================================*/
       public static function getDispatchCount($array){
       // print_r($array);exit;
        $type =  $array['type'];       
        $month = $array['month'];       
        $year = $array['year'];
       
      //  ECHO "SELECT d.*,format(d.[created_at], 'dd-MMM-yyyy') as cdate from datadispatch as d where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond.$areacond;EXIT;
     //   echo "SELECT d.*,format(d.[created_at], 'dd-MMM-yyyy') as cdate from datadispatch as d where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond.$areacond;exit;
        
$sql = DB::SELECT("SELECT id from datadispatch where type='".$type."' and month='".$month."' and dispyear='".$year."'");
        
        return $sql;

        
       }




     public static function saveRetails(Request $request){
          $input = $request->all();
         //  print_r($input);exit;
           $uid  = $request->get('user_id');
           $fid  = $request->get('finacial_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
             $fid = Session::get('finacial_id');
           }
         $date  = $request->get('date');
      
          $month  = $request->get('month');
          $mdata  = explode("-",$month);
          $month = $mdata[0];
            $date = date_parse($month);
         $month =  $date['month'];
         $year = '20'.$mdata[1];
         $final_arr = $request->get('final_arr');
         $wararray = array();
         $index  = json_decode($request->get('index'));
         $remark  = json_decode($request->get('remark'));
         $warning  = json_decode($request->get('warning'));
         $cdate  = date('Y-m-d');
         $time  = date('Y-m-d h:i:s');
                   //print_r( $wararray);exit;
         $homecontroller   =   new HomeController;
         $MasterSettings   =   new MasterSettings;
         $fy        =   $homecontroller->getFinacilaYear();
         $upcatid =  Session::get('retailcatid');
         $retcatid = Session::get('retailitemid');
         // print_r($sapcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('Retails',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $upcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Retails','uid'=> $uid));
           
           }

           if(empty($retcatid)){
            $string = strtoupper(substr('Retails',0,8));
         //   print_r(expression)
             $retcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'Retails','uid'=> $uid));
          
            }
         
         
          //print_r(  $dataarray);exit;
         //$values = $dataarray[0];
         $file = request()->file('file');
         $filename = $file->getClientOriginalName();
         $path     =   $fy.'/'.$month.'/'.$filename;
         $sum = 0;
         $details_array =array();
      //print_r($remark);exit;
          foreach ($remark as $key => $value) {
             array_push($wararray, array('remark'=> $value,'warning'=>$warning[$key]));
          }
         
         
         //to store file in server
         $upcontroller   =   new UploadsController;
         $storage  = $upcontroller->storageSave(request()->file('file'),$month,$path);
         //get dispatch data for the month
        $discond  = array('type'=>$retcatid,'month'=>$month,'status'=>'','year'=>$year);
        //$disvalue = self::getDispatch($discond);
$disvalue = self::getDispatchCount($discond);
      // print_r($disvalue);exit;
       if(empty($disvalue)){
            //uploads save
            $uparray   = array('filename' => $filename,'category'=>$upcatid,'path'=>$path,'uid'=>$uid);
            $newuploads = $upcontroller->saveUplaods($uparray);

            //dispatch save
            $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$retcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
            $dispid   =  self::saveDispatch($disarray);

        }else{
           //foreach ($disvalue as $key => $value) {
              //DB::table('datadispatch')->where('id',$value['id'])->update(['status'=>3]);
		DB::table('Datadispatch_DT')->where('ddid',$disvalue[0]->id)->delete();
                DB::table('datadispatch')->where('id',$disvalue[0]->id)->delete();
                DB::table('Townwise_data')->where('MONTY_YEAR',$year.$month)->where('TYPE',$retcatid)->delete();

          // }
          $uparray   = array('filename' => $filename,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
            $newuploads = $upcontroller->saveUplaods($uparray);

            //dispatch save
            $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$retcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
            $dispid   =  self::saveDispatch($disarray);
        }
        $final_arr = json_decode($final_arr);
//    print_r($final_arr);exit;
       foreach ($final_arr as $key => $value) {
     // print_r($value);exit;
          $dealerid  = $value->dealerid;
          $brandid  = $value->brand;
          $cnt  = $value->value;
          $townid =  DB::table('dealers')->where("id",$dealerid)->value('townid');
         
           $date = "Y-".$month."-d";
           $priormonth = date ('m', strtotime ( '-1 month' , strtotime ($date)));
        
          
        //  $disid = DB::table('datadispatch_dt')->insertGetId(['ddid' => $dispid,'value'=> $cnt,'productid'=>$brandid,'dealerid'=> $dealerid ,'uid'=>$uid,'townid'=>$townid]);
          //$cnt = ($value[$i]=='') ? 0 : $value[$i];
       //  array_push($details_array,array('ddid' => $dispid,'value'=> $cnt,'productid'=>$brandid,'dealerid'=> $dealerid ,'townid'=>$townid,'status'=>1));
         $id  =  datadispatchdet::insert(['ddid' => $dispid,'value'=> $cnt,'productid'=>$brandid,'dealerid'=> $dealerid ,'townid'=>$townid,'status'=>1]);
          }
         
          $monthName= date('M', mktime(0, 0, 0, $month, 10)); // March
          $category  = "File Uploaded";
          $homecontroller   =   new HomeController;
          $note   = 'Submitted Retail';
          $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
           $MasterSettings->generateTrigger($month,$year);
          return array('title'=>'Success!','messege'=>'Retails Uploaded Successfully','type'=>'success','load'=>'settings');
           }

 /*==========================================================
    Function: savePdplan
    Description: To upload  pdplan 
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
      public static function savePdplan(Request $request){
          $input = $request->all();
         //  print_r($input);exit;
          $uid  = $request->get('user_id');
          $fid  = $request->get('finacial_id');
           if(empty($uid)){
             $uid = Session::get('user_id');
             $fid = Session::get('finacial_id');
           }
         $date  = $request->get('date');
         $homecontroller   =   new HomeController;
         $MasterSettings   =   new MasterSettings;
         $fy        =   $homecontroller->getFinacilaYear();
          //get array from file
         $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
          //print_r(  $dataarray);exit;
         $values = $dataarray[0];
         $month  = $request->get('month');
         $cdate  = date('Y-m-d');
         $time   = date('Y-m-d h:i:s');
         $mdata  = explode("-",$month);
         $month = $mdata[0];
         $date = date_parse($month);
         $month =  $date['month'];
         $year = '20'.$mdata[1];
         $file   = request()->file('file');
         $filename = $file->getClientOriginalName();
         $path     =   $fy.'/'.$month.'/'.$filename;
         
         $upcatid = Session::get('pdcatid');
         $pdcatid =  Session::get('pditemid');
         // print_r($sapcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('PD Plan',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $upcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'PD Plan','uid'=> $uid));
           
           }

           if(empty($pdcatid)){
            $string = strtoupper(substr('PD Plan',0,8));
         //   print_r(expression)
             $pdcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $pdcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'PD Plan','uid'=> $uid));
          
            }
         
         //to store file in server
          $upcontroller   =   new UploadsController;
          $storage  = $upcontroller->storageSave(request()->file('file'),$month,$path);
           //get dispatch data for the month
          $discond  = array('type'=>$pdcatid,'month'=>$month,'status'=>1,'year'=>$year);
          $disvalue = self::getDispatch($discond);
        // print_r($disvalue);exit;
         if(empty($disvalue)){
              //uploads save
              $uparray   = array('filename' => $filename,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
              $newuploads = $upcontroller->saveUplaods($uparray);

              //dispatch save
              $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$pdcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>1);
              $dispid   =  self::saveDispatch($disarray);

          }else{

             $up_id = $disvalue[0]['uploadid'];
             $dispid = $disvalue[0]['id'];
              DB::table('uploads')
              ->where('id',$up_id)
              ->update(['updated_at'=>$time,'modifiedby'=>$uid,'link'=>$path,'filename' => $filename]);

              DB::table('datadispatch')->where('id',$dispid)->update(['updated_at'=>$time]);
          }
         
        
          
            $year = substr( $fy, -2);
            $monthName= date('M', mktime(0, 0, 0, $month, 10)); // March
            $category  = "File Uploaded";
            $homecontroller   =   new HomeController;
            $note   = 'Submitted PD Plan';
            $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
            
            
             return array('title'=>'Success!','messege'=>'Pd Plan Uploaded Successfully','type'=>'success');
           } 

    /*==========================================================
    Function: validatesiamUploads
    Description: To get validation 
    Author:
    Created Date: 20-12-2019
    Modification: 
    ==========================================================*/
      public static function validatesiamUploads(Request $request){
         $input = $request->all();
         $t_threshold  = $request->get('t_threshold');
         $m_threshold  = $request->get('m_threshold');
         $uid  = $request->get('user_id');
         if(empty($uid)){
            $t_threshold = Session::get('v_threshold');
            $m_threshold = Session::get('m_threshold');
          }
          $type = $request->get('type');
          $month  = $request->get('month');
          $mdata  = explode("-",$month);
          $month = $mdata[0];
          $date = date_parse($month);
          $month =  $date['month'];
          $year = $mdata[1];
          $warningarray = array();
          $headings = (new HeadingRowImport)->toArray(request()->file('file'));
          $MasterSettings   =   new MasterSettings;
          $homecontroller   =   new HomeController;
          //header validation
          $validate        =   $homecontroller->checkHeader($type,$headings[0][0]);
          $error_array = array();
          $output_arr = array();
          $dispdetails_array = array();
         if($validate['success'] == '1'){
           $siamcatid = Session::get('siamitemid');
           $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
           $data = $dataarray[0];
           $final_arr = array();
           foreach ($data as $key => $value) {
            $Manufacturer  = $value['Manufacturer'];
            $model  = $value['Models'];
            $Quantity  = $value['Quantity'];
            $final_arr[trim($Manufacturer)][$key] = array("Quantity"=>$Quantity,"Models"=>$model);
            }
           $manu_arr = array_unique(array_column(array_filter($data),'Manufacturer'));
           $imp_manu = ("('".implode("'),('", $manu_arr));
           $manudt =DB::SELECT("select * from (values $imp_manu')) as t(manufacturer) except select manufacturer from manufacture");
         
           if(!empty($manudt)){
            foreach ($manudt as $key => $value) {
              array_push($error_array,array("error"=>'Manufacture "'.$value->manufacturer.'" is not found in template'));
            }
          }
         
          foreach ($final_arr as $mkey => $mvalue) {
            foreach ($mvalue as $key => $value) {
            $Manufacturer  = $mkey;
            $model  = $value['Models'];
            $Quantity  = $value['Quantity'];
            $productid = DB::SELECT("SELECT b.id FROM  brand b  join manufacture m on m.id=b.mftid where brand = '".trim($model)."' AND manufacturer ='".trim($Manufacturer)."' ");
             if(empty($productid)){
               $cell = $key+2;
               $column = 'B';
                array_push($error_array,array("error"=>'Manufacturer "'.$Manufacturer.'" Brand "'.$model.'" not found in template')); 
              }else{
               $productid = $productid[0]->id;
               array_push($output_arr,array("productid"=> $productid,"quantity"=>$Quantity));
               if(!is_int($Quantity) && !empty($Quantity) ){
                  $cell = $key+2;
                  $column = 'C';
                  array_push($error_array,array("error"=>'Cell '.$column.$cell.' should be a whole number'));
                   
                   }   //check value is empty
                 else if(!isset($Quantity) ){
                    $cell = $key+2;
                  $column = 'C';
                    array_push($error_array,array("error"=>'Cell '.$column.$cell.' cannot be left blank'));
                  
                 }
                
               if(!empty($manufactvalidate)){
                if(!empty($productid)){
                  $discond  = array('type'=> $siamcatid,'product'=>$productid,'month'=>$month,'status'=>1,'year'=>$year);
                  $disvalue = self::checkThreshold($discond);
                if(!empty($disvalue)){
                  foreach ($disvalue as $key => $value) {
                   if(!empty($disvalue[0]['sum'])){
                    $olddispatch = $disvalue[0]['sum'];
                    $percentage = ($olddispatch *$t_threshold)/100;
                   // print_r($dispatch);exit;
                    if($percentage < $Quantity){
                      $cell = $key+2;
                      $column =  self::getAlphabest($cell);
                      $war = 'Cell'.$column.$cell.': Value is more than 200% of previous month value';
                      array_push($warningarray,array("index"=>$key,"warning"=>$war));
                    }else if($percentage > $Quantity){
                      $cell = $key+2;
                      $column =  self::getAlphabest($cell);
                      $war = 'Cell'.$column.$cell.': Value is less than 200% of previous month value';
                      array_push($warningarray,array("index"=>$key,"warning"=>$war));
                      }
                     }
                   }
                }
              }
            }
          }
         }
        }

      }else{
             array_push($error_array,array("error"=>$validate['error']));
          }
           return array("success"=>true,"warning"=>$warningarray,'error'=>$error_array,"final_arr"=>$output_arr);
               
          }
     /*==========================================================
    Function: saveSiam
    Description: To upload  siam
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
        public static function saveSiam(Request $request){
          $input = $request->all();
          $uid  = $request->get('user_id');
          $fid  = $request->get('finacial_id');
          if(empty($uid)){
            $uid = Session::get('user_id');
            $fid = Session::get('finacial_id');
          }
          $date  = $request->get('date');
          $homecontroller   =   new HomeController;
          $MasterSettings   =   new MasterSettings;
          $fy        =   $homecontroller->getFinacilaYear();
          $headings = (new HeadingRowImport)->toArray(request()->file('file'));
               //get array from file
          $dataarray = Excel::toArray(new UsersImport,  request()->file('file'));
            //print_r(  $dataarray);exit;
           $wararray = array();
           $index  = json_decode($request->get('index'));
           $remark  = json_decode($request->get('remark'));
           $warning  = json_decode($request->get('warning'));
           $details_array =array();
           $output_arr  = $request->get('final_arr');
           $values = $dataarray[0];
           $month  = $request->get('month');
           $cdate  = date('Y-m-d');
           $time  = date('Y-m-d h:i:s');
           $file = request()->file('file');
           $mdata  = explode("-",$month);
           $month = $mdata[0];
           $date = date_parse($month);
           $month =  $date['month'];
           $year = '20'.$mdata[1];
           $filename = $file->getClientOriginalName();
           $path     =   $fy.'/'.$month.'/'.$filename;
          //print_r($remark);exit;
     
         $upcatid = Session::get('siamcatid');
         $siamcatid = Session::get('siamitemid');
       // /  print_r($siamcatid);exit;
           if(empty($upcatid)){
            $string = strtoupper(substr('SIAM',0,8));
         //   print_r(expression)
            $upcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $upcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'SIAM','uid'=> $uid));
           
           }

           if(empty($siamcatid)){
            $string = strtoupper(substr('SIAM',0,8));
         //   print_r(expression)
             $siamcatid = $MasterSettings->saveItems(array('DDI_CATG_ID' => $uploadcatid,'DDI_DISP_SEQ'=> 500,'DDI_CODE_TXT' => $string,'DDI_DISP_TEXT'=> 'SIAM','uid'=> $uid));
          
            }
            foreach ($remark as $key => $value) {
               array_push($wararray, array('remark'=> $value,'warning'=>$warning[$key]));
            }
          //to store file in server
            $upcontroller   =   new UploadsController;
            $storage  = $upcontroller->storageSave(request()->file('file'),$month,$path);
             //get dispatch data for the month
            $discond  = array('type'=>$siamcatid,'month'=>$month,'status'=>'','year'=>$year);
           // $disvalue = self::getDispatch($discond);

		$disvalue = self::getDispatchCount($discond);
		
            //print_r($disvalue);exit;
           if(empty($disvalue)){
                //uploads save
                $uparray   = array('filename' => $filename,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
                $newuploads = $upcontroller->saveUplaods($uparray);
               //dispatch save
                $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$siamcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
                $dispid   =  self::saveDispatch($disarray);

            }else{
              //foreach ($disvalue as $key => $value) {
                 //DB::table('datadispatch')->where('id',$value['id'])->update(['status'=>3]);
		DB::table('Datadispatch_DT')->where('ddid',$disvalue[0]->id)->delete();
                DB::table('datadispatch')->where('id',$disvalue[0]->id)->delete();
                DB::table('Townwise_data')->where('MONTY_YEAR',$year.$month)->where('TYPE',$siamcatid)->delete();
             // }
                $uparray   = array('filename' => $filename,'category'=> $upcatid,'path'=>$path,'uid'=>$uid);
                $newuploads = $upcontroller->saveUplaods($uparray);
              //dispatch save
                $disarray = array('month' => $month,'uploadid'=>$newuploads,'type'=>$siamcatid,'uid'=>$uid,'year'=>$year,'status'=>1,'generate'=>0,'warning'=>json_encode($wararray));
                $dispid   =  self::saveDispatch($disarray);
      
            }

              $sum = 0;
              $output_arr = json_decode($output_arr);
              $prevprod = $prevdealerid = '';
              $dispdetails_array=array();
              $twn =DB::select("select * from town where town='All India Siam'");
              if(!empty($twn)){
              $townid = $twn[0]->id;
               }else{
                $zoneid = DB::table('zone')->insertGetId(['zone' => 'All India','update_at'=>$cdate,'status'=>1]);
                $stateid = DB::table('state')->insertGetId(['zoneid' => $zoneid,'state'=> 'All India','update_at'=>$cdate,'status'=>1]);
                $areaid = DB::table('area')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'area'=>'All India','update_at'=>$cdate,'status'=>1]);
                $districtid = DB::table('district')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'district'=> 'All India','update_at'=>$cdate,'status'=>1]);
                $townid = DB::table('town')->insertGetId(['zoneid' => $zoneid,'stateid'=> $stateid,'areaid'=>$areaid,'districtid'=> $districtid,'town'=>'All India Siam','update_at'=>$cdate,'status'=>1,"townclass"=>'All India Siam']);
              }

             
             //print_r($townid);exit;
            // foreach ($output_arr as $key => $value) {

            //    $brandid = DB::table('product')->where('id','=',$value->productid)->value('brandid');

            //   if(($brandid  == $prevprod)){

            //   $brandwiseqty[$brandid] = $brandwiseqty[$brandid] + $value->quantity;
               
            //   }
            //   else{  
            //         $brandwiseqty[$brandid] = $value->quantity;
            //      }
            
           
            //       if(!empty($disvalue)){
            //                 $dispid = $disvalue[0]['id'];
            //                              DB::table('datadispatch_dt')
            //                              ->where('ddid',$dispid)
            //                              ->update(['status'=>2]);
            //                       }
                                      
            //     array_push($details_array,array('ddid' => $dispid,'value'=>$value->quantity,'productid'=>$value->productid,'dealerid'=>'','uid'=>$uid,'table'=>'dispatch_model','townid'=>$townid));
            //       $prevprod = $brandid;
             
                         

            //         }
                    //print_r($dispid);exit;
                  //  //=============================================================================
            foreach ($output_arr as $key => $value) {
               $qty = $value->quantity;
               $prid = $value->productid;
               array_push($dispdetails_array,array('ddid' => $dispid,'value'=>$qty,'productid'=>$prid,'dealerid'=>'','uid'=>$uid,'townid'=>$townid));
              
            }
              //$dtid = self::saveDispatchdetails($details_array,$wararray);
               $detid = self::saveDispatchdetails($dispdetails_array,$wararray);
             
              $monthName= date('M', mktime(0, 0, 0, $month, 10)); // March
             // print_r($monthName);exit;
              $category  = "File Uploaded";
              $homecontroller   =   new HomeController;
              $note   = 'Submitted All India SIAM';
              $activity        =   $homecontroller->saveActivity($category,$fid,$note,$uid);
              $MasterSettings->generateTrigger($month,$year);   
            return array('title'=>'Success!','messege'=>'SIAM Uploaded Successfully','type'=>'success','load'=>'settings');
}
/*==========================================================
    Function: getDispatch
    Description: To upload  dispatch
    Author:
    Created Date: 07-01-2020
    Modification: 
    ==========================================================*/
       public static function getDispatch($array){
       // print_r($array);exit;
        $type =  $array['type'];
        $dealer = (array_key_exists('dealer', $array)) ? $array['dealer'] : '';
        $product = (array_key_exists('product', $array)) ? $array['product'] : '';
        $month = (array_key_exists('month', $array)) ? $array['month'] : '';
        $status = (array_key_exists('status', $array)) ? $array['status'] : ' ';
        $year = (array_key_exists('year', $array)) ? $array['year'] : '';
        $area = (array_key_exists('area', $array)) ? $array['area'] : '';
        $dealercond = ($dealer=='')?' ':" and dealerid='".$dealer."'";
        $productcond = ($product=='')?' ':" and productid='".$product."'";
        $monthcond = ($month=='')?' ':" and month='".$month."'";
        $statuscond = ($status=='')?' and status !=3 ':" and status ='".$status."'";
        $yearcond = ($year=='')?' ':" and dispyear='".$year."'";
        $areacond = ($area=='')?' ':" and area='".$area."'";
      //  ECHO "SELECT d.*,format(d.[created_at], 'dd-MMM-yyyy') as cdate from datadispatch as d where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond.$areacond;EXIT;
     //   echo "SELECT d.*,format(d.[created_at], 'dd-MMM-yyyy') as cdate from datadispatch as d where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond.$areacond;exit;
        $sql = DB::SELECT("SELECT d.*,format(d.[created_at], 'dd-MMM-yyyy') as cdate from datadispatch as d where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond.$areacond);
        $row = json_decode( json_encode($sql), true);
        return $row;

        
       }
/*==========================================================
    Function: getDispatchModel
    Description: To upload  dispatch
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getDispatchModel($array){
       // print_r($array);exit;
        $type =  $array['type'];
        $dealer = (array_key_exists('dealer', $array)) ? $array['dealer'] : '';
        $product = (array_key_exists('product', $array)) ? $array['product'] : '';
        $month = (array_key_exists('month', $array)) ? $array['month'] : '';
        $status = (array_key_exists('status', $array)) ? $array['status'] : '';
        $year = (array_key_exists('year', $array)) ? $array['year'] : '';
        $dealercond = ($dealer=='')?' ':" and dealerid='".$dealer."'";
        $productcond = ($product=='')?' ':" and productid='".$product."'";
        $monthcond = ($month=='')?' ':" and month='".$month."'";
        $yearcond = ($year=='')?' ':" and dispyear='".$year."'";
        $statuscond = ($status=='')?' ':" and d.status ='".$status."' and dt.status=1";
        $sql = DB::SELECT("SELECT d.*,dt.*,dt.id as pid,d.id as dispatchid, format(d.[created_at], 'dd-MMM -yyyy') as cdate  from datadispatch d left join datadispatch_dt dt on d.id=dt.ddid where type='".$type."'".$dealercond.$productcond.$monthcond.$statuscond.$yearcond);
        $row = json_decode( json_encode($sql), true);
        return $row;

        
       }
    /*==========================================================
    Function: getModel
    Description: To upload  dispatch
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getModel($where){
        $wherecond = ($where=='') ? '' : "where $where and d.status=1 and dt.status=1";
        $sql = DB::SELECT("SELECT d.*,dt.*,dt.id as productid,d.id manufactureid from product d left join product_dt dt on d.id=dt.productid  $wherecond");
        $row = json_decode( json_encode($sql), true);
       return $row;

       }
       /*==========================================================
    Function: getModel
    Description: To upload  dispatch
    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getBrand($where){
        $wherecond = ($where=='') ? '' : "where $where and d.status=1";
        $sql = DB::SELECT("SELECT *  from brand d left join manufacture dt on d.mftid=dt.id  $wherecond");
        $row = json_decode( json_encode($sql), true);
       return $row;

       }
    /*==========================================================
    Function: getDealer
    Description: To dealer

    Author:
    Created Date: 01-01-2020
    Modification: 
    ==========================================================*/
       public static function getDealer($array){
       // print_r($array);exit;
        $dealercode = (array_key_exists('dealercode', $array)) ? $array['dealercode'] : '';
        $dealername = (array_key_exists('dealername', $array)) ? $array['dealername'] : '';
        $townid = (array_key_exists('townid', $array)) ? $array['townid'] : '';
        $zonename = (array_key_exists('zonename', $array)) ? $array['zonename'] : '';
        $statename = (array_key_exists('statename', $array)) ? $array['statename'] : '';
        $dealercond = ($dealercode=='')?' ':" and dealercode='".$dealercode."'";
        $dealernamecond = ($dealername=='')?' ':" and dealername='".$dealername."'";
        $towncond = ($townid=='')?' ':" and townid='".$townid."'";
        $zonecond = ($zonename=='')?' ':" and z.zone ='".$zonename."'";
        $statecond = ($statename=='')?' ':" and s.state ='".$statename."'";
        $sql = DB::SELECT("SELECT d.id,d.dealercode,d.dealertown,t.id as townid from dealers d join town t on d.townid=t.id left join  zone z on z.id=t.zoneid join area a on a.id=t.areaid join district ds on ds.id=t.districtid join state s on s.id= t.stateid where d.status=1".$dealercond.$dealernamecond.$towncond.$towncond.$zonecond.$statecond);
        $row = json_decode( json_encode($sql), true);
       return $row;

       }
    /*==========================================================
    Function: DownloadTemplate
    Description: To download template in consolidated uplopad
    Author:
    Created Date: 21-01-2020
    Modification: 
    ==========================================================*/
        public static function DownloadRetailTemplate(Request $request){
               $input = $request->all();
               $conmonth  = $request->get('sub_frm');
               $month = str_replace('-','',$conmonth);
               $name = 'Retail_Format.xlsx';
           return Excel::download(new RetailsExport($month), 'Retail_Format.xlsx');
        }
    /*==========================================================
    Function: DownloadTemplate
    Description: To download template in SAP uplopad
    Author:
    Created Date: 19-02-2020
    Modification: 
    ==========================================================*/
        public static function DownloadAllindiaTemplate(Request $request){
               $input = $request->all();
               $conmonth  = $request->get('date');
               $month = str_replace('-','',$conmonth);
               
               $name = 'All India SIAM_'.$month.'.xlsx';
               //print_r( $name);exit;
           return Excel::download(new AllindiaExport($month), $name);
        }

    /*==========================================================
    Function: getLastupdate
    Description: To get last update data
    Author:
    Created Date: 09-03-2020
    Modification: 
    ==========================================================*/
       public static function getLastupdate(Request $request){
         $type = $request->get('type');
         if($type == 'generate'){
           $AdminUploads   =   new HomeController;
           $year =  $AdminUploads->getFinacilaYear();
           $tyear = $year+1;
           $from = $year.'-04-01';
           $to =    $tyear.'-03-01';
           $sql = DB::SELECT("SELECT  max(created_at) as maxdate from datadispatch  where generate=1 and CAST([created_at] AS DATE) between '".$from."' and '".$to."'");
          }else if($type=='master'){
            $MasterSettings   =   new MasterSettings;
            $uploadcatid =  $MasterSettings->getCategoryid('UPLOAD_TYPE');
            $upcatid =  $MasterSettings->getItemid($uploadcatid,'location');
            $pcatid =  $MasterSettings->getItemid($uploadcatid,'product');
           // ECHO "SELECT  max(updated_at) as maxdate from uploads  where category=$upcatid or category=$pcatid";EXIT;
           $sql = DB::SELECT("SELECT  max(updated_at) as maxdate from uploads  where category=$upcatid or category=$pcatid");
          }
         $row = json_decode( json_encode($sql), true);
        return $row;

       }

}