<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
      $category  = "Logout";
      $note   = 'Logout';
      $activity        =   app('App\Http\Controllers\HomeController')->saveActivity($category,'',$note,Session::get('user_id'));
        return redirect('/login');
    }
public function doLogin()
{


// validate the info, create rules for the inputs
  $rules = array(
      'email'    => 'required|email', // make sure the email is an actual email
      
  );

  // run the validation rules on the inputs from the form
  $validator = Validator::make(Input::all(), $rules);
  // if the validator fails, redirect back to the form
  if ($validator->fails()) {
      return Redirect::to('login')
          ->withErrors($validator) // send back all errors to the login form
          ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
  } else {

      // create our user data for the authentication
      $userdata = array(
          'email'     => Input::get('email'),
          'status'   => 1
          
      );
// //print_r(Auth::attempt($userdata));exit;
// $serverName = "oogdaserver.database.windows.net"; //serverName\instanceName
// $connectionInfo = array( "Database"=>"townwise", "UID"=>"oog_admin", "PWD"=>"TVSM1234#");
// $conn = sqlsrv_connect( $serverName, $connectionInfo);

// if( $conn ) {
//   //   
//     $sql = "SELECT * FROM users";
//     $params = array();
//     $options =  array();
//     $stmt = sqlsrv_query( $conn, $sql , $params, $options );
//     echo "Connection established.<br />";
// }else{
//      echo "Connection could not be established.<br />";exit;
//      die( print_r( sqlsrv_errors(), true));
// }


// //$sql = DB::SELECT("SELECT * FROM users");
// print_r(sqlsrv_fetch_array( $stmt));exit;
    // attempt to do the login
    if (Auth::attempt($userdata)) {

        // validation successful!
        // redirect them to the secure section or whatever
        // return Redirect::to('secure');
        // for now we'll just echo success (even though echoing in a controller is bad)
        $category  = "Login";
      $note   = 'Login';
      $activity        =   app('App\Http\Controllers\HomeController')->saveActivity($category,'',$note,Auth::user()->id);
        if((Auth::user()->role)=='1'){
          return redirect('/sapDispatch');
        }else if((Auth::user()->role)=='2'){
          return redirect('/status');
        }if((Auth::user()->role)=='3'){
          return redirect('/approve');
        }if((Auth::user()->role)=='4'){
          return redirect('/consolidateData');
        }
        echo 'SUCCESS!';exit;

    } else {        

        // validation not successful, send back to form 
     return redirect('/login');

    }

}
}
}
