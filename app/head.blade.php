<!-- Style.css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- Font-awesome  Css-->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
<link href="{{url('public/css/style.css')}}" rel="stylesheet">
<!-- Datepicker.css-->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">

<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="{{JS}}sweetalert-dev.js"></script>
<script src="{{JS}}moment.min.js "></script>

<script type="text/javascript" src="{{url('public/js/daterangepicker.js')}}"></script>
<script src="{{url('public/js/bootstrap-datepicker.js')}}"></script>   
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@2.0.0/dist/vee-validate.js"></script>
<script src="https://unpkg.com/vee-validate@2.0.0"></script>
<script src="https://unpkg.com/@trevoreyre/autocomplete-vue" type="text/javascript"></script>
<script src="https://unpkg.com/vuetable-2@1.6.0"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <!-- Lastly add this package -->
<script src="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3"></script>
<link href="https://cdn.jsdelivr.net/npm/vue-loading-overlay@3/dist/vue-loading.css" rel="stylesheet">
     <script src="https://www.jquery-az.com/boots/js/bootstrap-multiselect/bootstrap-multiselect.js"
    type="text/javascript"></script>
<link href="https://www.jquery-az.com/boots/css/bootstrap-multiselect/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" />

<script type="text/javascript">
// ------------------------- DATE PICKER SCRIPT -------------------------------
$(function () {
    $('#sub_from').datepicker({ format: 'DD/MM/YYYY',});
    $('#sub_to').datepicker({ format: 'DD/MM/YYYY',});
      
});

    var APP_URL = {!! json_encode(secure_url('/').'/'.env('BACKEND_PATH')) !!}
//var APP_URL ='https://townwise.azurewebsites.net/testblog/'.;
    var url = APP_URL.substring(0, APP_URL.lastIndexOf("/") + 1)

console.log(url)     
//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();

    if (element.is('li')) {
        element.addClass('active');
    }
});
      
</script>