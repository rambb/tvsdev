<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;
    class datagenerate extends Model
    {
    	protected $table = 'datagenerate';
         public $fillable = ['Despatch','Retail','manufacturer','zone','state','district','area','town','month','year','brand','category','segment','subsegment','towngroup','townclass','update_at'];
    }
    ?>