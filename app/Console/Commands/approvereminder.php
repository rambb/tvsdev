<?php

namespace App\Console\Commands;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use DB;
class reminderuploadthreedays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approvereminder:approvereminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

      public function handle(){
       
        $sql = DB::select("select d.created_at,d.area from datadispatch d join area a on a.id=d.area where staus=0 and type='Consolidated'");
        $details= json_decode( json_encode($sql), true);
        foreach ($details as $key => $value) {
          $area  = $value['area'];
          $created_at  = $value['created_at'];
          $now = time(); // or your date as well
          $your_date = strtotime($created_at);
          $datediff = $now - $your_date;
          $daycount = round($datediff / (60 * 60 * 24));
          $sql = DB::select("select email from users where role=3 and status=1 and area=$area");
          $area_details= json_decode( json_encode($sql), true);
          $areaemail = $area_details[0]['email'];
          $month  = date('M-yy');
           $valuelist = $manufacture = '';
          $request = new Request([
          'area'   => $area,
          'month'  =>$month,
           ]);
          $publish = new App\Http\Controllers\ApproveController;
          $approve_dt = $publish->Approve( $request);
          if(!empty($approve_dt)){
                  $valuelist = $approve_dt['valuelist'];
                  $manufacture = $approve_dt['manufacture'];

                }
          $subject =  $month.' '.$area.' Townwise data: Reminder to Approve – within  $daycount days';
          $miseremail = '';
          $data = array('manufacture'=>$manufacture,'valuelist'=>$valuelist,'email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$month,'name'=>$name,'view'=>'pages.email.approvereminder','days'=>$daycount);
         Mail::send($data['view'], $data, function($message) use ($data) {
           $message->to('');
           $message->to($areaemail);
           $message->subject($data['subject']);

          });
        }
       
    }
}
