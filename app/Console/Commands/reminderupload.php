<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class reminderupload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminderupload:reminderupload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

      public function handle(){
       
        $sql = DB::select("select email,area from users where role=4 and status=1");
        $mis_details= json_decode( json_encode($sql), true);
        foreach ($mis_details as $key => $value) {
          $area  = $value['area'];
          $name  = $value['name'];
          $misemail  = $value['email'];
          $sql = DB::select("select email from users where role=3 and status=1 and area=$area");
          $area_details= json_decode( json_encode($sql), true);
          $areaemail = $area_details[0]['email'];
          $month  = date('M-yy');
         
          $miseremail = '';
          $msql = DB::select("select metavalue from masters where metakey='upload_enable'");
          $due_dt= json_decode( json_encode($msql), true);
          if(!empty($due_dt)){
            $duedata = $due_dt[0]['metavalue'];
            if(is_numeric( $duedata)){
            $now = time(); // or your date as well
            $d_date = strtotime(date('Y-m'.$duedata));
            $datediff = $now - $d_date;
            $diff = round($datediff / (60 * 60 * 24));
           }
          }
          $flag = 0;
         if($diff == 3){
           $flag = 1;
           $subject =  $month.' '.$area.'  Townwise data: Reminder to upload – within 3 days';
         }else if($diff == 2){
           $flag = 1;
           $subject =  $month.' '.$area.'  Townwise data: Reminder to upload – within 2 days';

         }else if($diff == 0){
           $flag = 1;
           $subject =  $month.' '.$area.'  Townwise data: Reminder to upload – Option disables tonight!';

         }
         if($flag == 1){
         $data = array('email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$month,'name'=>$name,'view'=>'pages.email.remindermisuser','days'=>$days,'duedate'=>date('Y-m'.$duedata));
         Mail::send($data['view'], $data, function($message) use ($data) {
           $message->to('');
           $message->to($areaemail);
           $message->subject($data['subject']);

          });
         }
        }
       
    }
}
