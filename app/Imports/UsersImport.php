<?php

namespace App\Imports;

use App\User; use Illuminate\Support\Facades\Hash; use
Maatwebsite\Excel\Concerns\ToModel; use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToArray; use
Maatwebsite\Excel\Concerns\WithHeadingRow; use
Maatwebsite\Excel\Concerns\WithMultipleSheets; use
Maatwebsite\Excel\Imports\HeadingRowFormatter; use
Maatwebsite\Excel\Concerns\WithEvents; use
Maatwebsite\Excel\Events\BeforeSheet; HeadingRowFormatter::default('none');
class UsersImport implements ToArray, WithHeadingRow, WithEvents {

    public $sheetNames;
    public $sheetData;

    public function __construct(){
        $this->sheetNames = [];
        $this->sheetData = [];
    }
    public function array(array $array)
    {
      //  print_r($array);exit;
        if(!empty($array)){
        $this->sheetData[$this->sheetNames[count($this->sheetNames)-1]] = array_filter($array);
     }
    }
    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function(BeforeSheet $event) {
                $this->sheetNames[] = $event->getSheet()->getTitle();
            }
        ];
    }
    public function chunkSize(): int
    {
        return 100;
    }


    public function getSheetNames() {
        return $this->sheetNames;
    }

}


?>