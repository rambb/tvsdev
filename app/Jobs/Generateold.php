<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\configuration;
use App\masters;
use App\activity;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\zone;
use App\state;
use App\area;
use App\district;
use App\town;
use File;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\RetailsImport;
use App\Exports\FinalOutputExport;
Use App\datagenerate;
use App\datadispatchdet;
use App\datadispatch;
class Generate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $time  = date('Y-m-d h:i:s');
        $sql = DB::select('select id from datadispatch where status=3');
        $data = json_decode( json_encode($sql), true);
        $dispid =array();
       // print_r( $data);exit;
         $dispatchid = array_column($data,'id');
             DB::table('datadispatch')->whereIn('id',$dispatchid)->delete();
             DB::table('datadispatch_dt')->whereIn('ddid',$dispatchid)->delete();
             // $datadispatchdet = datadispatchdet::where('ddid',$id);
             // $datadispatchdet->delete();
             // $datadispatch = datadispatch::where('ddid',$id);
             // $datadispatch->delete();
         
         $sql = DB::select('select id from datagenerate');
         $data = json_decode( json_encode($sql), true);
       //  print_r($data);exit;
        //echo "SELECT  ', ' + id FROM    ( SELECT DISTINCT id FROM    datadispatch where status=1 and generate=0) x FOR      XML PATH('')")
        $arrayid = array_column($data,'id');
        if(count($arrayid) > 2000){
          $array1 = array_slice($arrayid, 0, 2000);
          $array2 = array_slice($arrayid, 2000, count($arrayid));
          if(count($array2) > 2000){
            $array3 = array_slice($array2, 0, 2000);
           if(count($array2) > 4000){
            $array4 = array_slice($array2, 2000, count($array2));
            }
            DB::table('datadispatch')->whereIn('id',$array1)->update(['updated_at'=>$time,'generate'=>1]);
            DB::table('datadispatch')->whereIn('id',$array3)->update(['updated_at'=>$time,'generate'=>1]);
            if(count( $array4) > 0){
             DB::table('datadispatch')->whereIn('id',$array4)->update(['updated_at'=>$time,'generate'=>1]);
             }
          }else{
            DB::table('datadispatch')->whereIn('id',$array1)->update(['updated_at'=>$time,'generate'=>1]);
            DB::table('datadispatch')->whereIn('id',$array2)->update(['updated_at'=>$time,'generate'=>1]);
          }
         // print_r( $array2);exit;

        }else{
        DB::table('datadispatch')->whereIn('id',$arrayid)->update(['updated_at'=>$time,'generate'=>1]);
        }
    
 /*AM User – Latest Townwise file is available for download*/
    // $userdt = DB::select("select email from users where area='".$areaid."' and role='3'");
    //    if(!empty($userdt)){
    //     $miseremail = $userdt[0]->email ;
        
    //    }
    //     $subject =  'Latest Townwise data is ready';
    //     $emailcontroller   =   new EmailController;
    //     $view = 'pages.email.generateemail';
    //     $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
    //     $emailcontroller->sendEmail($args);
 /*end AM User – Latest Townwise file is available for download*/
 /*HO Admin – Latest Townwise file is available for download*/
  $userdt = DB::select("select email from users where  role='1'");
       if(!empty($userdt)){
        $miseremail = $userdt[0]->email ;
        
       }
        $subject =  'Latest Townwise data is ready';
       
        $view = 'pages.email.generateemail';
        $args = array('email'=>$miseremail,'subject'=>$subject,'area'=>'','month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
        app('App\Http\Controllers\EmailController')->sendEmail($args);
 /* end HO Admin – Latest Townwise file is available for download*/

  /*HO User – Latest Townwise file is available for download*/
  // $userdt = DB::select("select email,a.area from users u join area a on a.id=u.area where  role='4' and u.status=1 and a.status=1");
       if(!empty($userdt)){
        foreach ($variable as $key => $value) {
          # code...
        $email = $userdt->email ;
        $area = $userdt->area ;
        $subject =  'Latest Townwise data is ready';
        
        $view = 'pages.email.generateemail';
        $args = array('email'=>$email,'subject'=>$subject,'area'=>$area,'month'=>$yearmonth,'name'=>$user_name,'view'=>$view);
        app('App\Http\Controllers\EmailController')->sendEmail($args);
         }
        }
 /* end HO Admin – Latest Townwise file is available for download*/
  return array('title'=>'Success!','messege'=>'Generated','type'=>'success');

    }
}
